<?php

namespace app\modules\administrator\controllers;

use Yii;
use common\models\OtherInspection;
use common\models\OtherInspectionSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;

/**
 * OtherInspectionController implements the CRUD actions for OtherInspection model.
 */
class OtherInspectionController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all OtherInspection models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new OtherInspectionSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single OtherInspection model.
     * @param integer $inspection_id
     * @param integer $patient_id
     * @return mixed
     */
    public function actionView($inspection_id, $patient_id)
    {
        return $this->render('view', [
            'model' => $this->findModel($inspection_id, $patient_id),
        ]);
    }

    /**
     * Creates a new OtherInspection model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($inspection_id, $patient_id)
    {
        $model = new OtherInspection();
        $model->inspection_id = $inspection_id;
        $model->patient_id = $patient_id;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'inspection_id' => $model->inspection_id, 'patient_id' => $model->patient_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Creates a new OtherInspection model.
     * If creation is successful, the browser will be redirected to the 'index' page.
     * Else, if creation wasn't successfull -> if there was errors - set json heade response and return formated errors
     * Else this is first handling -> render form through ajax
     * @return mixed
     */
    public function actionCreateModal($inspection_id, $patient_id) {
        $model = new OtherInspection();
        $model->inspection_id = $inspection_id;
        $model->patient_id = $patient_id;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', 'Запись добавлена!');
            return $this->redirect(['inspection/view', 'id' => $inspection_id, 'patient_id' => $patient_id, 'currentTab' => 'other-inspection']);
        } else {
            if ($model->hasErrors()) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return $this->getModelErrorsToString($model->errors);
            } else {
                return $this->renderAjax('_modal_form', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * Updates an existing OtherInspection model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $inspection_id
     * @param integer $patient_id
     * @return mixed
     */
    public function actionUpdate($inspection_id, $patient_id)
    {
        $model = $this->findModel($inspection_id, $patient_id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'inspection_id' => $model->inspection_id, 'patient_id' => $model->patient_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing OtherInspection model.
     * If update is successful, the browser will be redirected to the 'index' page.
     * Else, if creation wasn't successfull -> if there was errors - set json heade response and return formated errors
     * Else this is first handling -> render form through ajax
     * @param integer $inspection_id
     * @param integer $patient_id
     * @return mixed
     */
    public function actionUpdateModal($inspection_id, $patient_id) {
        $model = OtherInspection::findOne($inspection_id, $patient_id);
        $model->gallbladderStatusesIds = $model->getGallbladderStatusesIds();
        $model->kidneyStatusesIds = $model->getKidneyStatusesIds();
        $model->liverStatusesIds = $model->getLiverStatusesIds();
        $model->spleenStatusesIds = $model->getSpleenStatusesIds();
        $model->stomachStatusesIds = $model->getStomachStatusesIds();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['inspection/view', 'id' => $inspection_id, 'patient_id' => $patient_id, 'currentTab' => 'other-inspection']);
        } else {
            if ($model->hasErrors()) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return $this->getModelErrorsToString($model->errors);
            } else {
                return $this->renderAjax('_modal_form', [
                    'model' => $model,
                ]);                
            }
        }
    }

    /**
     * Deletes an existing OtherInspection model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $inspection_id
     * @param integer $patient_id
     * @return mixed
     */
    public function actionDelete($inspection_id, $patient_id)
    {
        $this->findModel($inspection_id, $patient_id)->delete();

        return $this->redirect(['inspection/view', 'id' => $inspection_id, 'patient_id' => $patient_id, 'currentTab' => 'other-inspection']);
    }

    /**
     * Finds the OtherInspection model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $inspection_id
     * @param integer $patient_id
     * @return OtherInspection the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($inspection_id, $patient_id)
    {
        if (($model = OtherInspection::findOne(['inspection_id' => $inspection_id, 'patient_id' => $patient_id])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Parse model errors to string
     * @param integer $inspection_id
     * @param integer $patient_id
     * @return OtherInspection model errors string
     */
    protected function getModelErrorsToString($errors) {
        $errorString = "";
        foreach ($errors as $error) {
            for ($i = 0, $maxi = count($error); $i < $maxi; $i++) {
                $errorString .= $error[$i]."\r\n";
            }
        }
        return $errorString;
    }
}
