<?php

namespace app\modules\administrator\controllers;

use Yii;
use common\models\NeurologicalInspection;
use common\models\NeurologicalInspectionSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;

/**
 * NeurologicalInspectionController implements the CRUD actions for NeurologicalInspection model.
 */
class NeurologicalInspectionController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all NeurologicalInspection models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new NeurologicalInspectionSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single NeurologicalInspection model.
     * @param integer $inspection_id
     * @param integer $patient_id
     * @return mixed
     */
    public function actionView($inspection_id, $patient_id)
    {
        return $this->render('view', [
            'model' => $this->findModel($inspection_id, $patient_id),
        ]);
    }

    /**
     * Creates a new NeurologicalInspection model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($inspection_id, $patient_id)
    {
        $model = new NeurologicalInspection();
        $model->inspection_id = $inspection_id;
        $model->patient_id = $patient_id;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'inspection_id' => $model->inspection_id, 'patient_id' => $model->patient_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Creates a new NeurologicalInspection model.
     * If creation is successful, the browser will be redirected to the 'index' page.
     * Else, if creation wasn't successfull -> if there was errors - set json heade response and return formated errors
     * Else this is first handling -> render form through ajax
     * @return mixed
     */
    public function actionCreateModal($inspection_id, $patient_id) {
        $model = new NeurologicalInspection();
        $model->inspection_id = $inspection_id;
        $model->patient_id = $patient_id;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', 'Запись добавлена!');
            return $this->redirect(['inspection/view', 'id' => $inspection_id, 'patient_id' => $patient_id, 'currentTab' => 'neurological-inspection']);
        } else {
            if ($model->hasErrors()) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return $this->getModelErrorsToString($model->errors);
            } else {
                return $this->renderAjax('_modal_form', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * Updates an existing NeurologicalInspection model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $inspection_id
     * @param integer $patient_id
     * @return mixed
     */
    public function actionUpdate($inspection_id, $patient_id)
    {
        $model = $this->findModel($inspection_id, $patient_id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'inspection_id' => $model->inspection_id, 'patient_id' => $model->patient_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing NeurologicalInspection model.
     * If update is successful, the browser will be redirected to the 'index' page.
     * Else, if creation wasn't successfull -> if there was errors - set json heade response and return formated errors
     * Else this is first handling -> render form through ajax
     * @param integer $inspection_id
     * @param integer $patient_id
     * @return mixed
     */
    public function actionUpdateModal($inspection_id, $patient_id) {
        $model = NeurologicalInspection::findOne($inspection_id, $patient_id);
        $model->fingerNoseStatusesIds = $model->getFingerNoseStatusesIds();
        $model->extremitiesMovementStatusesIds = $model->getExtremitiesMovementStatusesIds();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['inspection/view', 'id' => $inspection_id, 'patient_id' => $patient_id, 'currentTab' => 'neurological-inspection']);
        } else {
            if ($model->hasErrors()) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return $this->getModelErrorsToString($model->errors);
            } else {
                return $this->renderAjax('_modal_form', [
                    'model' => $model,
                ]);                
            }
        }
    }

    /**
     * Deletes an existing NeurologicalInspection model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $inspection_id
     * @param integer $patient_id
     * @return mixed
     */
    public function actionDelete($inspection_id, $patient_id)
    {
        $this->findModel($inspection_id, $patient_id)->delete();

        return $this->redirect(['inspection/view', 'id' => $inspection_id, 'patient_id' => $patient_id, 'currentTab' => 'neurological-inspection']);
    }

    /**
     * Finds the NeurologicalInspection model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $inspection_id
     * @param integer $patient_id
     * @return NeurologicalInspection the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($inspection_id, $patient_id)
    {
        if (($model = NeurologicalInspection::findOne(['inspection_id' => $inspection_id, 'patient_id' => $patient_id])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Parse model errors to string
     * @param integer $inspection_id
     * @param integer $patient_id
     * @return NeurologicalInspection model errors string
     */
    protected function getModelErrorsToString($errors) {
        $errorString = "";
        foreach ($errors as $error) {
            for ($i = 0, $maxi = count($error); $i < $maxi; $i++) {
                $errorString .= $error[$i]."\r\n";
            }
        }
        return $errorString;
    }
}
