<?php

namespace app\modules\administrator\controllers;

use Yii;

use common\models\Inspection;
use common\models\InspectionSearch;
use common\models\GeneralInspection;
use common\models\NeurologicalInspection;
use common\models\LungsInspection;
use common\models\Patient;
use common\models\InspectionDocument;
use common\models\CardiovascularInspection;
use common\models\OtherInspection;

use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

use PhpOffice\PhpWord\PhpWord;
use PhpOffice\PhpWord\TemplateProcessor;
use PhpOffice\PhpWord\IOFactory;
use PhpOffice\PhpWord\Style\Alignment;

use Arslanim\TemplateMapper\Document;

/**
 * InspectionController implements the CRUD actions for Inspection model.
 */
class InspectionController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Inspection models.
     * @return mixed
     */
    public function actionIndex($patient_id)
    {
        $patient = $this->findPatient($patient_id);
        $searchModel = new InspectionSearch();
        $searchModel->patient_id = $patient_id;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'patient' => $patient,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Inspection model.
     * @param integer $id
     * @param integer $patient_id
     * @return mixed
     */
    public function actionView($id, $patient_id, $currentTab)
    {
        $patient = $this->findPatient($patient_id);

        return $this->render('view', [
            'model' => $this->findModel($id, $patient_id),
            'patient' => $patient,
            'currentTab' => $currentTab,
        ]);
    }

    /**
     * Creates a new Inspection model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($patient_id, $currentTab)
    {
        $model = new Inspection();
        $patient = $this->findPatient($patient_id);
        $model->patient_id = $patient->id;
        if ($model->save()) {
            return $this->redirect(['view', 'id' => $model->id, 'patient_id' => $patient->id, 'currentTab' => $currentTab]);
        } else {
            Yii::$app->session->setFlash('error', 'Не удалось создать новый осмотр. Попробуйте еще раз или обратитесь к администратору.');
            return $this->redirect(['index', 'patient_id' => $patient->id]);
        }

        /*if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id, 'patient_id' => $model->patient_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }*/
    }

    /**
     * Updates an existing Inspection model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @param integer $patient_id
     * @return mixed
     */
    public function actionUpdate($id, $patient_id)
    {
        $model = $this->findModel($id, $patient_id);
        $model->surveysIds = $model->getSurveyIds();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id, 'patient_id' => $model->patient_id]);
        } else {
            if ($model->hasErrors()) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return $this->getModelErrorsToString($model->errors);
            } else {
                return $this->renderAjax('_inspection_form', [
                    'model' => $model,
                ]);
            }
        }
    }

    public function actionUpdateInspectionField($id, $patient_id, $fieldName) {
        $model = $this->findModel($id, $patient_id);
        $model->surveys = $model->getSurveyIds();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id, 'patient_id' => $model->patient_id, 'currentTab' => 'complaints']);
        } else {
            if ($model->hasErrors()) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return $this->getModelErrorsToString($model->errors);
            } else {
                return $this->renderAjax('_field_form', [
                    'model' => $model,
                    'fieldName' => $fieldName,
                ]);
            }
        }
    }

    public function actionUpdateInspectionResult($id, $patient_id) {
        $model = $this->findModel($id, $patient_id);
        $model->surveys = $model->getSurveyIds();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id, 'patient_id' => $model->patient_id, 'currentTab' => 'inspection-results']);
        } else {
            if ($model->hasErrors()) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return $this->getModelErrorsToString($model->errors);
            } else {
                return $this->renderAjax('_inspection_result_form', [
                    'model' => $model
                ]);
            }
        }
    }

    /**
     * Deletes an existing Inspection model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @param integer $patient_id
     * @return mixed
     */
    public function actionDelete($id, $patient_id)
    {
        $this->findModel($id, $patient_id)->delete();

        return $this->redirect(['index', 'patient_id' => $patient_id]);
    }

    public function actionPrint($id, $patient_id) {
        $patient = Patient::findOne($patient_id);
        $inspection = Inspection::find()
            ->where('id = :id AND patient_id = :patient_id', [':id' => $id, ':patient_id' => $patient_id])
            ->one();

        $templateProcessor = new TemplateProcessor(Yii::getAlias('@uploads').'\patterns\inspection.docx');
        $inspectionDocument = new Document\Document($templateProcessor, [
            ($patient) ? ($patient) : (new Patient),
        ]);

        $inspectionDocument->fillTemplate();

        $templateProcessor->saveAs(Yii::getAlias('@uploads').'\patterns\inspection-current.docx');

        //print_r($inspectionDocument);
        /*$patient = Patient::findOne($patient_id);
        $inspection = Inspection::find()
            ->where('id = :id AND patient_id = :patient_id', [':id' => $id, ':patient_id' => $patient_id])
            ->one();*/

        //load template    
        /*$templateProcessor = new TemplateProcessor(Yii::getAlias('@uploads').'\patterns\inspection.docx');
        
        $inspectionDocument = new InspectionDocument($templateProcessor, [
            ($patient) ? ($patient) : (new Patient),
            ($inspection) ? ($inspection) : (new Inspection),
            ($inspection->generalInspection) ? ($inspection->generalInspection) : (new GeneralInspection),
            ($inspection->neurologicalInspection) ? ($inspection->neurologicalInspection) : (new  NeurologicalInspection),
            ($inspection->lungsInspection) ? ($inspection->lungsInspection) : (new LungsInspection),
            ($inspection->cardiovascularInspection) ? ($inspection->cardiovascularInspection) : (new CardiovascularInspection),
            ($inspection->otherInspection) ? ($inspection->otherInspection) : (new OtherInspection),
        ]);
        $inspectionDocument->fillTemplate();

        $templateProcessor->saveAs(Yii::getAlias('@uploads').'\patterns\inspection-current.docx');*/
    }

    /**
     * Finds the Inspection model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @param integer $patient_id
     * @return Inspection the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id, $patient_id)
    {
        $model = Inspection::find()
            ->with()
            ->where('id = :id AND patient_id = :patient_id', [':id' => $id, ':patient_id' => $patient_id])
            ->one();
        if ($model !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Finds the Patient model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $patient_id
     * @return Patient the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findPatient($patient_id) {
        if ( ($patient = Patient::findOne($patient_id)) !== null ) {
            return $patient;
        } else {
            throw new NotFoundHttpException('Карта пациента не найдена.');   
        }
    }

    protected function getModelErrorsToString($errors) {
        $errorString = "";
        foreach ($errors as $error) {
            for ($i = 0, $maxi = count($error); $i < $maxi; $i++) {
                $errorString .= $error[$i]."\r\n";
            }
        }
        return $errorString;
    }

    public function actionFoo() {
        echo Yii::$app->encrypter->encrypt('string to encrypt');
        echo '<br>';
        echo Yii::$app->encrypter->decrypt('zyi/dVYOckMSGKbPRy2Rximr/15Pclu9oZScrOXdcGs=');
    }
}
