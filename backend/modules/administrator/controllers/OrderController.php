<?php

namespace app\modules\administrator\controllers;

use Yii;
use common\models\Order;
use common\models\ReceptionTime;
use common\models\OrderSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * OrderController implements the CRUD actions for Order model.
 */
class OrderController extends DefaultController
{
    /**
     * Lists all Order models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new OrderSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Order model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Order model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Order();

        if ($model->load(Yii::$app->request->post()))
        {
            if(ReceptionTime::isReceptionTimeFree($model->reception_time_id)) {
                Yii::$app->getSession()->setFlash('error', 'Кажется кто-то опередил Вас, записавшись на выбранное Вами время');
                return $this->render('create', [
                    'model' => $model,
                ]);
            } else {
                if($model->isOrderExists($model))
                {
                    Yii::$app->getSession()->setFlash('error', 'Гражданин с такими данными уже записан на прием в этот день');
                    return $this->render('create',['model' => $model]); 
                } else {
                    if($model->save()) {
                        Yii::$app->getSession()->setFlash('success', 'Запись успешно создана');
                        return $this->redirect(['view', 'id' => $model->id]);
                    } else {
                        Yii::$app->getSession()->setFlash('error', 'Не удалось сохранить заявку');
                        return $this->render('create',['model' => $model]);
                    }
                }
            }
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        //Получаем модель ReceptionTime чтобы в случае успешного изменения Ordera
        //обновить статус времени
        $reception_time = ReceptionTime::findOne($model->reception_time_id);

        if($model->load(Yii::$app->request->post())) 
        {
            if (ReceptionTime::isReceptionTimeFree($model->reception_time_id)) {

                Yii::$app->getSession()->setFlash('error', 'Кажется кто-то опередил Вас, записавшись на выбранное Вами время');
                
                return $this->render('update', [
                    'model' => $model,
                ]);

            } else {
                if($model->save()) {
                    if($model->reception_time_id != $reception_time->id) {
                        $reception_time->status = 0;
                        $reception_time->update();
                    }
                    return $this->redirect(['view', 'id' => $model->id]);
                } else {
                    Yii::$app->getSession()->setFlash('error', 'Не удалось обновить заявку');
                    return $this->render('update', [
                        'model' => $model,
                    ]);
                }
            }
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Order model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
       $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionGetOrder() {
        $params = json_decode(file_get_contents('php://input'));
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if ($params->data->orderId) {
            return Order::find()
                    ->where('id = :orderId', [':orderId' => $params->data->orderId])
                    ->with('receptionTime')
                    ->asArray()
                    ->one();
        } else {
            return null;
        }
    }

    public function actionGetReceptionTime() {
        $params = json_decode(file_get_contents('php://input'));
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if ($params->data->receptionDate) {
            if ($params->data->reception_time_id) {
                $receptionTime = ReceptionTime::find()
                ->join('JOIN', 'reception_day', 'reception_time.reception_day_id = reception_day.id')
                ->where('reception_day.reception_date = :reception_date AND (reception_time.status = 0 OR reception_time.id = :reception_time_id)', [':reception_date' => $params->data->receptionDate, ':reception_time_id' => $params->data->reception_time_id])
                ->orderBy('rec_time')
                ->all();
            } else {
                $receptionTime = ReceptionTime::find()
                ->join('JOIN', 'reception_day', 'reception_time.reception_day_id = reception_day.id')
                ->where('reception_day.reception_date = :reception_date AND reception_time.status = 0', [':reception_date' => $params->data->receptionDate])
                ->orderBy('rec_time')
                ->all();
            }
            return $receptionTime;
        } else {
            return null;
        }
    }

    /**
     * Finds the Order model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Order the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Order::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
