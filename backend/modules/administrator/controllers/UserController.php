<?php

namespace app\modules\administrator\controllers;

use Yii;
use app\modules\administrator\models\User;
use app\modules\administrator\models\UserSearch;
use app\modules\administrator\models\AuthAssignment;
use common\models\Administrator;
//use common\models\User;


use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\modules\administrator\models\SignupForm;
use app\modules\administrator\controllers\DefaultController;

/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends DefaultController
{
	 /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


	public function actionProfile($id)
    {
        $model = $this->findModel($id);
		$profile = AuthAssignment::find()->where(['user_id'=>$id])->one();
			
		switch($profile->item_name){
			case 'administrator':
				$this->redirect(['administrator/view', 'id' => $id]);
				break;
		}
    }
	
    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
        	if ($model->resetPassword) {
        		$model->password = $model->resetPassword;
        	}
        	$model->save();
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
		$model=$this->findModel($id);
        if ($model->id==Yii::$app->user->identity->id) {
            Yii::$app->getSession()->setFlash('error', 'Невозможно удалить свой профайл');
        } else {
            $model->delete();
        }
	   return $this->redirect(['index']); 
    } 


    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
	
	public function actionSignup() {
        /*$model = new User();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', 'Запись добавлена!');
            return $this->redirect(['index']);
        } else {
            return $this->render('signup', [
                'model' => $model,
            ]);
        }*/
		$model = new SignupForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup()) {
				$userRole = Yii::$app->authManager->getRole($model->role_name);
				Yii::$app->authManager->assign($userRole, $user->getId());

				switch($model->role_name){
					case 'administrator': {
                        //create default user profile
                        $profile = new Administrator();
                        $profile->user_id = $user->getId();
                        $profile->save();
                        $this->redirect(['administrator/update', 'id' => $profile->user_id]);
                    }break;
				}

				return;
			}	
        }
        return $this->render('signup', [
            'model' => $model,
        ]);
    }
	
    protected function getAdministratorById($id) 
    {
        if (($model = Administrator::findOne($id)) !== null){
            return $model;
        }else{
        	throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
     

    protected function getEditorById($id) {
        if (($model = Editor::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function getAuthorById($id) 
    {
        if(($model = Author::findOne($id)) !== null) {
            return $model;
        }else{
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
		
}
