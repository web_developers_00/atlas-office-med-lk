<?php

namespace app\modules\administrator\controllers;

use Yii;
use common\models\HeartStatus;
use common\models\HeartStatusSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;

use app\modules\administrator\controllers\DefaultController;

/**
 * HeartStatusController implements the CRUD actions for HeartStatus model.
 */
class HeartStatusController extends DefaultController
{
    /**
     * Lists all HeartStatus models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new HeartStatusSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single HeartStatus model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new HeartStatus model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new HeartStatus();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Creates a new HeartStatus model.
     * If creation is successful, the browser will be redirected to the 'index' page.
     * Else, if creation wasn't successfull -> if there was errors - set json heade response and return formated errors
     * Else this is first handling -> render form through ajax
     * @return mixed
     */
    public function actionCreateModal() {
        $model = new HeartStatus();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', 'Запись добавлена!');
            return $this->redirect(['index']);
        } else {
            if ($model->hasErrors()) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return $this->getModelErrorsToString($model->errors);
            } else {
                return $this->renderAjax('_modal_form', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * Updates an existing HeartStatus model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing HeartStatus model.
     * If update is successful, the browser will be redirected to the 'index' page.
     * Else, if creation wasn't successfull -> if there was errors - set json heade response and return formated errors
     * Else this is first handling -> render form through ajax
     * @param integer $id
     * @return mixed
     */
    public function actionUpdateModal($id) {
        $model = HeartStatus::findOne($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            if ($model->hasErrors()) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return $this->getModelErrorsToString($model->errors);
            } else {
                return $this->renderAjax('_modal_form', [
                    'model' => $model,
                ]);                
            }
        }
    }

    /**
     * Deletes an existing HeartStatus model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        if (!$this->findModel($id)->delete()) {
            Yii::$app->session->setFlash('warning', 'Невозможно удалить запись, т.к. она используется!');
        }

        return $this->redirect(['index']);
    }

    /**
     * Finds the HeartStatus model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return HeartStatus the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = HeartStatus::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Parse model errors to string
     * @param integer $id
     * @return HeartStatus model errors string
     */
    protected function getModelErrorsToString($errors) {
        $errorString = "";
        foreach ($errors as $error) {
            for ($i = 0, $maxi = count($error); $i < $maxi; $i++) {
                $errorString .= $error[$i]."\r\n";
            }
        }
        return $errorString;
    }
}
