<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\HelthStatus */

$this->title = 'Создание общего состояния здоровья';
$this->params['breadcrumbs'][] = ['label' => 'Виды состояний', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="helth-status-create">
    <h1>
        <p class="alert alert-success text-center">

            <?= Html::encode($this->title) ?>
        
        </p>
    </h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
