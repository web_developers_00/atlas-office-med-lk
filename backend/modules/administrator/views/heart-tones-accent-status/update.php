<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\HeartTonesAccentStatus */

$this->title = 'Редактирование акцента тонов сердца: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Акценты тонов сердца', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<div class="heart-tones-accent-status-update">
	<h1>
        <p class="alert alert-success text-center">

            <?= Html::encode($this->title) ?>
        
        </p>
    </h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
