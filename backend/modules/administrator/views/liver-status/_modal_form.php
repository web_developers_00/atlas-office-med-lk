<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\LiverStatus */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="liver-status-form">

    <?php $form = ActiveForm::begin([
    		'id' => 'modal-form'
    	]); ?>

    	<div class="alert alert-danger" role="alert" style="display: none"></div>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Обновить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
