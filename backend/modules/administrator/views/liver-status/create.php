<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\LiverStatus */

$this->title = 'Создание состояния печени';
$this->params['breadcrumbs'][] = ['label' => 'Состояния печени', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="liver-status-create">
	<h1>
        <p class="alert alert-success text-center">

            <?= Html::encode($this->title) ?>
        
        </p>
    </h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
