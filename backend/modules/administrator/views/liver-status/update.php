<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\LiverStatus */

$this->title = 'Редактирование состояния печени: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Состояния печени', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<div class="liver-status-update">
	<h1>
        <p class="alert alert-success text-center">

            <?= Html::encode($this->title) ?>
        
        </p>
    </h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
