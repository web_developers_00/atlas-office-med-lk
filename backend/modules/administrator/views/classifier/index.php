<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\Modal;

/* @var $this yii\web\View */
/* @var $searchModel common\models\ClassifierSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Категории обращений';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="classifier-index">

    <h3>
        <?= Html::tag('p', Html::encode($this->title), ['class' => 'alert alert-success text-center']) ?>
    </h3>

    <?php 

        Modal::begin([
            'header' => 'Категория обращения',
            'id' => 'editModalId',
            'class' => 'modal',
            'size' => 'modal-md',
        ]);

    ?>

        <div id="time-progressbar" class="progress">
          <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 45%">
            <span class="sr-only">45% Complete</span>
          </div>
        </div>
        <div class='modalContent'></div>

    <?php

        Modal::end();

    ?>

    <div class="form-group">
            
        <?= Html::a('Добавить категорию', ['create'], ['class'=>'btn btn-success modalButton', /*'title'=>'view/edit'*/]) ?>

    </div>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            'classifier_name',
            //'counter',

            ['class' => 'yii\grid\ActionColumn',
                'template' => '{update} {delete}',
                'buttons' => [
                    'update' => function ($url,$model) {
                        return Html::a('<span class="glyphicon glyphicon-pencil">&nbsp</span>', ['update?id='.$model->id],
                            ['title'=>'Редактировать', 'class' => 'modalButton']);
                    },
                ],
            ],
        ],
    ]); ?>

</div>
