<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Classifier */

$this->title = 'Обновить категорию';
$this->params['breadcrumbs'][] = ['label' => 'Категории', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Обновить';
?>
<div class="classifier-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
