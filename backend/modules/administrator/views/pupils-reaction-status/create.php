<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\PupilsReactionStatus */

$this->title = 'Создание вида реакции зрачков';
$this->params['breadcrumbs'][] = ['label' => 'Виды реакции зрачков', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pupils-reaction-status-create">
	<h1>
        <p class="alert alert-success text-center">

            <?= Html::encode($this->title) ?>
        
        </p>
    </h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
