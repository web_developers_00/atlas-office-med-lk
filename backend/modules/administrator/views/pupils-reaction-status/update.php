<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\PupilsReactionStatus */

$this->title = 'Редактирование вида реакции зрачков: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Виды реакции зрачков', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<div class="pupils-reaction-status-update">
	<h1>
        <p class="alert alert-success text-center">

            <?= Html::encode($this->title) ?>
        
        </p>
    </h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
