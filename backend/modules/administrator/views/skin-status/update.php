<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\SkinStatus */

$this->title = 'Редактирование состояния кожных покровов: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Состояния кожных покровов', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<div class="skin-status-update">
	<h1>
        <p class="alert alert-success text-center">

            <?= Html::encode($this->title) ?>
        
        </p>
    </h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
