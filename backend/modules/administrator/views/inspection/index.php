<?php

use yii\helpers\Html;
use yii\grid\GridView;
use dosamigos\datepicker\DatePicker;

/* @var $this yii\web\View */
/* @var $searchModel common\models\InspectionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = $patient->patient_name;
$this->params['breadcrumbs'][] = ['label' => 'Пациенты', 'url' => ['patient/']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="inspection-index">

    <h1>
        <p class="alert alert-success text-center"><?= Html::encode($this->title) ?></p>
    </h1>

    <?=
        $this->render('/patient/_view', [
            'model' => $patient
        ]);
    ?>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Начать новый осмотр', ['create', 'patient_id' => $patient->id, 'currentTab' => 'patient'], ['class' => 'btn btn-success']) ?>
    </p>
    <h2>
        <p class="alert alert-success text-center">Осмотры</p>
    </h2>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'inspection_date',
                'filter' => DatePicker::widget([
                    'model' => $searchModel,
                    'attribute' => 'inspection_date',
                    'language' => 'ru',
                    'clientOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd',
                        'clearBtn' => true,
                    ],
                ]),
                'contentOptions' => [
                    'class' => 'text-center',
                    'style' => 'width: 250px'
                ],
            ],
            'anamnesis:ntext',
            'complaints:ntext',
            // 'diagnosis:ntext',
            'treatment:ntext',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view} {delete}',
                'buttons' => [
                    'view' => function($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', ['view?id=' . $model->id . '&patient_id=' . $model->patient_id . '&currentTab=' . 'patient'],
                            [
                                'title'=>'Редактировать',
                            ]
                        );
                    },
                    'delete' => function($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', ['delete?id='.$model->id.'&patient_id='.$model->patient_id],
                            [
                                'title'=>'Удалить',
                                'data-confirm' => Yii::t('yii', 'Вы действительно хотите удалить запись?'),
                                'data-method' => 'post',
                            ]
                        );
                    },
                ],
                'contentOptions' => [
                    'class' => 'text-center',
                ],
            ],
        ],
    ]); ?>
</div>
