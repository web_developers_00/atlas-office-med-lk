<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\Survey;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model common\models\Inspection */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="inspection-complaints-form">

    <?php 
        $form = ActiveForm::begin([
            'id' => 'modal-form'
        ]); 
    ?>

        <div class="alert alert-danger" role="alert" style="display: none"></div>

        <div class="row">
            <div class="col-md-12">
                
                <?= $form->field($model, $fieldName)->textarea(['rows' => 6]) ?>

            </div>
        </div>
        <div class="form-group text-right">
            
            <?=
                Html::submitButton($model->isNewRecord ? 'Создать' : 'Обновить', [
                    'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary'
                ]) 
            ?>

        </div>

    <?php ActiveForm::end(); ?>

</div>
