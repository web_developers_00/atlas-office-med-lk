<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\RibCageStatus */

$this->title = 'Редактирование состояния грудной клетки: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Состояния грудной клетки', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<div class="rib-cage-status-update">
	<h1>
        <p class="alert alert-success text-center">

            <?= Html::encode($this->title) ?>
        
        </p>
    </h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
