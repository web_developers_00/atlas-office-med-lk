<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\GeneralInspection */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="general-inspection-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'inspection_id')->textInput() ?>

    <?= $form->field($model, 'patient_id')->textInput() ?>

    <?= $form->field($model, 'helth_status_id')->textInput() ?>

    <?= $form->field($model, 'feeding_status_id')->textInput() ?>

    <?= $form->field($model, 'peripheral_lymph_nodes_status_id')->textInput() ?>

    <?= $form->field($model, 'body_mass_index')->textInput() ?>

    <?= $form->field($model, 'rash')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'edema')->textInput() ?>

    <?= $form->field($model, 'dysuria')->textInput() ?>

    <?= $form->field($model, 'skin_inspection_description')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'thyroid_inspection_description')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'mammary_gland_inspection_description')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'osteoarticular_inspection_description')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'stool_inspection_description')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'tongue_inspection_description')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
