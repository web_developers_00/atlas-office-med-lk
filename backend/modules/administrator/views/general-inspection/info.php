<?php

use yii\helpers\Html;
use kartik\alert\Alert;

/* @var $this yii\web\View */
/* @var $model common\models\GeneralInspection */
?>
<fieldset class="inspection-fieldset">
    <legend>
        Основной осмотр
        
        <?=
            Html::a(empty($model) ? 'Создать' : 'Обновить', [
                (empty($model)) ? ('general-inspection/create-modal') : ('general-inspection/update-modal'), 
                'inspection_id' => $inspection->id,
                'patient_id' => $inspection->patient_id
            ], [
                'class' => empty($model) ? 'btn btn-success modalButton' : 'btn btn-primary modalButton'
            ])
        ?>

        <?= 
            (!empty($model)) ? (Html::a('Удалить', [
                'general-inspection/delete',
                'inspection_id' => $model->inspection_id,
                'patient_id' => $model->patient_id], [
                    'class' => 'btn btn-danger',
                    'data-confirm' => Yii::t('yii', 'Вы действительно хотите удалить запись?'),
                    'data-method' => 'post',
                ])) : ('')
        ?>
    
    </legend>
    
    <?php if ($model) { ?>

            <label class="control-label"><?= $model->attributeLabels()['helth_status_id'] ?>:</label>
            <?php if ($model->helthStatus) { ?>
                <?= $model->helthStatus->name ?>
            <?php } else { ?>
                <?= 'не определено' ?>
            <?php } ?>
                
            <hr>

                <label class="control-label"><?= $model->attributeLabels()['feeding_status_id'] ?>:</label>
                <?php if ($model->feedingStatus) { ?>
                    <?= $model->feedingStatus->name ?>
                <?php } else { ?>
                    <?= 'не определено' ?>
                <?php } ?>

            <hr>

                <label class="control-label"><?= $model->attributeLabels()['peripheral_lymph_nodes_status_id'] ?>:</label>
                <?php if ($model->peripheralLymphNodesStatus) { ?>
                    <?= $model->peripheralLymphNodesStatus->name ?>
                <?php } else { ?>
                    <?= 'не определено' ?>
                <?php } ?>

            <hr>

                <label class="control-label"><?= $model->attributeLabels()['body_mass_index'] ?>:</label>
                <?php if ($model->body_mass_index) { ?>
                    <?= $model->body_mass_index ?>
                <?php } else { ?>
                    <?= 'не определено' ?>
                <?php } ?>

            <hr>

                <label class="control-label"><?= $model->attributeLabels()['rash'] ?>:</label>
                <?php if ($model->rash) { ?>
                    <?= $model->rash ?>
                <?php } else { ?>
                    <?= 'не определено' ?>
                <?php } ?>

            <hr>

                <label class="control-label"><?= $model->attributeLabels()['dysuria'] ?>:</label>
                <?php if ($model->dysuria) { ?>
                    <?= 'есть' ?>
                <?php } else { ?>
                    <?= 'нет' ?>
                <?php } ?>

            <hr>

                <label class="control-label"><?= $model->attributeLabels()['edema'] ?>:</label>
                <?php if ($model->edema) { ?>
                    <?= 'есть' ?>
                <?php } else { ?>
                    <?= 'нет' ?>
                <?php } ?>
                <?php if ($model->edema_description) { ?>
                    <p>
                        <label class="control-label">Описание:</label>
                        <?= $model->edema_description ?>
                    </p>
                <?php } ?>

            <hr>

                <label class="control-label"><?= $model->attributeLabels()['skinStatuses'] ?>:</label>
                <?= $model->inspectionToString($model->skinStatuses) ?>
                <?php if ($model->skin_inspection_description) { ?>
                    <p>
                        <label class="control-label">Описание:</label>
                        <?= $model->skin_inspection_description ?>
                    </p>
                <?php } ?>

            <hr>

                <label class="control-label"><?= $model->attributeLabels()['thyroidStatuses'] ?>:</label>
                <?= $model->inspectionToString($model->thyroidStatuses) ?>
                <?php if ($model->thyroid_inspection_description) { ?>
                    <p>
                        <label class="control-label">Описание:</label>
                        <?= $model->thyroid_inspection_description ?>
                    </p>
                <?php } ?>

            <hr>

                <label class="control-label"><?= $model->attributeLabels()['mammaryGlandStatuses'] ?>:</label>
                <?= $model->inspectionToString($model->mammaryGlandStatuses) ?>
                <?php if ($model->mammary_gland_inspection_description) { ?>
                    <p>
                        <label class="control-label">Описание:</label>
                        <?= $model->mammary_gland_inspection_description ?>
                    </p>
                <?php } ?>

            <hr>

                <label class="control-label"><?= $model->attributeLabels()['osteoarticularSystemStatuses'] ?>:</label>
                <?= $model->inspectionToString($model->osteoarticularSystemStatuses) ?>
                <?php if ($model->osteoarticular_inspection_description) { ?>
                    <p>
                        <label class="control-label">Описание:</label>
                        <?= $model->osteoarticular_inspection_description ?>
                    </p>
                <?php } ?>

            <hr>

                <label class="control-label"><?= $model->attributeLabels()['stoolStatuses'] ?>:</label>
                <?= $model->inspectionToString($model->stoolStatuses) ?>
                <?php if ($model->stool_inspection_description) { ?>
                    <p>
                        <label class="control-label">Описание:</label>
                        <?= $model->stool_inspection_description ?>
                    </p>
                <?php } ?>

            <hr>

                <label class="control-label"><?= $model->attributeLabels()['tongueStatuses'] ?>:</label>
                <?= $model->inspectionToString($model->tongueStatuses) ?>
                <?php if ($model->tongue_inspection_description) { ?>
                    <p>
                        <label class="control-label">Описание:</label>
                        <?= $model->tongue_inspection_description ?>
                    </p>
                <?php } ?>
           
        <?php } else { ?>
            <?= Alert::widget([
                    'type' => Alert::TYPE_DANGER,
                    'titleOptions' => ['icon' => 'info-sign'],
                    'body' => 'Основной осмотр не создан',
                    'closeButton' => false
                ])
            ?>
        <?php } ?>
</fieldset>
