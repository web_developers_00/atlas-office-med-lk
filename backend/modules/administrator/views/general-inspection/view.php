<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\GeneralInspection */

$this->title = $model->inspection_id;
$this->params['breadcrumbs'][] = ['label' => 'General Inspections', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="general-inspection-view">
    <h1>
        <p class="alert alert-success text-center">

            <?= Html::encode($this->title) ?>
        
        </p>
    </h1>

    <p>
        <?= Html::a('Update', ['update', 'inspection_id' => $model->inspection_id, 'patient_id' => $model->patient_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'inspection_id' => $model->inspection_id, 'patient_id' => $model->patient_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы уверены, что хотите удалить запись?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'inspection_id',
            'patient_id',
            'helth_status_id',
            'feeding_status_id',
            'peripheral_lymph_nodes_status_id',
            'body_mass_index',
            'rash:ntext',
            'edema',
            'dysuria',
            'skin_inspection_description:ntext',
            'thyroid_inspection_description:ntext',
            'mammary_gland_inspection_description:ntext',
            'osteoarticular_inspection_description:ntext',
            'stool_inspection_description:ntext',
            'tongue_inspection_description:ntext',
        ],
    ]) ?>

</div>
