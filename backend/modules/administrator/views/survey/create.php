<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Survey */

$this->title = 'Создание вида обследования';
$this->params['breadcrumbs'][] = ['label' => 'Виды обследований', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="survey-create">
	<h1>
        <p class="alert alert-success text-center">

            <?= Html::encode($this->title) ?>
        
        </p>
    </h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
