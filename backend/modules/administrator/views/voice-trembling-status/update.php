<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\VoiceTremblingStatus */

$this->title = 'Редактирование вида голосового дрожания: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Голосовые дрожания', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<div class="voice-trembling-status-update">
	<h1>
        <p class="alert alert-success text-center">

            <?= Html::encode($this->title) ?>
        
        </p>
    </h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
