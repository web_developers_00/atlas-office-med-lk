<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\VoiceTremblingStatus */

$this->title = 'Создание вида голосового дрожания';
$this->params['breadcrumbs'][] = ['label' => 'Виды голосового дрожания', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="voice-trembling-status-create">
	<h1>
        <p class="alert alert-success text-center">

            <?= Html::encode($this->title) ?>
        
        </p>
    </h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
