<?php
/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;
use yii\widgets\ActiveForm;
use dosamigos\datepicker\DatePicker;
use kartik\time\TimePicker;
use yii\helpers\Url;
use yii\bootstrap\Modal;

$this->title = $model->reception_date;
$this->params['breadcrumbs'][] = ['label' => 'Дни приёма', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div id="earlier-appeal-view">

	<?= 
		DetailView::widget([
			'model' => $model,
			'attributes' => [
				'reception_date',
				[
					'attribute' => 'status',
					'value' => ($model->status) ? ('Опубликован') : ('На утверждении'),
				],
			],
		]);
	?>

	<?php 
		Modal::begin([
			'header' => 'День приёма: '.$model->reception_date,
			'id'=>'editModalId',
			'class' =>'modal',
			'size' => 'modal-md',
		]);
	?>
		
		<div id="time-progressbar" class="progress">
			<div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 45%">
				<span class="sr-only">45% Complete</span>
		  	</div>
		</div>
		<div class='modalContent'></div>

	<?php Modal::end(); ?>

	<div class="form-group">
			
		<?= Html::a('Добавить время приёма', ['create-time', 'id' => $model->id], ['class'=>'btn btn-success modalButton', 'title'=>'view/edit']) ?>

	</div>

    <?=
    	GridView::widget([
        	'dataProvider' => $dataProvider,
        	'filterModel' => $searchModel,
        	'columns' => [
            	['class' => 'yii\grid\SerialColumn'],
	            [
	            	'attribute' => 'rec_time',
	        		'value' => function($model){
	        			return $model->getTime($model->rec_time);
	        		}
	        	],
	            [
					'attribute' => 'status',
					'filter' => ['1' => 'Занято', '0' => 'Свободно'],
					'value' => function($model) {
						return ($model->status) ? ('Занято') : ('Свободно');
					},
				],
				[
					'format' => 'raw',
					'label' => 'Заявка',
					'value' => function($model) {
						$order = $model->orders[0];
						return ($order) ? (Html::a($order->citizen_name, ['order/view?id='.$order->id], ['title' => 'Заявка', 'target' => '_blank'])) : ('нет заявки');
					},
				],
            	[
            		'class' => 'yii\grid\ActionColumn',
                	'template' => '{update} {delete}',
                	'buttons' => [
                    	'update' => function ($url,$model) {
                        	return Html::a('<span class="glyphicon glyphicon-pencil">&nbsp</span>', ['edit-time?id='.$model->id],
                            ['title'=>'Редактировать', 'class' => 'modalButton']);
                    	},
                    	'delete' => function ($url,$model) {
                        	return Html::a('<span class="glyphicon glyphicon-trash"></span>', ['delete-time?id='.$model->id],
                            	['title'=>'Удалить',
                            	'data-confirm' => Yii::t('yii', 'Вы действительно хотите удалить время приёма?'),
                            	]);
                    	}
                	],
            	],
        	],
    	]);
    ?>

</div>
