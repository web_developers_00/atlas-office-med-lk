<?php
/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = "Изменить день приёма";
$this->params['breadcrumbs'][] = ['label' => 'Дни приёма', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Обновить';
?>
<div id="date-update">
	<h1> <?= Html::encode($this->title) ?> </h1>

	<?=

		$this->render('_form', [
			'model' => $model
		]);

	?>
	
</div>
