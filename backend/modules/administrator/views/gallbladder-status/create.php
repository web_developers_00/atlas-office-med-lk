<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\GallbladderStatus */

$this->title = 'Создание состояния желчного пузыря';
$this->params['breadcrumbs'][] = ['label' => 'Состояния желчного пузыря', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="gallbladder-status-create">
	<h1>
        <p class="alert alert-success text-center">

            <?= Html::encode($this->title) ?>
        
        </p>
    </h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
