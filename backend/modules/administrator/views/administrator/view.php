<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Administrator */

$this->title = 'Пользователь: '.$model->user->username;
$this->params['breadcrumbs'][] = ['label' => 'Пользователи', 'url' => ['user/index']];
$this->params['breadcrumbs'][] = ['label' => 'Администраторы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="administrator-view">

    <div class="panel panel-info">
        <div class="panel-heading"><h3><?= Html::encode($this->title) ?></h3></div> 

        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'user.username',
                'surname',
                'name',
                'lastname',
                'phone'
            ],
        ]) ?>

    </div>

    <p>
        <?= Html::a('Обновить', ['update', 'id' => $model->user_id], ['class' => 'btn btn-primary']) ?>
   </p>

</div>
