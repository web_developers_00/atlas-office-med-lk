<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\PupilsDiffStatus */

$this->title = 'Создание вида разниц зрачков';
$this->params['breadcrumbs'][] = ['label' => 'Виды разниц зрачков', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pupils-diff-status-create">
	<h1>
        <p class="alert alert-success text-center">

            <?= Html::encode($this->title) ?>
        
        </p>
    </h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
