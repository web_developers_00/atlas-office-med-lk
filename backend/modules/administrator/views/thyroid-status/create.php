<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\ThyroidStatus */

$this->title = 'Создание статуса щитовидной железы';
$this->params['breadcrumbs'][] = ['label' => 'Состояния щитовидной железы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="thyroid-status-create">
	<h1>
        <p class="alert alert-success text-center">

            <?= Html::encode($this->title) ?>
        
        </p>
    </h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
