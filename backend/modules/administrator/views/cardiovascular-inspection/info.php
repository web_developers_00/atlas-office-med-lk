<?php

use yii\helpers\Html;
use kartik\alert\Alert;

/* @var $this yii\web\View */
/* @var $model common\models\GeneralInspection */
?>
<fieldset class="inspection-fieldset">
    <legend>
        <a id="neorological-status">Сердечно-сосудистая система</a>
        
        <?=
            Html::a(empty($model) ? 'Создать' : 'Обновить', [
                (empty($model)) ? ('cardiovascular-inspection/create-modal') : ('cardiovascular-inspection/update-modal'), 
                'inspection_id' => $inspection->id,
                'patient_id' => $inspection->patient_id
            ], [
                'class' => empty($model) ? 'btn btn-success modalButton' : 'btn btn-primary modalButton'
            ])
        ?>

        <?= 
            (!empty($model)) ? (Html::a('Удалить', [
                'cardiovascular-inspection/delete',
                'inspection_id' => $model->inspection_id,
                'patient_id' => $model->patient_id], [
                    'class' => 'btn btn-danger',
                    'data-confirm' => Yii::t('yii', 'Вы действительно хотите удалить запись?'),
                    'data-method' => 'post',
                ])) : ('')
        ?>
    
    </legend>
    <?php if ($model) { ?>

            <label class="control-label"><?= $model->attributeLabels()['heart_status_id'] ?>:</label>
            <?php if (isset($model->heart_status_id)) { ?>
                    <?= ($model->heart_status_id === 1) ? ('увеличено') : ('не увеличено') ?>
            <?php } else { ?>
                    <?= 'не определено' ?>
            <?php } ?>

            <hr>

                <label class="control-label"><?= $model->attributeLabels()['heart_rate'] ?>:</label>
                <?php if ($model->heart_rate) { ?>
                        <?= $model->heart_rate ?>
                <?php } else { ?>
                        <?= 'не определено' ?>
                <?php } ?>

            <hr>

                <label class="control-label"><?= $model->attributeLabels()['left_blood_pressure'] ?>:</label>
                <?php if ($model->left_blood_pressure) { ?>
                        <?= $model->left_blood_pressure ?>
                <?php } else { ?>
                        <?= 'не определено' ?>
                <?php } ?>
                <br>
                <label class="control-label"><?= $model->attributeLabels()['right_blood_pressure'] ?>:</label>
                <?php if ($model->right_blood_pressure) { ?>
                        <?= $model->right_blood_pressure ?>
                <?php } else { ?>
                        <?= 'не определено' ?>
                <?php } ?>

            <hr>

                <label class="control-label"><?= $model->attributeLabels()['heartTonesStatuses'] ?>:</label>
                <?php if ($model->heartTonesStatuses) { ?>
                        <?= $model->inspectionToString($model->heartTonesStatuses) ?>
                <?php } else { ?>
                        <?= 'не определено' ?>
                <?php } ?>
                <?php if ($model->heart_tones_inspection_description) { ?>
                    <p>
                        <label class="control-label">Описание:</label>
                        <?= $model->heart_tones_inspection_description ?>
                    </p>
                <?php } ?>

            <hr>

                <label class="control-label"><?= $model->attributeLabels()['heartTonesAccentStatuses'] ?>:</label>
                <?php if ($model->heartTonesAccentStatuses) { ?>
                        <?= $model->inspectionToString($model->heartTonesAccentStatuses) ?>
                <?php } else { ?>
                        <?= 'не определено' ?>
                <?php } ?>
                <?php if ($model->heart_tones_accent_inspection_description) { ?>
                    <p>
                        <label class="control-label">Описание:</label>
                        <?= $model->heart_tones_accent_inspection_description ?>
                    </p>
                <?php } ?>

            <hr>

                <label class="control-label"><?= $model->attributeLabels()['heartNoisesStatuses'] ?>:</label>
                <?php if ($model->heartNoisesStatuses) { ?>
                        <?= $model->inspectionToString($model->heartNoisesStatuses) ?>
                <?php } else { ?>
                        <?= 'не определено' ?>
                <?php } ?>
                <?php if ($model->heart_noises_inspection_description) { ?>
                    <p>
                        <label class="control-label">Описание:</label>
                        <?= $model->heart_noises_inspection_description ?>
                    </p>
                <?php } ?>

            <hr>

    <?php } else { ?>
            <?= Alert::widget([
                    'type' => Alert::TYPE_DANGER,
                    'titleOptions' => ['icon' => 'info-sign'],
                    'body' => 'Осмотр не производился',
                    'closeButton' => false
                ])
            ?>
        <?php } ?>
</fieldset>
