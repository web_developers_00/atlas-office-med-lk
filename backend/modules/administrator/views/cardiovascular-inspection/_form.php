<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\CardiovascularInspection */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cardiovascular-inspection-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'inspection_id')->textInput() ?>

    <?= $form->field($model, 'patient_id')->textInput() ?>

    <?= $form->field($model, 'heart_status_id')->textInput() ?>

    <?= $form->field($model, 'heart_rate')->textInput() ?>

    <?= $form->field($model, 'left_blood_pressure')->textInput() ?>

    <?= $form->field($model, 'right_blood_pressure')->textInput() ?>

    <?= $form->field($model, 'heart_tones_inspection_description')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'heart_tones_accent_inspection_description')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'heart_noises_inspection_description')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
