<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\CardiovascularInspection */

$this->title = 'Create Cardiovascular Inspection';
$this->params['breadcrumbs'][] = ['label' => 'Cardiovascular Inspections', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cardiovascular-inspection-create">
	<h1>
        <p class="alert alert-success text-center">

            <?= Html::encode($this->title) ?>
        
        </p>
    </h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
