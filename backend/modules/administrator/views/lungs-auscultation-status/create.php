<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\LungsAuscultationStatus */

$this->title = 'Создание аускультативного состояния легких';
$this->params['breadcrumbs'][] = ['label' => 'Аскультативные состояния легких', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="lungs-auscultation-status-create">
	<h1>
        <p class="alert alert-success text-center">

            <?= Html::encode($this->title) ?>
        
        </p>
    </h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
