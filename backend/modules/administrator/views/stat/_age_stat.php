<?php

use miloschuman\highcharts\Highcharts;

/* @var $this yii\web\View */
/* @var $model common\models\City */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="age-stat">

	<?=
        Highcharts::widget([
            'scripts' => [
                'modules/exporting',
                'themes/grid-light',
            ],
            'options' => [
                'title' => [
                    'text' => 'Возраст',
                ],
                'labels' => [
                ],
                'series' => [
                    [
                        'type' => 'pie',
                        'name' => 'Всего',
                        'data' => $ageData->series,
                        'showInLegend' => true,
                        'dataLabels' => [
                            'enabled' => true,
                        ],
                    ],
                ],
            ]
        ]);
    ?>
    
</div>
