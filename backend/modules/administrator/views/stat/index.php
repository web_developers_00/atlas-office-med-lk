<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $searchModel common\models\CitySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Статистика';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="stat-index">
    <h3>

        <?= Html::tag('p', Html::encode($this->title), ['class' => 'alert alert-success text-center']) ?>
    
    </h3>
    <div class="panel panel-default">
      <div class="panel-heading text-center">
          <h3>Период</h3>
      </div>
      <div class="panel-body">

      	<?= 
            $this->render('_form', [
                'filterModel' => $filterModel,
            ])
        ?>

      </div>
    </div>
    <div class="row">
        <div class="col-md-6">

            <?=
                $this->render('_city_stat', [
                    'cityData' => $cityData,
                ])
            ?>

        </div>
        <div class="col-md-6">

        	<?=
                $this->render('_sex_stat', [
                    'sexData' => $sexData,
                ])
            ?>

        </div>
    </div>
    <div class="row">
    	<div class="col-md-6">
    		
    		<?=
    			$this->render('_classifier_stat', [
    				'classifierData' => $classifierData,
    			]);
    		?>

    	</div>
      <div class="col-md-6">

        <?=
          $this->render('_age_stat', [
              'ageData' => $ageData,
            ])
        ?>

      </div>
    </div>
</div>
