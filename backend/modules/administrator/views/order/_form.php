<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\EarlierAppeal;
use common\models\ReceptionTime;
use common\models\Classifier;
use backend\assets\AdminAsset;
use dosamigos\datepicker\DatePicker;

AdminAsset::register($this);

/* @var $this yii\web\View */
/* @var $model common\models\Order */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="order-form" ng-controller="OrderController" >

    <div class="panel panel-success block-shadow">
        <div class="panel-heading text-center">
            <h1>Форма заявки</h1>  
        </div>

        <?php $form = ActiveForm::begin(); ?>

            <div class="panel-body">
                <fieldset>
                    <legend>Общая информация</legend>

                    <?=
                        $form->field($model, 'citizen_name')->textInput([
                            'maxlength' => true,
                            'placeholder' => "Введите ФИО...",
                            'ng-model' => 'currentOrder.citizen_name',
                            'initial-value' => ''
                        ])
                    ?>

                    <div class="help-block"></div>
                    <div class="row">
                        <div class="col-md-6">

                            <?= 
                                $form->field($model, 'birth_date')->widget(DatePicker::ClassName(), [
                                    'language' => 'ru',
                                    'clientOptions' => [
                                        'autoclose' => true,
                                        'format' => 'yyyy-mm-dd',
                                    ]
                                ])
                            ?>

                            <div class="help-block"></div>
                        </div>
                        <div class="col-md-6">
                            
                            <?= 
                                $form->field($model, 'sex')->dropDownList(['1'=>'Мужской','0'=>'Женский'],['prompt' => 'Укажите свой пол...'])->label('Пол')
                            ?>

                            <div class="help-block"></div>
                        </div>
                    </div>
                </fieldset>
                <fieldset>
                    <legend>Адрес</legend>
                        <div class="address-form">
                            <div class="field" style="display:none">
                                <label>Регион</label>
                                <input type="text" name="region" value="Самарская">
                            </div>
                            <div class="field" style="display:none">
                                <label>Район</label>
                                <input type="text" name="district" value="Кинель-Черкасский">
                            </div>
                            <div class="row">
                                <div class="col-md-5">

                                    <?=
                                        $form->field($model, 'city')->textInput([
                                            'maxlength' => true,
                                            'placeholder' => "Укажите город, сельское поселение...",
                                            'class' => "form-control",
                                        ])->label('Город, село проживания')
                                    ?>

                                    <div class="help-block"></div>
                                </div>
                            <div class="col-md-3">

                                <?= 
                                    $form->field($model, 'street')->textInput([
                                        'maxlength' => true,
                                        'placeholder' => "Укажите улицу...",
                                        'class' => "form-control",
                                        'disabled' => '',
                                    ])->label('Улица')
                                ?>

                                <div class="help-block"></div>
                            </div>
                            <div class="col-md-2">

                                <?= 
                                    $form->field($model, 'house')->textInput([
                                        'maxlength' => true,
                                        'placeholder' => "Номер дома...",
                                        'class' => 'form-control',
                                        'disabled' => '',
                                    ])->label('Дом')
                                ?>

                                <div class="help-block"></div>
                            </div>
                            <div class="col-md-2">

                                <?= 
                                    $form->field($model, 'apartment_number')->textInput([
                                        'maxlength' => true,
                                        'placeholder' => "Номер квартиры...",
                                        'class' => 'form-control',
                                        'disabled' => '',
                                    ])->label('Квартира')
                                ?>

                                <div class="help-block"></div>
                            </div>
                        </div>
                    </fieldset>
                    <fieldset>
                        <legend>Контактные данные</legend>
                        <div class="row">
                            <div class="col-md-6">

                                <?= 
                                    $form->field($model, 'phone')->textInput([
                                        'maxlength' => true, 
                                        'placeholder' => "Укажите контактный телефон...", 
                                        'ng-model' => 'currentOrder.phone', 'initial-value' => ''
                                    ]) 
                                ?>

                            </div>
                            <div class="col-md-6">

                                <?= 
                                    $form->field($model, 'email')->textInput([
                                        'maxlength' => true, 
                                        'placeholder'=>"Укажите адрес электронной почты..."
                                    ])->label('Эл.почта') 
                                ?>

                            </div>
                        </div>
                        <div class="help-block"></div>
                    </fieldset>
                    <fieldset>
                        <legend>Содержание обращения</legend>
                        <div class="row">
                            <div class="col-md-6">

                                <?= 
                                    $form->field($model, 'classifier_id')->dropDownList(ArrayHelper::map(Classifier::find()->all(), 'id',
                                        function($model){
                                            return $model->classifier_name;
                                        }), ['prompt' => 'Выберите категорию...']) 
                                ?>

                            </div>
                            <div class="col-md-6">
                                
                                <?= 
                                    $form->field($model, 'earlier_appeal_id')->dropDownList(ArrayHelper::map(EarlierAppeal::find()->orderBy('id DESC')->all(), 'id',
                                        function($model){
                                            return $model->instance;
                                        }),['prompt' => 'Выберите инстанцию...', 'id' => 'firstOrganization'])
                                ?>

                            </div>
                        </div>
                        <div class="form-group field-earlierappeal-instance required" id="anotherOrganization" style="display: none;">

                            <?=
                                $form->field($model, 'earlier_appeal_text')->textInput([
                                    'maxlength' => true, 
                                    'placeholder' => "Укажите инстанцию...", 
                                    'ng-model' => 'currentOrder.earlier_appeal_text', 
                                    'initial-value' => ''
                                ]) 
                            ?>

                        </div>

                            <?=
                                $form->field($model, 'appeal')->textarea([
                                    'rows' => 6, 
                                    'placeholder'=>"Текст обращения", 
                                    'ng-model' => 'currentOrder.appeal', 
                                    'initial-value' => ''
                                ]) 
                            ?>
                    </fieldset>
                    <div class="row">
                        <div class="col-md-12">
                            <div ng-repeat="(key,val) in alerts" class="alert {{key}}">
                                <div ng-repeat="msg in val">{{msg}}</div>
                            </div>
                        </div>
                    </div>

                    <input type="text" value="<?= $model->id ?>" ng-model="order.id" initial-value="" style="display: none">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group field-order-reception-day">
                                <label class="control-label" for="order-phone">Приёмный день</label>

                                <?= 
                                    DatePicker::widget([
                                        'name' => 'reception-date',
                                        'value' => $model->receptionTime->receptionDay->reception_date,
                                        'language' => 'ru',
                                        'options' => [
                                            'ng-model' => 'receptionDate',
                                            'initial-value' => ''
                                        ],
                                        'template' => '{addon}{input}',
                                        'clientOptions' => [
                                            'autoclose' => true,
                                            'format' => 'yyyy-mm-dd',
                                            'clearBtn' => true,
                                        ]
                                    ]);
                                ?>

                            </div>
                        </div>
                        <div class="col-md-4">

                            <?=
                                $form->field($model, 'reception_time_id')->dropDownList([
                                    'prompt' => '----Время приёма----'], 
                                    ['ng-model' => 'currentReceptionTime', 
                                        'ng-options' => 'time.id as time.rec_time for time in receptionDayTime track by time.id', ])
                            ?>

                        </div>
                        <div class="col-md-4">
                            <div class="form-group field-order-reception_time_id required">
                                <div class="progress" id="progress-bar" style="display:none; margin: 30px 0 20px 0">
                                    <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 45%">
                                        <span class="sr-only">45% Complete</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">

                        <?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Обновить', 
                            ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary'])
                        ?>

                    </div>

                <?php ActiveForm::end(); ?>
    
            </div>
    </div>
</div>
