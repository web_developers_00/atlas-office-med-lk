<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\OsteoarticularSystemStatus */

$this->title = 'Создание состояние костно-суставной системы';
$this->params['breadcrumbs'][] = ['label' => 'Состояния костно суставной системы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="osteoarticular-system-status-create">
	<h1>
        <p class="alert alert-success text-center">

            <?= Html::encode($this->title) ?>
        
        </p>
    </h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
