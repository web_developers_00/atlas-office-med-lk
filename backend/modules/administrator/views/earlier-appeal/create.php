<?php
/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = "Добавить инстанцию";
$this->params['breadcrumbs'][] = ['label' => 'Инстанции', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div id="earlier-appeal-create">
	<h1> <?= Html::encode($this->title) ?> </h1>

	<?=

		$this->render('_form', [
			'model' => $model
		]);

	?>
	
</div>
