<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\HeartTonesStatus */

$this->title = 'Создание вида тонов сердца';
$this->params['breadcrumbs'][] = ['label' => 'Тоны сердца', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="heart-tones-status-create">
	<h1>
        <p class="alert alert-success text-center">

            <?= Html::encode($this->title) ?>
        
        </p>
    </h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
