<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\CrepitationStatus */

$this->title = 'Редактирование легочного хрипа: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Легочные хрипы', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<div class="crepitation-status-update">
	<h1>
        <p class="alert alert-success text-center">

            <?= Html::encode($this->title) ?>
        
        </p>
    </h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
