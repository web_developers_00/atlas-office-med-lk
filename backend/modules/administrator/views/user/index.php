<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Пользователи';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

    <h1>
    
        <?= Html::encode($this->title) ?>

    </h1>

    <p>
        <?= Html::a('Добавить пользователя', ['signup'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'username',
            'email:email',

            ['class' => 'yii\grid\ActionColumn',
                'controller' => 'user',
                'template' => '{update} {profile} {delete}',
                'buttons' => [
                    'profile' => function ($url,$model) {
                        return Html::a('<span class="glyphicon glyphicon-user"></span>', $url);
                    },
                ],
            ],
        ],
    ]); ?>

</div>
