<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model common\models\Patient */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="patient-form">
    <div class="panel panel-success">
        <div class="panel-heading text-center">
            <h1>Данные пациента</h1>
        </div>
        <div class="panel-body">

            <?php $form = ActiveForm::begin(); ?>

                <div class="row">
                    <div class="col-md-12">
                        
                        <?= $form->field($model, 'patient_name')->textInput(['maxlength' => true]) ?>

                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        
                        <?= $form->field($model, 'address')->textInput(['maxlength' => true]) ?>

                    </div>
                    <div class="col-md-6">
                        
                        <?= 
                            $form->field($model, 'birth_date')->widget(DatePicker::className(), [
                                'options' => ['placeholder' => 'Выберите дату'],
                                'removeButton' => [
                                    'icon' => 'trash',
                                ],
                                'pluginOptions' => [
                                    'autoclose' => true,
                                    'format' => 'yyyy-mm-dd',
                                ],
                            ]) 
                        ?>

                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                            
                        <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>

                    </div>
                </div>
                <div class="form-group text-right">
                    <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Обновить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                </div>

            <?php ActiveForm::end(); ?>

        </div>
    </div>
</div>
