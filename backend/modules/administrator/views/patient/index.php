<?php

use yii\helpers\Html;
use yii\grid\GridView;
use dosamigos\datepicker\DatePicker;

/* @var $this yii\web\View */
/* @var $searchModel common\models\PatientSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Пациенты клиники';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="patient-index">
    <h1>
        <p class="alert alert-success text-center">

            <?= Html::encode($this->title) ?>
        
        </p>
    </h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Добавить пациента', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'patient_name',
            [
                'attribute' => 'birth_date',
                'filter' => DatePicker::widget([
                    'model' => $searchModel,
                    'attribute' => 'birth_date',
                    'language' => 'ru',
                    'clientOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd',
                        'clearBtn' => true,
                    ],
                ]),
                'contentOptions' => [
                    'class' => 'text-center',
                    'style' => 'width: 150px'
                ],
            ],
            'address',
            [
                'attribute' => 'phone',
                'contentOptions' => ['class' => 'text-center'],
            ],
            [
                'attribute' => 'create_date',
                'filter' => DatePicker::widget([
                    'model' => $searchModel,
                    'attribute' => 'create_date',
                    'language' => 'ru',
                    'clientOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd',
                        'clearBtn' => true,
                    ],
                ]),
                'contentOptions' => [
                    'class' => 'text-center',
                    'style' => 'width: 150px'
                ],
            ],
            [
                'label' => 'Осмотры',
                'format' => 'raw',
                'value' => function($model){
                    return Html::a('Карта пациента', ['inspection/', 'patient_id' => $model->id], [
                        'title' => 'Список осмотров',
                        'target' => '_blank',
                        'class' => 'btn btn-success',
                    ]);
                }
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
