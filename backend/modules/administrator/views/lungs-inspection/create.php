<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\LungsInspection */

$this->title = 'Create Lungs Inspection';
$this->params['breadcrumbs'][] = ['label' => 'Lungs Inspections', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="lungs-inspection-create">
	<h1>
        <p class="alert alert-success text-center">

            <?= Html::encode($this->title) ?>
        
        </p>
    </h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
