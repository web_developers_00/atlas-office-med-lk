<?php

use yii\helpers\Html;
use kartik\alert\Alert;

/* @var $this yii\web\View */
/* @var $model common\models\GeneralInspection */
?>
<fieldset class="inspection-fieldset">
    <legend>
        <a id="neorological-status">Лёгкие</a>
        
        <?=
            Html::a(empty($model) ? 'Создать' : 'Обновить', [
                (empty($model)) ? ('lungs-inspection/create-modal') : ('lungs-inspection/update-modal'), 
                'inspection_id' => $inspection->id,
                'patient_id' => $inspection->patient_id
            ], [
                'class' => empty($model) ? 'btn btn-success modalButton' : 'btn btn-primary modalButton'
            ])
        ?>

        <?= 
            (!empty($model)) ? (Html::a('Удалить', [
                'lungs-inspection/delete',
                'inspection_id' => $model->inspection_id,
                'patient_id' => $model->patient_id], [
                    'class' => 'btn btn-danger',
                    'data-confirm' => Yii::t('yii', 'Вы действительно хотите удалить запись?'),
                    'data-method' => 'post',
                ])) : ('')
        ?>
    
    </legend>
    <?php if ($model) { ?>

                <label class="control-label"><?= $model->attributeLabels()['breathing_rate'] ?>:</label>
                <?php if ($model->breathing_rate) { ?>
                        <?= $model->breathing_rate ?>
                <?php } else { ?>
                        <?= 'не определено' ?>
                <?php } ?>

                <hr>

                    <label class="control-label"><?= $model->attributeLabels()['oxygen_saturation'] ?>:</label>
                    <?php if ($model->oxygen_saturation) { ?>
                            <?= $model->oxygen_saturation ?>
                    <?php } else { ?>
                            <?= 'не определено' ?>
                    <?php } ?>

                <hr>

                    <label class="control-label"><?= $model->attributeLabels()['lungsAuscultationStatuses'] ?>:</label>
                    <?php if ($model->lungsAuscultationStatuses) { ?>
                            <?= $model->inspectionToString($model->lungsAuscultationStatuses) ?>
                    <?php } else { ?>
                            <?= 'не определено' ?>
                    <?php } ?>
                    <?php if ($model->auscultation_inspection_drscription) { ?>
                        <p>
                            <label class="control-label">Описание:</label>
                            <?= $model->auscultation_inspection_drscription ?>
                        </p>
                    <?php } ?>

                <hr>

                    <label class="control-label"><?= $model->attributeLabels()['crepitationStatuses'] ?>:</label>
                    <?php if ($model->crepitationStatuses) { ?>
                            <?= $model->inspectionToString($model->crepitationStatuses) ?>
                    <?php } else { ?>
                            <?= 'не определено' ?>
                    <?php } ?>
                    <?php if ($model->crepitation_inspection_description) { ?>
                        <p>
                            <label class="control-label">Описание:</label>
                            <?= $model->crepitation_inspection_description ?>
                        </p>
                    <?php } ?>

                <hr>

                    <label class="control-label"><?= $model->attributeLabels()['lungsPercussionStatuses'] ?>:</label>
                    <?= $model->inspectionToString($model->lungsPercussionStatuses) ?>
                    <?php if ($model->percussion_inspection_description) { ?>
                        <p>
                            <label class="control-label">Описание:</label>
                            <?= $model->percussion_inspection_description ?>
                        </p>
                    <?php } ?>

                <hr>

                    <label class="control-label"><?= $model->attributeLabels()['ribCageStatuses'] ?>:</label>
                    <?php if ($model->ribCageStatuses) { ?>
                            <?= $model->inspectionToString($model->ribCageStatuses) ?>
                    <?php } else { ?>
                            <?= 'не определено' ?>
                    <?php } ?>
                    <?php if ($model->rib_cage_inspection_description) { ?>
                        <p>
                            <label class="control-label">Описание:</label>
                            <?= $model->rib_cage_inspection_description ?>
                        </p>
                    <?php } ?>

                <hr>

                    <label class="control-label"><?= $model->attributeLabels()['voiceTremblingStatuses'] ?>:</label>
                    <?php if ($model->voiceTremblingStatuses) { ?>
                            <?= $model->inspectionToString($model->voiceTremblingStatuses) ?>
                    <?php } else { ?>
                            <?= 'не определено' ?>
                    <?php } ?>
                    <?php if ($model->voice_trembling_inspection_description) { ?>
                        <p>
                            <label class="control-label">Описание:</label>
                            <?= $model->voice_trembling_inspection_description ?>
                        </p>
                    <?php } ?>

                <hr>

    <?php } else { ?>
            <?= Alert::widget([
                    'type' => Alert::TYPE_DANGER,
                    'titleOptions' => ['icon' => 'info-sign'],
                    'body' => 'Осмотр не производился',
                    'closeButton' => false
                ])
            ?>
        <?php } ?>
</fieldset>
