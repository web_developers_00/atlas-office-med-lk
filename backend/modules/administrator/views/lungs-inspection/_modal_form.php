<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model common\models\LungsInspection */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="lungs-inspection-form">

    <?php
        $form = ActiveForm::begin([
    		'id' => 'modal-form'
    	]);
    ?>

    <div class="alert alert-danger" role="alert" style="display: none"></div>
    <div class="row">
        <div class="col-md-6">
            
            <?= $form->field($model, 'breathing_rate')->textInput() ?>

        </div>
        <div class="col-md-6">
            
            <?= $form->field($model, 'oxygen_saturation')->textInput() ?>

        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <fieldset>
                <legend>Аускультативно</legend>

                <?= 
                    $form->field($model, 'lungsAuscultationStatuses')->widget(Select2::classname(), [
                        'data'=>$model->dropLungsAuscultationStatuses,
                        'options' => [
                            'multiple' => true,
                            'placeholder' => 'выбрать',
                        ],
                        'maintainOrder' => true,
                        'pluginOptions' => [
                            'allowClear' => true,
                            'closeOnSelect' => false,
                        ]
                    ])->label(false)
                ?>

                <?=
                    $form->field($model, 'auscultation_inspection_drscription')->textarea([
                        'rows' => 2,
                        'placeholder' => 'описание',
                    ])->label(false)
                ?>

            </fieldset>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <fieldset>
                <legend>Хрипы</legend>

                <?= 
                    $form->field($model, 'crepitationStatuses')->widget(Select2::classname(), [
                        'data'=>$model->dropCrepitationStatuses,
                        'options' => [
                            'multiple' => true,
                            'placeholder' => 'выбрать',
                        ],
                        'maintainOrder' => true,
                        'pluginOptions' => [
                            'allowClear' => true,
                            'closeOnSelect' => false,
                        ]
                    ])->label(false)
                ?>

                <?=
                    $form->field($model, 'crepitation_inspection_description')->textarea([
                        'rows' => 2,
                        'placeholder' => 'описание',
                    ])->label(false)
                ?>

            </fieldset>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <fieldset>
                <legend>Перкуторно</legend>

                <?= 
                    $form->field($model, 'lungsPercussionStatuses')->widget(Select2::classname(), [
                        'data'=>$model->dropLungsPercussionStatuses,
                        'options' => [
                            'multiple' => true,
                            'placeholder' => 'выбрать',
                        ],
                        'maintainOrder' => true,
                        'pluginOptions' => [
                            'allowClear' => true,
                            'closeOnSelect' => false,
                        ]
                    ])->label(false)
                ?>

                <?=
                    $form->field($model, 'percussion_inspection_description')->textarea([
                        'rows' => 2,
                        'placeholder' => 'описание',
                    ])->hiddenInput()->label(false)
                ?>

            </fieldset>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <fieldset>
                <legend>Грудная клетка</legend>

                <?= 
                    $form->field($model, 'ribCageStatuses')->widget(Select2::classname(), [
                        'data'=>$model->dropRibCageStatuses,
                        'options' => [
                            'multiple' => true,
                            'placeholder' => 'выбрать',
                        ],
                        'maintainOrder' => true,
                        'pluginOptions' => [
                            'allowClear' => true,
                            'closeOnSelect' => false,
                        ]
                    ])->label(false)
                ?>

                <?=
                    $form->field($model, 'rib_cage_inspection_description')->textarea([
                        'rows' => 2,
                        'placeholder' => 'описание',
                    ])->hiddenInput()->label(false)
                ?>

            </fieldset>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <fieldset>
                <legend>Голосовое дрожание</legend>

                <?= 
                    $form->field($model, 'voiceTremblingStatuses')->widget(Select2::classname(), [
                        'data'=>$model->dropVoiceTremblingStatuses,
                        'options' => [
                            'multiple' => true,
                            'placeholder' => 'выбрать',
                        ],
                        'maintainOrder' => true,
                        'pluginOptions' => [
                            'allowClear' => true,
                            'closeOnSelect' => false,
                        ]
                    ])->label(false)
                ?>

                <?=
                    $form->field($model, 'voice_trembling_inspection_description')->textarea([
                        'rows' => 2,
                        'placeholder' => 'описание',
                    ])->hiddenInput()->label(false)
                ?>

            </fieldset>
        </div>
    </div>
    <div class="form-group text-right">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Обновить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
