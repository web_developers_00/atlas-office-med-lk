<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\LungsInspectionSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="lungs-inspection-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'inspection_id') ?>

    <?= $form->field($model, 'patient_id') ?>

    <?= $form->field($model, 'breathing_rate') ?>

    <?= $form->field($model, 'oxygen_saturation') ?>

    <?= $form->field($model, 'rib_cage_inspection_description') ?>

    <?php // echo $form->field($model, 'percussion_inspection_description') ?>

    <?php // echo $form->field($model, 'voice_trembling_inspection_description') ?>

    <?php // echo $form->field($model, 'auscultation_inspection_drscription') ?>

    <?php // echo $form->field($model, 'crepitation_inspection_description') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
