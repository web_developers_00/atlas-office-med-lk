<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\StomachStatus */

$this->title = 'Создание состояния живота';
$this->params['breadcrumbs'][] = ['label' => 'Состояния живота', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="stomach-status-create">
	<h1>
        <p class="alert alert-success text-center">

            <?= Html::encode($this->title) ?>
        
        </p>
    </h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
