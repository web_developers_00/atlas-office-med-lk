<?php

/* @var $this \yii\web\View */
/* @var $content string */

use backend\assets\AdminAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use kartik\nav\NavX;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;
//use yii\widgets\Alert;


AdminAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>

<?php $this->beginBody() ?>

<div class="wrap">
    <?php

        NavBar::begin([
            'brandLabel' => 'Кабинет администратора',
            'brandUrl' => Yii::$app->homeUrl,
            'options' => [
                'class' => 'navbar-inverse navbar-fixed-top',
            ],
        ]);

            //$menuItems[] = ['label' => 'Пациенты клиники', 'url' => ['patient/index']];
            $menuItems[] = ['label' => 'Справочники', 'url' => ['index'],
                'items' => [
                    ['label' => 'Неврологический статус', 
                        'items' => [
                            ['label' => 'ПНП', 'url' => ['finger-nose-status/index']],
                            ['label' => 'Движения в конечностях', 'url' => ['extremities-movement-status/index']],
                            ['label' => 'Разница зрачков', 'url' => ['pupils-diff-status/index']],
                            ['label' => 'Реакция зрачков', 'url' => ['pupils-reaction-status/index']],
                        ]
                    ],
                    ['label' => 'Лёгкие', 
                        'items' => [
                            ['label' => 'Аускультативно', 'url' => ['lungs-auscultation-status/index']],
                            ['label' => 'Хрипы', 'url' => ['crepitation-status/index']],
                            ['label' => 'Перкуторно', 'url' => ['percussion-status/index']],
                            ['label' => 'Грудная клетка', 'url' => ['rib-cage-status/index']],
                            ['label' => 'Голосовое дрожание', 'url' => ['voice-trembling-status/index']],
                        ]
                    ],
                    ['label' => 'Сердечно-сосудистая система', 
                        'items' => [
                            ['label' => 'Сердце', 'url' => ['heart-status/index']],
                            ['label' => 'Тоны сердца', 'url' => ['heart-tones-status/index']],
                            ['label' => 'Акцент тонов сердца', 'url' => ['heart-tones-accent-status/index']],
                            ['label' => 'Шумы сердца', 'url' => ['heart-noises-status/index']],
                        ]
                    ],
                    ['label' => 'Общее состояние здоровья', 'url' => ['helth-status/index']],
                    ['label' => 'Периферические лимфоузлы', 'url' => ['peripheral-lymph-nodes-status/index']],
                    ['label' => 'Кожные покровы', 'url' => ['skin-status/index']],
                    ['label' => 'Щитовидная железа', 'url' => ['thyroid-status/index']],
                    ['label' => 'Молочные железы', 'url' => ['mammary-status/index']],
                    ['label' => 'Костно-суставная система', 'url' => ['osteoarticular-system-status/index']],
                    ['label' => 'Стул', 'url' => ['stool-status/index']],
                    ['label' => 'Почки', 'url' => ['kidneys-status/index']],
                    ['label' => 'Селезёнка', 'url' => ['spleen-status/index']],
                    ['label' => 'Печень', 'url' => ['liver-status/index']],
                    ['label' => 'Язык', 'url' => ['tongue-status/index']],
                    ['label' => 'Питание', 'url' => ['feeding-status/index']],
                    ['label' => 'Желчный пузырь', 'url' => ['gallbladder-status/index']],
                    ['label' => 'Живот', 'url' => ['stomach-status/index']],
                    ['label' => 'Обследования', 'url' => ['survey/index']],
                ]
            ];
            $menuItems[] = ['label' => 'Пользователи', 'url' => ['user/index']];
            $menuItems[] = ['label' => 'Рабочие кабинеты', 'url' => ['/site/login']];
            
            if (Yii::$app->user->isGuest) {
                $menuItems[] = ['label' => 'Вход', 'url' => ['/site/login']];
            } else {
                $menuItems[] = [
                    'label' => 'Выход ( '.Yii::$app->user->identity->username.' )',
                    'url' => ['/site/logout'],
                    'linkOptions' => ['data-method' => 'post']
                ];
            }

            echo NavX::widget([
                'options' => ['class' => 'navbar-nav navbar-right'],
                'items' => $menuItems,
                'activateParents' => true,
                'encodeLabels' => false
            ]);

        NavBar::end();
    /*NavBar::begin([
        'brandLabel' => 'Кабинет администратора',
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-default navbar-fixed-top',
        ],
    ]);
    $menuItems[] = ['label' => 'Пациенты клиники', 'url' => ['patient/index']];
    $menuItems[] = ['label' => 'Справочники', 'url' => ['index'],
        'items' => [
            ['label' => 'Общее состояние здоровья', 'url' => ['helth-status/index']],
            ['label' => 'Периферические лимфоузлы', 'url' => ['peripheral-lymph-nodes-status/index']],
            ['label' => 'Лёгочные хрипы', 'url' => ['crepitation-status/index']],
            ['label' => 'Движения в конечностях', 'url' => ['extremities-movement-status/index']],
            ['label' => 'Питание', 'url' => ['feeding-status/index']],
            ['label' => 'ПНП (пальценосовая проба)', 'url' => ['finger-nose-status/index']],
            ['label' => 'Желчный пузырь', 'url' => ['gallbladder-status/index']],
            ['label' => 'Шумы сердца', 'url' => ['heart-noises-status/index']],
            ['label' => 'Сердце', 'url' => ['heart-status/index']],
            ['label' => 'Акцент тонов сердца', 'url' => ['heart-tones-accent-status/index']],
            ['label' => 'Тоны сердца', 'url' => ['heart-tones-status/index']],
            ['label' => 'Голосовое дрожание', 'url' => ['voice-trembling-status/index']],
            ['label' => 'Язык', 'url' => ['tongue-status/index']],
            ['label' => 'Щитовидная железа', 'url' => ['thyroid-status/index']],
            ['label' => 'Стул', 'url' => ['stool-status/index']],
            ['label' => 'Живот', 'url' => ['stomach-status/index']],
            ['label' => 'Обследования', 'url' => ['survey/index']],
        ]
    ];
    $menuItems[] = ['label' => 'Пользователи', 'url' => ['user/index']];
    $menuItems[] = ['label' => 'Рабочие кабинеты', 'url' => ['/site/login']];
	
    if (Yii::$app->user->isGuest) {
        $menuItems[] = ['label' => 'Вход', 'url' => ['/site/login']];
    } else {
        $menuItems[] = [
            'label' => 'Выход ( '.Yii::$app->user->identity->username.' )',
            'url' => ['/site/logout'],
            'linkOptions' => ['data-method' => 'post']
        ];
    }
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => $menuItems,
    ]);
    NavBar::end();*/
    ?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; Система "Атлас-офис" <?= date('Y') ?></p>

        <p class="pull-right">
            <!--Разработано <a href="http://its-spc.ru">ИнтелТранс</a>-->
        </p>
    </div>
</footer>

<?php $this->endBody() ?>
<div id="backtop">&#9650;</div>
</body>
</html>
<?php $this->endPage() ?>
