<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\Modal;

/* @var $this yii\web\View */
/* @var $searchModel common\models\NeurologicalInspectionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Neurological Inspections';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="neurological-inspection-index">
    <h1>
        <p class="alert alert-success text-center">

            <?= Html::encode($this->title) ?>
        
        </p>
    </h1>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Neurological Inspection', ['create-modal'], ['class' => 'btn btn-success modalButton']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'inspection_id',
            'patient_id',
            'pupils_diff_status_id',
            'pupils_reaction_status_id',
            'finger_nose_inspection_description:ntext',
            // 'extremities_movement_inspection_description:ntext',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update} {delete}',
                'buttons' => [
                    'update' => function($url, $model) {
                        return Html::a('Редактировать', ['update-modal?id='.$model->id],
                            [
                                'title'=>'Редактировать',
                                'class' => 'modalButton btn btn-primary',
                            ]
                        );
                    },
                    'delete' => function($url, $model) {
                        return Html::a('Удалить', ['delete?id='.$model->id],
                            [
                                'title'=>'Удалить',
                                'data-confirm' => Yii::t('yii', 'Вы действительно хотите удалить запись?'),
                                'data-method' => 'post',
                                'class' => 'btn btn-danger',
                            ]
                        );
                    },
                ],
                'contentOptions' => [
                    'class' => 'text-center',
                ],
            ],
        ],
    ]); ?>

<?php    Modal::begin([
            'header' => Html::encode($this->title),
            'id' => 'editModalId',
            'class' => 'modal',
            'size' => 'modal-md',
            'options' => [
                'tabindex' => false,
            ],
        ]);
?>
    <div id="time-progressbar" class="progress">
      <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 45%">
        <span class="sr-only">45% Complete</span>
      </div>
    </div>
    <div class='modalContent'></div>

<?php    Modal::end();
?>
</div>
