<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\NeurologicalInspection */

$this->title = $model->inspection_id;
$this->params['breadcrumbs'][] = ['label' => 'Neurological Inspections', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="neurological-inspection-view">
    <h1>
        <p class="alert alert-success text-center">

            <?= Html::encode($this->title) ?>
        
        </p>
    </h1>

    <p>
        <?= Html::a('Update', ['update', 'inspection_id' => $model->inspection_id, 'patient_id' => $model->patient_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'inspection_id' => $model->inspection_id, 'patient_id' => $model->patient_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы уверены, что хотите удалить запись?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'inspection_id',
            'patient_id',
            'pupils_diff_status_id',
            'pupils_reaction_status_id',
            'finger_nose_inspection_description:ntext',
            'extremities_movement_inspection_description:ntext',
        ],
    ]) ?>

</div>
