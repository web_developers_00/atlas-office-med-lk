<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\MammaryStatus */

$this->title = 'Создание состояния молочных желез';
$this->params['breadcrumbs'][] = ['label' => 'Состояния молочных желез', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mammary-status-create">
	<h1>
        <p class="alert alert-success text-center">

            <?= Html::encode($this->title) ?>
        
        </p>
    </h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
