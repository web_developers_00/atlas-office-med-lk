<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\PeripheralLymphNodesStatus */

$this->title = 'Редактирвание состояния периферических лимфоузлов: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Состояния периферических лимфоузлов', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<div class="peripheral-lymph-nodes-status-update">
	<h1>
        <p class="alert alert-success text-center">

            <?= Html::encode($this->title) ?>
        
        </p>
    </h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
