<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\PeripheralLymphNodesStatus */

$this->title = 'Создание состояния периферических лимфоузлов';
$this->params['breadcrumbs'][] = ['label' => 'Состояния периферических лимфоузлов', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="peripheral-lymph-nodes-status-create">
	<h1>
        <p class="alert alert-success text-center">

            <?= Html::encode($this->title) ?>
        
        </p>
    </h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
