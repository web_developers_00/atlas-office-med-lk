<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\HeartStatus */

$this->title = 'Создание состояния сердца';
$this->params['breadcrumbs'][] = ['label' => 'Состояния сердца', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="heart-status-create">
	<h1>
        <p class="alert alert-success text-center">

            <?= Html::encode($this->title) ?>
        
        </p>
    </h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
