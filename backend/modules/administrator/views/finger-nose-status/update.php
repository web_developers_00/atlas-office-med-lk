<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\FingerNoseStatus */

$this->title = 'Редактирование состояния пальценосовой пробы: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Состояния пальценосовой пробы', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<div class="finger-nose-status-update">
	<h1>
        <p class="alert alert-success text-center">

            <?= Html::encode($this->title) ?>
        
        </p>
    </h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
