<?php
namespace app\modules\administrator\models;

use yii\base\Model;
use Yii;

/**
 * Signup form
 */
class OrderStatFilterForm extends Model
{
    public $fromDate;
    public $toDate;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fromDate', 'toDate'], 'required'],
            [['fromDate', 'toDate'], 'date', 'format' => 'php:Y-m-d']
        ];
    }
	
	public function attributeLabels()
    {
        return [
            'fromDate' => 'Начальная дата',
			'toDate' => 'Конечная дата',
        ];
    }
}
