<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\RibCageStatus */

$this->title = 'Создание состояния грудной клетки';
$this->params['breadcrumbs'][] = ['label' => 'Состояния грудной клетки', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rib-cage-status-create">
	<h1>
        <p class="alert alert-success text-center">

            <?= Html::encode($this->title) ?>
        
        </p>
    </h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
