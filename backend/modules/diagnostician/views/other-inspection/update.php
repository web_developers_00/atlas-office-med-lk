<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\OtherInspection */

$this->title = 'Update Other Inspection: ' . $model->inspection_id;
$this->params['breadcrumbs'][] = ['label' => 'Other Inspections', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->inspection_id, 'url' => ['view', 'inspection_id' => $model->inspection_id, 'patient_id' => $model->patient_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="other-inspection-update">
	<h1>
        <p class="alert alert-success text-center">

            <?= Html::encode($this->title) ?>
        
        </p>
    </h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
