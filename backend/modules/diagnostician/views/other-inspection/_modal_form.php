<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model common\models\OtherInspection */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="other-inspection-form">

    <?php
        $form = ActiveForm::begin([
    		'id' => 'modal-form'
    	]);
    ?>

    	<div class="alert alert-danger" role="alert" style="display: none"></div>
        <div class="row">
            <div class="col-md-12">
                <fieldset>
                    <legend>Желчный пузырь</legend>

                    <?= 
                        $form->field($model, 'gallbladderStatuses')->widget(Select2::classname(), [
                            'data'=>$model->dropGallbladderStatuses,
                            'options' => [
                                'multiple' => true,
                                'placeholder' => 'выбрать',
                            ],
                            'maintainOrder' => true,
                            'pluginOptions' => [
                                'allowClear' => true,
                                'closeOnSelect' => false,
                            ]
                        ])->label(false)
                    ?>

                    <?=
                        $form->field($model, 'gallbladder_inspection_description')->textarea([
                            'rows' => 2,
                            'placeholder' => 'описание',
                        ])->hiddenInput()->label(false)
                    ?>

                </fieldset>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <fieldset>
                    <legend>Почки</legend>

                    <?= 
                        $form->field($model, 'kidneyStatuses')->widget(Select2::classname(), [
                            'data'=>$model->dropKidneyStatuses,
                            'options' => [
                                'multiple' => true,
                                'placeholder' => 'выбрать',
                            ],
                            'maintainOrder' => true,
                            'pluginOptions' => [
                                'allowClear' => true,
                                'closeOnSelect' => false,
                            ]
                        ])->label(false)
                    ?>

                    <?=
                        $form->field($model, 'kidneys_inspection_description')->textarea([
                            'rows' => 1
                        ])->hiddenInput()->label(false)
                    ?>

                </fieldset>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <fieldset>
                    <legend>Печень</legend>

                    <?= 
                        $form->field($model, 'liverStatuses')->widget(Select2::classname(), [
                            'data'=>$model->dropLiverStatuses,
                            'options' => [
                                'multiple' => true,
                                'placeholder' => 'выбрать',
                            ],
                            'maintainOrder' => true,
                            'pluginOptions' => [
                                'allowClear' => true,
                                'closeOnSelect' => false,
                            ]
                        ])->label(false)
                    ?>

                    <?=
                        $form->field($model, 'liver_inspection_description')->textarea([
                            'rows' => 2,
                            'placeholder' => 'описание',
                        ])->label(false)
                    ?>

                </fieldset>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <fieldset>
                    <legend>Селезёнка</legend>

                    <?= 
                        $form->field($model, 'spleenStatuses')->widget(Select2::classname(), [
                            'data'=>$model->dropSpleenStatuses,
                            'options' => [
                                'multiple' => true,
                                'placeholder' => 'выбрать',
                            ],
                            'maintainOrder' => true,
                            'pluginOptions' => [
                                'allowClear' => true,
                                'closeOnSelect' => false,
                            ]
                        ])->label(false)
                    ?>

                    <?=
                        $form->field($model, 'spleen_inspection_description')->textarea([
                            'rows' => 2,
                            'placeholder' => 'описание',
                        ])->hiddenInput()->label(false)
                    ?>

                </fieldset>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <fieldset>
                    <legend>Живот</legend>

                    <?= 
                        $form->field($model, 'stomachStatuses')->widget(Select2::classname(), [
                            'data'=>$model->dropStomachStatuses,
                            'options' => [
                                'multiple' => true,
                                'placeholder' => 'выбрать',
                            ],
                            'maintainOrder' => true,
                            'pluginOptions' => [
                                'allowClear' => true,
                                'closeOnSelect' => false,
                            ]
                        ])->label(false)
                    ?>

                    <?=
                        $form->field($model, 'stomach_inspection_description')->textarea([
                            'rows' => 2,
                            'placeholder' => 'описание',
                        ])->label(false)
                    ?>

                </fieldset>
            </div>
        </div>
        <div class="form-group text-right">
            <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Обновить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>

    <?php ActiveForm::end(); ?>

</div>
