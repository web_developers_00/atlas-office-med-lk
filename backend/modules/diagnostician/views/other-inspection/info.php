<?php

use yii\helpers\Html;
use kartik\alert\Alert;

/* @var $this yii\web\View */
/* @var $model common\models\GeneralInspection */
?>
<fieldset class="inspection-fieldset">
    <legend>
        <a id="neorological-status">Другие осмотры</a>
        
        <?=
            Html::a(empty($model) ? 'Создать' : 'Обновить', [
                (empty($model)) ? ('other-inspection/create-modal') : ('other-inspection/update-modal'), 
                'inspection_id' => $inspection->id,
                'patient_id' => $inspection->patient_id
            ], [
                'class' => empty($model) ? 'btn btn-success modalButton' : 'btn btn-primary modalButton'
            ])
        ?>

        <?= 
            (!empty($model)) ? (Html::a('Удалить', [
                'other-inspection/delete',
                'inspection_id' => $model->inspection_id,
                'patient_id' => $model->patient_id], [
                    'class' => 'btn btn-danger',
                    'data-confirm' => Yii::t('yii', 'Вы действительно хотите удалить запись?'),
                    'data-method' => 'post',
                ])) : ('')
        ?>
    
    </legend>
    <?php if ($model) { ?>

                    <label class="control-label"><?= $model->attributeLabels()['gallbladderStatuses'] ?>:</label>
                    <?= $model->inspectionToString($model->gallbladderStatuses) ?>
                    <?php if ($model->gallbladder_inspection_description) { ?>
                        <p>
                            <label class="control-label">Описание:</label>
                            <?= $model->gallbladder_inspection_description ?>
                        </p>
                    <?php } ?>

                <hr>

                    <label class="control-label"><?= $model->attributeLabels()['kidneyStatuses'] ?>:</label>
                    <?= $model->inspectionToString($model->kidneyStatuses) ?>
                    <?php if ($model->kidneys_inspection_description) { ?>
                        <p>
                            <label class="control-label">Описание:</label>
                            <?= $model->kidneys_inspection_description ?>
                        </p>
                    <?php } ?>

                <hr>

                    <label class="control-label"><?= $model->attributeLabels()['liverStatuses'] ?>:</label>
                    <?= $model->inspectionToString($model->liverStatuses) ?>
                    <?php if ($model->liver_inspection_description) { ?>
                        <p>
                            <label class="control-label">Описание:</label>
                            <?= $model->liver_inspection_description ?>
                        </p>
                    <?php } ?>

                <hr>

                    <label class="control-label"><?= $model->attributeLabels()['spleenStatuses'] ?>:</label>
                    <?= $model->inspectionToString($model->spleenStatuses) ?>
                    <?php if ($model->spleen_inspection_description) { ?>
                        <p>
                            <label class="control-label">Описание:</label>
                            <?= $model->spleen_inspection_description ?>
                        </p>
                    <?php } ?>

                <hr>

                    <label class="control-label"><?= $model->attributeLabels()['stomachStatuses'] ?>:</label>
                    <?= $model->inspectionToString($model->stomachStatuses) ?>
                    <?php if ($model->stomach_inspection_description) { ?>
                        <p>
                            <label class="control-label">Описание:</label>
                            <?= $model->stomach_inspection_description ?>
                        </p>
                    <?php } ?>
                
                <hr>

    <?php } else { ?>
            <?= Alert::widget([
                    'type' => Alert::TYPE_DANGER,
                    'titleOptions' => ['icon' => 'info-sign'],
                    'body' => 'Другие осмотры не проводились',
                    'closeButton' => false
                ])
            ?>
        <?php } ?>
</fieldset>
