<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\FeedingStatus */

$this->title = 'Редактирование статуса питания: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Статусы питания', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<div class="feeding-status-update">
	<h1>
        <p class="alert alert-success text-center">

            <?= Html::encode($this->title) ?>
        
        </p>
    </h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
