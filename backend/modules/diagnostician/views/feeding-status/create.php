<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\FeedingStatus */

$this->title = 'Создание статуса питания';
$this->params['breadcrumbs'][] = ['label' => 'Статусы питания', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="feeding-status-create">
	<h1>
        <p class="alert alert-success text-center">

            <?= Html::encode($this->title) ?>
        
        </p>
    </h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
