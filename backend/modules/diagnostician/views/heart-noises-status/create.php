<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\HeartNoisesStatus */

$this->title = 'Создание вида шумов сердца';
$this->params['breadcrumbs'][] = ['label' => 'Шумы сердца', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="heart-noises-status-create">
	<h1>
        <p class="alert alert-success text-center">

            <?= Html::encode($this->title) ?>
        
        </p>
    </h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
