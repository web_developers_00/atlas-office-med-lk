<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\LungsInspection */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="lungs-inspection-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'inspection_id')->textInput() ?>

    <?= $form->field($model, 'patient_id')->textInput() ?>

    <?= $form->field($model, 'breathing_rate')->textInput() ?>

    <?= $form->field($model, 'oxygen_saturation')->textInput() ?>

    <?= $form->field($model, 'rib_cage_inspection_description')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'percussion_inspection_description')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'voice_trembling_inspection_description')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'auscultation_inspection_drscription')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'crepitation_inspection_description')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
