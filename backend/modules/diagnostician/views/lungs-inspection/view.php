<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\LungsInspection */

$this->title = $model->inspection_id;
$this->params['breadcrumbs'][] = ['label' => 'Lungs Inspections', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="lungs-inspection-view">
    <h1>
        <p class="alert alert-success text-center">

            <?= Html::encode($this->title) ?>
        
        </p>
    </h1>

    <p>
        <?= Html::a('Update', ['update', 'inspection_id' => $model->inspection_id, 'patient_id' => $model->patient_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'inspection_id' => $model->inspection_id, 'patient_id' => $model->patient_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы уверены, что хотите удалить запись?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'inspection_id',
            'patient_id',
            'breathing_rate',
            'oxygen_saturation',
            'rib_cage_inspection_description:ntext',
            'percussion_inspection_description:ntext',
            'voice_trembling_inspection_description:ntext',
            'auscultation_inspection_drscription:ntext',
            'crepitation_inspection_description:ntext',
        ],
    ]) ?>

</div>
