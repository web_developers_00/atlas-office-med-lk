<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use common\models\PupilsDiffStatus;
use common\models\PupilsReactionStatus;

/* @var $this yii\web\View */
/* @var $model common\models\NeurologicalInspection */
/* @var $form yii\widgets\ActiveForm */
$this->title = 'Неврологический статус';
?>
<?php $this->beginContent('@app/modules/diagnostician/views/layouts/inspection.php'); ?>
    
    <div class="row">
        <div class="col-md-12">
            <div class="card">

                <?=
                    $this->render('/patient/info', [
                        'patient' => $model->patient,
                    ])
                ?>

            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="neurological-inspection-form card">
                <fieldset>

                    <?php
                        $form = ActiveForm::begin([
                        ]);
                    ?>

                        <legend>

                            <a href="#"><?= $this->title ?></a>

                            <div class="form-group text-left" style="margin:10px 0 10px 0">
                            
                                <?=
                                    Html::submitButton($model->isNewRecord ? 'Создать' : 'Обновить', [
                                        'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary'
                                    ])
                                ?>

                            </div>

                        </legend>
                        <div class="row">
                            <div class="col-md-12">
                                <fieldset>
                                    <legend>Зрачки</legend>
                                    <div class="row">
                                        <div class="col-md-6">
                                            
                                            <?= 
                                                $form->field($model, 'pupils_diff_status_id')->dropDownList(ArrayHelper::map(PupilsDiffStatus::find()->all(), 'id', function($model) {
                                                        return $model->name;
                                                }), ['prompt' => 'Оцените разницу зрачков'])
                                            ?>

                                        </div>
                                        <div class="col-md-6">
                                            
                                            <?= 
                                                $form->field($model, 'pupils_reaction_status_id')->dropDownList(ArrayHelper::map(PupilsReactionStatus::find()->all(), 'id', function($model) {
                                                        return $model->name;
                                                }), ['prompt' => 'Оцените реакцию зрачков'])
                                            ?>

                                        </div>
                                    </div>
                                </fieldset>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <fieldset>
                                    <legend>Пальценосовая проба</legend>

                                    <?= 
                                        $form->field($model, 'fingerNoseStatuses')->widget(Select2::classname(), [
                                            'data'=>$model->dropFingerNoseStatuses,
                                            'options' => [
                                                'multiple' => true,
                                                'placeholder' => 'пальценосовая проба',
                                            ],
                                            'maintainOrder' => true,
                                            'pluginOptions' => [
                                                'allowClear' => true,
                                                'closeOnSelect' => false,
                                            ]
                                        ])->label(false)
                                    ?>

                                    <?=
                                        $form->field($model, 'finger_nose_inspection_description')->textarea([
                                            'rows' => 1
                                        ])->hiddenInput()->label(false)
                                    ?>

                                </fieldset>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <fieldset>
                                    <legend>Движение в конечностях</legend>

                                    <?= 
                                        $form->field($model, 'extremitiesMovementStatuses')->widget(Select2::classname(), [
                                            'data'=>$model->dropExtremitiesMovementStatuses,
                                            'options' => [
                                                'multiple' => true,
                                                'placeholder' => 'движение в конечностях',
                                            ],
                                            'maintainOrder' => true,
                                            'pluginOptions' => [
                                                'allowClear' => true,
                                                'closeOnSelect' => false,
                                            ]
                                        ])->label(false)
                                    ?>

                                    <?=
                                        $form->field($model, 'extremities_movement_inspection_description')->textarea([
                                            'rows' => 1
                                        ])->hiddenInput()->label(false)
                                    ?>

                                </fieldset>
                            </div>
                        </div>

                    <?php ActiveForm::end(); ?>

                </fieldset>
            </div>

        </div>
    </div>
<?php $this->endContent(); ?>
