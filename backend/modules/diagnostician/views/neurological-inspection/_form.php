<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\NeurologicalInspection */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="neurological-inspection-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'inspection_id')->textInput() ?>

    <?= $form->field($model, 'patient_id')->textInput() ?>

    <?= $form->field($model, 'pupils_diff_status_id')->textInput() ?>

    <?= $form->field($model, 'pupils_reaction_status_id')->textInput() ?>

    <?= $form->field($model, 'finger_nose_inspection_description')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'extremities_movement_inspection_description')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
