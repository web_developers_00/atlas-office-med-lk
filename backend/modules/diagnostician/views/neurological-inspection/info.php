<?php

use yii\helpers\Html;
use kartik\alert\Alert;

/* @var $this yii\web\View */
/* @var $model common\models\GeneralInspection */
?>
<fieldset class="inspection-fieldset">
    <legend>
        <a id="neorological-status">Неврологический статус</a>
        
        <?=
            Html::a(empty($model) ? 'Создать' : 'Обновить', [
                (empty($model)) ? ('neurological-inspection/create-modal') : ('neurological-inspection/update-modal'), 
                'inspection_id' => $inspection->id,
                'patient_id' => $inspection->patient_id
            ], [
                'class' => empty($model) ? 'btn btn-success modalButton' : 'btn btn-primary modalButton'
            ])
        ?>

        <?= 
            (!empty($model)) ? (Html::a('Удалить', [
                'neurological-inspection/delete',
                'inspection_id' => $model->inspection_id,
                'patient_id' => $model->patient_id], [
                    'class' => 'btn btn-danger',
                    'data-confirm' => Yii::t('yii', 'Вы действительно хотите удалить запись?'),
                    'data-method' => 'post',
                ])) : ('')
        ?>
    
    </legend>
    <?php if ($model) { ?>

            <label class="control-label">Разница зрачков:</label>
            <?php if ($model->pupilsDiffStatus) { ?>
                    <?= $model->pupilsDiffStatus->name ?>
            <?php } else { ?>
                    <?= 'не определено' ?>
            <?php } ?>
                
            <hr>

                <label class="control-label">Реакция зрачков:</label>
                <?php if ($model->pupilsReactionStatus) { ?>
                        <?= $model->pupilsReactionStatus->name ?>
                <?php } else { ?>
                        <?= 'не определено' ?>
                <?php } ?>
                
            <hr>

                <label class="control-label">Пальценосовая проба:</label>
                <?= $model->inspectionToString($model->fingerNoseStatuses) ?>
                <?php if ($model->finger_nose_inspection_description) { ?>
                    <p>
                        <label class="control-label">Описание:</label>
                        <?= $model->finger_nose_inspection_description ?>
                    </p>
                <?php } ?>

            <hr>

                <label class="control-label">Движения в конечностях:</label>
                <?= $model->inspectionToString($model->extremitiesMovementStatuses) ?>
                <?php if ($model->extremities_movement_inspection_description) { ?>
                    <p>
                        <label class="control-label">Описание:</label>
                        <?= $model->extremities_movement_inspection_description ?>
                    </p>
                <?php } ?>

            <hr>

    <?php } else { ?>
            <?= Alert::widget([
                    'type' => Alert::TYPE_DANGER,
                    'titleOptions' => ['icon' => 'info-sign'],
                    'body' => 'Неврологический статус неизвестен',
                    'closeButton' => false
                ])
            ?>
        <?php } ?>
</fieldset>
