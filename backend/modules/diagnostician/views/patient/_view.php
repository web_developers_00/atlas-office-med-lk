<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Patient */
?>
<?= DetailView::widget([
    'model' => $model,
    'attributes' => [
        'patient_name',
        'birth_date',
        'address',
        'phone',
        'create_date',
    ],
]) ?>
