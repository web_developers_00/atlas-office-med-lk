<fieldset class="inspection-fieldset">
    <legend>
        <a id="patient-info">Данные пациента</a>
    </legend>
    <div class="form-group has-success">
      <label class="control-label"><?= $patient->attributeLabels()['patient_name'] ?>:</label>
      <span><?= $patient->patient_name ?></span>
    </div>
    <div class="form-group has-success">
      <label class="control-label"><?= $patient->attributeLabels()['birth_date'] ?>:</label>
      <span><?= $patient->birth_date ?> (полных лет - <?= $patient->getFullYears($patient->birth_date) ?>)</span>
    </div>
</fieldset>