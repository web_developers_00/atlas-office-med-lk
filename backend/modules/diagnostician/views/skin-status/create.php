<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\SkinStatus */

$this->title = 'Создание состояния кожных покровов';
$this->params['breadcrumbs'][] = ['label' => 'Состояния кожных покровов', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="skin-status-create">
	<h1>
        <p class="alert alert-success text-center">

            <?= Html::encode($this->title) ?>
        
        </p>
    </h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
