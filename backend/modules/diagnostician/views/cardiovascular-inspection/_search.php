<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\CardiovascularInspectionSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cardiovascular-inspection-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'inspection_id') ?>

    <?= $form->field($model, 'patient_id') ?>

    <?= $form->field($model, 'heart_status_id') ?>

    <?= $form->field($model, 'heart_rate') ?>

    <?= $form->field($model, 'left_blood_pressure') ?>

    <?php // echo $form->field($model, 'right_blood_pressure') ?>

    <?php // echo $form->field($model, 'heart_tones_inspection_description') ?>

    <?php // echo $form->field($model, 'heart_tones_accent_inspection_description') ?>

    <?php // echo $form->field($model, 'heart_noises_inspection_description') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
