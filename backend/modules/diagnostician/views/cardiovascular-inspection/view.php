<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\CardiovascularInspection */

$this->title = $model->inspection_id;
$this->params['breadcrumbs'][] = ['label' => 'Cardiovascular Inspections', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cardiovascular-inspection-view">
    <h1>
        <p class="alert alert-success text-center">

            <?= Html::encode($this->title) ?>
        
        </p>
    </h1>

    <p>
        <?= Html::a('Update', ['update', 'inspection_id' => $model->inspection_id, 'patient_id' => $model->patient_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'inspection_id' => $model->inspection_id, 'patient_id' => $model->patient_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы уверены, что хотите удалить запись?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'inspection_id',
            'patient_id',
            'heart_status_id',
            'heart_rate',
            'left_blood_pressure',
            'right_blood_pressure',
            'heart_tones_inspection_description:ntext',
            'heart_tones_accent_inspection_description:ntext',
            'heart_noises_inspection_description:ntext',
        ],
    ]) ?>

</div>
