<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use common\models\HeartStatus;

/* @var $this yii\web\View */
/* @var $model common\models\CardiovascularInspection */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cardiovascular-inspection-form">

    <?php $form = ActiveForm::begin([
    		'id' => 'modal-form'
    	]); ?>

    	<div class="alert alert-danger" role="alert" style="display: none"></div>
        <div class="row">
            <div class="col-md-12">
                <fieldset>
                    <legend><?= $model->attributeLabels()['heart_status_id'] ?></legend>
                </fieldset>
                
                <?=
                    $form->field($model, 'heart_status_id')->dropDownList(ArrayHelper::map(HeartStatus::find()->all(), 'id', function($model) {
                            return $model->name; 
                    }), ['prompt' => 'Сердце'])->label(false)
                ?>

            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <fieldset>
                    <legend><?= $model->attributeLabels()['heart_rate'] ?></legend>

                    <?= $form->field($model, 'heart_rate')->textInput()->label(false) ?>

                </fieldset>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <fieldset>
                    <legend>Артериальное давление</legend>
                    <div class="row">
                        <div class="col-md-6">
                            
                            <?= $form->field($model, 'left_blood_pressure')->textInput() ?>
                            
                        </div>
                        <div class="col-md-6">
                            
                            <?= $form->field($model, 'right_blood_pressure')->textInput() ?>
                        
                        </div>
                    </div>
                </fieldset>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <fieldset>
                    <legend>Тоны сердца</legend>

                    <?= 
                        $form->field($model, 'heartTonesStatuses')->widget(Select2::classname(), [
                            'data'=>$model->dropHeartTonesStatuses,
                            'options' => [
                                'multiple' => true,
                                'placeholder' => 'выбрать',
                            ],
                            'maintainOrder' => true,
                            'pluginOptions' => [
                                'allowClear' => true,
                                'closeOnSelect' => false,
                            ]
                        ])->label(false)
                    ?>

                    <?=
                        $form->field($model, 'heart_tones_inspection_description')->textarea([
                            'rows' => 2,
                            'placeholder' => 'описание',
                        ])->label(false)
                    ?>

                </fieldset>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <fieldset>
                    <legend><?= $model->attributeLabels()['heartTonesAccentStatuses'] ?></legend>

                    <?= 
                        $form->field($model, 'heartTonesAccentStatuses')->widget(Select2::classname(), [
                            'data'=>$model->dropHeartTonesAccentStatuses,
                            'options' => [
                                'multiple' => true,
                                'placeholder' => 'выбрать',
                            ],
                            'maintainOrder' => true,
                            'pluginOptions' => [
                                'allowClear' => true,
                                'closeOnSelect' => false,
                            ]
                        ])->label(false)
                    ?>

                    <?=
                        $form->field($model, 'heart_tones_accent_inspection_description')->textarea([
                            'rows' => 2,
                            'placeholder' => 'описание',
                        ])->label(false)
                    ?>

                </fieldset>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <fieldset>
                    <legend><?= $model->attributeLabels()['heartNoisesStatuses'] ?></legend>

                    <?= 
                        $form->field($model, 'heartNoisesStatuses')->widget(Select2::classname(), [
                            'data'=>$model->dropHeartNoisesStatuses,
                            'options' => [
                                'multiple' => true,
                                'placeholder' => 'выбрать',
                            ],
                            'maintainOrder' => true,
                            'pluginOptions' => [
                                'allowClear' => true,
                                'closeOnSelect' => false,
                            ]
                        ])->label(false)
                    ?>

                    <?=
                        $form->field($model, 'heart_noises_inspection_description')->textarea([
                            'rows' => 2,
                            'placeholder' => 'описание',
                        ])->label(false)
                    ?>

                </fieldset>
            </div>
        </div>

        <div class="form-group text-right">
            <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Обновить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>

    <?php ActiveForm::end(); ?>

</div>
