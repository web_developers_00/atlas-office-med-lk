<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\PercussionStatus */

$this->title = 'Создание перкуторного состояния легких';
$this->params['breadcrumbs'][] = ['label' => 'Перкуторные состояния лёгких', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="percussion-status-create">
	<h1>
        <p class="alert alert-success text-center">

            <?= Html::encode($this->title) ?>
        
        </p>
    </h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
