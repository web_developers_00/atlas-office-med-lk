<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\CrepitationStatus */

$this->title = 'Создание легочного хрипа';
$this->params['breadcrumbs'][] = ['label' => 'Легочные хрипы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="crepitation-status-create">
	<h1>
        <p class="alert alert-success text-center">

            <?= Html::encode($this->title) ?>
        
        </p>
    </h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
