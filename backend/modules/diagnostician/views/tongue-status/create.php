<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\TongueStatus */

$this->title = 'Создание состояния языка';
$this->params['breadcrumbs'][] = ['label' => 'Состояния языка', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tongue-status-create">
	<h1>
        <p class="alert alert-success text-center">

            <?= Html::encode($this->title) ?>
        
        </p>
    </h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
