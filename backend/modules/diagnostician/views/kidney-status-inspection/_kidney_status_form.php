<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model common\models\OtherInspection */
/* @var $form yii\widgets\ActiveForm */

$this->title = $model->attributeLabels()['kidneyStatuses'];
?>
<?php $this->beginContent('@app/modules/diagnostician/views/layouts/inspection.php'); ?>
    
    <div class="row">
        <div class="col-md-12">
            <div class="card">

                <?=
                    $this->render('/patient/info', [
                        'patient' => $model->patient,
                    ])
                ?>

            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            
            <div class="other-inspection-form card">
                <fieldset>

                    <?php
                        $form = ActiveForm::begin([
                        ]);
                    ?>

                    <legend>
                        <a href="#"><?= $this->title ?></a>

                        <div class="form-group text-left" style="margin:10px 0 10px 0">
                        
                            <?=
                                Html::submitButton($model->isNewRecord ? 'Создать' : 'Обновить', [
                                    'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary'
                                ])
                            ?>

                        </div>
                    </legend>
                </fieldset>
                <div class="row">
                    <div class="col-md-12">
                        <fieldset>

                                <?= 
                                    $form->field($model, 'kidneyStatuses')->widget(Select2::classname(), [
                                        'data'=>$model->dropKidneyStatuses,
                                        'options' => [
                                            'multiple' => true,
                                            'placeholder' => 'выбрать',
                                        ],
                                        'maintainOrder' => true,
                                        'pluginOptions' => [
                                            'allowClear' => true,
                                            'closeOnSelect' => false,
                                        ]
                                    ])->label(false)
                                ?>

                                <?=
                                    $form->field($model, 'kidneys_inspection_description')->textarea([
                                        'rows' => 5,
                                        'placeholder' => 'описание',
                                    ])->hiddenInput()->label(false)
                                ?>

                        </fieldset>
                    </div>
                </div>

                <?php ActiveForm::end(); ?>

            </div>

        </div>
    </div>

<?php $this->endContent(); ?>
