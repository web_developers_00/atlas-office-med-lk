<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\GeneralInspectionSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="general-inspection-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'inspection_id') ?>

    <?= $form->field($model, 'patient_id') ?>

    <?= $form->field($model, 'helth_status_id') ?>

    <?= $form->field($model, 'feeding_status_id') ?>

    <?= $form->field($model, 'peripheral_lymph_nodes_status_id') ?>

    <?php // echo $form->field($model, 'body_mass_index') ?>

    <?php // echo $form->field($model, 'rash') ?>

    <?php // echo $form->field($model, 'edema') ?>

    <?php // echo $form->field($model, 'dysuria') ?>

    <?php // echo $form->field($model, 'skin_inspection_description') ?>

    <?php // echo $form->field($model, 'thyroid_inspection_description') ?>

    <?php // echo $form->field($model, 'mammary_gland_inspection_description') ?>

    <?php // echo $form->field($model, 'osteoarticular_inspection_description') ?>

    <?php // echo $form->field($model, 'stool_inspection_description') ?>

    <?php // echo $form->field($model, 'tongue_inspection_description') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
