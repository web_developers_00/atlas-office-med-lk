<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\HeartTonesAccentStatus */

$this->title = 'Создание вида акцента тонов сердца';
$this->params['breadcrumbs'][] = ['label' => 'Акценты тонов сердца', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="heart-tones-accent-status-create">
	<h1>
        <p class="alert alert-success text-center">

            <?= Html::encode($this->title) ?>
        
        </p>
    </h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
