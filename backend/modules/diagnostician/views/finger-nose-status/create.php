<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\FingerNoseStatus */

$this->title = 'Создание состояния пальценосовой пробы';
$this->params['breadcrumbs'][] = ['label' => 'Состояния пальценосовой пробы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="finger-nose-status-create">
	<h1>
        <p class="alert alert-success text-center">

            <?= Html::encode($this->title) ?>
        
        </p>
    </h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
