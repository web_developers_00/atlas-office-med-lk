<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\SpleenStatus */

$this->title = 'Создание состояния селезёнки';
$this->params['breadcrumbs'][] = ['label' => 'Состояния селезёнки', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="spleen-status-create">
	<h1>
        <p class="alert alert-success text-center">

            <?= Html::encode($this->title) ?>
        
        </p>
    </h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
