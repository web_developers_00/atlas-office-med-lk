<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\HelthStatus;
use common\models\FeedingStatus;
use common\models\PeripheralLymphNodesStatus;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model common\models\GeneralInspection */
/* @var $form yii\widgets\ActiveForm */
$this->title = 'Основной осмотр';
?>
<?php $this->beginContent('@app/modules/diagnostician/views/layouts/inspection.php'); ?>
    
    <div class="row">
        <div class="col-md-12">
            <div class="card">

                <?=
                    $this->render('/patient/info', [
                        'patient' => $model->patient,
                    ])
                ?>

            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="general-inspection-form card">
                <fieldset>
        
                    <?php
                        $form = ActiveForm::begin([
                        ]);
                    ?>

                        <legend>

                            <a href="#"><?= $this->title ?></a>

                            <div class="form-group text-left" style="margin:10px 0 10px 0">
                            
                                <?=
                                    Html::submitButton($model->isNewRecord ? 'Создать' : 'Обновить', [
                                        'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary'
                                    ])
                                ?>

                            </div>

                        </legend>
                        <div class="row">
                            <div class="col-md-6">
                                
                                <?= 
                                    $form->field($model, 'helth_status_id')->dropDownList(ArrayHelper::map(HelthStatus::find()->all(), 'id', function($model) {
                                        return $model->name;
                                    }), ['prompt' => 'Оцените общее состояние пациента']) 
                                ?>

                            </div>
                            <div class="col-md-6">
                                
                                <?= 
                                    $form->field($model, 'feeding_status_id')->dropDownList(ArrayHelper::map(FeedingStatus::find()->all(), 'id', function($model) {
                                        return $model->name;
                                    }), ['prompt' => 'Выберите питание'])
                                ?>

                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                
                                <?=
                                    $form->field($model, 'peripheral_lymph_nodes_status_id')->dropDownList(ArrayHelper::map(PeripheralLymphNodesStatus::find()->all(), 'id', function($model) {
                                        return $model->name;
                                    }), ['prompt' => 'Оцените состояние периферических лимфоузлов'])
                                ?>

                            </div>
                            <div class="col-md-6">

                                <?= $form->field($model, 'body_mass_index')->textInput() ?>

                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                
                                <?=
                                    $form->field($model, 'dysuria')->dropDownList([
                                        '0' => 'Нет',
                                        '1' => 'Есть',
                                    ], ['prompt' => 'Определите наличие дизурий'])
                                ?>

                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <fieldset>
                                    <legend>Отёки</legend>
                                
                                    <?=
                                        $form->field($model, 'edema')->dropDownList([
                                            '0' => 'Нет',
                                            '1' => 'Есть'
                                        ], ['prompt' => 'Определите наличие отёков'])->label(false)
                                    ?>

                                    <?=
                                        $form->field($model, 'edema_description')->textarea([
                                            'rows' => 2,
                                            'placeholder' => 'описание',
                                        ])->label(false)
                                    ?>

                                </fieldset>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <fieldset>
                                    <legend>Кожные покровы</legend>

                                    <?= 
                                        $form->field($model, 'skinStatuses')->widget(Select2::classname(), [
                                            'data'=>$model->dropSkinStatuses,
                                            'options' => [
                                                'multiple' => true,
                                                'placeholder' => 'кожные покровы',
                                            ],
                                            'maintainOrder' => true,
                                            'pluginOptions' => [
                                                'allowClear' => true,
                                                'closeOnSelect' => false,
                                            ]
                                        ])->label(false)
                                    ?>

                                    <?= $form->field($model, 'rash')->textarea(['rows' => 2]) ?>

                                    <?=
                                        $form->field($model, 'skin_inspection_description')->textarea([
                                            'rows' => 1
                                        ])->hiddenInput()->label(false)
                                    ?>

                                </fieldset>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <fieldset>
                                    <legend>Щитовидная железа</legend>

                                    <?= 
                                        $form->field($model, 'thyroidStatuses')->widget(Select2::classname(), [
                                            'data'=>$model->dropThyroidStatuses,
                                            'options' => [
                                                'multiple' => true,
                                                'placeholder' => 'щитовидная железа'
                                            ],
                                            'maintainOrder' => true,
                                            'pluginOptions' => [
                                                'allowClear' => true,
                                                'closeOnSelect' => false,
                                            ]
                                        ])->label(false)
                                    ?>

                                    <?=
                                        $form->field($model, 'thyroid_inspection_description')->textarea([
                                            'rows' => 2,
                                            'placeholder' => 'описание'
                                        ])->label(false)
                                    ?>

                                </fieldset>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <fieldset>
                                    <legend>Молочные железы</legend>

                                    <?= 
                                        $form->field($model, 'mammaryGlandStatuses')->widget(Select2::classname(), [
                                            'data'=>$model->dropMammaryGlandStatuses,
                                            'options' => [
                                                'multiple' => true,
                                                'placeholder' => 'молочные железы'
                                            ],
                                            'maintainOrder' => true,
                                            'pluginOptions' => [
                                                'allowClear' => true,
                                                'closeOnSelect' => false,
                                            ]
                                        ])->label(false)
                                    ?>

                                    <?= 
                                        $form->field($model, 'mammary_gland_inspection_description')->textarea([
                                            'rows' => 2,
                                            'placeholder' => 'описание',
                                        ])->label(false)
                                    ?>

                                </fieldset>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <fieldset>
                                    <legend>Костно-суставная система</legend>

                                    <?=
                                        $form->field($model, 'osteoarticularSystemStatuses')->widget(Select2::classname(), [
                                            'data' => $model->dropOsteoarticularSystemStatuses,
                                            'options' => [
                                                'multiple' => true,
                                                'placeholder' => 'костно-суставная система'
                                            ],
                                            'maintainOrder' => true,
                                            'pluginOptions' => [
                                                'allowClear' => true,
                                                'closeOnSelect' => false,
                                            ],
                                        ])->label(false)
                                    ?>

                                    <?= 
                                        $form->field($model, 'osteoarticular_inspection_description')->textarea([
                                            'rows' => 2,
                                            'placeholder' => 'описание'
                                        ])->label(false)
                                    ?>

                                </fieldset>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <fieldset>
                                    <legend>Стул</legend>

                                    <?=
                                        $form->field($model, 'stoolStatuses')->widget(Select2::classname(), [
                                            'data' => $model->dropStoolStatuses,
                                            'options' => [
                                                'multiple' => true,
                                                'placeholder' => 'стул',
                                            ],
                                            'maintainOrder' => true,
                                            'pluginOptions' => [
                                                'allowClear' => true,
                                                'closeOnSelect' => false,
                                            ],
                                        ])->label(false)
                                    ?>

                                    <?=
                                        $form->field($model, 'stool_inspection_description')->textarea([
                                            'rows' => 2,
                                            'placeholder' => 'описание',
                                        ])->label(false)
                                    ?>

                                </fieldset>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <fieldset>
                                    <legend>Язык</legend>
                                    
                                    <?=
                                        $form->field($model, 'tongueStatuses')->widget(Select2::classname(), [
                                            'data' => $model->dropTongueStatuses,
                                            'options' => [
                                                'multiple' => true,
                                                'placeholder' => 'язык',
                                            ],
                                            'maintainOrder' => true,
                                            'pluginOptions' => [
                                                'allowClear' => true,
                                                'closeOnSelect' => false,
                                            ],
                                        ])->label(false)
                                    ?>
                                    
                                    <?=
                                        $form->field($model, 'tongue_inspection_description')->textarea([
                                            'rows' => 1
                                        ])->hiddenInput()->label(false)
                                    ?>

                                </fieldset>
                            </div>
                        </div>

                    <?php ActiveForm::end(); ?>

                </fieldset>
            </div>
        </div>
    </div>

<?php $this->endContent(); ?>
