<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\Survey;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model common\models\Inspection */
/* @var $form yii\widgets\ActiveForm */
$this->title = 'Результаты осмотра';
?>
<?php $this->beginContent('@app/modules/diagnostician/views/layouts/inspection.php'); ?>
    
    <div class="row">
        <div class="col-md-12">
            <div class="card">

                <?=
                    $this->render('/patient/info', [
                        'patient' => $model->patient,
                    ])
                ?>

            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="inspection-anamnesis-form card">
                <fieldset>

                    <?php 
                        $form = ActiveForm::begin([
                        ]); 
                    ?>

                        <legend>
                            <a href="#"><?= $this->title ?></a>
                            <div class="form-group text-left" style="margin:10px 0 10px 0">
                            
                                <?=
                                    Html::submitButton($model->isNewRecord ? 'Создать' : 'Обновить', [
                                        'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary'
                                    ])
                                ?>

                            </div>
                        </legend>
                        <div class="row">
                            <div class="col-md-12">

                                <?= $form->field($model, 'diagnosis')->textarea(['rows' => 6]) ?>

                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                
                                <?= $form->field($model, 'treatment')->textarea(['rows' => 6]) ?>

                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                   
                                <?= 
                                    $form->field($model, 'surveys')->widget(Select2::classname(), [
                                        'data'=>$model->dropSurvey,
                                        'options' => ['multiple' => true],
                                        'maintainOrder' => true,
                                        'pluginOptions' => [
                                            'allowClear' => true,
                                            'closeOnSelect' => false,
                                        ]
                                    ])->label('Необходимые обследования')
                                ?>

                            </div>
                        </div>

                    <?php ActiveForm::end(); ?>

                </fieldset>
            </div>
        </div>
    </div>

<?php $this->endContent(); ?>
