<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\bootstrap\Modal;
use kartik\select2\Select2;
use common\models\Survey;
use yii\helpers\ArrayHelper;
use kartik\alert\Alert;

/* @var $this yii\web\View */
/* @var $model common\models\Inspection */

$this->title = 'Осмотр от ' . $model->inspection_date;
$this->params['breadcrumbs'][] = ['label' => 'Пациенты', 'url' => ['patient/']];
$this->params['breadcrumbs'][] = ['label' => $patient->patient_name, 'url' => ['index', 'patient_id' => $patient->id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<?php $this->beginContent('@app/modules/diagnostician/views/layouts/inspection.php'); ?>

<div class="inspection-view">

    <div class="row">
        <div class="col-md-12">
            
            <?=
                Html::a('Получить текстовый документ', [
                    'print',
                    'id' => $model->id,
                    'patient_id' => $model->patient_id,
                ], [
                    'class' => 'btn btn-success'
                ])
            ?>

        </div>
    </div>

    <h1 class="alert alert-success text-center">
    
        <?= Html::encode($this->title) ?>

    </h1>

<!--*******************************************-->

<div>
  <!-- Навигация -->
  <ul class="nav nav-tabs" role="tablist">
    <!--<li class="<?php //echo ($currentTab == 'patient') ? ('active') : ('') ?>"><a href="#patient" aria-controls="home" role="tab" data-toggle="tab">Данные пациента</a></li>-->
    <li class="<?php echo ($currentTab == 'complaints') ? ('active') : ('') ?>"><a href="#complaints" aria-controls="profile" role="tab" data-toggle="tab">Жалобы и анамнез</a></li>
    <li class="<?php echo ($currentTab == 'general-inspection') ? ('active') : ('') ?>"><a href="#general-inspection" aria-controls="profile" role="tab" data-toggle="tab">Основной осмотр</a></li>
    <li class="<?php echo ($currentTab == 'neurological-inspection') ? ('active') : ('') ?>"><a href="#neurological-inspection" aria-controls="messages" role="tab" data-toggle="tab">Неврологический статус</a></li>
    <li class="<?php echo ($currentTab == 'lungs-inspection') ? ('active') : ('') ?>"><a href="#lungs-inspection" aria-controls="messages" role="tab" data-toggle="tab">Лёгкие</a></li>
    <li class="<?php echo ($currentTab == 'other-inspection') ? ('active') : ('') ?>"><a href="#other-inspection" aria-controls="settings" role="tab" data-toggle="tab">Другие осмотры</a></li>
    <li class="<?php echo ($currentTab == 'cardiovascular-inspection') ? ('active') : ('') ?>"><a href="#cardiovascular-inspection" aria-controls="settings" role="tab" data-toggle="tab">Сердечно-сосудистая система</a></li>
    <li class="<?php echo ($currentTab == 'inspection-results') ? ('active') : ('') ?>"><a href="#inspection-results" aria-controls="settings" role="tab" data-toggle="tab">Результаты осмотра</a></li>
  </ul>
  <!-- Содержимое вкладок -->
  <div class="tab-content">
    <!--<div role="tabpanel" class="tab-pane fade <?php //echo ($currentTab == 'patient') ? ('in active') : ('') ?>" id="patient">
        <fieldset class="inspection-fieldset">
            <legend>
                <a id="patient-info">Данные пациента</a>
            </legend>
            <dl class="dl-horizontal inspection-patient-info">
              <dt class="inspection-label">ФИО пациента:</dt>
              <dd><?php// $patient->patient_name ?></dd>
              <dt class="inspection-label">Дата рождения:</dt>
              <dd><?php// $patient->birth_date ?> (полных лет - <?= $patient->getFullYears($patient->birth_date) ?>)</dd>
              <dt class="inspection-label">Адрес проживания:</dt>
              <dd><?php// $patient->address ?></dd>
              <dt class="inspection-label">Телефон:</dt>
              <dd><?php// $patient->phone ?></dd>
              <dt class="inspection-label">Дата регистрации:</dt>
              <dd><?php// $patient->create_date ?></dd>
            </dl>
        </fieldset>
    </div>-->
    <div role="tabpanel" class="tab-pane fade <?php echo ($currentTab == 'complaints') ? ('in active') : ('') ?>" id="complaints">

    	<?php
            /*$this->render('/inspection/_complaints_form', [
                'model' => $model,
            ])*/
        ?>

        <fieldset class="inspection-fieldset">
            <legend>
                <a id="patient-complaints">Жалобы</a>
                <?=
                    Html::a('Обновить', [
                        'update-inspection-field',
                        'id' => $model->id,
                        'patient_id' => $model->patient_id,
                        'fieldName' => 'complaints',
                    ], [
                        'class' => 'btn btn-primary modalButton'
                    ])
                ?>
            </legend>
            <p class="text-justify">
                <?= $model->complaints ?>
            </p>
        </fieldset>
        <fieldset class="inspection-fieldset">
            <legend>
                <a id="patient-anamnesis">Анамнез</a>
                <?=
                    Html::a('Обновить', [
                        'update-inspection-field',
                        'id' => $model->id,
                        'patient_id' => $model->patient_id,
                        'fieldName' => 'anamnesis',
                    ], [
                        'class' => 'btn btn-primary modalButton'
                    ])
                ?>
            </legend>
            <p class="text-justify">
                <?= $model->anamnesis ?>
            </p>
        </fieldset>
    </div>
    <div role="tabpanel" class="tab-pane fade <?php echo ($currentTab == 'general-inspection') ? ('in active') : ('') ?>" id="general-inspection">
        
        <?=
            $this->render('/general-inspection/info', [
                'model' => $model->generalInspection,
                'inspection' => $model,
            ])
        ?>

    </div>
    <div role="tabpanel" class="tab-pane fade <?php echo ($currentTab == 'neurological-inspection') ? ('in active') : ('') ?>" id="neurological-inspection">
        
        <?=
            $this->render('/neurological-inspection/info', [
                'model' => $model->neurologicalInspection,
                'inspection' => $model,
            ])
        ?>

    </div>
    <div role="tabpanel" class="tab-pane fade <?php echo ($currentTab == 'lungs-inspection') ? ('in active') : ('') ?>" id="lungs-inspection">
        
        <?=
            $this->render('/lungs-inspection/info', [
                'model' => $model->lungsInspection,
                'inspection' => $model,
            ])
        ?>

    </div>
    <div role="tabpanel" class="tab-pane fade <?php echo ($currentTab == 'other-inspection') ? ('in active') : ('') ?>" id="other-inspection">
        
        <?=
            $this->render('/other-inspection/info', [
                'model' => $model->otherInspection,
                'inspection' => $model,
            ])
        ?>

    </div>
    <div role="tabpanel" class="tab-pane fade <?php echo ($currentTab == 'cardiovascular-inspection') ? ('in active') : ('') ?>" id="cardiovascular-inspection">

        <?=
            $this->render('/cardiovascular-inspection/info', [
                'model' => $model->cardiovascularInspection,
                'inspection' => $model,
            ])
        ?>

    </div>
    <div role="tabpanel" class="tab-pane fade <?php echo ($currentTab == 'inspection-results') ? ('in active') : ('') ?>" id="inspection-results">
        <fieldset class="inspection-fieldset">
            <legend>
                <a id="inspection-results">Результаты осмотра</a>
                <?=
                    Html::a('Обновить', [
                        'update-inspection-result',
                        'id' => $model->id,
                        'patient_id' => $model->patient_id
                    ], [
                        'class' => 'btn btn-primary modalButton'
                    ])
                ?>
            </legend>
            <dl class="inspection-patient-info">

                <?php if ($model->diagnosis) { ?>
                    <dt class="inspection-label">Диагноз:</dt>
                    <dd><?= $model->diagnosis ?></dd>
                <?php } else { ?>
                    <?= Alert::widget([
                            'type' => Alert::TYPE_DANGER,
                            'titleOptions' => ['icon' => 'info-sign'],
                            'body' => 'Диагноз не поставлен',
                            'closeButton' => false
                        ])
                    ?>
                <?php } ?>

                <hr>

                <?php if ($model->surveys) { ?>
                    <dt class="inspection-label">Рекомендуемые обследования:</dt>
                    <dd><?= $model->inspectionToString($model->surveys) ?></dd>
                <?php } else { ?>
                    <?= Alert::widget([
                            'type' => Alert::TYPE_DANGER,
                            'titleOptions' => ['icon' => 'info-sign'],
                            'body' => 'Обследования не назначены',
                            'closeButton' => false
                        ])
                    ?>
                <?php } ?>

                <hr>

                <?php if ($model->treatment) { ?>
                    <dt class="inspection-label">Лечение:</dt>
                    <dd><?= $model->treatment ?></dd>
                <?php } else { ?>
                    <?= Alert::widget([
                            'type' => Alert::TYPE_DANGER,
                            'titleOptions' => ['icon' => 'info-sign'],
                            'body' => 'Лечение не назначено',
                            'closeButton' => false
                        ])
                    ?>
               <?php } ?>
              
            </dl>
        </fieldset>
    </div>
  </div>
</div>

<!--*******************************************-->

    <?php 

        Modal::begin([
            'header' => 'Осмотр',
            'id' => 'editModalId',
            'class' => 'modal',
            'size' => 'modal-lg',
            'clientOptions' => ['backdrop' => 'static', 'keyboard' => FALSE],
            'options' => [
                'tabindex' => false,
            ],
        ]);

    ?>

        <div id="time-progressbar" class="progress">
          <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 45%">
            <span class="sr-only">45% Complete</span>
          </div>
        </div>
        <div class='modalContent'></div>

    <?php

        Modal::end();

    ?>

</div>
<?php $this->endContent(); ?>
