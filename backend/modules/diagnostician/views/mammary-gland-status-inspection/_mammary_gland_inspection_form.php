<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\HelthStatus;
use common\models\FeedingStatus;
use common\models\PeripheralLymphNodesStatus;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model common\models\GeneralInspection */
/* @var $form yii\widgets\ActiveForm */
$this->title = 'Молочные железы';
?>
<?php $this->beginContent('@app/modules/diagnostician/views/layouts/inspection.php'); ?>
    
    <div class="row">
        <div class="col-md-12">
            <div class="card">

                <?=
                    $this->render('/patient/info', [
                        'patient' => $model->patient,
                    ])
                ?>

            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="general-inspection-form card">
                <fieldset>
        
                    <?php
                        $form = ActiveForm::begin([
                        ]);
                    ?>

                        <legend>

                            <a href="#"><?= $this->title ?></a>

                            <div class="form-group text-left" style="margin:10px 0 10px 0">
                            
                                <?=
                                    Html::submitButton($model->isNewRecord ? 'Создать' : 'Обновить', [
                                        'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary'
                                    ])
                                ?>

                            </div>

                        </legend>
                        <div class="row">
                            <div class="col-md-12">
                                <fieldset>
                                    <legend>Молочные железы</legend>

                                    <?= 
                                        $form->field($model, 'mammaryGlandStatuses')->widget(Select2::classname(), [
                                            'data'=>$model->dropMammaryGlandStatuses,
                                            'options' => [
                                                'multiple' => true,
                                                'placeholder' => 'молочные железы'
                                            ],
                                            'maintainOrder' => true,
                                            'pluginOptions' => [
                                                'allowClear' => true,
                                                'closeOnSelect' => false,
                                            ]
                                        ])->label(false)
                                    ?>

                                    <?= 
                                        $form->field($model, 'mammary_gland_inspection_description')->textarea([
                                            'rows' => 2,
                                            'placeholder' => 'описание',
                                        ])->label(false)
                                    ?>

                                </fieldset>
                            </div>
                        </div>

                    <?php ActiveForm::end(); ?>

                </fieldset>
            </div>
        </div>
    </div>

<?php $this->endContent(); ?>
