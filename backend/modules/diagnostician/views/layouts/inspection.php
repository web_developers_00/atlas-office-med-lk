<?php

use kartik\sidenav\SideNav;
use common\widgets\Alert;
use yii\helpers\Html;
?>

<div class="row">
    <div class="col-md-3 inspection-list">
        
        <?php
            echo SideNav::widget([
                    'type' => SideNav::TYPE_DEFAULT,
                    'heading' => 'Карточка осмотра ' . Html::a('печать', ['print', 'inspection_id' => Yii::$app->controller->inspectionId, 'patient_id' => Yii::$app->controller->patientId], ['class' => 'btn btn-success btn-xs']),
                    'containerOptions' => [
                        'id' => 'inspection-sidenav',
                        //'class' => 'navbar-fixed-side navbar-fixed-side-left'
                    ],
                    'items' => [

                        [
                            'url' => ['inspection/complaints', 
                                'inspection_id' => Yii::$app->controller->inspectionId,
                                'patient_id' => Yii::$app->controller->patientId
                            ],
                            'label' => 'Жалобы и анамнез',
                        ],
                        [
                            'url' => [
                                'inspection/helth-status-inspection', 
                                'inspection_id' => Yii::$app->controller->inspectionId,
                                'patient_id' => Yii::$app->controller->patientId
                            ],
                            'label' => 'Общее состояние',
                        ],
                        [
                            'url' => [
                                'inspection/feeding-status-inspection', 
                                'inspection_id' => Yii::$app->controller->inspectionId,
                                'patient_id' => Yii::$app->controller->patientId
                            ],
                            'label' => 'Питание',
                        ],
                        [
                            'url' => [
                                'inspection/body-mass-index-inspection', 
                                'inspection_id' => Yii::$app->controller->inspectionId,
                                'patient_id' => Yii::$app->controller->patientId
                            ],
                            'label' => 'Индекс массы тела',
                        ],
                        [
                            'url' => [
                                'inspection/skin-status-inspection', 
                                'inspection_id' => Yii::$app->controller->inspectionId,
                                'patient_id' => Yii::$app->controller->patientId
                            ],
                            'label' => 'Кожные покровы',
                        ],
                        [
                            'url' => [
                                'inspection/edema-inspection', 
                                'inspection_id' => Yii::$app->controller->inspectionId,
                                'patient_id' => Yii::$app->controller->patientId
                            ],
                            'label' => 'Отёки',
                        ],
                        [
                            'url' => [
                                'inspection/peripheral-lymph-nodes-status-inspection', 
                                'inspection_id' => Yii::$app->controller->inspectionId,
                                'patient_id' => Yii::$app->controller->patientId
                            ],
                            'label' => 'Периферические лимфоузлы',
                        ],
                        [
                            'url' => [
                                'inspection/thyroid-inspection', 
                                'inspection_id' => Yii::$app->controller->inspectionId,
                                'patient_id' => Yii::$app->controller->patientId
                            ],
                            'label' => 'Щитовидная железа',
                        ],
                        [
                            'url' => [
                                'inspection/mammary-gland-status-inspection', 
                                'inspection_id' => Yii::$app->controller->inspectionId,
                                'patient_id' => Yii::$app->controller->patientId
                            ],
                            'label' => 'Молочные железы',
                        ],
                        [
                            'url' => [
                                'inspection/osteoarticular-status-inspection', 
                                'inspection_id' => Yii::$app->controller->inspectionId,
                                'patient_id' => Yii::$app->controller->patientId
                            ],
                            'label' => 'Костно-суставная система',
                        ],
                        [
                            'url' => [
                                'inspection/neurological-inspection', 
                                'inspection_id' => Yii::$app->controller->inspectionId,
                                'patient_id' => Yii::$app->controller->patientId
                            ],
                            'label' => 'Неврологический статус',
                        ],
                        [
                            'url' => [
                                'inspection/lungs-inspection', 
                                'inspection_id' => Yii::$app->controller->inspectionId,
                                'patient_id' => Yii::$app->controller->patientId
                            ],
                            'label' => 'Лёгкие',
                        ],
                        [
                            'url' => [
                                'inspection/cardiovascular-inspection', 
                                'inspection_id' => Yii::$app->controller->inspectionId,
                                'patient_id' => Yii::$app->controller->patientId
                            ],
                            'label' => 'Сердечно-сосудистая система',
                        ],
                        [
                            'url' => [
                                'inspection/tongue-status-inspection', 
                                'inspection_id' => Yii::$app->controller->inspectionId,
                                'patient_id' => Yii::$app->controller->patientId
                            ],
                            'label' => 'Язык',
                        ],
                        [
                            'url' => [
                                'inspection/stomach-status-inspection', 
                                'inspection_id' => Yii::$app->controller->inspectionId,
                                'patient_id' => Yii::$app->controller->patientId
                            ],
                            'label' => 'Живот',
                        ],
                        [
                            'url' => [
                                'inspection/liver-status-inspection', 
                                'inspection_id' => Yii::$app->controller->inspectionId,
                                'patient_id' => Yii::$app->controller->patientId
                            ],
                            'label' => 'Печень',
                        ],
                        [
                            'url' => [
                                'inspection/gallbladder-status-inspection', 
                                'inspection_id' => Yii::$app->controller->inspectionId,
                                'patient_id' => Yii::$app->controller->patientId
                            ],
                            'label' => 'Желчный пузырь',
                        ],
                        [
                            'url' => [
                                'inspection/spleen-status-inspection', 
                                'inspection_id' => Yii::$app->controller->inspectionId,
                                'patient_id' => Yii::$app->controller->patientId
                            ],
                            'label' => 'Селезёнка',
                        ],
                        [
                            'url' => [
                                'inspection/kidney-status-inspection', 
                                'inspection_id' => Yii::$app->controller->inspectionId,
                                'patient_id' => Yii::$app->controller->patientId
                            ],
                            'label' => 'Почки',
                        ],
                        [
                            'url' => [
                                'inspection/dysuria-inspection', 
                                'inspection_id' => Yii::$app->controller->inspectionId,
                                'patient_id' => Yii::$app->controller->patientId
                            ],
                            'label' => 'Дизурии',
                        ],
                        [
                            'url' => [
                                'inspection/stool-status-inspection', 
                                'inspection_id' => Yii::$app->controller->inspectionId,
                                'patient_id' => Yii::$app->controller->patientId
                            ],
                            'label' => 'Стул',
                        ],
                        [
                            'url' => [
                                'inspection/result-inspection', 
                                'inspection_id' => Yii::$app->controller->inspectionId,
                                'patient_id' => Yii::$app->controller->patientId
                            ],
                            'label' => 'Результаты осмотра',
                        ],
                        /*[
                            'url' => ['inspection/general-inspection', 'inspection_id' => Yii::$app->controller->inspectionId, 'patient_id' => Yii::$app->controller->patientId],
                            'label' => 'Основной осмотр',
                        ],
                        */
                    ],
                ]);
        ?>

    </div>
    <div class="col-md-9">
        <?= Alert::widget() ?>
        <?= $content ?>

    </div>
</div>

