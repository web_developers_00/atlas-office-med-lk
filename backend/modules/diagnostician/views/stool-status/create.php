<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\StoolStatus */

$this->title = 'Создание состояния стула';
$this->params['breadcrumbs'][] = ['label' => 'Состояния стула', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="stool-status-create">
	<h1>
        <p class="alert alert-success text-center">

            <?= Html::encode($this->title) ?>
        
        </p>
    </h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
