<?php

namespace app\modules\diagnostician;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\diagnostician\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
