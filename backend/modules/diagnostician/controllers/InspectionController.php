<?php

namespace app\modules\diagnostician\controllers;

use Yii;

use common\models\Inspection;
use common\models\InspectionSearch;
use common\models\ComplaintsInspection;
use common\models\ResultInspection;
use common\models\HelthStatusInspection;
use common\models\FeedingStatusInspection;
use common\models\PeripheralLymphNodesStatusInspection;
use common\models\BodyMassIndexInspection;
use common\models\DysuriaInspection;
use common\models\SkinStatusInspection;
use common\models\EdemaInspection;
use common\models\ThyroidStatusInspection;
use common\models\MammaryGlandStatusInspection;
use common\models\OsteoarticularStatusInspection;
use common\models\StoolStatusInspection;
use common\models\TongueStatusInspection;
use common\models\GallbladderStatusInspection;
use common\models\KidneysStatusInspection;
use common\models\LiverStatusInspection;
use common\models\SpleenStatusInspection;
use common\models\StomachStatusInspection;
use common\models\Signature;



use common\models\GeneralInspection;
use common\models\NeurologicalInspection;
use common\models\LungsInspection;
use common\models\Patient;
use common\models\InspectionDocument;
use common\models\CardiovascularInspection;
use common\models\OtherInspection;

use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;

use PhpOffice\PhpWord\PhpWord;
use PhpOffice\PhpWord\Style\Font;
use PhpOffice\PhpWord\TemplateProcessor;
use PhpOffice\PhpWord\IOFactory;
use PhpOffice\PhpWord\Style\Alignment;
use Arslanim\TemplateMapper\DocumentMapper;

use app\modules\diagnostician\controllers\DefaultController;

/**
 * InspectionController implements the CRUD actions for Inspection model.
 */
class InspectionController extends DefaultController
{

    public $inspectionId;
    public $patientId;

    public $inspection;

    public function beforeAction($action) {

        $this->layout = 'main-inspection';

        /*$this->inspectionId = Yii::$app->getRequest()->getQueryParam('inspection_id');
        $this->patientId = Yii::$app->getRequest()->getQueryParam('patient_id');*/

        return parent::beforeAction($action);
    }

    public function actionPatient($patient_id) {
        $this->layout = 'main';

        $patient = $this->findPatient($patient_id);
        $searchModel = new InspectionSearch();
        $searchModel->patient_id = $patient_id;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'patient' => $patient,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionIndex()
    {
        return $this->redirect(['/diagnostician']);
    }
    /**
     * Lists all Inspection models.
     * @return mixed
     */
    /*public function actionIndex($patient_id)
    {
        $this->layout = 'main';

        $patient = $this->findPatient($patient_id);
        $searchModel = new InspectionSearch();
        $searchModel->patient_id = $patient_id;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'patient' => $patient,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }*/

    /**
     * Creates a new Inspection model.
     * If creation is successful, the browser will be redirected to the 'complaints' page.
     * @return mixed
     */
    public function actionCreate($patient_id)
    {
        $model = new Inspection();
        $patient = $this->findPatient($patient_id);
        $model->patient_id = $patient->id;
        if ($model->save()) {
            return $this->redirect(['complaints', 'inspection_id' => $model->id, 'patient_id' => $patient->id]);
        } else {
            Yii::$app->session->setFlash('error', 'Не удалось создать новый осмотр. Попробуйте еще раз или обратитесь к администратору.');
            return $this->redirect(['index', 'patient_id' => $patient->id]);
        }
    }

    public function actionComplaints($inspection_id, $patient_id) {

        $this->inspectionId = $inspection_id;
        $this->patientId = $patient_id;

        $inspection = $this->findModel($inspection_id, $patient_id);
        $model = $inspection->complaintsInspection;
        if ($model === null) {
            $model = new ComplaintsInspection();
            $model->inspection_id = $inspection_id;
            $model->patient_id = $patient_id;
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', 'Обновлено успешно');
            return $this->redirect(Yii::$app->request->referrer);
        } else {
            if ($model->hasErrors()) {
                Yii::$app->session->setFlash('error', $this->getModelErrorsToString($model->errors));
            }
            return $this->render('/complaints-inspection/_complaints_form', [
                'model' => $model,
            ]);    
        }
    }

    public function actionResultInspection($inspection_id, $patient_id) {

        $this->inspectionId = $inspection_id;
        $this->patientId = $patient_id;

        $inspection = $this->findModel($inspection_id, $patient_id);
        $model = $inspection->resultInspection;
        if ($model === null) {
            $model = new ResultInspection();
            $model->inspection_id = $inspection_id;
            $model->patient_id = $patient_id;
        } 

        $model->surveysIds = $model->getSurveyIds();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', 'Обновлено успешно');
            return $this->redirect(Yii::$app->request->referrer);
        } else {
            if ($model->hasErrors()) {
                Yii::$app->session->setFlash('error', $this->getModelErrorsToString($model->errors));
            }
            return $this->render('/result-inspection/_result_inspection_form', [
                'model' => $model,
            ]);    
        }

    }

    public function actionHelthStatusInspection($inspection_id, $patient_id) {

        $this->inspectionId = $inspection_id;
        $this->patientId = $patient_id;

        $inspection = $this->findModel($inspection_id, $patient_id);
        $model = $inspection->helthStatusInspection;

        if ($model === null) {
            $model = new HelthStatusInspection();
            $model->inspection_id = $inspection_id;
            $model->patient_id = $patient_id;
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', 'Обновлено успешно');
            return $this->redirect(Yii::$app->request->referrer);
        } else {
            if ($model->hasErrors()) {
                Yii::$app->session->setFlash('error', $this->getModelErrorsToString($model->errors));
            }
            return $this->render('/helth-status-inspection/_helth_status_inspection_form', [
                'model' => $model,
            ]);    
        }

    }

    public function actionFeedingStatusInspection($inspection_id, $patient_id) {

        $this->inspectionId = $inspection_id;
        $this->patientId = $patient_id;

        $inspection = $this->findModel($inspection_id, $patient_id);
        $model = $inspection->feedingStatusInspection;

        if ($model === null) {
            $model = new FeedingStatusInspection();
            $model->inspection_id = $inspection_id;
            $model->patient_id = $patient_id;
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', 'Обновлено успешно');
            return $this->redirect(Yii::$app->request->referrer);
        } else {
            if ($model->hasErrors()) {
                Yii::$app->session->setFlash('error', $this->getModelErrorsToString($model->errors));
            }
            return $this->render('/feeding-status-inspection/_feeding_status_inspection_form', [
                'model' => $model,
            ]);    
        }

    }

    public function actionPeripheralLymphNodesStatusInspection($inspection_id, $patient_id) {

        $this->inspectionId = $inspection_id;
        $this->patientId = $patient_id;

        $inspection = $this->findModel($inspection_id, $patient_id);
        $model = $inspection->peripheralLymphNodesStatusInspection;

        if ($model === null) {
            $model = new PeripheralLymphNodesStatusInspection();
            $model->inspection_id = $inspection_id;
            $model->patient_id = $patient_id;
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', 'Обновлено успешно');
            return $this->redirect(Yii::$app->request->referrer);
        } else {
            if ($model->hasErrors()) {
                Yii::$app->session->setFlash('error', $this->getModelErrorsToString($model->errors));
            }
            return $this->render('/peripheral-lymph-nodes-status-inspection/_peripheral_lymph_nodes_status_inspection_form', [
                'model' => $model,
            ]);    
        }

    }

    public function actionBodyMassIndexInspection($inspection_id, $patient_id) {

        $this->inspectionId = $inspection_id;
        $this->patientId = $patient_id;

        $inspection = $this->findModel($inspection_id, $patient_id);
        $model = $inspection->bodyMassIndexInspection;

        if ($model === null) {
            $model = new BodyMassIndexInspection();
            $model->inspection_id = $inspection_id;
            $model->patient_id = $patient_id;
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', 'Обновлено успешно');
            return $this->redirect(Yii::$app->request->referrer);
        } else {
            if ($model->hasErrors()) {
                Yii::$app->session->setFlash('error', $this->getModelErrorsToString($model->errors));
            }
            return $this->render('/body-mass-index-inspection/_body_mass_index_inspection_form', [
                'model' => $model,
            ]);    
        }

    }

    public function actionDysuriaInspection($inspection_id, $patient_id) {

        $this->inspectionId = $inspection_id;
        $this->patientId = $patient_id;

        $inspection = $this->findModel($inspection_id, $patient_id);
        $model = $inspection->dysuriaInspection;

        if ($model === null) {
            $model = new DysuriaInspection();
            $model->inspection_id = $inspection_id;
            $model->patient_id = $patient_id;
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', 'Обновлено успешно');
            return $this->redirect(Yii::$app->request->referrer);
        } else {
            if ($model->hasErrors()) {
                Yii::$app->session->setFlash('error', $this->getModelErrorsToString($model->errors));
            }
            return $this->render('/dysuria-inspection/_dysuria_inspection_form', [
                'model' => $model,
            ]);    
        }

    }

    public function actionSkinStatusInspection($inspection_id, $patient_id) {
        $this->inspectionId = $inspection_id;
        $this->patientId = $patient_id;

        $inspection = $this->findModel($inspection_id, $patient_id);
        $model = $inspection->skinStatusInspection;
        if ($model === null) {
            $model = new SkinStatusInspection();
            $model->inspection_id = $inspection_id;
            $model->patient_id = $patient_id;
        } 

        $model->skinStatusesIds = $model->getSkinStatusesIds();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', 'Обновлено успешно');
            return $this->redirect(Yii::$app->request->referrer);
        } else {
            if ($model->hasErrors()) {
                Yii::$app->session->setFlash('error', $this->getModelErrorsToString($model->errors));
            }
            return $this->render('/skin-status-inspection/_skin_status_inspection_form', [
                'model' => $model,
            ]);    
        }
    }

    public function actionEdemaInspection($inspection_id, $patient_id) {
        $this->inspectionId = $inspection_id;
        $this->patientId = $patient_id;

        $inspection = $this->findModel($inspection_id, $patient_id);
        $model = $inspection->edemaInspection;
        if ($model === null) {
            $model = new EdemaInspection();
            $model->inspection_id = $inspection_id;
            $model->patient_id = $patient_id;
        } 

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', 'Обновлено успешно');
            return $this->redirect(Yii::$app->request->referrer);
        } else {
            if ($model->hasErrors()) {
                Yii::$app->session->setFlash('error', $this->getModelErrorsToString($model->errors));
            }
            return $this->render('/edema-inspection/_edema_inspection_form', [
                'model' => $model,
            ]);    
        }
    }

    public function actionThyroidInspection($inspection_id, $patient_id) {
        $this->inspectionId = $inspection_id;
        $this->patientId = $patient_id;

        $inspection = $this->findModel($inspection_id, $patient_id);
        $model = $inspection->thyroidStatusInspection;
        if ($model === null) {
            $model = new ThyroidStatusInspection();
            $model->inspection_id = $inspection_id;
            $model->patient_id = $patient_id;
        } 

        $model->thyroidStatusesIds = $model->getThyroidStatusesIds();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', 'Обновлено успешно');
            return $this->redirect(Yii::$app->request->referrer);
        } else {
            if ($model->hasErrors()) {
                Yii::$app->session->setFlash('error', $this->getModelErrorsToString($model->errors));
            }
            return $this->render('/thyroid-status-inspection/_thyroid_status_inspection_form', [
                'model' => $model,
            ]);    
        }
    }

    public function actionMammaryGlandStatusInspection($inspection_id, $patient_id) {
        $this->inspectionId = $inspection_id;
        $this->patientId = $patient_id;

        $inspection = $this->findModel($inspection_id, $patient_id);
        $model = $inspection->mammaryGlandStatusInspection;
        if ($model === null) {
            $model = new MammaryGlandStatusInspection();
            $model->inspection_id = $inspection_id;
            $model->patient_id = $patient_id;
        } 

        $model->mammaryGlandStatusesIds = $model->getMammaryGlandStatusesIds();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', 'Обновлено успешно');
            return $this->redirect(Yii::$app->request->referrer);
        } else {
            if ($model->hasErrors()) {
                Yii::$app->session->setFlash('error', $this->getModelErrorsToString($model->errors));
            }
            return $this->render('/mammary-gland-status-inspection/_mammary_gland_inspection_form', [
                'model' => $model,
            ]);    
        }
    }

    public function actionOsteoarticularStatusInspection($inspection_id, $patient_id) {
        $this->inspectionId = $inspection_id;
        $this->patientId = $patient_id;

        $inspection = $this->findModel($inspection_id, $patient_id);
        $model = $inspection->osteoarticularStatusInspection;
        if ($model === null) {
            $model = new OsteoarticularStatusInspection();
            $model->inspection_id = $inspection_id;
            $model->patient_id = $patient_id;
        } 

        $model->osteoarticularSystemStatusesIds = $model->getOsteoarticularSystemStatusesIds();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', 'Обновлено успешно');
            return $this->redirect(Yii::$app->request->referrer);
        } else {
            if ($model->hasErrors()) {
                Yii::$app->session->setFlash('error', $this->getModelErrorsToString($model->errors));
            }
            return $this->render('/osteoarticular-status-inspection/_osteoarticular_inspection_form', [
                'model' => $model,
            ]);    
        }
    }

    public function actionStoolStatusInspection($inspection_id, $patient_id) {
        $this->inspectionId = $inspection_id;
        $this->patientId = $patient_id;

        $inspection = $this->findModel($inspection_id, $patient_id);
        $model = $inspection->stoolStatusInspection;
        if ($model === null) {
            $model = new StoolStatusInspection();
            $model->inspection_id = $inspection_id;
            $model->patient_id = $patient_id;
        } 

        $model->stoolStatusesIds = $model->getStoolStatusesIds();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', 'Обновлено успешно');
            return $this->redirect(Yii::$app->request->referrer);
        } else {
            if ($model->hasErrors()) {
                Yii::$app->session->setFlash('error', $this->getModelErrorsToString($model->errors));
            }
            return $this->render('/stool-status-inspection/_stool_inspection_form', [
                'model' => $model,
            ]);    
        }
    }

    public function actionTongueStatusInspection($inspection_id, $patient_id) {
        $this->inspectionId = $inspection_id;
        $this->patientId = $patient_id;

        $inspection = $this->findModel($inspection_id, $patient_id);
        $model = $inspection->tongueStatusInspection;
        if ($model === null) {
            $model = new TongueStatusInspection();
            $model->inspection_id = $inspection_id;
            $model->patient_id = $patient_id;
        } 

        $model->tongueStatusesIds = $model->getTongueStatusesIds();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', 'Обновлено успешно');
            return $this->redirect(Yii::$app->request->referrer);
        } else {
            if ($model->hasErrors()) {
                Yii::$app->session->setFlash('error', $this->getModelErrorsToString($model->errors));
            }
            return $this->render('/tongue-status-inspection/_tongue_inspection_form', [
                'model' => $model,
            ]);    
        }
    }

    public function actionNeurologicalInspection($inspection_id, $patient_id) {

        $this->inspectionId = $inspection_id;
        $this->patientId = $patient_id;

        $inspection = $this->findModel($inspection_id, $patient_id);
        $model = $inspection->neurologicalInspection;
        if ($model === null) {
            $model = new NeurologicalInspection();
            $model->inspection_id = $inspection_id;
            $model->patient_id = $patient_id;
        }

        $model->fingerNoseStatusesIds = $model->getFingerNoseStatusesIds();
        $model->extremitiesMovementStatusesIds = $model->getExtremitiesMovementStatusesIds();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', 'Обновлено успешно');
            return $this->redirect(Yii::$app->request->referrer);
        } else {
            if ($model->hasErrors()) {
                Yii::$app->session->setFlash('error', $this->getModelErrorsToString($model->errors));
            }
            return $this->render('/neurological-inspection/_neurological_inspection_form', [
                'model' => $model,
            ]);    
        }
    }

    public function actionLungsInspection($inspection_id, $patient_id) {

        $this->inspectionId = $inspection_id;
        $this->patientId = $patient_id;

        $inspection = $this->findModel($inspection_id, $patient_id);
        $model = $inspection->lungsInspection;
        if ($model === null) {
            $model = new LungsInspection();
            $model->inspection_id = $inspection_id;
            $model->patient_id = $patient_id;
        }

        $model->lungsAuscultationStatusesIds = $model->getLungsAuscultationStatusesIds();
        $model->crepitationStatusesIds = $model->getCrepitationStatusesIds();
        $model->lungsPercussionStatusesIds = $model->getLungsPercussionStatusesIds();
        $model->ribCageStatusesIds = $model->getRibCageStatusesIds();
        $model->voiceTremblingStatusesIds = $model->getVoiceTremblingStatusesIds();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', 'Обновлено успешно');
            return $this->redirect(Yii::$app->request->referrer);
        } else {
            if ($model->hasErrors()) {
                Yii::$app->session->setFlash('error', $this->getModelErrorsToString($model->errors));
            }
            return $this->render('/lungs-inspection/_lungs_inspection_form', [
                'model' => $model,
            ]);    
        }

    }

    public function actionCardiovascularInspection($inspection_id, $patient_id) {

        $this->inspectionId = $inspection_id;
        $this->patientId = $patient_id;

        $inspection = $this->findModel($inspection_id, $patient_id);
        $model = $inspection->cardiovascularInspection;
        if ($model === null) {
            $model = new CardiovascularInspection();
            $model->inspection_id = $inspection_id;
            $model->patient_id = $patient_id;  
        }

        $model->heartTonesStatusesIds = $model->getHeartTonesStatusesIds();
        $model->heartTonesAccentStatusesIds = $model->getHeartTonesAccentStatusesIds();
        $model->heartNoisesStatusesIds = $model->getHeartNoisesStatusesIds();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', 'Обновлено успешно');
            return $this->redirect(Yii::$app->request->referrer);
        } else {
            if ($model->hasErrors()) {
                Yii::$app->session->setFlash('error', $this->getModelErrorsToString($model->errors));
            }
            return $this->render('/cardiovascular-inspection/_cardiovascular_inspection_form', [
                'model' => $model,
            ]);    
        }

    }

    public function actionGallbladderStatusInspection($inspection_id, $patient_id) {

        $this->inspectionId = $inspection_id;
        $this->patientId = $patient_id;

        $inspection = $this->findModel($inspection_id, $patient_id);
        $model = $inspection->gallbladderStatusInspection;
        if ($model == null){ 
            $model = new GallbladderStatusInspection();
            $model->inspection_id = $inspection_id;
            $model->patient_id = $patient_id;
        }
            $model->gallbladderStatusesIds = $model->getGallbladderStatusesIds();      

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', 'Обновлено успешно');
            return $this->redirect(Yii::$app->request->referrer);
        } else {
            if ($model->hasErrors()) {
                Yii::$app->session->setFlash('error', $this->getModelErrorsToString($model->errors));
            }
            return $this->render('/gallbladder-status-inspection/_gallbladder_status_form', [
                'model' => $model,
            ]);    
        }

    }

    public function actionKidneyStatusInspection($inspection_id, $patient_id) {

        $this->inspectionId = $inspection_id;
        $this->patientId = $patient_id;

        $inspection = $this->findModel($inspection_id, $patient_id);
        $model = $inspection->kidneysStatusInspection;
        if ($model == null){ 
            $model = new KidneysStatusInspection();
            $model->inspection_id = $inspection_id;
            $model->patient_id = $patient_id;
        }
        
        $model->kidneyStatusesIds = $model->getKidneyStatusesIds();      

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', 'Обновлено успешно');
            return $this->redirect(Yii::$app->request->referrer);
        } else {
            if ($model->hasErrors()) {
                Yii::$app->session->setFlash('error', $this->getModelErrorsToString($model->errors));
            }
            return $this->render('/kidney-status-inspection/_kidney_status_form', [
                'model' => $model,
            ]);    
        }

    }

    public function actionLiverStatusInspection($inspection_id, $patient_id) {

        $this->inspectionId = $inspection_id;
        $this->patientId = $patient_id;

        $inspection = $this->findModel($inspection_id, $patient_id);
        $model = $inspection->liverStatusInspection;
        if ($model == null){ 
            $model = new LiverStatusInspection();
            $model->inspection_id = $inspection_id;
            $model->patient_id = $patient_id;
        }
        
        $model->liverStatusesIds = $model->getLiverStatusesIds();      

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', 'Обновлено успешно');
            return $this->redirect(Yii::$app->request->referrer);
        } else {
            if ($model->hasErrors()) {
                Yii::$app->session->setFlash('error', $this->getModelErrorsToString($model->errors));
            }
            return $this->render('/liver-status-inspection/_liver_status_inspection_form', [
                'model' => $model,
            ]);    
        }

    }

    public function actionSpleenStatusInspection($inspection_id, $patient_id) {

        $this->inspectionId = $inspection_id;
        $this->patientId = $patient_id;

        $inspection = $this->findModel($inspection_id, $patient_id);
        $model = $inspection->spleenStatusInspection;
        if ($model == null){ 
            $model = new SpleenStatusInspection();
            $model->inspection_id = $inspection_id;
            $model->patient_id = $patient_id;
        }
        
        $model->spleenStatusesIds = $model->getSpleenStatusesIds();      

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', 'Обновлено успешно');
            return $this->redirect(Yii::$app->request->referrer);
        } else {
            if ($model->hasErrors()) {
                Yii::$app->session->setFlash('error', $this->getModelErrorsToString($model->errors));
            }
            return $this->render('/spleen-status-inspection/_spleen_status_inspection_form', [
                'model' => $model,
            ]);    
        }

    }

    public function actionStomachStatusInspection($inspection_id, $patient_id) {

        $this->inspectionId = $inspection_id;
        $this->patientId = $patient_id;

        $inspection = $this->findModel($inspection_id, $patient_id);
        $model = $inspection->stomachStatusInspection;
        if ($model == null){ 
            $model = new StomachStatusInspection();
            $model->inspection_id = $inspection_id;
            $model->patient_id = $patient_id;
        }
        
        $model->stomachStatusesIds = $model->getStomachStatusesIds();      

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', 'Обновлено успешно');
            return $this->redirect(Yii::$app->request->referrer);
        } else {
            if ($model->hasErrors()) {
                Yii::$app->session->setFlash('error', $this->getModelErrorsToString($model->errors));
            }
            return $this->render('/stomach-status-inspection/_stomach_status_inspection_form', [
                'model' => $model,
            ]);    
        }

    }

    protected function getModelErrorsToString($errors) {
        $errorString = "";
        foreach ($errors as $error) {
            for ($i = 0, $maxi = count($error); $i < $maxi; $i++) {
                $errorString .= $error[$i]."\r\n";
            }
        }
        return $errorString;
    }

    /**
     * Finds the Patient model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $patient_id
     * @return Patient the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findPatient($patient_id) {
        if ( ($patient = Patient::findOne($patient_id)) !== null ) {
            return $patient;
        } else {
            throw new NotFoundHttpException('Карта пациента не найдена.');   
        }
    }

    /*---------------------------------------------------------------------------------*/

    /**
     * Updates an existing Inspection model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @param integer $patient_id
     * @return mixed
     */
    public function actionUpdate($id, $patient_id)
    {
        $model = $this->findModel($id, $patient_id);
        $model->surveysIds = $model->getSurveyIds();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', 'Обновлено успешно');
            return $this->redirect(Yii::$app->request->referrer);
            //return $this->redirect(['view', 'id' => $model->id, 'patient_id' => $model->patient_id, 'currentTab' => 'complaints']);
        } else {
            if ($model->hasErrors()) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return $this->getModelErrorsToString($model->errors);
            } else {
                return $this->renderAjax('_inspection_form', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * Deletes an existing Inspection model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @param integer $patient_id
     * @return mixed
     */
    public function actionDelete($id, $patient_id)
    {
        $this->findModel($id, $patient_id)->delete();

        return $this->redirect(['patient', 'patient_id' => $patient_id]);
    }

    public function actionPrint($inspection_id, $patient_id) {
        $patient = Patient::findOne($patient_id);
        $inspection = Inspection::find()
            ->where('id = :id AND patient_id = :patient_id', [':id' => $inspection_id, ':patient_id' => $patient_id])
            ->one();

        $phpWord = new PhpWord();
        //стили
        $phpWord->addFontStyle('titleStyle', array('bold' => true, 'italic' => false, 'size' => 10, 'name' => 'Verdana'));
        $phpWord->addParagraphStyle('centeredParagraphStyle', array('align'=>'center', 'spaceAfter' => 200));
        $phpWord->addParagraphStyle('textParagraphStyle', array('align'=>'both', 'spaceAfter'=>100, 'lineHeight'=>1.0));
        $phpWord->addFontStyle('textStyle', array('size' => 9, 'name' => 'Verdana'));
        $phpWord->addFontStyle('inspectionPositionTextStyle', array('size' => 9, 'name' => 'Verdana', 'bold' => true));

        $phpWord->addParagraphStyle('multipleTab', [ 'tabs' => [
                                                                    new \PhpOffice\PhpWord\Style\Tab('left', 1550),
                                                                    new \PhpOffice\PhpWord\Style\Tab('center', 3200),
                                                                    new \PhpOffice\PhpWord\Style\Tab('right', 5300),
                                                                ]
        ]);
        $phpWord->addParagraphStyle('rightTab', [
                                                    'tabs' => [
                                                                new \PhpOffice\PhpWord\Style\Tab('right', 9090)
                                                            ],
                                                    'spaceBefore'=>300
        ]);
        $phpWord->addParagraphStyle('centerTab', ['tabs' => [
                                                                new \PhpOffice\PhpWord\Style\Tab('center', 4680)
                                                            ]
        ]);

        $inspectionDocument = new DocumentMapper(null, $phpWord, [
            ($inspection) ? ($inspection) : (new Inspection),
            ($patient) ? ($patient) : (new Patient),
            ($inspection->complaintsInspection) ? ($inspection->complaintsInspection) : (new ComplaintsInspection),
            ($inspection->helthStatusInspection) ? ($inspection->helthStatusInspection) : (new HelthStatusInspection),
            ($inspection->feedingStatusInspection) ? ($inspection->feedingStatusInspection) : (new FeedingStatusInspection),
            ($inspection->bodyMassIndexInspection) ? ($inspection->bodyMassIndexInspection) : (new BodyMassIndexInspection),
            ($inspection->skinStatusInspection) ? ($inspection->skinStatusInspection) : (new SkinStatusInspection),
            ($inspection->edemaInspection) ? ($inspection->edemaInspection) : (new EdemaInspection),
            ($inspection->peripheralLymphNodesStatusInspection) ? ($inspection->peripheralLymphNodesStatusInspection) : (new PeripheralLymphNodesStatusInspection),
            ($inspection->thyroidStatusInspection) ? ($inspection->thyroidStatusInspection) : (new ThyroidStatusInspection),
            ($inspection->mammaryGlandStatusInspection) ? ($inspection->mammaryGlandStatusInspection) : (new MammaryGlandStatusInspection),
            ($inspection->osteoarticularStatusInspection) ? ($inspection->osteoarticularStatusInspection) : (new OsteoarticularStatusInspection),
            ($inspection->neurologicalInspection) ? ($inspection->neurologicalInspection) : (new NeurologicalInspection),
            ($inspection->lungsInspection) ? ($inspection->lungsInspection) : (new LungsInspection),
            ($inspection->cardiovascularInspection) ? ($inspection->cardiovascularInspection) : (new CardiovascularInspection),
            ($inspection->tongueStatusInspection) ? ($inspection->tongueStatusInspection) : (new TongueStatusInspection),
            ($inspection->stomachStatusInspection) ? ($inspection->stomachStatusInspection) : (new StomachStatusInspection),
            ($inspection->liverStatusInspection) ? ($inspection->liverStatusInspection) : (new LiverStatusInspection),
            ($inspection->gallbladderStatusInspection) ? ($inspection->gallbladderStatusInspection) : (new GallbladderStatusInspection),
            ($inspection->spleenStatusInspection) ? ($inspection->spleenStatusInspection) : (new SpleenStatusInspection),
            ($inspection->kidneysStatusInspection) ? ($inspection->kidneysStatusInspection) : (new KidneysStatusInspection),
            ($inspection->dysuriaInspection) ? ($inspection->dysuriaInspection) : (new DysuriaInspection),
            ($inspection->stoolStatusInspection) ? ($inspection->stoolStatusInspection) : (new StoolStatusInspection),
            ($inspection->resultInspection) ? ($inspection->resultInspection) : (new ResultInspection),
            new Signature("З.А. Имамутдинова", $inspection->inspection_date),
        ]);

        $inspectionDocument->createSections();

        $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
        $fileName = $patient->patient_name . " (" . $inspection->inspection_date . ").docx";
        $objWriter->save(Yii::getAlias('@uploads').'/inspections/' . $fileName);

        $this->downloadInspectionFile(Yii::getAlias('@uploads').'/inspections/' . $fileName);
        
        //load template    
        /*$templateProcessor = new TemplateProcessor(Yii::getAlias('@uploads').'/patterns/inspection.docx');

        $inspectionDocumentMapper = new DocumentMapper($templateProcessor, null, [
            ($patient) ? ($patient) : (new Patient),
            ($inspection) ? ($inspection) : (new Inspection),
            ($inspection->complaintsInspection) ? ($inspection->complaintsInspection) : (new ComplaintsInspection),
            ($inspection->helthStatusInspection) ? ($inspection->helthStatusInspection) : (new HelthStatusInspection),
            ($inspection->feedingStatusInspection) ? ($inspection->feedingStatusInspection) : (new FeedingStatusInspection),
            ($inspection->bodyMassIndexInspection) ? ($inspection->bodyMassIndexInspection) : (new BodyMassIndexInspection),
            ($inspection->skinStatusInspection) ? ($inspection->skinStatusInspection) : (new SkinStatusInspection),
            ($inspection->edemaInspection) ? ($inspection->edemaInspection) : (new EdemaInspection),
            ($inspection->peripheralLymphNodesStatusInspection) ? ($inspection->peripheralLymphNodesStatusInspection) : (new PeripheralLymphNodesStatusInspection),
            ($inspection->thyroidStatusInspection) ? ($inspection->thyroidStatusInspection) : (new ThyroidStatusInspection),
            ($inspection->mammaryGlandStatusInspection) ? ($inspection->mammaryGlandStatusInspection) : (new MammaryGlandStatusInspection),
            ($inspection->neurologicalInspection) ? ($inspection->neurologicalInspection) : (new NeurologicalInspection),
            ($inspection->osteoarticularStatusInspection) ? ($inspection->osteoarticularStatusInspection) : (new OsteoarticularStatusInspection),
            ($inspection->lungsInspection) ? ($inspection->lungsInspection) : (new LungsInspection),
            ($inspection->cardiovascularInspection) ? ($inspection->cardiovascularInspection) : (new CardiovascularInspection),
            ($inspection->tongueStatusInspection) ? ($inspection->tongueStatusInspection) : (new TongueStatusInspection),
            ($inspection->stomachStatusInspection) ? ($inspection->stomachStatusInspection) : (new StomachStatusInspection),
            ($inspection->liverStatusInspection) ? ($inspection->liverStatusInspection) : (new LiverStatusInspection),
            ($inspection->gallbladderStatusInspection) ? ($inspection->gallbladderStatusInspection) : (new GallbladderStatusInspection),
            ($inspection->spleenStatusInspection) ? ($inspection->spleenStatusInspection) : (new SpleenStatusInspection),
            ($inspection->kidneysStatusInspection) ? ($inspection->kidneysStatusInspection) : (new KidneysStatusInspection),
            ($inspection->dysuriaInspection) ? ($inspection->dysuriaInspection) : (new DysuriaInspection),
            ($inspection->stoolStatusInspection) ? ($inspection->stoolStatusInspection) : (new StoolStatusInspection),
            ($inspection->resultInspection) ? ($inspection->resultInspection) : (new ResultInspection),
        ]);

        $inspectionDocumentMapper->mapObjects();

        $templateProcessor->saveAs(Yii::getAlias('@uploads').'/inspections/' . Yii::$app->params['systemValues']['inspectionResult'] . '-' . $patient->patient_name . '-' . $inspection->inspection_date . '.docx');

        $this->downloadInspectionFile(Yii::getAlias('@uploads').'/inspections/' . Yii::$app->params['systemValues']['inspectionResult'] . '-' . $patient->patient_name . '-' . $inspection->inspection_date . '.docx');*/
    }

    /**
     * Finds the Inspection model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @param integer $patient_id
     * @return Inspection the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id, $patient_id)
    {
        $model = Inspection::find()
            ->with()
            ->where('id = :id AND patient_id = :patient_id', [':id' => $id, ':patient_id' => $patient_id])
            ->one();
        if ($model !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function downloadInspectionFile($filePath) {
        if (!empty($filePath)) {
            header("Content-type:application/doc");
            header('Content-Disposition: attachment; filename="'.basename($filePath).'"'); 
            header('Content-Length: ' . filesize($filePath));
            readfile($filePath);
            Yii::app()->end();
        }
    }

    public function actionFoo() {
        echo Yii::$app->encrypter->encrypt('string to encrypt');
        echo '<br>';
        echo Yii::$app->encrypter->decrypt('zyi/dVYOckMSGKbPRy2Rximr/15Pclu9oZScrOXdcGs=');
    }
}
