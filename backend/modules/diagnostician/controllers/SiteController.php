<?php

namespace app\modules\diagnostician\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use common\models\LoginForm;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use app\modules\administrator\controllers\DefaultController;

/**
 * Site controller
 */
class SiteController extends DefaultController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionIndex()
    {
    	foreach (Yii::$app->authManager->getRolesByUser(Yii::$app->user->ID) as $user_role) {
            switch ($user_role->name) {
                case 'administrator': {
                    $this->redirect(Url::home().'administrator');
                }break;
                /*default: {
                    return $this->goBack();
                }break;*/
                }
            }
        return $this->render('index');
    }

    public function actionLogin()
    {
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
        	foreach (Yii::$app->authManager->getRolesByUser(Yii::$app->user->ID) as $user_role) {
                switch ($user_role->name) {
                    case 'administrator': {
                        $this->redirect(Url::home().'administrator');
                    }break;
                    default: {
                        return $this->goBack();
                    }break;
                }
            }
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }
}
