<?php

namespace app\modules\diagnostician\controllers;

use Yii;
use common\models\Patient;
use common\models\PatientSearch;
use common\models\Contract;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

use PhpOffice\PhpWord\PhpWord;
use PhpOffice\PhpWord\TemplateProcessor;
use PhpOffice\PhpWord\IOFactory;
use PhpOffice\PhpWord\Style\Alignment;
use Arslanim\TemplateMapper\DocumentMapper;
use app\modules\diagnostician\controllers\DefaultController;

/**
 * PatientController implements the CRUD actions for Patient model.
 */
class PatientController extends DefaultController
{
    /**
     * Lists all Patient models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PatientSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Patient model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Patient model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Patient();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            
            return $this->redirect(['inspection/', 'patient_id' => $model->id]);
            //return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    public function actionCreateContract($patient_id) {
        $model = $this->findModel($patient_id);
        $contract = new Contract();
        $contract->patient_id = $patient_id;
        /*$maxContractNumber = Contract::find()
            ->select('contract_number')
            ->max('contract_number');
        if ($maxContractNumber) {
            $contract->contract_number = $maxContractNumber + 1;
        } else {
            $contract->contract_number = 1;
        }*/
        $contract->contract_date = date("Y-m-d");

        //if ($contract->save()) {

            //load template    
            $templateProcessor = new TemplateProcessor(Yii::getAlias('@uploads').'/patterns/contract.docx');

            $patientContractMapper = new DocumentMapper($templateProcessor, null, [
                ($model) ? ($model) : (new Patient),
                ($contract) ? ($contract) : (new Contract),
            ]);
            $patientContractMapper->mapObjects();

            $templateProcessor->saveAs(Yii::getAlias('@uploads').'/contracts/contract-' . $model->patient_name . '-' . $contract->contract_date . '.docx');

            $this->downloadContractFile(Yii::getAlias('@uploads').'/contracts/contract-' . $model->patient_name . '-' . $contract->contract_date . '.docx');

        //}
    }

    /**
     * Updates an existing Patient model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Patient model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Patient model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Patient the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Patient::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function downloadContractFile($filePath) {
        if (!empty($filePath)) {
            header("Content-type:application/doc");
            header('Content-Disposition: attachment; filename="'.basename($filePath).'"'); 
            header('Content-Length: ' . filesize($filePath));
            readfile($filePath);
            Yii::app()->end();
        }
    }
}
