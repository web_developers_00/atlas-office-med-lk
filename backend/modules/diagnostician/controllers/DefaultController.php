<?php

namespace app\modules\diagnostician\controllers;

use Yii;

use yii\web\Controller;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;

class DefaultController extends Controller {

	public function behaviors() {
		return [
			'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                    	'allow' => true,
                    	'roles' => ['diagnostician'],
                    ],
                ],
            ],
        ];
	}

    public function actionIndex() {
        return $this->render('index');
    }
}
