<?php

namespace app\modules\api\controllers;

use yii\rest\ActiveController;
use yii\filters\ContentNegotiator;
use yii\web\Response;
use yii\filters\AccessControl;
use common\models\User;

class UserController extends ActiveController {

	public function behaviors() {
		$behaviors = parent::behaviors();

		$behaviors['access'] = [
		    'class' => AccessControl::className(),
		    'rules' => [
		        [
		            'allow' => true,
		            'roles' => ['administrator'],
		        ],
		    ],
		];
		$behaviors['contentNegotiator'] = [
			'class' =>  ContentNegotiator::className(),
			'formats' => [
				'application/json' => Response::FORMAT_JSON,
			],
		];

		return $behaviors;
	}

	public $modelClass = 'common\models\GeneralInspection';

	public function actionFuck($id) {
		$user = User::find()
			->where('id = :id', [':id' => $id])
			->one();
		return $user;
	}

}