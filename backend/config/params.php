<?php
return [
    'adminEmail' => 'admin@example.com',
    'systemValues' => [
    	'defaultValue' => 'не определено',
    	'hyphenValue' => '-',
    	'inspectionResult' => 'inspection-result',
    ],
];
