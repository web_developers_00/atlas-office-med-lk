<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-backend',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'backend\controllers',
    'bootstrap' => ['log'],
    'modules' => [
        'administrator' => [
            'class' => 'app\modules\administrator\Module',
            'layout' => 'main',
        ],
        'diagnostician' => [
            'class' => 'app\modules\diagnostician\Module',
            'layout' => 'main',
        ],
        'api' => [
        	'class' => 'app\modules\api\Module',
        ],
    ],
    'components' => [
    	'request' => [
		    'class' => '\yii\web\Request',
		    'parsers' => [
		        'application/json' => 'yii\web\JsonParser',
		    ],
		],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'urlManager' => [
            'class' => 'yii\web\UrlManager',
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
            	[
            		'class' => 'yii\rest\UrlRule',
            		'controller' => ['api/user'],
            		'pluralize' => false,
            		'extraPatterns' => [
				        'GET fuck/{id}' => 'fuck',
				    ],
            	],
            ],
            /*'rules' => array(
                '<module:\w+>/<controller:\w+>/<action:\w+>' => '<module>/<controller>/<action>', 
                '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
            ),*/
        ],
    ],
    'params' => $params,
];
