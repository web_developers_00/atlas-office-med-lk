<?php
namespace backend\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use common\models\LoginForm;
use yii\filters\VerbFilter;
use yii\helpers\Url;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionIndex() {
        $this->redirect(['login']);
        /*if (Yii::$app->user->isGuest) {
            return $this->render('index');
        } else {
            $userRoles = Yii::$app->authManager->getRolesByUser(Yii::$app->user->ID);
            if (count($userRoles) === 0) {
                Yii::$app->session->setFlash('warning', 'Не удалось определить роль пользователя!');
                return $this->render('index');
            } else {
                if (count($userRoles) === 1) {
                    switch ($user_role->name) {
                        case 'administrator': {
                            $this->redirect(Url::home().'administrator');
                        }break;
                        default: {
                            return $this->goBack();
                        }break;
                    }
                } else {
                    return $this->render('work-panel-selection', [
                        'userRoles' => $userRoles,
                    ]);*/
                    //print_r($userRoles);
                    /*foreach (Yii::$app->authManager->getRolesByUser(Yii::$app->user->ID) as $user_role) {
                        switch ($user_role->name) {
                            case 'administrator': {
                                $this->redirect(Url::home().'administrator');
                            }break;
                            default: {
                                return $this->goBack();
                            }break;
                        }
                    }*/
                /*}
            }*/
        //}

    	
        //return $this->render('index');
        //print_r(Url::home());
    }

    public function actionLogin() {
        
        $this->layout = 'control-panel';

        /*if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }*/
        if (Yii::$app->user->isGuest) {
            $model = new LoginForm();
            if ($model->load(Yii::$app->request->post()) && $model->login()) {
                $this->redirect(['login']);
                /*foreach (Yii::$app->authManager->getRolesByUser(Yii::$app->user->ID) as $user_role) {
                    switch ($user_role->name) {
                        case 'administrator': {
                            $this->redirect(Url::home().'administrator');
                        }break;
                        default: {
                            return $this->goBack();
                        }break;
                    }
                }*/
            } else {
                return $this->render('login', [
                    'model' => $model,
                ]);
            }
        } else {
            $userRoles = Yii::$app->authManager->getRolesByUser(Yii::$app->user->ID);
            if ((count($userRoles) === 0) || (count($userRoles) > 1)) {

                if (count($userRoles) === 0) Yii::$app->session->setFlash('warning', 'Нет доступа ни к одному из кабинетов! Обратитесь к администратору системы');
                return $this->render('work-panel-selection', [
                    'userRoles' => $userRoles,
                ]);
            } else {
                if (count($userRoles) === 1) {
                    switch ($user_role->name) {
                        case 'administrator': {
                            $this->redirect(Url::home().'administrator');
                        }break;
                        default: {
                            return $this->goBack();
                        }break;
                    }
                }
            }
        }
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }
}
