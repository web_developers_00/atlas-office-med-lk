<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AdminAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/site.css',
        '../css/jquery.kladr.min.css',
    ];
    public $js = [
        'js/angular.min.js',
        'js/angular-initial-value.js',
        'js/order.module.js',
        '../js/anotherOrganization.js',
        'js/modal.window.module.js',
        '../js/jquery.kladr.min.js',
        '../js/addresses.module.js',
        //'js/smooth.scroll.js',
        'js/float-panel.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
