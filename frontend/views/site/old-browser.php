<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

$this->title = 'Используется старая версия браузера';
?>

<div class="row text-center">
    <div class="col-md-12">
        <div class="alert alert-warning block-shadow" role="alert">
            <p>
                Вы используете старую версию браузера!
            </p>
        </div>
    </div>
</div>
