<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\EarlierAppeal;
use common\models\ReceptionTime;
use common\models\Classifier;
use dosamigos\datepicker\DatePicker;

$this->title = \Yii::$app->params['siteTtitle'];
?>
<div class="row" ng-controller="ErrorController">
    <div class="col-md-12">
        <div ng-repeat="(key,val) in alerts" class="alert {{key}}">
            <div ng-repeat="msg in val">{{msg}}</div>
        </div>
    </div>
</div>
<div class="index-page" ng-controller="DayController">
</div>
