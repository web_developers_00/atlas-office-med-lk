-- phpMyAdmin SQL Dump
-- version 4.0.10.10
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1:3306
-- Время создания: Дек 18 2016 г., 12:54
-- Версия сервера: 5.5.45
-- Версия PHP: 5.4.44

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `med_office`
--

-- --------------------------------------------------------

--
-- Структура таблицы `administrator`
--

CREATE TABLE IF NOT EXISTS `administrator` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `surname` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `lastname` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`user_id`),
  KEY `FK_administrator_user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `auscultation_inspection`
--

CREATE TABLE IF NOT EXISTS `auscultation_inspection` (
  `lungs_inspection_id` int(11) NOT NULL,
  `patient_id` int(11) NOT NULL,
  `lungs_auscultation_status_id` int(11) NOT NULL,
  PRIMARY KEY (`lungs_inspection_id`,`patient_id`,`lungs_auscultation_status_id`),
  KEY `fk_aus_inspection_lungs_inspection_id` (`lungs_inspection_id`),
  KEY `fk_aus_inspection_patient_id` (`patient_id`),
  KEY `fk_aus_inspection_lungs_aus_status_id` (`lungs_auscultation_status_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `auscultation_inspection`
--

INSERT INTO `auscultation_inspection` (`lungs_inspection_id`, `patient_id`, `lungs_auscultation_status_id`) VALUES
(24, 6, 1),
(24, 6, 2),
(24, 6, 3);

-- --------------------------------------------------------

--
-- Структура таблицы `auth_assignment`
--

CREATE TABLE IF NOT EXISTS `auth_assignment` (
  `item_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`item_name`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `auth_assignment`
--

INSERT INTO `auth_assignment` (`item_name`, `user_id`, `created_at`) VALUES
('administrator', '1', NULL),
('diagnostician', '1', NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `auth_item`
--

CREATE TABLE IF NOT EXISTS `auth_item` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `type` int(11) NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `rule_name` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data` text COLLATE utf8_unicode_ci,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`name`),
  KEY `rule_name` (`rule_name`),
  KEY `idx-auth_item-type` (`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `auth_item`
--

INSERT INTO `auth_item` (`name`, `type`, `description`, `rule_name`, `data`, `created_at`, `updated_at`) VALUES
('administrator', 1, 'Администратор системы', NULL, NULL, NULL, NULL),
('diagnostician', 1, 'Диагност', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `auth_item_child`
--

CREATE TABLE IF NOT EXISTS `auth_item_child` (
  `parent` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `child` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`parent`,`child`),
  KEY `child` (`child`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `auth_rule`
--

CREATE TABLE IF NOT EXISTS `auth_rule` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `data` text COLLATE utf8_unicode_ci,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `cardiovascular_inspection`
--

CREATE TABLE IF NOT EXISTS `cardiovascular_inspection` (
  `inspection_id` int(11) NOT NULL,
  `patient_id` int(11) NOT NULL,
  `heart_status_id` int(11) NOT NULL,
  `heart_rate` double DEFAULT NULL,
  `left_blood_pressure` double DEFAULT NULL,
  `right_blood_pressure` double DEFAULT NULL,
  `heart_tones_inspection_description` text,
  `heart_tones_accent_inspection_description` text,
  `heart_noises_inspection_description` text,
  PRIMARY KEY (`inspection_id`,`patient_id`),
  KEY `fk_cardio_inspection_inspection_id` (`inspection_id`),
  KEY `fk_cardio_inspection_patient_id` (`patient_id`),
  KEY `fk_cardio_inspection_heart_status_id` (`heart_status_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `cardiovascular_inspection`
--

INSERT INTO `cardiovascular_inspection` (`inspection_id`, `patient_id`, `heart_status_id`, `heart_rate`, `left_blood_pressure`, `right_blood_pressure`, `heart_tones_inspection_description`, `heart_tones_accent_inspection_description`, `heart_noises_inspection_description`) VALUES
(24, 6, 1, 12, 12, 12, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc vulputate, eros a volutpat maximus, risus ipsum imperdiet orci, sed blandit quam erat sit amet nulla. Donec vitae viverra odio. Vestibulum nec egestas eros. Quisque non dignissim magna. Pellentesque sed orci non orci laoreet vulputate. Donec pellentesque mi vel augue efficitur, vitae fermentum neque maximus. Etiam lectus mauris, egestas quis erat a, ultrices euismod velit.', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc vulputate, eros a volutpat maximus, risus ipsum imperdiet orci, sed blandit quam erat sit amet nulla. Donec vitae viverra odio. Vestibulum nec egestas eros. Quisque non dignissim magna. Pellentesque sed orci non orci laoreet vulputate. Donec pellentesque mi vel augue efficitur, vitae fermentum neque maximus. Etiam lectus mauris, egestas quis erat a, ultrices euismod velit.', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc vulputate, eros a volutpat maximus, risus ipsum imperdiet orci, sed blandit quam erat sit amet nulla. Donec vitae viverra odio. Vestibulum nec egestas eros. Quisque non dignissim magna. Pellentesque sed orci non orci laoreet vulputate. Donec pellentesque mi vel augue efficitur, vitae fermentum neque maximus. Etiam lectus mauris, egestas quis erat a, ultrices euismod velit.');

-- --------------------------------------------------------

--
-- Структура таблицы `contract`
--

CREATE TABLE IF NOT EXISTS `contract` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `patient_id` int(11) NOT NULL,
  `contract_number` bigint(20) NOT NULL,
  `contract_date` date NOT NULL,
  PRIMARY KEY (`id`,`patient_id`),
  KEY `patient_id` (`patient_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=40 ;

--
-- Дамп данных таблицы `contract`
--

INSERT INTO `contract` (`id`, `patient_id`, `contract_number`, `contract_date`) VALUES
(37, 6, 1, '2016-11-28'),
(38, 6, 2, '2016-12-15'),
(39, 6, 3, '2016-12-15');

-- --------------------------------------------------------

--
-- Структура таблицы `crepitation_inspection`
--

CREATE TABLE IF NOT EXISTS `crepitation_inspection` (
  `lungs_inspection_id` int(11) NOT NULL,
  `patient_id` int(11) NOT NULL,
  `crepitation_status_id` int(11) NOT NULL,
  PRIMARY KEY (`lungs_inspection_id`,`patient_id`,`crepitation_status_id`),
  KEY `fk_crepitation_inspection_lungs_inspection_id` (`lungs_inspection_id`),
  KEY `fk_crepitation_inspection_patient_id` (`patient_id`),
  KEY `fk_crepitation_inspection_lungs_crepitation_status_id` (`crepitation_status_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `crepitation_inspection`
--

INSERT INTO `crepitation_inspection` (`lungs_inspection_id`, `patient_id`, `crepitation_status_id`) VALUES
(24, 6, 3);

-- --------------------------------------------------------

--
-- Структура таблицы `crepitation_status`
--

CREATE TABLE IF NOT EXISTS `crepitation_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Дамп данных таблицы `crepitation_status`
--

INSERT INTO `crepitation_status` (`id`, `name`) VALUES
(1, 'влажный'),
(2, 'сухой'),
(3, 'крепитирующие');

-- --------------------------------------------------------

--
-- Структура таблицы `extremities_movement_inspection`
--

CREATE TABLE IF NOT EXISTS `extremities_movement_inspection` (
  `neurological_inspection_id` int(11) NOT NULL,
  `patient_id` int(11) NOT NULL,
  `extremities_movement_status_id` int(11) NOT NULL,
  PRIMARY KEY (`neurological_inspection_id`,`patient_id`,`extremities_movement_status_id`),
  KEY `fk_extrem_mov_inspection_neurological_inspection_id` (`neurological_inspection_id`),
  KEY `fk_extrem_mov_inspection_patient_id` (`patient_id`),
  KEY `fk_extrem_mov_inspection_extremities_movement_status_id` (`extremities_movement_status_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `extremities_movement_inspection`
--

INSERT INTO `extremities_movement_inspection` (`neurological_inspection_id`, `patient_id`, `extremities_movement_status_id`) VALUES
(24, 6, 2),
(24, 6, 9);

-- --------------------------------------------------------

--
-- Структура таблицы `extremities_movement_status`
--

CREATE TABLE IF NOT EXISTS `extremities_movement_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;

--
-- Дамп данных таблицы `extremities_movement_status`
--

INSERT INTO `extremities_movement_status` (`id`, `name`) VALUES
(2, 'ограничены справа'),
(7, 'ограничены слева'),
(8, 'сохранены справа'),
(9, 'сохранены слева');

-- --------------------------------------------------------

--
-- Структура таблицы `feeding_status`
--

CREATE TABLE IF NOT EXISTS `feeding_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Дамп данных таблицы `feeding_status`
--

INSERT INTO `feeding_status` (`id`, `name`) VALUES
(1, 'нормальное'),
(2, 'избыточное'),
(4, 'пониженное');

-- --------------------------------------------------------

--
-- Структура таблицы `finger_nose_inspection`
--

CREATE TABLE IF NOT EXISTS `finger_nose_inspection` (
  `neurological_inspection_id` int(11) NOT NULL,
  `patient_id` int(11) NOT NULL,
  `finger_nose_status_id` int(11) NOT NULL,
  PRIMARY KEY (`neurological_inspection_id`,`patient_id`,`finger_nose_status_id`),
  KEY `fk_finger_nose_inspection_neurological_inspection_id` (`neurological_inspection_id`),
  KEY `fk_finger_nose_inspection_patient_id` (`patient_id`),
  KEY `fk_finger_nose_inspection_finger_nose_status_id` (`finger_nose_status_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `finger_nose_inspection`
--

INSERT INTO `finger_nose_inspection` (`neurological_inspection_id`, `patient_id`, `finger_nose_status_id`) VALUES
(24, 6, 2),
(24, 6, 12);

-- --------------------------------------------------------

--
-- Структура таблицы `finger_nose_status`
--

CREATE TABLE IF NOT EXISTS `finger_nose_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=14 ;

--
-- Дамп данных таблицы `finger_nose_status`
--

INSERT INTO `finger_nose_status` (`id`, `name`) VALUES
(2, 'выполняется не точно справа'),
(11, 'выполняется не точно слева'),
(12, 'выполняется точно слева'),
(13, 'выполняется точно справа');

-- --------------------------------------------------------

--
-- Структура таблицы `gallbladder_inspection`
--

CREATE TABLE IF NOT EXISTS `gallbladder_inspection` (
  `other_inspection_id` int(11) NOT NULL,
  `patient_id` int(11) NOT NULL,
  `gallbladder_status_id` int(11) NOT NULL,
  PRIMARY KEY (`other_inspection_id`,`patient_id`,`gallbladder_status_id`),
  KEY `fk_gallbladder_inspection_other_inspection_id` (`other_inspection_id`),
  KEY `fk_gallbladder_inspection_patient_id` (`patient_id`),
  KEY `fk_gallbladder_inspection_gallbladder_status_id` (`gallbladder_status_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `gallbladder_inspection`
--

INSERT INTO `gallbladder_inspection` (`other_inspection_id`, `patient_id`, `gallbladder_status_id`) VALUES
(24, 6, 2);

-- --------------------------------------------------------

--
-- Структура таблицы `gallbladder_status`
--

CREATE TABLE IF NOT EXISTS `gallbladder_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Дамп данных таблицы `gallbladder_status`
--

INSERT INTO `gallbladder_status` (`id`, `name`) VALUES
(1, 'пальпируется'),
(2, 'не пальпируется');

-- --------------------------------------------------------

--
-- Структура таблицы `general_inspection`
--

CREATE TABLE IF NOT EXISTS `general_inspection` (
  `inspection_id` int(11) NOT NULL,
  `patient_id` int(11) NOT NULL,
  `helth_status_id` int(11) DEFAULT NULL,
  `feeding_status_id` int(11) DEFAULT NULL,
  `peripheral_lymph_nodes_status_id` int(11) DEFAULT NULL,
  `body_mass_index` double DEFAULT NULL,
  `rash` text,
  `edema` tinyint(1) DEFAULT NULL,
  `edema_description` text,
  `dysuria` tinyint(1) DEFAULT NULL,
  `skin_inspection_description` text,
  `thyroid_inspection_description` text,
  `mammary_gland_inspection_description` text,
  `osteoarticular_inspection_description` text,
  `stool_inspection_description` text,
  `tongue_inspection_description` text,
  PRIMARY KEY (`inspection_id`,`patient_id`),
  KEY `fk_general_inspection_inspection_id` (`inspection_id`),
  KEY `fk_general_inspection_patient_id` (`patient_id`),
  KEY `fk_general_inspection_helth_status_id` (`helth_status_id`),
  KEY `fk_general_inspection_feeding_status_id` (`feeding_status_id`),
  KEY `fk_general_inspection_lymph_nodes_status_id` (`peripheral_lymph_nodes_status_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `general_inspection`
--

INSERT INTO `general_inspection` (`inspection_id`, `patient_id`, `helth_status_id`, `feeding_status_id`, `peripheral_lymph_nodes_status_id`, `body_mass_index`, `rash`, `edema`, `edema_description`, `dysuria`, `skin_inspection_description`, `thyroid_inspection_description`, `mammary_gland_inspection_description`, `osteoarticular_inspection_description`, `stool_inspection_description`, `tongue_inspection_description`) VALUES
(24, 6, 4, 1, 2, 12, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc vulputate, eros a volutpat maximus, risus ipsum imperdiet orci, sed blandit quam erat sit amet nulla. Donec vitae viverra odio. Vestibulum nec egestas eros. Quisque non dignissim magna. Pellentesque sed orci non orci laoreet vulputate. Donec pellentesque mi vel augue efficitur, vitae fermentum neque maximus. Etiam lectus mauris, egestas quis erat a, ultrices euismod velit.', 0, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc vulputate, eros a volutpat maximus, risus ipsum imperdiet orci, sed blandit quam erat sit amet nulla. Donec vitae viverra odio. Vestibulum nec egestas eros. Quisque non dignissim magna. Pellentesque sed orci non orci laoreet vulputate. Donec pellentesque mi vel augue efficitur, vitae fermentum neque maximus. Etiam lectus mauris, egestas quis erat a, ultrices euismod velit.', 0, '', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc vulputate, eros a volutpat maximus, risus ipsum imperdiet orci, sed blandit quam erat sit amet nulla. Donec vitae viverra odio. Vestibulum nec egestas eros. Quisque non dignissim magna. Pellentesque sed orci non orci laoreet vulputate. Donec pellentesque mi vel augue efficitur, vitae fermentum neque maximus. Etiam lectus mauris, egestas quis erat a, ultrices euismod velit.', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc vulputate, eros a volutpat maximus, risus ipsum imperdiet orci, sed blandit quam erat sit amet nulla. Donec vitae viverra odio. Vestibulum nec egestas eros. Quisque non dignissim magna. Pellentesque sed orci non orci laoreet vulputate. Donec pellentesque mi vel augue efficitur, vitae fermentum neque maximus. Etiam lectus mauris, egestas quis erat a, ultrices euismod velit.', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc vulputate, eros a volutpat maximus, risus ipsum imperdiet orci, sed blandit quam erat sit amet nulla. Donec vitae viverra odio. Vestibulum nec egestas eros. Quisque non dignissim magna. Pellentesque sed orci non orci laoreet vulputate. Donec pellentesque mi vel augue efficitur, vitae fermentum neque maximus. Etiam lectus mauris, egestas quis erat a, ultrices euismod velit.', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc vulputate, eros a volutpat maximus, risus ipsum imperdiet orci, sed blandit quam erat sit amet nulla. Donec vitae viverra odio. Vestibulum nec egestas eros. Quisque non dignissim magna. Pellentesque sed orci non orci laoreet vulputate. Donec pellentesque mi vel augue efficitur, vitae fermentum neque maximus. Etiam lectus mauris, egestas quis erat a, ultrices euismod velit.', '');

-- --------------------------------------------------------

--
-- Структура таблицы `heart_noises_inspection`
--

CREATE TABLE IF NOT EXISTS `heart_noises_inspection` (
  `cardiovascular_inspection_id` int(11) NOT NULL,
  `patient_id` int(11) NOT NULL,
  `heart_noises_status_id` int(11) NOT NULL,
  PRIMARY KEY (`cardiovascular_inspection_id`,`patient_id`,`heart_noises_status_id`),
  KEY `fk_h_n_inspection_cardio_inspection_id` (`cardiovascular_inspection_id`),
  KEY `fk_h_n_inspection_patient_id` (`patient_id`),
  KEY `fk_h_n_inspection_h_n_status_id` (`heart_noises_status_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `heart_noises_inspection`
--

INSERT INTO `heart_noises_inspection` (`cardiovascular_inspection_id`, `patient_id`, `heart_noises_status_id`) VALUES
(24, 6, 2);

-- --------------------------------------------------------

--
-- Структура таблицы `heart_noises_status`
--

CREATE TABLE IF NOT EXISTS `heart_noises_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Дамп данных таблицы `heart_noises_status`
--

INSERT INTO `heart_noises_status` (`id`, `name`) VALUES
(1, 'выслушиваются'),
(2, 'не выслушиваются');

-- --------------------------------------------------------

--
-- Структура таблицы `heart_status`
--

CREATE TABLE IF NOT EXISTS `heart_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Дамп данных таблицы `heart_status`
--

INSERT INTO `heart_status` (`id`, `name`) VALUES
(1, 'увеличено'),
(4, 'не увеличино');

-- --------------------------------------------------------

--
-- Структура таблицы `heart_tones_accent_inspection`
--

CREATE TABLE IF NOT EXISTS `heart_tones_accent_inspection` (
  `cardiovascular_inspection_id` int(11) NOT NULL,
  `patient_id` int(11) NOT NULL,
  `heart_tones_accent_status_id` int(11) NOT NULL,
  PRIMARY KEY (`cardiovascular_inspection_id`,`patient_id`,`heart_tones_accent_status_id`),
  KEY `fk_t_a_inspection_cardio_inspection_id` (`cardiovascular_inspection_id`),
  KEY `fk_t_a_inspection_patient_id` (`patient_id`),
  KEY `fk_t_a_inspection_t_a_status_id` (`heart_tones_accent_status_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `heart_tones_accent_inspection`
--

INSERT INTO `heart_tones_accent_inspection` (`cardiovascular_inspection_id`, `patient_id`, `heart_tones_accent_status_id`) VALUES
(24, 6, 1),
(24, 6, 3);

-- --------------------------------------------------------

--
-- Структура таблицы `heart_tones_accent_status`
--

CREATE TABLE IF NOT EXISTS `heart_tones_accent_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Дамп данных таблицы `heart_tones_accent_status`
--

INSERT INTO `heart_tones_accent_status` (`id`, `name`) VALUES
(1, 'II тона на аорте'),
(3, 'II тона на лёгочной артерии');

-- --------------------------------------------------------

--
-- Структура таблицы `heart_tones_inspection`
--

CREATE TABLE IF NOT EXISTS `heart_tones_inspection` (
  `cardiovascular_inspection_id` int(11) NOT NULL,
  `patient_id` int(11) NOT NULL,
  `heart_tones_status_id` int(11) NOT NULL,
  PRIMARY KEY (`cardiovascular_inspection_id`,`patient_id`,`heart_tones_status_id`),
  KEY `fk_h_t_inspection_cardio_inspection_id` (`cardiovascular_inspection_id`),
  KEY `fk_h_t_inspection_patient_id` (`patient_id`),
  KEY `fk_h_t_inspection_h_t_status_id` (`heart_tones_status_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `heart_tones_inspection`
--

INSERT INTO `heart_tones_inspection` (`cardiovascular_inspection_id`, `patient_id`, `heart_tones_status_id`) VALUES
(24, 6, 4);

-- --------------------------------------------------------

--
-- Структура таблицы `heart_tones_status`
--

CREATE TABLE IF NOT EXISTS `heart_tones_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Дамп данных таблицы `heart_tones_status`
--

INSERT INTO `heart_tones_status` (`id`, `name`) VALUES
(4, 'аритмичные'),
(5, 'ритмичные');

-- --------------------------------------------------------

--
-- Структура таблицы `helth_status`
--

CREATE TABLE IF NOT EXISTS `helth_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Дамп данных таблицы `helth_status`
--

INSERT INTO `helth_status` (`id`, `name`) VALUES
(2, 'среднетяжёлое'),
(3, 'тяжёлое'),
(4, 'удовлетворительное');

-- --------------------------------------------------------

--
-- Структура таблицы `inspection`
--

CREATE TABLE IF NOT EXISTS `inspection` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `patient_id` int(11) NOT NULL,
  `inspection_date` date NOT NULL,
  `anamnesis` text,
  `complaints` text,
  `diagnosis` text,
  `treatment` text,
  PRIMARY KEY (`id`,`patient_id`),
  KEY `fk_inspection_patient_id` (`patient_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=25 ;

--
-- Дамп данных таблицы `inspection`
--

INSERT INTO `inspection` (`id`, `patient_id`, `inspection_date`, `anamnesis`, `complaints`, `diagnosis`, `treatment`) VALUES
(24, 6, '2016-12-15', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc vulputate, eros a volutpat maximus, risus ipsum imperdiet orci, sed blandit quam erat sit amet nulla. Donec vitae viverra odio. Vestibulum nec egestas eros. Quisque non dignissim magna. Pellentesque sed orci non orci laoreet vulputate. Donec pellentesque mi vel augue efficitur, vitae fermentum neque maximus. Etiam lectus mauris, egestas quis erat a, ultrices euismod velit.', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc vulputate, eros a volutpat maximus, risus ipsum imperdiet orci, sed blandit quam erat sit amet nulla. Donec vitae viverra odio. Vestibulum nec egestas eros. Quisque non dignissim magna. Pellentesque sed orci non orci laoreet vulputate. Donec pellentesque mi vel augue efficitur, vitae fermentum neque maximus. Etiam lectus mauris, egestas quis erat a, ultrices euismod velit.', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc vulputate, eros a volutpat maximus, risus ipsum imperdiet orci, sed blandit quam erat sit amet nulla. Donec vitae viverra odio. Vestibulum nec egestas eros. Quisque non dignissim magna. Pellentesque sed orci non orci laoreet vulputate. Donec pellentesque mi vel augue efficitur, vitae fermentum neque maximus. Etiam lectus mauris, egestas quis erat a, ultrices euismod velit.', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc vulputate, eros a volutpat maximus, risus ipsum imperdiet orci, sed blandit quam erat sit amet nulla. Donec vitae viverra odio. Vestibulum nec egestas eros. Quisque non dignissim magna. Pellentesque sed orci non orci laoreet vulputate. Donec pellentesque mi vel augue efficitur, vitae fermentum neque maximus. Etiam lectus mauris, egestas quis erat a, ultrices euismod velit.');

-- --------------------------------------------------------

--
-- Структура таблицы `kidneys_inspection`
--

CREATE TABLE IF NOT EXISTS `kidneys_inspection` (
  `other_inspection_id` int(11) NOT NULL,
  `patient_id` int(11) NOT NULL,
  `kidneys_status_id` int(11) NOT NULL,
  PRIMARY KEY (`other_inspection_id`,`patient_id`,`kidneys_status_id`),
  KEY `fk_kidneys_inspection_other_inspection_id` (`other_inspection_id`),
  KEY `fk_kidneys_inspection_patient_id` (`patient_id`),
  KEY `fk_kidneys_inspection_kidneys_status_id` (`kidneys_status_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `kidneys_inspection`
--

INSERT INTO `kidneys_inspection` (`other_inspection_id`, `patient_id`, `kidneys_status_id`) VALUES
(24, 6, 1),
(24, 6, 3);

-- --------------------------------------------------------

--
-- Структура таблицы `kidneys_status`
--

CREATE TABLE IF NOT EXISTS `kidneys_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Дамп данных таблицы `kidneys_status`
--

INSERT INTO `kidneys_status` (`id`, `name`) VALUES
(1, 'не пальпируются'),
(2, 'пальпируются'),
(3, 'синдром поколачивания отрицательный'),
(4, 'синдром поколачивания положительный'),
(5, 'справа'),
(6, 'слева');

-- --------------------------------------------------------

--
-- Структура таблицы `liver_inspection`
--

CREATE TABLE IF NOT EXISTS `liver_inspection` (
  `other_inspection_id` int(11) NOT NULL,
  `patient_id` int(11) NOT NULL,
  `liver_status_id` int(11) NOT NULL,
  PRIMARY KEY (`other_inspection_id`,`patient_id`,`liver_status_id`),
  KEY `fk_liver_inspection_other_inspection_id` (`other_inspection_id`),
  KEY `fk_liver_inspection_patient_id` (`patient_id`),
  KEY `fk_liver_inspection_liver_status_id` (`liver_status_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `liver_inspection`
--

INSERT INTO `liver_inspection` (`other_inspection_id`, `patient_id`, `liver_status_id`) VALUES
(24, 6, 2),
(24, 6, 3);

-- --------------------------------------------------------

--
-- Структура таблицы `liver_status`
--

CREATE TABLE IF NOT EXISTS `liver_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Дамп данных таблицы `liver_status`
--

INSERT INTO `liver_status` (`id`, `name`) VALUES
(1, 'не пальпируется'),
(2, 'увеличена'),
(3, 'болезненная'),
(4, 'безболезненная'),
(5, 'плотная');

-- --------------------------------------------------------

--
-- Структура таблицы `lungs_auscultation_status`
--

CREATE TABLE IF NOT EXISTS `lungs_auscultation_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Дамп данных таблицы `lungs_auscultation_status`
--

INSERT INTO `lungs_auscultation_status` (`id`, `name`) VALUES
(1, 'нормальное везикулярное дыхание'),
(2, 'ослабленное везикулярное дыхание'),
(3, 'жесткое везикулярное дыхание');

-- --------------------------------------------------------

--
-- Структура таблицы `lungs_inspection`
--

CREATE TABLE IF NOT EXISTS `lungs_inspection` (
  `inspection_id` int(11) NOT NULL,
  `patient_id` int(11) NOT NULL,
  `breathing_rate` double DEFAULT NULL,
  `oxygen_saturation` double DEFAULT NULL,
  `rib_cage_inspection_description` text,
  `percussion_inspection_description` text,
  `voice_trembling_inspection_description` text,
  `auscultation_inspection_drscription` text,
  `crepitation_inspection_description` text,
  PRIMARY KEY (`inspection_id`,`patient_id`),
  KEY `fk_lungs_inspection_inspection_id` (`inspection_id`),
  KEY `fk_lungs_inspection_patient_id` (`patient_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `lungs_inspection`
--

INSERT INTO `lungs_inspection` (`inspection_id`, `patient_id`, `breathing_rate`, `oxygen_saturation`, `rib_cage_inspection_description`, `percussion_inspection_description`, `voice_trembling_inspection_description`, `auscultation_inspection_drscription`, `crepitation_inspection_description`) VALUES
(24, 6, 12, 12, '', '', '', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc vulputate, eros a volutpat maximus, risus ipsum imperdiet orci, sed blandit quam erat sit amet nulla. Donec vitae viverra odio. Vestibulum nec egestas eros. Quisque non dignissim magna. Pellentesque sed orci non orci laoreet vulputate. Donec pellentesque mi vel augue efficitur, vitae fermentum neque maximus. Etiam lectus mauris, egestas quis erat a, ultrices euismod velit.', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc vulputate, eros a volutpat maximus, risus ipsum imperdiet orci, sed blandit quam erat sit amet nulla. Donec vitae viverra odio. Vestibulum nec egestas eros. Quisque non dignissim magna. Pellentesque sed orci non orci laoreet vulputate. Donec pellentesque mi vel augue efficitur, vitae fermentum neque maximus. Etiam lectus mauris, egestas quis erat a, ultrices euismod velit.');

-- --------------------------------------------------------

--
-- Структура таблицы `mammary_gland_inspection`
--

CREATE TABLE IF NOT EXISTS `mammary_gland_inspection` (
  `general_inspection_id` int(11) NOT NULL,
  `patient_id` int(11) NOT NULL,
  `mammary_status_id` int(11) NOT NULL,
  PRIMARY KEY (`general_inspection_id`,`patient_id`,`mammary_status_id`),
  KEY `fk_m_g_inspection_general_inspection_id` (`general_inspection_id`),
  KEY `fk_m_g_inspection_patient_id` (`patient_id`),
  KEY `fk_m_g_inspection_mammary_status_id` (`mammary_status_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `mammary_gland_inspection`
--

INSERT INTO `mammary_gland_inspection` (`general_inspection_id`, `patient_id`, `mammary_status_id`) VALUES
(24, 6, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `mammary_status`
--

CREATE TABLE IF NOT EXISTS `mammary_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Дамп данных таблицы `mammary_status`
--

INSERT INTO `mammary_status` (`id`, `name`) VALUES
(1, 'очаговые образования не пальпируются'),
(2, 'очаговые образования пальпируются');

-- --------------------------------------------------------

--
-- Структура таблицы `migration`
--

CREATE TABLE IF NOT EXISTS `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `migration`
--

INSERT INTO `migration` (`version`, `apply_time`) VALUES
('m000000_000000_base', 1469605121),
('m130524_201442_init', 1469605125),
('m140506_102106_rbac_init', 1469707909),
('m160722_125421_med_office', 1472558651);

-- --------------------------------------------------------

--
-- Структура таблицы `neurological_inspection`
--

CREATE TABLE IF NOT EXISTS `neurological_inspection` (
  `inspection_id` int(11) NOT NULL,
  `patient_id` int(11) NOT NULL,
  `pupils_diff_status_id` int(11) DEFAULT NULL,
  `pupils_reaction_status_id` int(11) DEFAULT NULL,
  `finger_nose_inspection_description` text,
  `extremities_movement_inspection_description` text,
  PRIMARY KEY (`inspection_id`,`patient_id`),
  KEY `fk_neurological_inspection_inspection_id` (`inspection_id`),
  KEY `fk_neurological_inspection_patient_id` (`patient_id`),
  KEY `fk_neurological_inspection_pupils_diff_status_id` (`pupils_diff_status_id`),
  KEY `fk_neurological_inspection_pupils_reaction_status_id` (`pupils_reaction_status_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `neurological_inspection`
--

INSERT INTO `neurological_inspection` (`inspection_id`, `patient_id`, `pupils_diff_status_id`, `pupils_reaction_status_id`, `finger_nose_inspection_description`, `extremities_movement_inspection_description`) VALUES
(24, 6, 1, 1, '', '');

-- --------------------------------------------------------

--
-- Структура таблицы `osteoarticular_inspection`
--

CREATE TABLE IF NOT EXISTS `osteoarticular_inspection` (
  `general_inspection_id` int(11) NOT NULL,
  `patient_id` int(11) NOT NULL,
  `osteoarticular_system_status_id` int(11) NOT NULL,
  PRIMARY KEY (`general_inspection_id`,`patient_id`,`osteoarticular_system_status_id`),
  KEY `fk_osteo_inspection_general_inspection_id` (`general_inspection_id`),
  KEY `fk_osteo_inspection_patient_id` (`patient_id`),
  KEY `fk_osteo_inspection_osteo_status_id` (`osteoarticular_system_status_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `osteoarticular_inspection`
--

INSERT INTO `osteoarticular_inspection` (`general_inspection_id`, `patient_id`, `osteoarticular_system_status_id`) VALUES
(24, 6, 2);

-- --------------------------------------------------------

--
-- Структура таблицы `osteoarticular_system_status`
--

CREATE TABLE IF NOT EXISTS `osteoarticular_system_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Дамп данных таблицы `osteoarticular_system_status`
--

INSERT INTO `osteoarticular_system_status` (`id`, `name`) VALUES
(1, 'возрастные изменения'),
(2, 'без особенностей');

-- --------------------------------------------------------

--
-- Структура таблицы `other_inspection`
--

CREATE TABLE IF NOT EXISTS `other_inspection` (
  `inspection_id` int(11) NOT NULL,
  `patient_id` int(11) NOT NULL,
  `gallbladder_inspection_description` text,
  `kidneys_inspection_description` text,
  `liver_inspection_description` text,
  `spleen_inspection_description` text,
  `stomach_inspection_description` text,
  PRIMARY KEY (`inspection_id`,`patient_id`),
  KEY `fk_other_inspection_inspection_id` (`inspection_id`),
  KEY `fk_other_inspection_patient_id` (`patient_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `other_inspection`
--

INSERT INTO `other_inspection` (`inspection_id`, `patient_id`, `gallbladder_inspection_description`, `kidneys_inspection_description`, `liver_inspection_description`, `spleen_inspection_description`, `stomach_inspection_description`) VALUES
(24, 6, '', '', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc vulputate, eros a volutpat maximus, risus ipsum imperdiet orci, sed blandit quam erat sit amet nulla. Donec vitae viverra odio. Vestibulum nec egestas eros. Quisque non dignissim magna. Pellentesque sed orci non orci laoreet vulputate. Donec pellentesque mi vel augue efficitur, vitae fermentum neque maximus. Etiam lectus mauris, egestas quis erat a, ultrices euismod velit.', '', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc vulputate, eros a volutpat maximus, risus ipsum imperdiet orci, sed blandit quam erat sit amet nulla. Donec vitae viverra odio. Vestibulum nec egestas eros. Quisque non dignissim magna. Pellentesque sed orci non orci laoreet vulputate. Donec pellentesque mi vel augue efficitur, vitae fermentum neque maximus. Etiam lectus mauris, egestas quis erat a, ultrices euismod velit.');

-- --------------------------------------------------------

--
-- Структура таблицы `patient`
--

CREATE TABLE IF NOT EXISTS `patient` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `patient_name` varchar(255) NOT NULL,
  `birth_date` date NOT NULL,
  `address` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `create_date` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Дамп данных таблицы `patient`
--

INSERT INTO `patient` (`id`, `patient_name`, `birth_date`, `address`, `phone`, `create_date`) VALUES
(6, 'qtC+BMJtufm1kWWabwAoCNy3+UmuqczY2N87JN6C1XvS3T8RcL6h3mtDaBoPv8iyWaYmvObo9x2AXAb3Cd//eA==', '1900-12-15', 'GotjWMwEZ+6jp34ZLZ/Oow==', 'L/1I/jDvoWy0abGq7vV8zQ==', '2016-11-28');

-- --------------------------------------------------------

--
-- Структура таблицы `percussion_inspection`
--

CREATE TABLE IF NOT EXISTS `percussion_inspection` (
  `lungs_inspection_id` int(11) NOT NULL,
  `patient_id` int(11) NOT NULL,
  `percussion_status_id` int(11) NOT NULL,
  PRIMARY KEY (`lungs_inspection_id`,`patient_id`,`percussion_status_id`),
  KEY `fk_percussion_inspection_lungs_inspection_id` (`lungs_inspection_id`),
  KEY `fk_percussion_inspection_patient_id` (`patient_id`),
  KEY `fk_percussion_inspection_percussion_status_id` (`percussion_status_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `percussion_inspection`
--

INSERT INTO `percussion_inspection` (`lungs_inspection_id`, `patient_id`, `percussion_status_id`) VALUES
(24, 6, 2),
(24, 6, 3);

-- --------------------------------------------------------

--
-- Структура таблицы `percussion_status`
--

CREATE TABLE IF NOT EXISTS `percussion_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Дамп данных таблицы `percussion_status`
--

INSERT INTO `percussion_status` (`id`, `name`) VALUES
(1, 'ясный лёгочный звук справа'),
(2, 'ясный лёгочный звук с коробочным оттенком справа'),
(3, 'притупление справа'),
(4, 'притупление слева'),
(7, 'ясный лёгочный звук слева'),
(8, 'ясный лёгочный звук с коробочным оттенком слева');

-- --------------------------------------------------------

--
-- Структура таблицы `peripheral_lymph_nodes_status`
--

CREATE TABLE IF NOT EXISTS `peripheral_lymph_nodes_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Дамп данных таблицы `peripheral_lymph_nodes_status`
--

INSERT INTO `peripheral_lymph_nodes_status` (`id`, `name`) VALUES
(2, 'не увеличены'),
(3, 'увеличены');

-- --------------------------------------------------------

--
-- Структура таблицы `pupils_diff_status`
--

CREATE TABLE IF NOT EXISTS `pupils_diff_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Дамп данных таблицы `pupils_diff_status`
--

INSERT INTO `pupils_diff_status` (`id`, `name`) VALUES
(1, 'S > d'),
(4, 'S < d'),
(5, 'S = d');

-- --------------------------------------------------------

--
-- Структура таблицы `pupils_reaction_status`
--

CREATE TABLE IF NOT EXISTS `pupils_reaction_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Дамп данных таблицы `pupils_reaction_status`
--

INSERT INTO `pupils_reaction_status` (`id`, `name`) VALUES
(1, 'живая'),
(3, 'вялая');

-- --------------------------------------------------------

--
-- Структура таблицы `rib_cage_inspection`
--

CREATE TABLE IF NOT EXISTS `rib_cage_inspection` (
  `lungs_inspection_id` int(11) NOT NULL,
  `patient_id` int(11) NOT NULL,
  `rib_cage_status_id` int(11) NOT NULL,
  PRIMARY KEY (`lungs_inspection_id`,`patient_id`,`rib_cage_status_id`),
  KEY `fk_rib_cage_inspection_lungs_inspection_id` (`lungs_inspection_id`),
  KEY `fk_rib_cage_inspection_patient_id` (`patient_id`),
  KEY `fk_rib_cage_inspection_rib_cage_status_id` (`rib_cage_status_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `rib_cage_inspection`
--

INSERT INTO `rib_cage_inspection` (`lungs_inspection_id`, `patient_id`, `rib_cage_status_id`) VALUES
(24, 6, 2),
(24, 6, 3);

-- --------------------------------------------------------

--
-- Структура таблицы `rib_cage_status`
--

CREATE TABLE IF NOT EXISTS `rib_cage_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Дамп данных таблицы `rib_cage_status`
--

INSERT INTO `rib_cage_status` (`id`, `name`) VALUES
(1, 'нормостеничная'),
(2, 'эмфизематозная'),
(3, 'астеничная'),
(4, 'ассиметричная');

-- --------------------------------------------------------

--
-- Структура таблицы `skin_inspection`
--

CREATE TABLE IF NOT EXISTS `skin_inspection` (
  `general_inspection_id` int(11) NOT NULL,
  `patient_id` int(11) NOT NULL,
  `skin_status_id` int(11) NOT NULL,
  PRIMARY KEY (`general_inspection_id`,`patient_id`,`skin_status_id`),
  KEY `fk_skin_inspection_general_inspection_id` (`general_inspection_id`),
  KEY `fk_skin_inspection_patient_id` (`patient_id`),
  KEY `fk_skin_inspection_skin_status_id` (`skin_status_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `skin_inspection`
--

INSERT INTO `skin_inspection` (`general_inspection_id`, `patient_id`, `skin_status_id`) VALUES
(24, 6, 3);

-- --------------------------------------------------------

--
-- Структура таблицы `skin_status`
--

CREATE TABLE IF NOT EXISTS `skin_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Дамп данных таблицы `skin_status`
--

INSERT INTO `skin_status` (`id`, `name`) VALUES
(1, 'бледные'),
(2, 'Желтушные'),
(3, 'физиологическая окраска');

-- --------------------------------------------------------

--
-- Структура таблицы `spleen_inspection`
--

CREATE TABLE IF NOT EXISTS `spleen_inspection` (
  `other_inspection_id` int(11) NOT NULL,
  `patient_id` int(11) NOT NULL,
  `spleen_status_id` int(11) NOT NULL,
  PRIMARY KEY (`other_inspection_id`,`patient_id`,`spleen_status_id`),
  KEY `fk_spleen_inspection_other_inspection_id` (`other_inspection_id`),
  KEY `fk_spleen_inspection_patient_id` (`patient_id`),
  KEY `fk_spleen_inspection_spleen_status_id` (`spleen_status_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `spleen_inspection`
--

INSERT INTO `spleen_inspection` (`other_inspection_id`, `patient_id`, `spleen_status_id`) VALUES
(24, 6, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `spleen_status`
--

CREATE TABLE IF NOT EXISTS `spleen_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Дамп данных таблицы `spleen_status`
--

INSERT INTO `spleen_status` (`id`, `name`) VALUES
(1, 'не пальпируется'),
(2, 'увеличена');

-- --------------------------------------------------------

--
-- Структура таблицы `stomach_inspection`
--

CREATE TABLE IF NOT EXISTS `stomach_inspection` (
  `other_inspection_id` int(11) NOT NULL,
  `patient_id` int(11) NOT NULL,
  `stomach_status_id` int(11) NOT NULL,
  PRIMARY KEY (`other_inspection_id`,`patient_id`,`stomach_status_id`),
  KEY `fk_stomach_inspection_other_inspection_id` (`other_inspection_id`),
  KEY `fk_stomach_inspection_patient_id` (`patient_id`),
  KEY `fk_stomach_inspection_stomach_status_id` (`stomach_status_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `stomach_inspection`
--

INSERT INTO `stomach_inspection` (`other_inspection_id`, `patient_id`, `stomach_status_id`) VALUES
(24, 6, 2),
(24, 6, 3),
(24, 6, 4);

-- --------------------------------------------------------

--
-- Структура таблицы `stomach_status`
--

CREATE TABLE IF NOT EXISTS `stomach_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Дамп данных таблицы `stomach_status`
--

INSERT INTO `stomach_status` (`id`, `name`) VALUES
(1, 'симметричный'),
(2, 'ассиметричный'),
(3, 'мягкий'),
(4, 'болезненный'),
(5, 'безболезненный');

-- --------------------------------------------------------

--
-- Структура таблицы `stool_inspection`
--

CREATE TABLE IF NOT EXISTS `stool_inspection` (
  `general_inspection_id` int(11) NOT NULL,
  `patient_id` int(11) NOT NULL,
  `stool_status_id` int(11) NOT NULL,
  PRIMARY KEY (`general_inspection_id`,`patient_id`,`stool_status_id`),
  KEY `fk_stool_inspection_general_inspection_id` (`general_inspection_id`),
  KEY `fk_stool_inspection_patient_id` (`patient_id`),
  KEY `fk_stool_inspection_stool_status_id` (`stool_status_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `stool_inspection`
--

INSERT INTO `stool_inspection` (`general_inspection_id`, `patient_id`, `stool_status_id`) VALUES
(24, 6, 2);

-- --------------------------------------------------------

--
-- Структура таблицы `stool_status`
--

CREATE TABLE IF NOT EXISTS `stool_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Дамп данных таблицы `stool_status`
--

INSERT INTO `stool_status` (`id`, `name`) VALUES
(1, 'регулярный'),
(2, 'запоры');

-- --------------------------------------------------------

--
-- Структура таблицы `survey`
--

CREATE TABLE IF NOT EXISTS `survey` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;

--
-- Дамп данных таблицы `survey`
--

INSERT INTO `survey` (`id`, `name`) VALUES
(1, 'ЛДГ'),
(2, 'ГГТП'),
(3, 'биллирубин'),
(4, 'триглицериды'),
(5, 'ВЛПНП'),
(6, 'ВЛПВП'),
(7, 'креатенин'),
(8, 'мочевина'),
(9, 'полный анализ крови');

-- --------------------------------------------------------

--
-- Структура таблицы `survey_inspection`
--

CREATE TABLE IF NOT EXISTS `survey_inspection` (
  `survey_id` int(11) NOT NULL,
  `patient_id` int(11) NOT NULL,
  `inspection_id` int(11) NOT NULL,
  PRIMARY KEY (`survey_id`,`patient_id`,`inspection_id`),
  KEY `fk_survey_inspection_survey_id` (`survey_id`),
  KEY `fk_survey_inspection_patient_id` (`patient_id`),
  KEY `fk_survey_inspection_inspection_id` (`inspection_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `survey_inspection`
--

INSERT INTO `survey_inspection` (`survey_id`, `patient_id`, `inspection_id`) VALUES
(1, 6, 24),
(2, 6, 24),
(3, 6, 24),
(4, 6, 24),
(5, 6, 24),
(6, 6, 24),
(7, 6, 24),
(8, 6, 24),
(9, 6, 24);

-- --------------------------------------------------------

--
-- Структура таблицы `thyroid_inspection`
--

CREATE TABLE IF NOT EXISTS `thyroid_inspection` (
  `general_inspection_id` int(11) NOT NULL,
  `patient_id` int(11) NOT NULL,
  `thyroid_status_id` int(11) NOT NULL,
  PRIMARY KEY (`general_inspection_id`,`patient_id`,`thyroid_status_id`),
  KEY `fk_thyroid_inspection_general_inspection_id` (`general_inspection_id`),
  KEY `fk_thyroid_inspection_patient_id` (`patient_id`),
  KEY `fk_thyroid_inspection_skin_status_id` (`thyroid_status_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `thyroid_inspection`
--

INSERT INTO `thyroid_inspection` (`general_inspection_id`, `patient_id`, `thyroid_status_id`) VALUES
(24, 6, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `thyroid_status`
--

CREATE TABLE IF NOT EXISTS `thyroid_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Дамп данных таблицы `thyroid_status`
--

INSERT INTO `thyroid_status` (`id`, `name`) VALUES
(1, 'не увеличена'),
(2, 'гиперплазия I степени'),
(3, 'гиперплазия II степени'),
(4, 'гиперплазия III степени');

-- --------------------------------------------------------

--
-- Структура таблицы `tongue_inspection`
--

CREATE TABLE IF NOT EXISTS `tongue_inspection` (
  `general_inspection_id` int(11) NOT NULL,
  `patient_id` int(11) NOT NULL,
  `tongue_status_id` int(11) NOT NULL,
  PRIMARY KEY (`general_inspection_id`,`patient_id`,`tongue_status_id`),
  KEY `fk_tongue_inspection_general_inspection_id` (`general_inspection_id`),
  KEY `fk_tongue_inspection_patient_id` (`patient_id`),
  KEY `fk_tongue_inspection_stool_status_id` (`tongue_status_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `tongue_inspection`
--

INSERT INTO `tongue_inspection` (`general_inspection_id`, `patient_id`, `tongue_status_id`) VALUES
(24, 6, 1),
(24, 6, 7);

-- --------------------------------------------------------

--
-- Структура таблицы `tongue_status`
--

CREATE TABLE IF NOT EXISTS `tongue_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Дамп данных таблицы `tongue_status`
--

INSERT INTO `tongue_status` (`id`, `name`) VALUES
(1, 'чистый по срединной линии'),
(2, 'обложен по срединной линии'),
(3, 'влажный по срединной линии'),
(4, 'сухой по срединной линии'),
(7, 'девиация');

-- --------------------------------------------------------

--
-- Структура таблицы `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '10',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `password_reset_token` (`password_reset_token`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Дамп данных таблицы `user`
--

INSERT INTO `user` (`id`, `username`, `auth_key`, `password_hash`, `password_reset_token`, `email`, `status`, `created_at`, `updated_at`) VALUES
(1, 'administrator', 'Af6BrPrBz2xquPTYuVFev8ONFxwV7kjg', '$2y$13$btPKLyXkFtyIlZ8ZQwnxBuSk/Renks4R5mPlS0sz6X1yUspPZ2rhi', NULL, 'itsspc@mail.ru', 10, 1456920760, 1469177058),
(3, 'unknown', 'Af6BrPrBz2xquPTYuVFev8ONFxwV7kjg', '$2y$13$btPKLyXkFtyIlZ8ZQwnxBuSk/Renks4R5mPlS0sz6X1yUspPZ2rhi', NULL, 'itsspttrthc@mail.ru', 10, 0, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `voice_trembling_inspection`
--

CREATE TABLE IF NOT EXISTS `voice_trembling_inspection` (
  `lungs_inspection_id` int(11) NOT NULL,
  `patient_id` int(11) NOT NULL,
  `voice_trembling_status_id` int(11) NOT NULL,
  PRIMARY KEY (`lungs_inspection_id`,`patient_id`,`voice_trembling_status_id`),
  KEY `fk_v_t_inspection_lungs_inspection_id` (`lungs_inspection_id`),
  KEY `fk_v_t_inspection_patient_id` (`patient_id`),
  KEY `fk_v_t_inspection_v_t_status_id` (`voice_trembling_status_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `voice_trembling_inspection`
--

INSERT INTO `voice_trembling_inspection` (`lungs_inspection_id`, `patient_id`, `voice_trembling_status_id`) VALUES
(24, 6, 3),
(24, 6, 7);

-- --------------------------------------------------------

--
-- Структура таблицы `voice_trembling_status`
--

CREATE TABLE IF NOT EXISTS `voice_trembling_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Дамп данных таблицы `voice_trembling_status`
--

INSERT INTO `voice_trembling_status` (`id`, `name`) VALUES
(1, 'проводится справа'),
(3, 'не проводится справа'),
(4, 'не проводится слева'),
(7, 'проводится слева');

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `administrator`
--
ALTER TABLE `administrator`
  ADD CONSTRAINT `FK_administrator_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`);

--
-- Ограничения внешнего ключа таблицы `auscultation_inspection`
--
ALTER TABLE `auscultation_inspection`
  ADD CONSTRAINT `fk_aus_inspection_lungs_aus_status_id` FOREIGN KEY (`lungs_auscultation_status_id`) REFERENCES `lungs_auscultation_status` (`id`),
  ADD CONSTRAINT `fk_aus_inspection_lungs_inspection_id` FOREIGN KEY (`lungs_inspection_id`) REFERENCES `lungs_inspection` (`inspection_id`);

--
-- Ограничения внешнего ключа таблицы `auth_assignment`
--
ALTER TABLE `auth_assignment`
  ADD CONSTRAINT `auth_assignment_ibfk_1` FOREIGN KEY (`item_name`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `auth_item`
--
ALTER TABLE `auth_item`
  ADD CONSTRAINT `auth_item_ibfk_1` FOREIGN KEY (`rule_name`) REFERENCES `auth_rule` (`name`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `auth_item_child`
--
ALTER TABLE `auth_item_child`
  ADD CONSTRAINT `auth_item_child_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `auth_item_child_ibfk_2` FOREIGN KEY (`child`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `cardiovascular_inspection`
--
ALTER TABLE `cardiovascular_inspection`
  ADD CONSTRAINT `fk_cardio_inspection_heart_status_id` FOREIGN KEY (`heart_status_id`) REFERENCES `heart_status` (`id`),
  ADD CONSTRAINT `fk_cardio_inspection_inspection_id` FOREIGN KEY (`inspection_id`) REFERENCES `inspection` (`id`);

--
-- Ограничения внешнего ключа таблицы `contract`
--
ALTER TABLE `contract`
  ADD CONSTRAINT `contract_ibfk_1` FOREIGN KEY (`patient_id`) REFERENCES `patient` (`id`);

--
-- Ограничения внешнего ключа таблицы `crepitation_inspection`
--
ALTER TABLE `crepitation_inspection`
  ADD CONSTRAINT `fk_crepitation_inspection_lungs_crepitation_status_id` FOREIGN KEY (`crepitation_status_id`) REFERENCES `crepitation_status` (`id`),
  ADD CONSTRAINT `fk_crepitation_inspection_lungs_inspection_id` FOREIGN KEY (`lungs_inspection_id`) REFERENCES `lungs_inspection` (`inspection_id`);

--
-- Ограничения внешнего ключа таблицы `extremities_movement_inspection`
--
ALTER TABLE `extremities_movement_inspection`
  ADD CONSTRAINT `fk_extrem_mov_inspection_extremities_movement_status_id` FOREIGN KEY (`extremities_movement_status_id`) REFERENCES `extremities_movement_status` (`id`),
  ADD CONSTRAINT `fk_extrem_mov_inspection_neurological_inspection_id` FOREIGN KEY (`neurological_inspection_id`) REFERENCES `neurological_inspection` (`inspection_id`);

--
-- Ограничения внешнего ключа таблицы `finger_nose_inspection`
--
ALTER TABLE `finger_nose_inspection`
  ADD CONSTRAINT `fk_finger_nose_inspection_finger_nose_status_id` FOREIGN KEY (`finger_nose_status_id`) REFERENCES `finger_nose_status` (`id`),
  ADD CONSTRAINT `fk_finger_nose_inspection_neurological_inspection_id` FOREIGN KEY (`neurological_inspection_id`) REFERENCES `neurological_inspection` (`inspection_id`);

--
-- Ограничения внешнего ключа таблицы `gallbladder_inspection`
--
ALTER TABLE `gallbladder_inspection`
  ADD CONSTRAINT `fk_gallbladder_inspection_gallbladder_status_id` FOREIGN KEY (`gallbladder_status_id`) REFERENCES `gallbladder_status` (`id`),
  ADD CONSTRAINT `fk_gallbladder_inspection_other_inspection_id` FOREIGN KEY (`other_inspection_id`) REFERENCES `other_inspection` (`inspection_id`);

--
-- Ограничения внешнего ключа таблицы `general_inspection`
--
ALTER TABLE `general_inspection`
  ADD CONSTRAINT `fk_general_inspection_feeding_status_id` FOREIGN KEY (`feeding_status_id`) REFERENCES `feeding_status` (`id`),
  ADD CONSTRAINT `fk_general_inspection_helth_status_id` FOREIGN KEY (`helth_status_id`) REFERENCES `helth_status` (`id`),
  ADD CONSTRAINT `fk_general_inspection_inspection_id` FOREIGN KEY (`inspection_id`) REFERENCES `inspection` (`id`),
  ADD CONSTRAINT `fk_general_inspection_lymph_nodes_status_id` FOREIGN KEY (`peripheral_lymph_nodes_status_id`) REFERENCES `peripheral_lymph_nodes_status` (`id`);

--
-- Ограничения внешнего ключа таблицы `heart_noises_inspection`
--
ALTER TABLE `heart_noises_inspection`
  ADD CONSTRAINT `fk_h_n_inspection_cardio_inspection_id` FOREIGN KEY (`cardiovascular_inspection_id`) REFERENCES `cardiovascular_inspection` (`inspection_id`),
  ADD CONSTRAINT `fk_h_n_inspection_h_n_status_id` FOREIGN KEY (`heart_noises_status_id`) REFERENCES `heart_noises_status` (`id`);

--
-- Ограничения внешнего ключа таблицы `heart_tones_accent_inspection`
--
ALTER TABLE `heart_tones_accent_inspection`
  ADD CONSTRAINT `fk_t_a_inspection_cardio_inspection_id` FOREIGN KEY (`cardiovascular_inspection_id`) REFERENCES `cardiovascular_inspection` (`inspection_id`),
  ADD CONSTRAINT `fk_t_a_inspection_t_a_status_id` FOREIGN KEY (`heart_tones_accent_status_id`) REFERENCES `heart_tones_accent_status` (`id`);

--
-- Ограничения внешнего ключа таблицы `heart_tones_inspection`
--
ALTER TABLE `heart_tones_inspection`
  ADD CONSTRAINT `fk_h_t_inspection_cardio_inspection_id` FOREIGN KEY (`cardiovascular_inspection_id`) REFERENCES `cardiovascular_inspection` (`inspection_id`),
  ADD CONSTRAINT `fk_h_t_inspection_h_t_status_id` FOREIGN KEY (`heart_tones_status_id`) REFERENCES `heart_tones_status` (`id`);

--
-- Ограничения внешнего ключа таблицы `inspection`
--
ALTER TABLE `inspection`
  ADD CONSTRAINT `fk_inspection_patient_id` FOREIGN KEY (`patient_id`) REFERENCES `patient` (`id`);

--
-- Ограничения внешнего ключа таблицы `kidneys_inspection`
--
ALTER TABLE `kidneys_inspection`
  ADD CONSTRAINT `fk_kidneys_inspection_kidneys_status_id` FOREIGN KEY (`kidneys_status_id`) REFERENCES `kidneys_status` (`id`),
  ADD CONSTRAINT `fk_kidneys_inspection_other_inspection_id` FOREIGN KEY (`other_inspection_id`) REFERENCES `other_inspection` (`inspection_id`);

--
-- Ограничения внешнего ключа таблицы `liver_inspection`
--
ALTER TABLE `liver_inspection`
  ADD CONSTRAINT `fk_liver_inspection_liver_status_id` FOREIGN KEY (`liver_status_id`) REFERENCES `liver_status` (`id`),
  ADD CONSTRAINT `fk_liver_inspection_other_inspection_id` FOREIGN KEY (`other_inspection_id`) REFERENCES `other_inspection` (`inspection_id`);

--
-- Ограничения внешнего ключа таблицы `lungs_inspection`
--
ALTER TABLE `lungs_inspection`
  ADD CONSTRAINT `fk_lungs_inspection_inspection_id` FOREIGN KEY (`inspection_id`) REFERENCES `inspection` (`id`);

--
-- Ограничения внешнего ключа таблицы `mammary_gland_inspection`
--
ALTER TABLE `mammary_gland_inspection`
  ADD CONSTRAINT `fk_m_g_inspection_general_inspection_id` FOREIGN KEY (`general_inspection_id`) REFERENCES `general_inspection` (`inspection_id`),
  ADD CONSTRAINT `fk_m_g_inspection_mammary_status_id` FOREIGN KEY (`mammary_status_id`) REFERENCES `mammary_status` (`id`);

--
-- Ограничения внешнего ключа таблицы `neurological_inspection`
--
ALTER TABLE `neurological_inspection`
  ADD CONSTRAINT `fk_neurological_inspection_inspection_id` FOREIGN KEY (`inspection_id`) REFERENCES `inspection` (`id`),
  ADD CONSTRAINT `fk_neurological_inspection_pupils_diff_status_id` FOREIGN KEY (`pupils_diff_status_id`) REFERENCES `pupils_diff_status` (`id`),
  ADD CONSTRAINT `fk_neurological_inspection_pupils_reaction_status_id` FOREIGN KEY (`pupils_reaction_status_id`) REFERENCES `pupils_reaction_status` (`id`);

--
-- Ограничения внешнего ключа таблицы `osteoarticular_inspection`
--
ALTER TABLE `osteoarticular_inspection`
  ADD CONSTRAINT `fk_osteo_inspection_general_inspection_id` FOREIGN KEY (`general_inspection_id`) REFERENCES `general_inspection` (`inspection_id`),
  ADD CONSTRAINT `fk_osteo_inspection_osteo_status_id` FOREIGN KEY (`osteoarticular_system_status_id`) REFERENCES `osteoarticular_system_status` (`id`);

--
-- Ограничения внешнего ключа таблицы `other_inspection`
--
ALTER TABLE `other_inspection`
  ADD CONSTRAINT `fk_other_inspection_inspection_id` FOREIGN KEY (`inspection_id`) REFERENCES `inspection` (`id`);

--
-- Ограничения внешнего ключа таблицы `percussion_inspection`
--
ALTER TABLE `percussion_inspection`
  ADD CONSTRAINT `fk_percussion_inspection_lungs_inspection_id` FOREIGN KEY (`lungs_inspection_id`) REFERENCES `lungs_inspection` (`inspection_id`),
  ADD CONSTRAINT `fk_percussion_inspection_percussion_status_id` FOREIGN KEY (`percussion_status_id`) REFERENCES `percussion_status` (`id`);

--
-- Ограничения внешнего ключа таблицы `rib_cage_inspection`
--
ALTER TABLE `rib_cage_inspection`
  ADD CONSTRAINT `fk_rib_cage_inspection_lungs_inspection_id` FOREIGN KEY (`lungs_inspection_id`) REFERENCES `lungs_inspection` (`inspection_id`),
  ADD CONSTRAINT `fk_rib_cage_inspection_rib_cage_status_id` FOREIGN KEY (`rib_cage_status_id`) REFERENCES `rib_cage_status` (`id`);

--
-- Ограничения внешнего ключа таблицы `skin_inspection`
--
ALTER TABLE `skin_inspection`
  ADD CONSTRAINT `fk_skin_inspection_general_inspection_id` FOREIGN KEY (`general_inspection_id`) REFERENCES `general_inspection` (`inspection_id`),
  ADD CONSTRAINT `fk_skin_inspection_skin_status_id` FOREIGN KEY (`skin_status_id`) REFERENCES `skin_status` (`id`);

--
-- Ограничения внешнего ключа таблицы `spleen_inspection`
--
ALTER TABLE `spleen_inspection`
  ADD CONSTRAINT `fk_spleen_inspection_other_inspection_id` FOREIGN KEY (`other_inspection_id`) REFERENCES `other_inspection` (`inspection_id`),
  ADD CONSTRAINT `fk_spleen_inspection_spleen_status_id` FOREIGN KEY (`spleen_status_id`) REFERENCES `spleen_status` (`id`);

--
-- Ограничения внешнего ключа таблицы `stomach_inspection`
--
ALTER TABLE `stomach_inspection`
  ADD CONSTRAINT `fk_stomach_inspection_other_inspection_id` FOREIGN KEY (`other_inspection_id`) REFERENCES `other_inspection` (`inspection_id`),
  ADD CONSTRAINT `fk_stomach_inspection_stomach_status_id` FOREIGN KEY (`stomach_status_id`) REFERENCES `stomach_status` (`id`);

--
-- Ограничения внешнего ключа таблицы `stool_inspection`
--
ALTER TABLE `stool_inspection`
  ADD CONSTRAINT `fk_stool_inspection_general_inspection_id` FOREIGN KEY (`general_inspection_id`) REFERENCES `general_inspection` (`inspection_id`),
  ADD CONSTRAINT `fk_stool_inspection_stool_status_id` FOREIGN KEY (`stool_status_id`) REFERENCES `stool_status` (`id`);

--
-- Ограничения внешнего ключа таблицы `survey_inspection`
--
ALTER TABLE `survey_inspection`
  ADD CONSTRAINT `fk_survey_inspection_inspection_id` FOREIGN KEY (`inspection_id`) REFERENCES `inspection` (`id`),
  ADD CONSTRAINT `fk_survey_inspection_survey_id` FOREIGN KEY (`survey_id`) REFERENCES `survey` (`id`);

--
-- Ограничения внешнего ключа таблицы `thyroid_inspection`
--
ALTER TABLE `thyroid_inspection`
  ADD CONSTRAINT `fk_thyroid_inspection_general_inspection_id` FOREIGN KEY (`general_inspection_id`) REFERENCES `general_inspection` (`inspection_id`),
  ADD CONSTRAINT `fk_thyroid_inspection_skin_status_id` FOREIGN KEY (`thyroid_status_id`) REFERENCES `thyroid_status` (`id`);

--
-- Ограничения внешнего ключа таблицы `tongue_inspection`
--
ALTER TABLE `tongue_inspection`
  ADD CONSTRAINT `fk_tongue_inspection_general_inspection_id` FOREIGN KEY (`general_inspection_id`) REFERENCES `general_inspection` (`inspection_id`),
  ADD CONSTRAINT `fk_tongue_inspection_stool_status_id` FOREIGN KEY (`tongue_status_id`) REFERENCES `tongue_status` (`id`);

--
-- Ограничения внешнего ключа таблицы `voice_trembling_inspection`
--
ALTER TABLE `voice_trembling_inspection`
  ADD CONSTRAINT `fk_v_t_inspection_lungs_inspection_id` FOREIGN KEY (`lungs_inspection_id`) REFERENCES `lungs_inspection` (`inspection_id`),
  ADD CONSTRAINT `fk_v_t_inspection_v_t_status_id` FOREIGN KEY (`voice_trembling_status_id`) REFERENCES `voice_trembling_status` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
