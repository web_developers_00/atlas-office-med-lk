<?php

namespace common\models;

use Yii;
use Arslanim\TemplateMapper\Interfaces\TemplateFillStrategy;
use common\helpers\DateTranslater;

/**
 * This is the model class for table "contract".
 *
 * @property integer $id
 * @property integer $patient_id
 * @property string $contract_number
 * @property string $contract_date
 *
 * @property Patient $patient
 */
class Contract extends \yii\db\ActiveRecord implements TemplateFillStrategy
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'contract';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['patient_id', 'contract_number', 'contract_date'], 'required'],
            [['patient_id', 'contract_number'], 'integer'],
            [['contract_date'], 'safe'],
            [['patient_id'], 'exist', 'skipOnError' => true, 'targetClass' => Patient::className(), 'targetAttribute' => ['patient_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'patient_id' => 'Patient ID',
            'contract_number' => 'Contract Number',
            'contract_date' => 'Contract Date',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPatient()
    {
        return $this->hasOne(Patient::className(), ['id' => 'patient_id']);
    }

    public function mapObject($template) {
        $template->setValue('contract_number', ($this->contract_number) ? ($this->contract_number) : (''));
        $template->setValue('contract_date', ($this->contract_date) ? (DateTranslater::translateDate($this->contract_date)) : (Yii::$app->params['systemValues']['defaultValue']));        

        return $template;
    }
}
