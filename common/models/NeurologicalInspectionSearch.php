<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\NeurologicalInspection;

/**
 * NeurologicalInspectionSearch represents the model behind the search form about `common\models\NeurologicalInspection`.
 */
class NeurologicalInspectionSearch extends NeurologicalInspection
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['inspection_id', 'patient_id', 'pupils_diff_status_id', 'pupils_reaction_status_id'], 'integer'],
            [['finger_nose_inspection_description', 'extremities_movement_inspection_description'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = NeurologicalInspection::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'inspection_id' => $this->inspection_id,
            'patient_id' => $this->patient_id,
            'pupils_diff_status_id' => $this->pupils_diff_status_id,
            'pupils_reaction_status_id' => $this->pupils_reaction_status_id,
        ]);

        $query->andFilterWhere(['like', 'finger_nose_inspection_description', $this->finger_nose_inspection_description])
            ->andFilterWhere(['like', 'extremities_movement_inspection_description', $this->extremities_movement_inspection_description]);

        return $dataProvider;
    }
}
