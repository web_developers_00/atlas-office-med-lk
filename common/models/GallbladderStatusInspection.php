<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;
use Arslanim\TemplateMapper\Interfaces\TemplateFillStrategy;
use Arslanim\TemplateMapper\Interfaces\CreateDocumentStrategy;

/**
 * This is the model class for table "gallbladder_status_inspection".
 *
 * @property integer $inspection_id
 * @property integer $patient_id
 * @property string $gallbladder_inspection_description
 *
 * @property GallbladderInspection[] $gallbladderInspections
 * @property Inspection $inspection
 */
class GallbladderStatusInspection extends \yii\db\ActiveRecord implements TemplateFillStrategy, CreateDocumentStrategy
{
    public $gallbladderStatusesIds = [];
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'gallbladder_status_inspection';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['inspection_id', 'patient_id'], 'required'],
            [['inspection_id', 'patient_id'], 'integer'],
            [['gallbladder_inspection_description'], 'string'],
            [['inspection_id'], 'exist', 'skipOnError' => true, 'targetClass' => Inspection::className(), 'targetAttribute' => ['inspection_id' => 'id']],
            [['gallbladderStatusesIds', 'gallbladderStatuses'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'inspection_id' => 'Inspection ID',
            'patient_id' => 'Patient ID',
            'gallbladderStatuses' => 'Желчный пузырь',
            'gallbladder_inspection_description' => 'Желчный пузырь дополнительно',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGallbladderInspections()
    {
        return $this->hasMany(GallbladderInspection::className(), ['other_inspection_id' => 'inspection_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInspection()
    {
        return $this->hasOne(Inspection::className(), ['id' => 'inspection_id']);
    }

    public function setGallbladderStatuses($gallbladderStatusesIds) {
        $this->gallbladderStatusesIds = $gallbladderStatusesIds;
    }

    public function getDropGallbladderStatuses() {
        return ArrayHelper::map(GallbladderStatus::find()->asArray()->all(), 'id', 'name');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGallbladderStatuses() {
        return $this->hasMany(GallbladderStatus::className(), ['id' => 'gallbladder_status_id'])
            ->via('gallbladderInspections');
    }

    public function getGallbladderStatusesIds() {
       $this->gallbladderStatusesIds = ArrayHelper::getColumn($this->getGallbladderInspections()->asArray()->all(), 'gallbladder_status_id');
       return $this->gallbladderStatusesIds;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPatient()
    {
        return $this->hasOne(Patient::className(), ['id' => 'patient_id']);
    }

    protected function updateGallbladderInspection()
    {
        GallbladderInspection::deleteAll(array('other_inspection_id' => $this->inspection_id));
        if (is_array($this->gallbladderStatusesIds)) {
            foreach ($this->gallbladderStatusesIds as $id) {
                $gallbladderInspection = new GallbladderInspection();
                $gallbladderInspection->other_inspection_id = $this->inspection_id;
                $gallbladderInspection->patient_id = $this->patient_id;
                $gallbladderInspection->gallbladder_status_id = $id;
                $gallbladderInspection->save();
            }
        }
    }

    public function afterSave($insert, $changedAttributes)
    {
        $this->updateGallbladderInspection();
        parent::afterSave($insert, $changedAttributes);
    }

    public function beforeDelete() {
        if (parent::beforeDelete()) {
            GallbladderInspection::deleteAll('other_inspection_id = :inspection_id AND patient_id = :patient_id', [
                'inspection_id' => $this->inspection_id,
                'patient_id' => $this->patient_id
            ]);

            return true;
        } else {
            return false;
        }
    }

    public function inspectionToString($inspections) {
        $resultArray = [];
        foreach ($inspections as $inspection) {
            array_push($resultArray, $inspection->name);
        }

        return (count($resultArray)) ? (implode(", ", $resultArray)) : ("");
    }

    public function mapObject($template) {
        $template->setValue('gallbladderStatuses', $this->inspectionToString($this->gallbladderStatuses));

        return $template;
    }

    public function createSection($phpWord, $section) {
        $gallbladderStatuses = $this->inspectionToString($this->gallbladderStatuses);

        if ($gallbladderStatuses) {
            $textrun = $section->createTextRun('textParagraphStyle');
            $textrun->addText($this->attributeLabels()['gallbladderStatuses'] . ": ", 'inspectionPositionTextStyle');

            $textrun->addText($gallbladderStatuses, 'textStyle');
        }
    }
}
