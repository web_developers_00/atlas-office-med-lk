<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;
use Arslanim\TemplateMapper\Interfaces\TemplateFillStrategy;
use Arslanim\TemplateMapper\Interfaces\CreateDocumentStrategy;

/**
 * This is the model class for table "osteoarticular_status_inspection".
 *
 * @property integer $inspection_id
 * @property integer $patient_id
 * @property string $osteoarticular_inspection_description
 *
 * @property OsteoarticularInspection[] $osteoarticularInspections
 * @property Inspection $inspection
 */
class OsteoarticularStatusInspection extends \yii\db\ActiveRecord implements TemplateFillStrategy, CreateDocumentStrategy
{
    public $osteoarticularSystemStatusesIds = [];
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'osteoarticular_status_inspection';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['inspection_id', 'patient_id'], 'required'],
            [['inspection_id', 'patient_id'], 'integer'],
            [['osteoarticular_inspection_description'], 'string'],
            [['inspection_id'], 'exist', 'skipOnError' => true, 'targetClass' => Inspection::className(), 'targetAttribute' => ['inspection_id' => 'id']],
            [['osteoarticularSystemStatuses', 'osteoarticularSystemStatusesIds'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'inspection_id' => 'Inspection ID',
            'patient_id' => 'Patient ID',
            'osteoarticularSystemStatuses' => 'Костно-суставная система',
            'osteoarticular_inspection_description' => 'Дополнительное описание состояния костно-суставной системы',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOsteoarticularInspections()
    {
        return $this->hasMany(OsteoarticularInspection::className(), ['general_inspection_id' => 'inspection_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInspection()
    {
        return $this->hasOne(Inspection::className(), ['id' => 'inspection_id']);
    }

    public function setOsteoarticularSystemStatuses($osteoarticularSystemStatusesIds) {
        $this->osteoarticularSystemStatusesIds = $osteoarticularSystemStatusesIds;
    }

    public function getDropOsteoarticularSystemStatuses() {
        return ArrayHelper::map(OsteoarticularSystemStatus::find()->asArray()->all(), 'id', 'name');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOsteoarticularSystemStatuses() {
        return $this->hasMany(OsteoarticularSystemStatus::className(), ['id' => 'osteoarticular_system_status_id'])
            ->via('osteoarticularInspections');
    }

    public function getOsteoarticularSystemStatusesIds() {
       $this->osteoarticularSystemStatusesIds = ArrayHelper::getColumn($this->getOsteoarticularInspections()->asArray()->all(), 'osteoarticular_system_status_id');
       return $this->osteoarticularSystemStatusesIds;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPatient()
    {
        return $this->hasOne(Patient::className(), ['id' => 'patient_id']);
    }

    protected function updateOsteoarticularSystemInspection()
    {
        OsteoarticularInspection::deleteAll(array('general_inspection_id' => $this->inspection_id));
        if (is_array($this->osteoarticularSystemStatusesIds)) {
            foreach ($this->osteoarticularSystemStatusesIds as $id) {
                $osteoarticularInspection = new OsteoarticularInspection();
                $osteoarticularInspection->general_inspection_id = $this->inspection_id;
                $osteoarticularInspection->patient_id = $this->patient_id;
                $osteoarticularInspection->osteoarticular_system_status_id = $id;
                $osteoarticularInspection->save();
            }
        }
    }

    public function beforeDelete() {
        if (parent::beforeDelete()) {
            OsteoarticularInspection::deleteAll('general_inspection_id = :inspection_id AND patient_id = :patient_id', [
                'inspection_id' => $this->inspection_id,
                'patient_id' => $this->patient_id
            ]);
            return true;
        } else {
            return false;
        }
    }

    public function afterSave($insert, $changedAttributes)
    {
        $this->updateOsteoarticularSystemInspection();
        parent::afterSave($insert, $changedAttributes);
    }

    public function inspectionToString($inspections) {
        $resultArray = [];
	$inspectionString = "";        
	foreach ($inspections as $inspection) {
            array_push($resultArray, $inspection->name);
        }
	if (count($resultArray)) {
	    $inspectionString = implode(", ", $resultArray) . '; ';
	}
	
	return $inspectionString;
    }

    public function mapObject($template) {
        $template->setValue('osteoarticularSystemStatuses', $this->inspectionToString($this->osteoarticularSystemStatuses));
        $template->setValue('osteoarticular_inspection_description', $this->osteoarticular_inspection_description);

        return $template;
    }

    public function createSection($phpWord, $section) {
        $osteoarticularSystemStatuses = $this->inspectionToString($this->osteoarticularSystemStatuses);
        $osteoarticular_inspection_description = $this->osteoarticular_inspection_description;

        if ($osteoarticularSystemStatuses || $osteoarticular_inspection_description) {
            
            $textrun = $section->createTextRun('textParagraphStyle');
            $textrun->addText($this->attributeLabels()['osteoarticularSystemStatuses'] . ": ", 'inspectionPositionTextStyle');

            $textrun->addText($osteoarticularSystemStatuses, 'textStyle');
            $textrun->addText($osteoarticular_inspection_description, 'textStyle');
        }
    }
}
