<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;
use Arslanim\TemplateMapper\Interfaces\TemplateFillStrategy;
use Arslanim\TemplateMapper\Interfaces\CreateDocumentStrategy;

/**
 * This is the model class for table "tongue_status_inspection".
 *
 * @property integer $inspection_id
 * @property integer $patient_id
 * @property string $tongue_inspection_description
 *
 * @property TongueInspection[] $tongueInspections
 * @property Inspection $inspection
 */
class TongueStatusInspection extends \yii\db\ActiveRecord implements TemplateFillStrategy, CreateDocumentStrategy
{
    public $tongueStatusesIds = [];
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tongue_status_inspection';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['inspection_id', 'patient_id'], 'required'],
            [['inspection_id', 'patient_id'], 'integer'],
            [['tongue_inspection_description'], 'string'],
            [['inspection_id'], 'exist', 'skipOnError' => true, 'targetClass' => Inspection::className(), 'targetAttribute' => ['inspection_id' => 'id']],
            [['tongueStatuses', 'tongueStatusesIds'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'inspection_id' => 'Inspection ID',
            'patient_id' => 'Patient ID',
            'tongue_inspection_description' => 'Tongue Inspection Description',
            'tongueStatuses' => 'Язык',
            'tongue_inspection_description' => 'Дополнительное описание состояния языка',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTongueInspections()
    {
        return $this->hasMany(TongueInspection::className(), ['general_inspection_id' => 'inspection_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPatient()
    {
        return $this->hasOne(Patient::className(), ['id' => 'patient_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInspection()
    {
        return $this->hasOne(Inspection::className(), ['id' => 'inspection_id']);
    }

    public function setTongueStatuses($tongueStatusesIds) {
        $this->tongueStatusesIds = $tongueStatusesIds;
    }

    public function getDropTongueStatuses() {
        return ArrayHelper::map(TongueStatus::find()->asArray()->all(), 'id', 'name');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTongueStatuses() {
        return $this->hasMany(TongueStatus::className(), ['id' => 'tongue_status_id'])
            ->via('tongueInspections');
    }

    public function getTongueStatusesIds() {
       $this->tongueStatusesIds = ArrayHelper::getColumn($this->getTongueInspections()->asArray()->all(), 'tongue_status_id');
       return $this->tongueStatusesIds;
    }

    protected function updateTongueInspection() {
        TongueInspection::deleteAll(array('general_inspection_id' => $this->inspection_id));
        if (is_array($this->tongueStatusesIds)) {
            foreach ($this->tongueStatusesIds as $id) {
                $tongueInspection = new TongueInspection();
                $tongueInspection->general_inspection_id = $this->inspection_id;
                $tongueInspection->patient_id = $this->patient_id;
                $tongueInspection->tongue_status_id = $id;
                $tongueInspection->save();
            }
        }
    }

    public function beforeDelete() {
        if (parent::beforeDelete()) {
            TongueInspection::deleteAll('general_inspection_id = :inspection_id AND patient_id = :patient_id', [
                'inspection_id' => $this->inspection_id,
                'patient_id' => $this->patient_id
            ]);
            return true;
        } else {
            return false;
        }
    }

    public function afterSave($insert, $changedAttributes)
    {
        $this->updateTongueInspection();
        parent::afterSave($insert, $changedAttributes);
    }

    public function inspectionToString($inspections) {
        $resultArray = [];
        foreach ($inspections as $inspection) {
            array_push($resultArray, $inspection->name);
        }

        return (count($resultArray)) ? (implode(", ", $resultArray)) : ("");
    }

    public function mapObject($template) {
        $template->setValue('tongueStatuses', $this->inspectionToString($this->tongueStatuses));

        return $template;
    }

    public function createSection($phpWord, $section) {
        $tongueStatuses = $this->inspectionToString($this->tongueStatuses);

        if ($tongueStatuses) {
            $textrun = $section->createTextRun('textParagraphStyle');
            $textrun->addText($this->attributeLabels()['tongueStatuses'] . ": ", 'inspectionPositionTextStyle');

            $textrun->addText($tongueStatuses, 'textStyle');
        }
    }
}
