<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "survey".
 *
 * @property integer $id
 * @property string $name
 *
 * @property SurveyInspection[] $surveyInspections
 */
class Survey extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'survey';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            ['name', 'unique'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Наименование обследования',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSurveyInspections()
    {
        return $this->hasMany(SurveyInspection::className(), ['survey_id' => 'id']);
    }

    public function beforeDelete() {
        if (parent::beforeDelete()) {

            if (SurveyInspection::find()->where('survey_id = :id', ['id' => $this->id])->exists()) {
                return false;
            } else {
                return true;
            }

        } else {
            return false;
        }
    }
}
