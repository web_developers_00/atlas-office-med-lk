<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "mammary_gland_inspection".
 *
 * @property integer $general_inspection_id
 * @property integer $patient_id
 * @property integer $mammary_status_id
 *
 * @property GeneralInspection $generalInspection
 * @property MammaryStatus $mammaryStatus
 * @property GeneralInspection $patient
 */
class MammaryGlandInspection extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mammary_gland_inspection';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['general_inspection_id', 'patient_id', 'mammary_status_id'], 'required'],
            [['general_inspection_id', 'patient_id', 'mammary_status_id'], 'integer'],
            [['general_inspection_id'], 'exist', 'skipOnError' => true, 'targetClass' => MammaryGlandStatusInspection::className(), 'targetAttribute' => ['general_inspection_id' => 'inspection_id']],
            [['mammary_status_id'], 'exist', 'skipOnError' => true, 'targetClass' => MammaryStatus::className(), 'targetAttribute' => ['mammary_status_id' => 'id']],
            [['patient_id'], 'exist', 'skipOnError' => true, 'targetClass' => MammaryGlandStatusInspection::className(), 'targetAttribute' => ['patient_id' => 'patient_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'general_inspection_id' => 'General Inspection ID',
            'patient_id' => 'Patient ID',
            'mammary_status_id' => 'Mammary Status ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMammaryGlandStatusInspection()
    {
        return $this->hasOne(MammaryGlandStatusInspection::className(), ['inspection_id' => 'general_inspection_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMammaryStatus()
    {
        return $this->hasOne(MammaryStatus::className(), ['id' => 'mammary_status_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPatient()
    {
        return $this->hasOne(MammaryGlandStatusInspection::className(), ['patient_id' => 'patient_id']);
    }
}
