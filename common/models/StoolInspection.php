<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "stool_inspection".
 *
 * @property integer $general_inspection_id
 * @property integer $patient_id
 * @property integer $stool_status_id
 *
 * @property GeneralInspection $generalInspection
 * @property GeneralInspection $patient
 * @property StoolStatus $stoolStatus
 */
class StoolInspection extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'stool_inspection';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['general_inspection_id', 'patient_id', 'stool_status_id'], 'required'],
            [['general_inspection_id', 'patient_id', 'stool_status_id'], 'integer'],
            [['general_inspection_id'], 'exist', 'skipOnError' => true, 'targetClass' => StoolStatusInspection::className(), 'targetAttribute' => ['general_inspection_id' => 'inspection_id']],
            [['patient_id'], 'exist', 'skipOnError' => true, 'targetClass' => StoolStatusInspection::className(), 'targetAttribute' => ['patient_id' => 'patient_id']],
            [['stool_status_id'], 'exist', 'skipOnError' => true, 'targetClass' => StoolStatus::className(), 'targetAttribute' => ['stool_status_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'general_inspection_id' => 'General Inspection ID',
            'patient_id' => 'Patient ID',
            'stool_status_id' => 'Stool Status ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStoolStatusInspection()
    {
        return $this->hasOne(StoolStatusInspection::className(), ['inspection_id' => 'general_inspection_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPatient()
    {
        return $this->hasOne(Patient::className(), ['patient_id' => 'patient_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStoolStatus()
    {
        return $this->hasOne(StoolStatus::className(), ['id' => 'stool_status_id']);
    }
}
