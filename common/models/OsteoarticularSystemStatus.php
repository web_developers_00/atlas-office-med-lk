<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "osteoarticular_system_status".
 *
 * @property integer $id
 * @property string $name
 *
 * @property OsteoarticularInspection[] $osteoarticularInspections
 */
class OsteoarticularSystemStatus extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'osteoarticular_system_status';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            ['name', 'unique'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Костно-суставная система',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOsteoarticularInspections()
    {
        return $this->hasMany(OsteoarticularInspection::className(), ['osteoarticular_system_status_id' => 'id']);
    }

    public function beforeDelete() {
        if (parent::beforeDelete()) {

            if (OsteoarticularInspection::find()->where('osteoarticular_system_status_id = :id', ['id' => $this->id])->exists()) {
                return false;
            } else {
                return true;
            }

        } else {
            return false;
        }
    }
}
