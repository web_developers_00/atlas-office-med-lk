<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "kidneys_status".
 *
 * @property integer $id
 * @property string $name
 *
 * @property KidneysInspection[] $kidneysInspections
 */
class KidneysStatus extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'kidneys_status';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            ['name', 'unique'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Состояние почек',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKidneysInspections()
    {
        return $this->hasMany(KidneysInspection::className(), ['kidneys_status_id' => 'id']);
    }
}
