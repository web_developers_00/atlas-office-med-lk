<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;
use Arslanim\TemplateMapper\Interfaces\TemplateFillStrategy;
use Arslanim\TemplateMapper\Interfaces\CreateDocumentStrategy;

/**
 * This is the model class for table "inspection".
 *
 * @property integer $id
 * @property integer $patient_id
 * @property string $inspection_date
 * @property string $anamnesis
 * @property string $complaints
 * @property string $diagnosis
 * @property string $treatment
 *
 * @property CardiovascularInspection[] $cardiovascularInspections
 * @property CardiovascularInspection[] $cardiovascularInspections0
 * @property Inspection[] $patients
 * @property Inspection[] $inspections
 * @property GeneralInspection[] $generalInspections
 * @property GeneralInspection[] $generalInspections0
 * @property Inspection[] $patients0
 * @property Inspection[] $inspections0
 * @property Patient $patient
 * @property LungsInspection[] $lungsInspections
 * @property LungsInspection[] $lungsInspections0
 * @property Inspection[] $patients1
 * @property Inspection[] $inspections1
 * @property NeurologicalInspection[] $neurologicalInspections
 * @property NeurologicalInspection[] $neurologicalInspections0
 * @property Inspection[] $patients2
 * @property Inspection[] $inspections2
 * @property OtherInspection[] $otherInspections
 * @property OtherInspection[] $otherInspections0
 * @property Inspection[] $patients3
 * @property Inspection[] $inspections3
 * @property SurveyInspection[] $surveyInspections
 * @property SurveyInspection[] $surveyInspections0
 */
class Inspection extends \yii\db\ActiveRecord implements TemplateFillStrategy, CreateDocumentStrategy
{
    public $surveysIds = [];

    public function behaviors() { 
        return [ 
            'timestamp' => [ 
                'class' => 'yii\behaviors\TimestampBehavior', 
                'attributes' => [ 
                    \yii\db\ActiveRecord::EVENT_BEFORE_INSERT => ['inspection_date'],
                ], 
                'value' => function() { 
                    return date('Y-m-d');
                }
            ], 
            /*'encryption' => [
                'class' => '\nickcv\encrypter\behaviors\EncryptionBehavior',
                'attributes' => [
                    'anamnesis',
                    'complaints',
                    'diagnosis',
                ],
            ],*/
        ]; 
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'inspection';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['patient_id'], 'required'],
            [['patient_id'], 'integer'],
            [['inspection_date'/*, 'surveys', 'surveysIds'*/], 'safe'],
            //[['anamnesis', 'complaints', 'diagnosis', 'treatment'], 'string'],
            [['patient_id'], 'exist', 'skipOnError' => true, 'targetClass' => Patient::className(), 'targetAttribute' => ['patient_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'patient_id' => 'Patient ID',
            'inspection_date' => 'Дата осмотра',
            'name' => 'Медицинский осмотр'
            /*'anamnesis' => 'Анамнез',
            'complaints' => 'Жалобы',
            'diagnosis' => 'Диагноз',
            'treatment' => 'Лечение',*/
        ];
    }

    /*public function setSurveys($surveysIds) {
        $this->surveysIds = $surveysIds;
    }

    public function getDropSurvey() {
        return ArrayHelper::map(Survey::find()->asArray()->all(), 'id', 'name');
    }

    public function getSurveyIds() {
       $this->surveysIds = ArrayHelper::getColumn($this->getSurveyInspections()->asArray()->all(), 'survey_id');
       return $this->surveysIds;
    }

    public function getSurveyInspections()
    {
        return $this->hasMany(SurveyInspection::className(), ['inspection_id' => 'id']);
    }

    public function getSurveys() {
        return $this->hasMany(Survey::className(), ['id' => 'survey_id'])
            ->via('surveyInspections');
    }*/

    /*public function getSurveys() {
        return $this->hasMany(Survey::className(), ['id' => 'survey_id'])
            ->via('survey_inspection');
    }*/

    /**
     * @return \yii\db\ActiveQuery
     */
    /*public function getSurveyInspections0()
    {
        return $this->hasMany(SurveyInspection::className(), ['patient_id' => 'patient_id']);
    }*/

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getResultInspections()
    {
        return $this->hasMany(ResultInspection::className(), ['inspection_id' => 'id']);
    }
    public function getResultInspection() {
        return $this->hasOne(ResultInspection::className(), ['inspection_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getComplaintsInspections()
    {
        return $this->hasMany(ComplaintsInspection::className(), ['inspection_id' => 'id']);
    }
    public function getComplaintsInspection() {
        return $this->hasOne(ComplaintsInspection::className(), ['inspection_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHelthStatusInspections()
    {
        return $this->hasMany(HelthStatusInspection::className(), ['inspection_id' => 'id']);
    }
    public function getHelthStatusInspection() {
        return $this->hasOne(HelthStatusInspection::className(), ['inspection_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFeedingStatusInspections()
    {
        return $this->hasMany(FeedingStatusInspection::className(), ['inspection_id' => 'id']);
    }
    public function getFeedingStatusInspection() {
        return $this->hasOne(FeedingStatusInspection::className(), ['inspection_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPeripheralLymphNodesStatusInspections()
    {
        return $this->hasMany(PeripheralLymphNodesStatusInspection::className(), ['inspection_id' => 'id']);
    }
    public function getPeripheralLymphNodesStatusInspection() {
        return $this->hasOne(PeripheralLymphNodesStatusInspection::className(), ['inspection_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBodyMassIndexInspections()
    {
        return $this->hasMany(BodyMassIndexInspection::className(), ['inspection_id' => 'id']);
    }
    public function getBodyMassIndexInspection() {
        return $this->hasOne(BodyMassIndexInspection::className(), ['inspection_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDysuriaInspections()
    {
        return $this->hasMany(DysuriaInspection::className(), ['inspection_id' => 'id']);
    }
    public function getDysuriaInspection() {
        return $this->hasOne(DysuriaInspection::className(), ['inspection_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSkinStatusInspections()
    {
        return $this->hasMany(SkinStatusInspection::className(), ['inspection_id' => 'id']);
    }
    public function getSkinStatusInspection() {
        return $this->hasOne(SkinStatusInspection::className(), ['inspection_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEdemaInspections()
    {
        return $this->hasMany(EdemaInspection::className(), ['inspection_id' => 'id']);
    }
    public function getEdemaInspection() {
        return $this->hasOne(EdemaInspection::className(), ['inspection_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getThyroidStatusInspections()
    {
        return $this->hasMany(ThyroidStatusInspection::className(), ['inspection_id' => 'id']);
    }
    public function getThyroidStatusInspection() {
        return $this->hasOne(ThyroidStatusInspection::className(), ['inspection_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMammaryGlandStatusInspections()
    {
        return $this->hasMany(MammaryGlandStatusInspection::className(), ['inspection_id' => 'id']);
    }
    public function getMammaryGlandStatusInspection() {
        return $this->hasOne(MammaryGlandStatusInspection::className(), ['inspection_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOsteoarticularStatusInspections()
    {
        return $this->hasMany(OsteoarticularStatusInspection::className(), ['inspection_id' => 'id']);
    }
    public function getOsteoarticularStatusInspection() {
        return $this->hasOne(OsteoarticularStatusInspection::className(), ['inspection_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStoolStatusInspections()
    {
        return $this->hasMany(StoolStatusInspection::className(), ['inspection_id' => 'id']);
    }
    public function getStoolStatusInspection() {
        return $this->hasOne(StoolStatusInspection::className(), ['inspection_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTongueStatusInspections()
    {
        return $this->hasMany(TongueStatusInspection::className(), ['inspection_id' => 'id']);
    }
    public function getTongueStatusInspection() {
        return $this->hasOne(TongueStatusInspection::className(), ['inspection_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGallbladderStatusInspections()
    {
        return $this->hasMany(GallbladderStatusInspection::className(), ['inspection_id' => 'id']);
    }
    public function getGallbladderStatusInspection() {
        return $this->hasOne(GallbladderStatusInspection::className(), ['inspection_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKidneysStatusInspections()
    {
        return $this->hasMany(KidneysStatusInspection::className(), ['inspection_id' => 'id']);
    }
    public function getKidneysStatusInspection() {
        return $this->hasOne(KidneysStatusInspection::className(), ['inspection_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLiverStatusInspections()
    {
        return $this->hasMany(LiverStatusInspection::className(), ['inspection_id' => 'id']);
    }
    public function getLiverStatusInspection() {
        return $this->hasOne(LiverStatusInspection::className(), ['inspection_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSpleenStatusInspections()
    {
        return $this->hasMany(SpleenStatusInspection::className(), ['inspection_id' => 'id']);
    }
    public function getSpleenStatusInspection() {
        return $this->hasOne(SpleenStatusInspection::className(), ['inspection_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStomachStatusInspections()
    {
        return $this->hasMany(StomachStatusInspection::className(), ['inspection_id' => 'id']);
    }
    public function getStomachStatusInspection() {
        return $this->hasOne(StomachStatusInspection::className(), ['inspection_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCardiovascularInspections()
    {
        return $this->hasMany(CardiovascularInspection::className(), ['inspection_id' => 'id']);
    }
    public function getCardiovascularInspection() {
        return $this->hasOne(CardiovascularInspection::className(), ['inspection_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCardiovascularInspections0()
    {
        return $this->hasMany(CardiovascularInspection::className(), ['patient_id' => 'patient_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPatients()
    {
        return $this->hasMany(Inspection::className(), ['patient_id' => 'patient_id'])->viaTable('cardiovascular_inspection', ['inspection_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInspections()
    {
        return $this->hasMany(Inspection::className(), ['id' => 'inspection_id'])->viaTable('cardiovascular_inspection', ['patient_id' => 'patient_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGeneralInspections()
    {
        return $this->hasMany(GeneralInspection::className(), ['inspection_id' => 'id']);
    }
    public function getGeneralInspection() {
        return $this->hasOne(GeneralInspection::className(), ['inspection_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGeneralInspections0()
    {
        return $this->hasMany(GeneralInspection::className(), ['patient_id' => 'patient_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPatients0()
    {
        return $this->hasMany(Inspection::className(), ['patient_id' => 'patient_id'])->viaTable('general_inspection', ['inspection_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInspections0()
    {
        return $this->hasMany(Inspection::className(), ['id' => 'inspection_id'])->viaTable('general_inspection', ['patient_id' => 'patient_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPatient()
    {
        return $this->hasOne(Patient::className(), ['id' => 'patient_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLungsInspections()
    {
        return $this->hasMany(LungsInspection::className(), ['inspection_id' => 'id']);
    }
    public function getLungsInspection() {
        return $this->hasOne(LungsInspection::className(), ['inspection_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLungsInspections0()
    {
        return $this->hasMany(LungsInspection::className(), ['patient_id' => 'patient_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPatients1()
    {
        return $this->hasMany(Inspection::className(), ['patient_id' => 'patient_id'])->viaTable('lungs_inspection', ['inspection_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInspections1()
    {
        return $this->hasMany(Inspection::className(), ['id' => 'inspection_id'])->viaTable('lungs_inspection', ['patient_id' => 'patient_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNeurologicalInspections()
    {
        return $this->hasMany(NeurologicalInspection::className(), ['inspection_id' => 'id']);
    }
    public function getNeurologicalInspection() {
        return $this->hasOne(NeurologicalInspection::className(), ['inspection_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNeurologicalInspections0()
    {
        return $this->hasMany(NeurologicalInspection::className(), ['patient_id' => 'patient_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPatients2()
    {
        return $this->hasMany(Inspection::className(), ['patient_id' => 'patient_id'])->viaTable('neurological_inspection', ['inspection_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInspections2()
    {
        return $this->hasMany(Inspection::className(), ['id' => 'inspection_id'])->viaTable('neurological_inspection', ['patient_id' => 'patient_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOtherInspections()
    {
        return $this->hasMany(OtherInspection::className(), ['inspection_id' => 'id']);
    }
    public function getOtherInspection() {
        return $this->hasOne(OtherInspection::className(), ['inspection_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOtherInspections0()
    {
        return $this->hasMany(OtherInspection::className(), ['patient_id' => 'patient_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPatients3()
    {
        return $this->hasMany(Inspection::className(), ['patient_id' => 'patient_id'])->viaTable('other_inspection', ['inspection_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInspections3()
    {
        return $this->hasMany(Inspection::className(), ['id' => 'inspection_id'])->viaTable('other_inspection', ['patient_id' => 'patient_id']);
    }

    public function inspectionToString($inspections) {
        $resultArray = [];
        foreach ($inspections as $inspection) {
            array_push($resultArray, $inspection->name);
        }

        return (count($resultArray)) ? (implode(", ", $resultArray)) : (\Yii::$app->params['systemValues']['defaultValue']);
    }

    public function beforeDelete() {
        if (parent::beforeDelete()) {

            $complaintsInspection = ComplaintsInspection::findOne(['inspection_id' => $this->id, 'patient_id' => $this->patient_id]);
            if ($complaintsInspection) $complaintsInspection->delete();

            $resultInspection = ResultInspection::findOne(['inspection_id' => $this->id, 'patient_id' => $this->patient_id]);
            if ($resultInspection) $resultInspection->delete();

            $helthStatusInspection = HelthStatusInspection::findOne(['inspection_id' => $this->id, 'patient_id' => $this->patient_id]);
            if ($helthStatusInspection) $helthStatusInspection->delete();

            $feedingStatusInspection = FeedingStatusInspection::findOne(['inspection_id' => $this->id, 'patient_id' => $this->patient_id]);
            if ($feedingStatusInspection) $feedingStatusInspection->delete();

            $peripheralLymphNodesStatusInspection = PeripheralLymphNodesStatusInspection::findOne(['inspection_id' => $this->id, 'patient_id' => $this->patient_id]);
            if ($peripheralLymphNodesStatusInspection) $peripheralLymphNodesStatusInspection->delete();

            $bodyMassIndexInspection = BodyMassIndexInspection::findOne(['inspection_id' => $this->id, 'patient_id' => $this->patient_id]);
            if ($bodyMassIndexInspection) $bodyMassIndexInspection->delete();

            $dysuriaInspection = DysuriaInspection::findOne(['inspection_id' => $this->id, 'patient_id' => $this->patient_id]);
            if ($dysuriaInspection) $dysuriaInspection->delete();

            $skinStatusInspection = SkinStatusInspection::findOne(['inspection_id' => $this->id, 'patient_id' => $this->patient_id]);
            if ($skinStatusInspection) $skinStatusInspection->delete();

            $edemaInspection = EdemaInspection::findOne(['inspection_id' => $this->id, 'patient_id' => $this->patient_id]);
            if ($edemaInspection) $edemaInspection->delete();

            $thyroidStatusInspection = ThyroidStatusInspection::findOne(['inspection_id' => $this->id, 'patient_id' => $this->patient_id]);
            if ($thyroidStatusInspection) $thyroidStatusInspection->delete();

            $mammaryGlandStatusInspection = MammaryGlandStatusInspection::findOne(['inspection_id' => $this->id, 'patient_id' => $this->patient_id]);
            if ($mammaryGlandStatusInspection) $mammaryGlandStatusInspection->delete();

            $osteoarticularStatusInspection = OsteoarticularStatusInspection::findOne(['inspection_id' => $this->id, 'patient_id' => $this->patient_id]);
            if ($osteoarticularStatusInspection) $osteoarticularStatusInspection->delete();

            $stoolStatusInspection = StoolStatusInspection::findOne(['inspection_id' => $this->id, 'patient_id' => $this->patient_id]);
            if ($stoolStatusInspection) $stoolStatusInspection->delete();

            $tongueStatusInspection = TongueStatusInspection::findOne(['inspection_id' => $this->id, 'patient_id' => $this->patient_id]);
            if ($tongueStatusInspection) $tongueStatusInspection->delete();

            $gallbladderStatusInspection = GallbladderStatusInspection::findOne(['inspection_id' => $this->id, 'patient_id' => $this->patient_id]);
            if ($gallbladderStatusInspection) $gallbladderStatusInspection->delete();

            $kidneysStatusInspection = KidneysStatusInspection::findOne(['inspection_id' => $this->id, 'patient_id' => $this->patient_id]);
            if ($kidneysStatusInspection) $kidneysStatusInspection->delete();

            $liverStatusInspection = LiverStatusInspection::findOne(['inspection_id' => $this->id, 'patient_id' => $this->patient_id]);
            if ($liverStatusInspection) $liverStatusInspection->delete();

            $spleenStatusInspection = SpleenStatusInspection::findOne(['inspection_id' => $this->id, 'patient_id' => $this->patient_id]);
            if ($spleenStatusInspection) $spleenStatusInspection->delete();

            $stomachStatusInspection = StomachStatusInspection::findOne(['inspection_id' => $this->id, 'patient_id' => $this->patient_id]);
            if ($stomachStatusInspection) $stomachStatusInspection->delete();

            $cardiovascularInspection = CardiovascularInspection::findOne(['inspection_id' => $this->id, 'patient_id' => $this->patient_id]);
            if ($cardiovascularInspection) $cardiovascularInspection->delete();

            $lungsInspection = LungsInspection::findOne(['inspection_id' => $this->id, 'patient_id' => $this->patient_id]);
            if ($lungsInspection) $lungsInspection->delete();

            $neurologicalInspection = NeurologicalInspection::findOne(['inspection_id' => $this->id, 'patient_id' => $this->patient_id]);
            if ($neurologicalInspection) $neurologicalInspection->delete();

            return true;
        } else {
            return false;
        }
    }

    public function afterSave($insert, $changedAttributes)
    {
        $this->updateSurveys();
        parent::afterSave($insert, $changedAttributes);
    }

    protected function updateSurveys()
    {
        SurveyInspection::deleteAll(array('inspection_id' => $this->id));
        if (is_array($this->surveysIds)) {
            foreach ($this->surveysIds as $id) {
                $surveyInspection = new SurveyInspection();
                $surveyInspection->inspection_id = $this->id;
                $surveyInspection->patient_id = $this->patient_id;
                $surveyInspection->survey_id = $id;
                $surveyInspection->save();
            }
        }
    }

    public function mapObject($template) {
        $template->setValue('inspection_date', ($this->inspection_date) ? ($this->inspection_date) : (Yii::$app->params['systemValues']['defaultValue']));
        /*$template->setValue('complaints', ($this->complaints) ? ($this->complaints) : (Yii::$app->params['systemValues']['defaultValue'])); 
        $template->setValue('anamnesis', ($this->anamnesis) ? ($this->anamnesis) : (Yii::$app->params['systemValues']['defaultValue']));

        $template->setValue('diagnosis', ($this->diagnosis) ? ($this->diagnosis) : (Yii::$app->params['systemValues']['defaultValue']));
        $template->setValue('surveys', $this->inspectionToString($this->surveys));
        $template->setValue('treatment', ($this->treatment) ? ($this->treatment) : (Yii::$app->params['systemValues']['defaultValue']));*/

        return $template;
    }

    public function createSection($phpWord, $section) {
        $inspection_date = $this->inspection_date;

        if ($inspection_date) {
            $section->addText($this->attributeLabels()['name'], 'titleStyle', 'centeredParagraphStyle');
            $section->addText("от " . $inspection_date, 'textStyle', 'centeredParagraphStyle');
        }
    }
}
