<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;
use Arslanim\TemplateMapper\Interfaces\TemplateFillStrategy;
use Arslanim\TemplateMapper\Interfaces\CreateDocumentStrategy;

/**
 * This is the model class for table "result_inspection".
 *
 * @property integer $inspection_id
 * @property integer $patient_id
 * @property string $diagnosis
 * @property string $treatment
 *
 * @property Inspection $inspection
 * @property SurveyInspection[] $surveyInspections
 */
class ResultInspection extends \yii\db\ActiveRecord implements TemplateFillStrategy, CreateDocumentStrategy
{

    public $surveysIds = [];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'result_inspection';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['inspection_id', 'patient_id'], 'required'],
            [['inspection_id', 'patient_id'], 'integer'],
            [['diagnosis', 'treatment', 'survey_inspection_description'], 'string'],
            [['surveysIds', 'surveys'], 'safe'],
            [['inspection_id'], 'exist', 'skipOnError' => true, 'targetClass' => Inspection::className(), 'targetAttribute' => ['inspection_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'inspection_id' => 'Inspection ID',
            'patient_id' => 'Patient ID',
            'diagnosis' => 'Диагноз',
            'treatment' => 'Лечение',
	        'survey' => 'Обследования',
            'survey_inspection_description' => 'Обследование дополнительно'
        ];
    }

    public function setSurveys($surveysIds) {
        $this->surveysIds = $surveysIds;
    }

    public function getDropSurvey() {
        return ArrayHelper::map(Survey::find()->asArray()->all(), 'id', 'name');
    }

    public function getSurveyIds() {
       $this->surveysIds = ArrayHelper::getColumn($this->getSurveyInspections()->asArray()->all(), 'survey_id');
       return $this->surveysIds;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSurveys() {
        return $this->hasMany(Survey::className(), ['id' => 'survey_id'])
            ->via('surveyInspections');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInspection()
    {
        return $this->hasOne(Inspection::className(), ['id' => 'inspection_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSurveyInspections()
    {
        return $this->hasMany(SurveyInspection::className(), ['inspection_id' => 'inspection_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPatient()
    {
        return $this->hasOne(Patient::className(), ['id' => 'patient_id']);
    }

    protected function updateSurveyInspection() {
        SurveyInspection::deleteAll(array('inspection_id' => $this->inspection_id));
        if (is_array($this->surveysIds)) {
            foreach ($this->surveysIds as $id) {
                $surveyInspection = new SurveyInspection();
                $surveyInspection->inspection_id = $this->inspection_id;
                $surveyInspection->patient_id = $this->patient_id;
                $surveyInspection->survey_id = $id;
                $surveyInspection->save();
            }
        }
    }

    public function beforeDelete() {
        if (parent::beforeDelete()) {
            SurveyInspection::deleteAll('inspection_id = :inspection_id AND patient_id = :patient_id', [
                'inspection_id' => $this->inspection_id,
                'patient_id' => $this->patient_id
            ]);
            return true;
        } else {
            return false;
        }
    }

    public function afterSave($insert, $changedAttributes)
    {
        $this->updateSurveyInspection();
        parent::afterSave($insert, $changedAttributes);
    }

    public function inspectionToString($inspections) {
        $resultArray = [];
	$inspectionString = "";        
	foreach ($inspections as $inspection) {
            array_push($resultArray, $inspection->name);
        }
	if (count($resultArray)) {
	    $inspectionString = implode(", ", $resultArray) . '; ';
	}
	
	return $inspectionString;
    }

    public function mapObject($template) {

        $template->setValue('diagnosis', ($this->diagnosis) ? ($this->diagnosis) : (Yii::$app->params['systemValues']['hyphenValue']));
        $template->setValue('surveys', $this->inspectionToString($this->surveys));
        $template->setValue('treatment', ($this->treatment) ? ($this->treatment) : (Yii::$app->params['systemValues']['hyphenValue']));
	    $template->setValue('survey_inspection_description', $this->survey_inspection_description);

        return $template;
    }

    public function createSection($phpWord, $section) {
        $diagnosis = $this->diagnosis;
        $surveys = $this->inspectionToString($this->surveys);
        $treatment = $this->treatment;
        $survey_inspection_description = $this->survey_inspection_description;

        $inspection = $this->inspection;

        if ($diagnosis) {
            $textrun = $section->createTextRun('textParagraphStyle');
            $textrun->addText($this->attributeLabels()['diagnosis'] . ": ", 'inspectionPositionTextStyle');

            $textrun->addText($diagnosis, 'textStyle');
        }

        if ($surveys || $survey_inspection_description) {
            $textrun = $section->createTextRun('textParagraphStyle');
            $textrun->addText($this->attributeLabels()['survey'] . ": ", 'inspectionPositionTextStyle');

            $textrun->addText($surveys, 'textStyle');
            $textrun->addText($survey_inspection_description, 'textStyle');
        }

        if ($treatment) {
            $textrun = $section->createTextRun('textParagraphStyle');
            $textrun->addText($this->attributeLabels()['treatment'] . ": ", 'inspectionPositionTextStyle');

            $textrun->addText($treatment, 'textStyle');
        }
    }
}
