<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "kidneys_inspection".
 *
 * @property integer $other_inspection_id
 * @property integer $patient_id
 * @property integer $kidneys_status_id
 *
 * @property KidneysStatus $kidneysStatus
 * @property OtherInspection $otherInspection
 * @property OtherInspection $patient
 */
class KidneysInspection extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'kidneys_inspection';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['other_inspection_id', 'patient_id', 'kidneys_status_id'], 'required'],
            [['other_inspection_id', 'patient_id', 'kidneys_status_id'], 'integer'],
            [['kidneys_status_id'], 'exist', 'skipOnError' => true, 'targetClass' => KidneysStatus::className(), 'targetAttribute' => ['kidneys_status_id' => 'id']],
            [['other_inspection_id'], 'exist', 'skipOnError' => true, 'targetClass' => KidneysStatusInspection::className(), 'targetAttribute' => ['other_inspection_id' => 'inspection_id']],
            [['patient_id'], 'exist', 'skipOnError' => true, 'targetClass' => KidneysStatusInspection::className(), 'targetAttribute' => ['patient_id' => 'patient_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'other_inspection_id' => 'Other Inspection ID',
            'patient_id' => 'Patient ID',
            'kidneys_status_id' => 'Kidneys Status ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKidneysStatus()
    {
        return $this->hasOne(KidneysStatus::className(), ['id' => 'kidneys_status_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKidneysStatusInspection()
    {
        return $this->hasOne(KidneysStatusInspection::className(), ['inspection_id' => 'other_inspection_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPatient()
    {
        return $this->hasOne(OtherInspection::className(), ['patient_id' => 'patient_id']);
    }
}
