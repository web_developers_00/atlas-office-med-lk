<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;
use Arslanim\TemplateMapper\Interfaces\TemplateFillStrategy;
use Arslanim\TemplateMapper\Interfaces\CreateDocumentStrategy;

/**
 * This is the model class for table "stomach_status_inspection".
 *
 * @property integer $inspection_id
 * @property integer $patient_id
 * @property string $stomach_inspection_description
 *
 * @property StomachInspection[] $stomachInspections
 * @property Inspection $inspection
 */
class StomachStatusInspection extends \yii\db\ActiveRecord implements TemplateFillStrategy, CreateDocumentStrategy
{

    public $stomachStatusesIds = [];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'stomach_status_inspection';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['inspection_id', 'patient_id'], 'required'],
            [['inspection_id', 'patient_id'], 'integer'],
            [['stomach_inspection_description'], 'string'],
            [['inspection_id'], 'exist', 'skipOnError' => true, 'targetClass' => Inspection::className(), 'targetAttribute' => ['inspection_id' => 'id']],
            [['stomachStatusesIds', 'stomachStatuses'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'inspection_id' => 'Inspection ID',
            'patient_id' => 'Patient ID',
            'stomachStatuses' => 'Живот',
            'stomach_inspection_description' => 'Живот дополнительно',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStomachInspections()
    {
        return $this->hasMany(StomachInspection::className(), ['other_inspection_id' => 'inspection_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInspection()
    {
        return $this->hasOne(Inspection::className(), ['id' => 'inspection_id']);
    }

    public function setStomachStatuses($stomachStatusesIds) {
        $this->stomachStatusesIds = $stomachStatusesIds;
    }

    public function getDropStomachStatuses() {
        return ArrayHelper::map(StomachStatus::find()->asArray()->all(), 'id', 'name');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStomachStatuses() {
        return $this->hasMany(StomachStatus::className(), ['id' => 'stomach_status_id'])
            ->via('stomachInspections');
    }

    public function getStomachStatusesIds() {
       $this->stomachStatusesIds = ArrayHelper::getColumn($this->getStomachInspections()->asArray()->all(), 'stomach_status_id');
       return $this->stomachStatusesIds;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPatient()
    {
        return $this->hasOne(Patient::className(), ['id' => 'patient_id']);
    }

    protected function updateStomachInspection()
    {
        StomachInspection::deleteAll(array('other_inspection_id' => $this->inspection_id));
        if (is_array($this->stomachStatusesIds)) {
            foreach ($this->stomachStatusesIds as $id) {
                $stomachInspection = new StomachInspection();
                $stomachInspection->other_inspection_id = $this->inspection_id;
                $stomachInspection->patient_id = $this->patient_id;
                $stomachInspection->stomach_status_id = $id;
                $stomachInspection->save();
            }
        }
    }

    public function afterSave($insert, $changedAttributes)
    {
        $this->updateStomachInspection();
        
        parent::afterSave($insert, $changedAttributes);
    }

    public function beforeDelete() {
        if (parent::beforeDelete()) {
            StomachInspection::deleteAll('other_inspection_id = :inspection_id AND patient_id = :patient_id', [
                'inspection_id' => $this->inspection_id,
                'patient_id' => $this->patient_id
            ]);

            return true;
        } else {
            return false;
        }
    }

    public function inspectionToString($inspections) {
        $resultArray = [];
	$inspectionString = "";        
	foreach ($inspections as $inspection) {
            array_push($resultArray, $inspection->name);
        }
	if (count($resultArray)) {
	    $inspectionString = implode(", ", $resultArray) . '; ';
	}
	
	return $inspectionString;
    }

    public function mapObject($template) {
        $template->setValue('stomachStatuses', $this->inspectionToString($this->stomachStatuses));
        $template->setValue('stomach_inspection_description', $this->stomach_inspection_description);

        return $template;
    }

    public function createSection($phpWord, $section) {
        $stomachStatuses = $this->inspectionToString($this->stomachStatuses);
        $stomach_inspection_description = $this->stomach_inspection_description;

        if ($stomachStatuses || $stomach_inspection_description) {
            $textrun = $section->createTextRun('textParagraphStyle');
            $textrun->addText($this->attributeLabels()['stomachStatuses'] . ": ", 'inspectionPositionTextStyle');

            $textrun->addText($stomachStatuses, 'textStyle');
            $textrun->addText($stomach_inspection_description, 'textStyle');
        }
    }
}
