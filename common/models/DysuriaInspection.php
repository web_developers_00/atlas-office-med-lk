<?php

namespace common\models;

use Yii;
use Arslanim\TemplateMapper\Interfaces\TemplateFillStrategy;
use Arslanim\TemplateMapper\Interfaces\CreateDocumentStrategy;

/**
 * This is the model class for table "dysuria_inspection".
 *
 * @property integer $inspection_id
 * @property integer $patient_id
 * @property integer $dysuria
 *
 * @property Inspection $inspection
 */
class DysuriaInspection extends \yii\db\ActiveRecord implements TemplateFillStrategy, CreateDocumentStrategy
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'dysuria_inspection';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['inspection_id', 'patient_id'], 'required'],
            [['inspection_id', 'patient_id', 'dysuria'], 'integer'],
            [['inspection_id'], 'exist', 'skipOnError' => true, 'targetClass' => Inspection::className(), 'targetAttribute' => ['inspection_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'inspection_id' => 'Inspection ID',
            'patient_id' => 'Patient ID',
            'dysuria' => 'Дизурии',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInspection()
    {
        return $this->hasOne(Inspection::className(), ['id' => 'inspection_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPatient()
    {
        return $this->hasOne(Patient::className(), ['id' => 'patient_id']);
    }

    public function inspectionToString($inspections) {
        $resultArray = [];
        foreach ($inspections as $inspection) {
            array_push($resultArray, $inspection->name);
        }

        return (count($resultArray)) ? (implode(", ", $resultArray)) : (\Yii::$app->params['systemValues']['hyphenValue']);
    }

    public function mapObject($template) {
        $template->setValue('dysuria', isset($this->dysuria) ? (($this->dysuria) ? ('есть') : ('нет')) : (Yii::$app->params['systemValues']['defaultValue']));

        return $template;
    }

    public function createSection($phpWord, $section) {
        $dysuria = isset($this->dysuria) ? (($this->dysuria) ? ('есть') : ('нет')) : ("");

        if ($dysuria) {
            $textrun = $section->createTextRun('textParagraphStyle');
            $textrun->addText($this->attributeLabels()['dysuria'] . ": ", 'inspectionPositionTextStyle');

            $textrun->addText($dysuria, 'textStyle');
        }
    }
}
