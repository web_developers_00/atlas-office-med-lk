<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "survey_inspection".
 *
 * @property integer $survey_id
 * @property integer $patient_id
 * @property integer $inspection_id
 *
 * @property Inspection $inspection
 * @property Inspection $patient
 * @property Survey $survey
 */
class SurveyInspection extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'survey_inspection';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['survey_id', 'patient_id', 'inspection_id'], 'required'],
            [['survey_id', 'patient_id', 'inspection_id'], 'integer'],
            [['inspection_id'], 'exist', 'skipOnError' => true, 'targetClass' => ResultInspection::className(), 'targetAttribute' => ['inspection_id' => 'inspection_id']],
            [['patient_id'], 'exist', 'skipOnError' => true, 'targetClass' => ResultInspection::className(), 'targetAttribute' => ['patient_id' => 'patient_id']],
            [['survey_id'], 'exist', 'skipOnError' => true, 'targetClass' => Survey::className(), 'targetAttribute' => ['survey_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'survey_id' => 'Survey ID',
            'patient_id' => 'Patient ID',
            'inspection_id' => 'Inspection ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getResultInspection()
    {
        return $this->hasOne(ResultInspection::className(), ['inspection_id' => 'inspection_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPatient()
    {
        return $this->hasOne(Patient::className(), ['id' => 'patient_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSurvey()
    {
        return $this->hasOne(Survey::className(), ['id' => 'survey_id']);
    }
}
