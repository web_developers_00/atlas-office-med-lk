<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;
use Arslanim\TemplateMapper\Interfaces\TemplateFillStrategy;
use Arslanim\TemplateMapper\Interfaces\CreateDocumentStrategy;

/**
 * This is the model class for table "mammary_gland_status_inspection".
 *
 * @property integer $inspection_id
 * @property integer $patient_id
 * @property string $mammary_gland_inspection_description
 *
 * @property MammaryGlandInspection[] $mammaryGlandInspections
 * @property Inspection $inspection
 */
class MammaryGlandStatusInspection extends \yii\db\ActiveRecord implements TemplateFillStrategy, CreateDocumentStrategy
{
    public $mammaryGlandStatusesIds = [];
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mammary_gland_status_inspection';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['inspection_id', 'patient_id'], 'required'],
            [['inspection_id', 'patient_id'], 'integer'],
            [['mammary_gland_inspection_description'], 'string'],
            [['inspection_id'], 'exist', 'skipOnError' => true, 'targetClass' => Inspection::className(), 'targetAttribute' => ['inspection_id' => 'id']],
            [['mammaryGlandStatuses', 'mammaryGlandStatusesIds'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'inspection_id' => 'Inspection ID',
            'patient_id' => 'Patient ID',
            'mammaryGlandStatuses' => 'Молочные железы',
            'mammary_gland_inspection_description' => 'Дополнительное описание состояния молочных желез',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMammaryGlandInspections()
    {
        return $this->hasMany(MammaryGlandInspection::className(), ['general_inspection_id' => 'inspection_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInspection()
    {
        return $this->hasOne(Inspection::className(), ['id' => 'inspection_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPatient()
    {
        return $this->hasOne(Patient::className(), ['id' => 'patient_id']);
    }

    public function setMammaryGlandStatuses($mammaryGlandStatusesIds) {
        $this->mammaryGlandStatusesIds = $mammaryGlandStatusesIds;
    }

    public function getDropMammaryGlandStatuses() {
        return ArrayHelper::map(MammaryStatus::find()->asArray()->all(), 'id', 'name');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMammaryGlandStatuses() {
        return $this->hasMany(MammaryStatus::className(), ['id' => 'mammary_status_id'])
            ->via('mammaryGlandInspections');
    }

    public function getMammaryGlandStatusesIds() {
       $this->mammaryGlandStatusesIds = ArrayHelper::getColumn($this->getMammaryGlandInspections()->asArray()->all(), 'mammary_status_id');
       return $this->mammaryGlandStatusesIds;
    }

    protected function updateMammaryGlandInspection()
    {
        MammaryGlandInspection::deleteAll(array('general_inspection_id' => $this->inspection_id));
        if (is_array($this->mammaryGlandStatusesIds)) {
            foreach ($this->mammaryGlandStatusesIds as $id) {
                $mammaryGlandInspection = new MammaryGlandInspection();
                $mammaryGlandInspection->general_inspection_id = $this->inspection_id;
                $mammaryGlandInspection->patient_id = $this->patient_id;
                $mammaryGlandInspection->mammary_status_id = $id;
                $mammaryGlandInspection->save();
            }
        }
    }

    public function beforeDelete() {
        if (parent::beforeDelete()) {
            MammaryGlandInspection::deleteAll('general_inspection_id = :inspection_id AND patient_id = :patient_id', [
                'inspection_id' => $this->inspection_id,
                'patient_id' => $this->patient_id
            ]);
            return true;
        } else {
            return false;
        }
    }

    public function afterSave($insert, $changedAttributes)
    {
        $this->updateMammaryGlandInspection();
        parent::afterSave($insert, $changedAttributes);
    }

    public function inspectionToString($inspections) {
        $resultArray = [];
	$inspectionString = "";        
	foreach ($inspections as $inspection) {
            array_push($resultArray, $inspection->name);
        }
	if (count($resultArray)) {
	    $inspectionString = implode(", ", $resultArray) . '; ';
	}
	
	return $inspectionString;
    }

    public function mapObject($template) {
        $template->setValue('mammaryGlandStatuses', $this->inspectionToString($this->mammaryGlandStatuses));
        $template->setValue('mammary_gland_inspection_description', $this->mammary_gland_inspection_description);

        return $template;
    }

    public function createSection($phpWord, $section) {
        $mammaryGlandStatuses = $this->inspectionToString($this->mammaryGlandStatuses);
        $mammary_gland_inspection_description = $this->mammary_gland_inspection_description;

        if ($mammaryGlandStatuses || $mammary_gland_inspection_description) {
            
            $textrun = $section->createTextRun('textParagraphStyle');
            $textrun->addText($this->attributeLabels()['mammaryGlandStatuses'] . ": ", 'inspectionPositionTextStyle');

            $textrun->addText($mammaryGlandStatuses, 'textStyle');
            $textrun->addText($mammary_gland_inspection_description, 'textStyle');
        }
    }
}
