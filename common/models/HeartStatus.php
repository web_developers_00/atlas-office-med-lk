<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "heart_status".
 *
 * @property integer $id
 * @property string $name
 *
 * @property CardiovascularInspection[] $cardiovascularInspections
 */
class HeartStatus extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'heart_status';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            ['name', 'unique'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Сердце',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCardiovascularInspections()
    {
        return $this->hasMany(CardiovascularInspection::className(), ['heart_status_id' => 'id']);
    }

    public function beforeDelete() {
        if (parent::beforeDelete()) {

            if (CardiovascularInspection::find()->where('heart_status_id = :id', ['id' => $this->id])->exists()) {
                return false;
            } else {
                return true;
            }

        } else {
            return false;
        }
    }
}
