<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;
use Arslanim\TemplateMapper\Interfaces\TemplateFillStrategy;
use Arslanim\TemplateMapper\Interfaces\CreateDocumentStrategy;

/**
 * This is the model class for table "liver_status_inspection".
 *
 * @property integer $inspection_id
 * @property integer $patient_id
 * @property string $liver_inspection_description
 *
 * @property LiverInspection[] $liverInspections
 * @property Inspection $inspection
 */
class LiverStatusInspection extends \yii\db\ActiveRecord implements TemplateFillStrategy, CreateDocumentStrategy
{
    public $liverStatusesIds = [];
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'liver_status_inspection';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['inspection_id', 'patient_id'], 'required'],
            [['inspection_id', 'patient_id'], 'integer'],
            [['liver_inspection_description'], 'string'],
            [['inspection_id'], 'exist', 'skipOnError' => true, 'targetClass' => Inspection::className(), 'targetAttribute' => ['inspection_id' => 'id']],
            [['liverStatusesIds', 'liverStatuses'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'inspection_id' => 'Inspection ID',
            'patient_id' => 'Patient ID',
            'liverStatuses' => 'Печень',
            'liver_inspection_description' => 'Печень дополнительно',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLiverInspections()
    {
        return $this->hasMany(LiverInspection::className(), ['other_inspection_id' => 'inspection_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInspection()
    {
        return $this->hasOne(Inspection::className(), ['id' => 'inspection_id']);
    }

    public function setLiverStatuses($liverStatusesIds) {
        $this->liverStatusesIds = $liverStatusesIds;
    }

    public function getDropLiverStatuses() {
        return ArrayHelper::map(LiverStatus::find()->asArray()->all(), 'id', 'name');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLiverStatuses() {
        return $this->hasMany(LiverStatus::className(), ['id' => 'liver_status_id'])
            ->via('liverInspections');
    }

    public function getLiverStatusesIds() {
       $this->liverStatusesIds = ArrayHelper::getColumn($this->getLiverInspections()->asArray()->all(), 'liver_status_id');
       return $this->liverStatusesIds;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPatient()
    {
        return $this->hasOne(Patient::className(), ['id' => 'patient_id']);
    }

    protected function updateLiverInspection()
    {
        LiverInspection::deleteAll(array('other_inspection_id' => $this->inspection_id));
        if (is_array($this->liverStatusesIds)) {
            foreach ($this->liverStatusesIds as $id) {
                $liverInspection = new LiverInspection();
                $liverInspection->other_inspection_id = $this->inspection_id;
                $liverInspection->patient_id = $this->patient_id;
                $liverInspection->liver_status_id = $id;
                $liverInspection->save();
            }
        }
    }

    public function afterSave($insert, $changedAttributes)
    {
        $this->updateLiverInspection();
        parent::afterSave($insert, $changedAttributes);
    }

    public function beforeDelete() {
        if (parent::beforeDelete()) {
            LiverInspection::deleteAll('other_inspection_id = :inspection_id AND patient_id = :patient_id', [
                'inspection_id' => $this->inspection_id,
                'patient_id' => $this->patient_id
            ]);

            return true;
        } else {
            return false;
        }
    }

    public function inspectionToString($inspections) {
        $resultArray = [];
	$inspectionString = "";        
	foreach ($inspections as $inspection) {
            array_push($resultArray, $inspection->name);
        }
	if (count($resultArray)) {
	    $inspectionString = implode(", ", $resultArray) . '; ';
	}
	
	return $inspectionString;
    }

    public function mapObject($template) {
        $template->setValue('liverStatuses', $this->inspectionToString($this->liverStatuses));
        $template->setValue('liver_inspection_description', $this->liver_inspection_description);

        return $template;
    }

    public function createSection($phpWord, $section) {
        $liverStatuses = $this->inspectionToString($this->liverStatuses);
        $liver_inspection_description = $this->liver_inspection_description;

        if ($liverStatuses || $liver_inspection_description) {
            $textrun = $section->createTextRun('textParagraphStyle');
            $textrun->addText($this->attributeLabels()['liverStatuses'] . ": ", 'inspectionPositionTextStyle');

            $textrun->addText($liverStatuses, 'textStyle');
            $textrun->addText($liver_inspection_description, 'textStyle');
        }
    }
}
