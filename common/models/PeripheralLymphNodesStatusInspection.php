<?php

namespace common\models;

use Yii;
use Arslanim\TemplateMapper\Interfaces\TemplateFillStrategy;
use Arslanim\TemplateMapper\Interfaces\CreateDocumentStrategy;

/**
 * This is the model class for table "peripheral_lymph_nodes_status_inspection".
 *
 * @property integer $inspection_id
 * @property integer $patient_id
 * @property integer $peripheral_lymph_nodes_status_id
 *
 * @property PeripheralLymphNodesStatus $peripheralLymphNodesStatus
 * @property Inspection $inspection
 */
class PeripheralLymphNodesStatusInspection extends \yii\db\ActiveRecord implements TemplateFillStrategy, CreateDocumentStrategy
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'peripheral_lymph_nodes_status_inspection';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['inspection_id', 'patient_id'], 'required'],
            [['inspection_id', 'patient_id', 'peripheral_lymph_nodes_status_id'], 'integer'],
            [['peripheral_lymph_nodes_status_id'], 'exist', 'skipOnError' => true, 'targetClass' => PeripheralLymphNodesStatus::className(), 'targetAttribute' => ['peripheral_lymph_nodes_status_id' => 'id']],
            [['inspection_id'], 'exist', 'skipOnError' => true, 'targetClass' => Inspection::className(), 'targetAttribute' => ['inspection_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'inspection_id' => 'Inspection ID',
            'patient_id' => 'Patient ID',
            'peripheral_lymph_nodes_status_id' => 'Периферические лимфоузлы',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPeripheralLymphNodesStatus()
    {
        return $this->hasOne(PeripheralLymphNodesStatus::className(), ['id' => 'peripheral_lymph_nodes_status_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInspection()
    {
        return $this->hasOne(Inspection::className(), ['id' => 'inspection_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPatient()
    {
        return $this->hasOne(Patient::className(), ['id' => 'patient_id']);
    }

    public function mapObject($template) {
        $template->setValue('peripheral_lymph_nodes_status', isset($this->peripheralLymphNodesStatus) ? ($this->peripheralLymphNodesStatus->name) : (Yii::$app->params['systemValues']['hyphenValue']));

        return $template;
    }

    public function createSection($phpWord, $section) {
        $peripheral_lymph_nodes_status = isset($this->peripheralLymphNodesStatus) ? ($this->peripheralLymphNodesStatus->name) : ("");

        if ($peripheral_lymph_nodes_status) {
            $textrun = $section->createTextRun('textParagraphStyle');
            $textrun->addText($this->attributeLabels()['peripheral_lymph_nodes_status_id'] . ": ", 'inspectionPositionTextStyle');
            $textrun->addText($peripheral_lymph_nodes_status, 'textStyle');
        }
    }
}
