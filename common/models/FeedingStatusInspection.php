<?php

namespace common\models;

use Yii;
use Arslanim\TemplateMapper\Interfaces\TemplateFillStrategy;
use Arslanim\TemplateMapper\Interfaces\CreateDocumentStrategy;

/**
 * This is the model class for table "feeding_status_inspection".
 *
 * @property integer $inspection_id
 * @property integer $patient_id
 * @property integer $feeding_status_id
 *
 * @property FeedingStatus $feedingStatus
 * @property Inspection $inspection
 */
class FeedingStatusInspection extends \yii\db\ActiveRecord implements TemplateFillStrategy, CreateDocumentStrategy
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'feeding_status_inspection';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['inspection_id', 'patient_id'], 'required'],
            [['inspection_id', 'patient_id', 'feeding_status_id'], 'integer'],
            [['feeding_status_id'], 'exist', 'skipOnError' => true, 'targetClass' => FeedingStatus::className(), 'targetAttribute' => ['feeding_status_id' => 'id']],
            [['inspection_id'], 'exist', 'skipOnError' => true, 'targetClass' => Inspection::className(), 'targetAttribute' => ['inspection_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'inspection_id' => 'Inspection ID',
            'patient_id' => 'Patient ID',
            'feeding_status_id' => 'Питание',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFeedingStatus()
    {
        return $this->hasOne(FeedingStatus::className(), ['id' => 'feeding_status_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInspection()
    {
        return $this->hasOne(Inspection::className(), ['id' => 'inspection_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPatient()
    {
        return $this->hasOne(Patient::className(), ['id' => 'patient_id']);
    }

    public function mapObject($template) {
        $template->setValue('feeding_status', isset($this->feedingStatus) ? ($this->feedingStatus->name) : (Yii::$app->params['systemValues']['hyphenValue']));

        return $template;
    }

    public function createSection($phpWord, $section) {
        $feedingStatus = $this->feedingStatus;

        if ($feedingStatus) {
            $textrun = $section->createTextRun('textParagraphStyle');
            $textrun->addText($feedingStatus->attributeLabels()['name'] . ": ", 'inspectionPositionTextStyle');
            $textrun->addText($feedingStatus->name, 'textStyle');
        }
    }
}
