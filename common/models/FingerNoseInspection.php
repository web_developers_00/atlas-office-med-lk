<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "finger_nose_inspection".
 *
 * @property integer $neurological_inspection_id
 * @property integer $patient_id
 * @property integer $finger_nose_status_id
 *
 * @property FingerNoseStatus $fingerNoseStatus
 * @property NeurologicalInspection $neurologicalInspection
 * @property NeurologicalInspection $patient
 */
class FingerNoseInspection extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'finger_nose_inspection';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['neurological_inspection_id', 'patient_id', 'finger_nose_status_id'], 'required'],
            [['neurological_inspection_id', 'patient_id', 'finger_nose_status_id'], 'integer'],
            [['finger_nose_status_id'], 'exist', 'skipOnError' => true, 'targetClass' => FingerNoseStatus::className(), 'targetAttribute' => ['finger_nose_status_id' => 'id']],
            [['neurological_inspection_id'], 'exist', 'skipOnError' => true, 'targetClass' => NeurologicalInspection::className(), 'targetAttribute' => ['neurological_inspection_id' => 'inspection_id']],
            [['patient_id'], 'exist', 'skipOnError' => true, 'targetClass' => NeurologicalInspection::className(), 'targetAttribute' => ['patient_id' => 'patient_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'neurological_inspection_id' => 'Neurological Inspection ID',
            'patient_id' => 'Patient ID',
            'finger_nose_status_id' => 'Finger Nose Status ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFingerNoseStatus()
    {
        return $this->hasOne(FingerNoseStatus::className(), ['id' => 'finger_nose_status_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNeurologicalInspection()
    {
        return $this->hasOne(NeurologicalInspection::className(), ['inspection_id' => 'neurological_inspection_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPatient()
    {
        return $this->hasOne(NeurologicalInspection::className(), ['patient_id' => 'patient_id']);
    }
}
