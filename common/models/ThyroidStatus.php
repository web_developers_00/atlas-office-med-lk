<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "thyroid_status".
 *
 * @property integer $id
 * @property string $name
 *
 * @property ThyroidInspection[] $thyroidInspections
 */
class ThyroidStatus extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'thyroid_status';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            ['name', 'unique'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Щитовидная железа',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getThyroidInspections()
    {
        return $this->hasMany(ThyroidInspection::className(), ['thyroid_status_id' => 'id']);
    }

    public function beforeDelete() {
        if (parent::beforeDelete()) {

            if (ThyroidInspection::find()->where('thyroid_status_id = :id', ['id' => $this->id])->exists()) {
                return false;
            } else {
                return true;
            }

        } else {
            return false;
        }
    }
}
