<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "osteoarticular_inspection".
 *
 * @property integer $general_inspection_id
 * @property integer $patient_id
 * @property integer $osteoarticular_system_status_id
 *
 * @property GeneralInspection $generalInspection
 * @property OsteoarticularSystemStatus $osteoarticularSystemStatus
 * @property GeneralInspection $patient
 */
class OsteoarticularInspection extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'osteoarticular_inspection';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['general_inspection_id', 'patient_id', 'osteoarticular_system_status_id'], 'required'],
            [['general_inspection_id', 'patient_id', 'osteoarticular_system_status_id'], 'integer'],
            [['general_inspection_id'], 'exist', 'skipOnError' => true, 'targetClass' => OsteoarticularStatusInspection::className(), 'targetAttribute' => ['general_inspection_id' => 'inspection_id']],
            [['osteoarticular_system_status_id'], 'exist', 'skipOnError' => true, 'targetClass' => OsteoarticularSystemStatus::className(), 'targetAttribute' => ['osteoarticular_system_status_id' => 'id']],
            [['patient_id'], 'exist', 'skipOnError' => true, 'targetClass' => OsteoarticularStatusInspection::className(), 'targetAttribute' => ['patient_id' => 'patient_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'general_inspection_id' => 'General Inspection ID',
            'patient_id' => 'Patient ID',
            'osteoarticular_system_status_id' => 'Osteoarticular System Status ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOsteoarticularStatusInspection()
    {
        return $this->hasOne(OsteoarticularStatusInspection::className(), ['inspection_id' => 'general_inspection_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOsteoarticularSystemStatus()
    {
        return $this->hasOne(OsteoarticularSystemStatus::className(), ['id' => 'osteoarticular_system_status_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPatient()
    {
        return $this->hasOne(Patient::className(), ['patient_id' => 'patient_id']);
    }
}
