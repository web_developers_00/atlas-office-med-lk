<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "stomach_inspection".
 *
 * @property integer $other_inspection_id
 * @property integer $patient_id
 * @property integer $stomach_status_id
 *
 * @property OtherInspection $otherInspection
 * @property OtherInspection $patient
 * @property StomachStatus $stomachStatus
 */
class StomachInspection extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'stomach_inspection';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['other_inspection_id', 'patient_id', 'stomach_status_id'], 'required'],
            [['other_inspection_id', 'patient_id', 'stomach_status_id'], 'integer'],
            [['other_inspection_id'], 'exist', 'skipOnError' => true, 'targetClass' => StomachStatusInspection::className(), 'targetAttribute' => ['other_inspection_id' => 'inspection_id']],
            [['patient_id'], 'exist', 'skipOnError' => true, 'targetClass' => StomachStatusInspection::className(), 'targetAttribute' => ['patient_id' => 'patient_id']],
            [['stomach_status_id'], 'exist', 'skipOnError' => true, 'targetClass' => StomachStatus::className(), 'targetAttribute' => ['stomach_status_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'other_inspection_id' => 'Other Inspection ID',
            'patient_id' => 'Patient ID',
            'stomach_status_id' => 'Stomach Status ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStomachStatusInspection()
    {
        return $this->hasOne(StomachStatusInspection::className(), ['inspection_id' => 'other_inspection_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPatient()
    {
        return $this->hasOne(Patient::className(), ['id' => 'patient_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStomachStatus()
    {
        return $this->hasOne(StomachStatus::className(), ['id' => 'stomach_status_id']);
    }
}
