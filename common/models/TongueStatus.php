<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "tongue_status".
 *
 * @property integer $id
 * @property string $name
 *
 * @property TongueInspection[] $tongueInspections
 */
class TongueStatus extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tongue_status';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            ['name', 'unique'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Язык',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTongueInspections()
    {
        return $this->hasMany(TongueInspection::className(), ['tongue_status_id' => 'id']);
    }

    public function beforeDelete() {
        if (parent::beforeDelete()) {

            if (TongueInspection::find()->where('tongue_status_id = :id', ['id' => $this->id])->exists()) {
                return false;
            } else {
                return true;
            }

        } else {
            return false;
        }
    }
}
