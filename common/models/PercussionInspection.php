<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "percussion_inspection".
 *
 * @property integer $lungs_inspection_id
 * @property integer $patient_id
 * @property integer $percussion_status_id
 *
 * @property LungsInspection $lungsInspection
 * @property LungsInspection $patient
 * @property PercussionStatus $percussionStatus
 */
class PercussionInspection extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'percussion_inspection';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['lungs_inspection_id', 'patient_id', 'percussion_status_id'], 'required'],
            [['lungs_inspection_id', 'patient_id', 'percussion_status_id'], 'integer'],
            [['lungs_inspection_id'], 'exist', 'skipOnError' => true, 'targetClass' => LungsInspection::className(), 'targetAttribute' => ['lungs_inspection_id' => 'inspection_id']],
            [['patient_id'], 'exist', 'skipOnError' => true, 'targetClass' => LungsInspection::className(), 'targetAttribute' => ['patient_id' => 'patient_id']],
            [['percussion_status_id'], 'exist', 'skipOnError' => true, 'targetClass' => PercussionStatus::className(), 'targetAttribute' => ['percussion_status_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'lungs_inspection_id' => 'Lungs Inspection ID',
            'patient_id' => 'Patient ID',
            'percussion_status_id' => 'Percussion Status ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLungsInspection()
    {
        return $this->hasOne(LungsInspection::className(), ['inspection_id' => 'lungs_inspection_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPatient()
    {
        return $this->hasOne(LungsInspection::className(), ['patient_id' => 'patient_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPercussionStatus()
    {
        return $this->hasOne(PercussionStatus::className(), ['id' => 'percussion_status_id']);
    }
}
