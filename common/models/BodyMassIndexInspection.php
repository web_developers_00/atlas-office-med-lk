<?php

namespace common\models;

use Yii;
use Arslanim\TemplateMapper\Interfaces\TemplateFillStrategy;
use Arslanim\TemplateMapper\Interfaces\CreateDocumentStrategy;

/**
 * This is the model class for table "body_mass_index_inspection".
 *
 * @property integer $inspection_id
 * @property integer $patient_id
 * @property double $body_mass_index
 *
 * @property Inspection $inspection
 */
class BodyMassIndexInspection extends \yii\db\ActiveRecord implements TemplateFillStrategy, CreateDocumentStrategy
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'body_mass_index_inspection';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['inspection_id', 'patient_id'], 'required'],
            [['inspection_id', 'patient_id'], 'integer'],
            [['body_mass_index'], 'number'],
            [['inspection_id'], 'exist', 'skipOnError' => true, 'targetClass' => Inspection::className(), 'targetAttribute' => ['inspection_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'inspection_id' => 'Inspection ID',
            'patient_id' => 'Patient ID',
            'body_mass_index' => 'Индекс массы тела',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInspection()
    {
        return $this->hasOne(Inspection::className(), ['id' => 'inspection_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPatient()
    {
        return $this->hasOne(Patient::className(), ['id' => 'patient_id']);
    }

    public function mapObject($template) {
        $template->setValue('body_mass_index', isset($this->body_mass_index) ? ($this->body_mass_index) : (Yii::$app->params['systemValues']['hyphenValue']));

        return $template;
    }

    public function createSection($phpWord, $section) {
        $bodyMassIndex = $this->body_mass_index;

        if ($bodyMassIndex) {
            $textrun = $section->createTextRun('textParagraphStyle');
            $textrun->addText($this->attributeLabels()['body_mass_index'] . ": ", 'inspectionPositionTextStyle');
            $textrun->addText($bodyMassIndex, 'textStyle');
        }
    }
}
