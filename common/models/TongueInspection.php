<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "tongue_inspection".
 *
 * @property integer $general_inspection_id
 * @property integer $patient_id
 * @property integer $tongue_status_id
 *
 * @property GeneralInspection $generalInspection
 * @property GeneralInspection $patient
 * @property TongueStatus $tongueStatus
 */
class TongueInspection extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tongue_inspection';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['general_inspection_id', 'patient_id', 'tongue_status_id'], 'required'],
            [['general_inspection_id', 'patient_id', 'tongue_status_id'], 'integer'],
            [['general_inspection_id'], 'exist', 'skipOnError' => true, 'targetClass' => TongueStatusInspection::className(), 'targetAttribute' => ['general_inspection_id' => 'inspection_id']],
            [['patient_id'], 'exist', 'skipOnError' => true, 'targetClass' => TongueStatusInspection::className(), 'targetAttribute' => ['patient_id' => 'patient_id']],
            [['tongue_status_id'], 'exist', 'skipOnError' => true, 'targetClass' => TongueStatus::className(), 'targetAttribute' => ['tongue_status_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'general_inspection_id' => 'General Inspection ID',
            'patient_id' => 'Patient ID',
            'tongue_status_id' => 'Tongue Status ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTongueStatusInspection()
    {
        return $this->hasOne(TongueStatusInspection::className(), ['inspection_id' => 'general_inspection_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPatient()
    {
        return $this->hasOne(Patient::className(), ['patient_id' => 'patient_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTongueStatus()
    {
        return $this->hasOne(TongueStatus::className(), ['id' => 'tongue_status_id']);
    }
}
