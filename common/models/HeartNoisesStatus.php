<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "heart_noises_status".
 *
 * @property integer $id
 * @property string $name
 *
 * @property HeartNoisesInspection[] $heartNoisesInspections
 */
class HeartNoisesStatus extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'heart_noises_status';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            ['name', 'unique'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Шумы сердца',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHeartNoisesInspections()
    {
        return $this->hasMany(HeartNoisesInspection::className(), ['heart_noises_status_id' => 'id']);
    }

    public function beforeDelete() {
        if (parent::beforeDelete()) {

            if (HeartNoisesInspection::find()->where('heart_noises_status_id = :id', ['id' => $this->id])->exists()) {
                return false;
            } else {
                return true;
            }

        } else {
            return false;
        }
    }
}
