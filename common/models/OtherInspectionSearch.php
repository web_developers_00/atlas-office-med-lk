<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\OtherInspection;

/**
 * OtherInspectionSearch represents the model behind the search form about `common\models\OtherInspection`.
 */
class OtherInspectionSearch extends OtherInspection
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['inspection_id', 'patient_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = OtherInspection::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'inspection_id' => $this->inspection_id,
            'patient_id' => $this->patient_id,
        ]);

        return $dataProvider;
    }
}
