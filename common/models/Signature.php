<?php

namespace common\models;

use Arslanim\TemplateMapper\Interfaces\TemplateFillStrategy;
use Arslanim\TemplateMapper\Interfaces\CreateDocumentStrategy;

class Signature implements TemplateFillStrategy, CreateDocumentStrategy
{
    protected $signerName;
    protected $signDate;

    public function __construct($signerName = null, $signDate = null) {
        
        $this->signerName = $signerName;
        $this->signDate = $signDate;
    }

    public function setSignerName($signerName) {
        $this->signerName = $signerName;
    }

    public function setSignDate($signDate) {
        $this->signDate = $signDate;
    }

    public function getSignerName() {
        return $this->signerName;
    }

    public function getSignDate() {
        return $this->signDate;
    }

    public function mapObject($template) {

        return $template;
    }

    public function createSection($phpWord, $section) {
        if ($this->signerName && $this->signDate) {
            $section->addText(htmlspecialchars($this->signDate . "\t" . "___________" . "/" . $this->signerName), null, 'rightTab');
        }

        //$signSection->addText(htmlspecialchars("Multiple Tabs:\tOne\tTwo\tThree"), null, 'multipleTab');
        //$signSection->addText(htmlspecialchars("\tCenter Aligned"), null, 'centerTab');
    }
}
