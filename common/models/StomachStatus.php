<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "stomach_status".
 *
 * @property integer $id
 * @property string $name
 *
 * @property StomachInspection[] $stomachInspections
 */
class StomachStatus extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'stomach_status';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            ['name', 'unique'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Живот',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStomachInspections()
    {
        return $this->hasMany(StomachInspection::className(), ['stomach_status_id' => 'id']);
    }

    public function beforeDelete() {
        if (parent::beforeDelete()) {

            if (StomachInspection::find()->where('stomach_status_id = :id', ['id' => $this->id])->exists()) {
                return false;
            } else {
                return true;
            }

        } else {
            return false;
        }
    }
}
