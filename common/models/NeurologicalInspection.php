<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;
use Arslanim\TemplateMapper\Interfaces\TemplateFillStrategy;
use Arslanim\TemplateMapper\Interfaces\CreateDocumentStrategy;

/**
 * This is the model class for table "neurological_inspection".
 *
 * @property integer $inspection_id
 * @property integer $patient_id
 * @property integer $pupils_diff_status_id
 * @property integer $pupils_reaction_status_id
 * @property string $finger_nose_inspection_description
 * @property string $extremities_movement_inspection_description
 *
 * @property ExtremitiesMovementInspection[] $extremitiesMovementInspections
 * @property ExtremitiesMovementInspection[] $extremitiesMovementInspections0
 * @property FingerNoseInspection[] $fingerNoseInspections
 * @property FingerNoseInspection[] $fingerNoseInspections0
 * @property Inspection $inspection
 * @property Inspection $patient
 * @property PupilsDiffStatus $pupilsDiffStatus
 * @property PupilsReactionStatus $pupilsReactionStatus
 */
class NeurologicalInspection extends \yii\db\ActiveRecord implements TemplateFillStrategy, CreateDocumentStrategy
{
    public $fingerNoseStatusesIds = [];
    public $extremitiesMovementStatusesIds = [];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'neurological_inspection';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['inspection_id', 'patient_id', /*'pupils_diff_status_id', 'pupils_reaction_status_id'*/], 'required'],
            [['inspection_id', 'patient_id', 'pupils_diff_status_id', 'pupils_reaction_status_id'], 'integer'],
            [['finger_nose_inspection_description', 'extremities_movement_inspection_description'], 'string'],
            [['inspection_id'], 'exist', 'skipOnError' => true, 'targetClass' => Inspection::className(), 'targetAttribute' => ['inspection_id' => 'id']],
            [['patient_id'], 'exist', 'skipOnError' => true, 'targetClass' => Inspection::className(), 'targetAttribute' => ['patient_id' => 'patient_id']],
            [['pupils_diff_status_id'], 'exist', 'skipOnError' => true, 'targetClass' => PupilsDiffStatus::className(), 'targetAttribute' => ['pupils_diff_status_id' => 'id']],
            [['pupils_reaction_status_id'], 'exist', 'skipOnError' => true, 'targetClass' => PupilsReactionStatus::className(), 'targetAttribute' => ['pupils_reaction_status_id' => 'id']],
            [['fingerNoseStatuses', 'fingerNoseStatusesIds', 'extremitiesMovementStatuses', 'extremitiesMovementStatusesIds'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'inspection_id' => 'Inspection ID',
            'patient_id' => 'Patient ID',
            'pupils_diff_status_id' => 'Разница зрачков',
            'pupils_reaction_status_id' => 'Реакция зрачков',
            'fingerNoseStatuses' => 'Пальценосовой проба',
            'finger_nose_inspection_description' => 'Дополнительное описание выполнения пальценосовой пробы',
            'extremitiesMovementStatuses' => 'Движения в конечностях',
            'extremities_movement_inspection_description' => 'Дополнительное описание статуса движения в конечностях',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExtremitiesMovementInspections()
    {
        return $this->hasMany(ExtremitiesMovementInspection::className(), ['neurological_inspection_id' => 'inspection_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExtremitiesMovementInspections0()
    {
        return $this->hasMany(ExtremitiesMovementInspection::className(), ['patient_id' => 'patient_id']);
    }

    public function setExtremitiesMovementStatuses($extremitiesMovementStatusesIds) {
        $this->extremitiesMovementStatusesIds = $extremitiesMovementStatusesIds;
    }

    public function getDropExtremitiesMovementStatuses() {
        return ArrayHelper::map(ExtremitiesMovementStatus::find()->asArray()->all(), 'id', 'name');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExtremitiesMovementStatuses() {
        return $this->hasMany(ExtremitiesMovementStatus::className(), ['id' => 'extremities_movement_status_id'])
            ->via('extremitiesMovementInspections');
    }

    public function getExtremitiesMovementStatusesIds() {
       $this->extremitiesMovementStatusesIds = ArrayHelper::getColumn($this->getExtremitiesMovementInspections()->asArray()->all(), 'extremities_movement_status_id');
       return $this->extremitiesMovementStatusesIds;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFingerNoseInspections()
    {
        return $this->hasMany(FingerNoseInspection::className(), ['neurological_inspection_id' => 'inspection_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFingerNoseInspections0()
    {
        return $this->hasMany(FingerNoseInspection::className(), ['patient_id' => 'patient_id']);
    }

    public function setFingerNoseStatuses($fingerNoseStatusesIds) {
        $this->fingerNoseStatusesIds = $fingerNoseStatusesIds;
    }

    public function getDropFingerNoseStatuses() {
        return ArrayHelper::map(FingerNoseStatus::find()->asArray()->all(), 'id', 'name');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFingerNoseStatuses() {
        return $this->hasMany(FingerNoseStatus::className(), ['id' => 'finger_nose_status_id'])
            ->via('fingerNoseInspections');
    }

    public function getFingerNoseStatusesIds() {
       $this->fingerNoseStatusesIds = ArrayHelper::getColumn($this->getFingerNoseInspections()->asArray()->all(), 'finger_nose_status_id');
       return $this->fingerNoseStatusesIds;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInspection()
    {
        return $this->hasOne(Inspection::className(), ['id' => 'inspection_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPatient()
    {
        return $this->hasOne(Patient::className(), ['id' => 'patient_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPupilsDiffStatus()
    {
        return $this->hasOne(PupilsDiffStatus::className(), ['id' => 'pupils_diff_status_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPupilsReactionStatus()
    {
        return $this->hasOne(PupilsReactionStatus::className(), ['id' => 'pupils_reaction_status_id']);
    }

    public function beforeDelete() {
        if (parent::beforeDelete()) {
            FingerNoseInspection::deleteAll('neurological_inspection_id = :neurological_inspection_id AND patient_id = :patient_id', [
                'neurological_inspection_id' => $this->inspection_id,
                'patient_id' => $this->patient_id
            ]);
            ExtremitiesMovementInspection::deleteAll('neurological_inspection_id = :neurological_inspection_id AND patient_id = :patient_id', [
                'neurological_inspection_id' => $this->inspection_id,
                'patient_id' => $this->patient_id
            ]);
            return true;
        } else {
            return false;
        }
    }

    public function afterSave($insert, $changedAttributes)
    {
        $this->updateFingerNoseInspection();
        $this->updateExtremitiesMovementInspection();
        parent::afterSave($insert, $changedAttributes);
    }

    protected function updateFingerNoseInspection()
    {
        FingerNoseInspection::deleteAll(array('neurological_inspection_id' => $this->inspection_id));
        if (is_array($this->fingerNoseStatusesIds)) {
            foreach ($this->fingerNoseStatusesIds as $id) {
                $fingerNoseInspection = new FingerNoseInspection();
                $fingerNoseInspection->neurological_inspection_id = $this->inspection_id;
                $fingerNoseInspection->patient_id = $this->patient_id;
                $fingerNoseInspection->finger_nose_status_id = $id;
                $fingerNoseInspection->save();
            }
        }
    }

    protected function updateExtremitiesMovementInspection()
    {
        ExtremitiesMovementInspection::deleteAll(array('neurological_inspection_id' => $this->inspection_id));
        if (is_array($this->extremitiesMovementStatusesIds)) {
            foreach ($this->extremitiesMovementStatusesIds as $id) {
                $extremitiesMovementInspection = new ExtremitiesMovementInspection();
                $extremitiesMovementInspection->neurological_inspection_id = $this->inspection_id;
                $extremitiesMovementInspection->patient_id = $this->patient_id;
                $extremitiesMovementInspection->extremities_movement_status_id = $id;
                $extremitiesMovementInspection->save();
            }
        }
    }

    public function inspectionToString($inspections) {
        $resultArray = [];
        foreach ($inspections as $inspection) {
            array_push($resultArray, $inspection->name);
        }

        return (count($resultArray)) ? (implode(", ", $resultArray)) : ("");
    }

    public function mapObject($template) {
        $template->setValue('pupils_diff_status', isset($this->pupilsDiffStatus) ? ($this->pupilsDiffStatus->name) : (Yii::$app->params['systemValues']['hyphenValue']));
        $template->setValue('pupils_reaction_status', isset($this->pupilsReactionStatus) ? ($this->pupilsReactionStatus->name) : (Yii::$app->params['systemValues']['hyphenValue']));
        $template->setValue('fingerNoseStatuses', $this->inspectionToString($this->fingerNoseStatuses));
        $template->setValue('extremitiesMovementStatuses', $this->inspectionToString($this->extremitiesMovementStatuses));

        return $template;
    }

    public function createSection($phpWord, $section) {
        $pupils_diff_status = isset($this->pupilsDiffStatus) ? ($this->pupilsDiffStatus->name) : ("");
        $pupils_reaction_status = isset($this->pupilsReactionStatus) ? ($this->pupilsReactionStatus->name) : ("");
        $fingerNoseStatuses = $this->inspectionToString($this->fingerNoseStatuses);
        $extremitiesMovementStatuses = $this->inspectionToString($this->extremitiesMovementStatuses);

        if ($pupils_diff_status) {
            $textrun = $section->createTextRun('textParagraphStyle');
            $textrun->addText($this->attributeLabels()['pupils_diff_status_id'] . ": ", 'inspectionPositionTextStyle');
            $textrun->addText($pupils_diff_status, 'textStyle');
        }

        if ($pupils_reaction_status) {
            $textrun = $section->createTextRun('textParagraphStyle');
            $textrun->addText($this->attributeLabels()['pupils_reaction_status_id'] . ": ", 'inspectionPositionTextStyle');
            $textrun->addText($pupils_reaction_status, 'textStyle');
        }

        if ($fingerNoseStatuses) {
            $textrun = $section->createTextRun('textParagraphStyle');
            $textrun->addText($this->attributeLabels()['fingerNoseStatuses'] . ": ", 'inspectionPositionTextStyle');
            $textrun->addText($fingerNoseStatuses, 'textStyle');
        }

        if ($extremitiesMovementStatuses) {
            $textrun = $section->createTextRun('textParagraphStyle');
            $textrun->addText($this->attributeLabels()['extremitiesMovementStatuses'] . ": ", 'inspectionPositionTextStyle');
            $textrun->addText($extremitiesMovementStatuses, 'textStyle');
        }
    }
}
