<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "auscultation_inspection".
 *
 * @property integer $lungs_inspection_id
 * @property integer $patient_id
 * @property integer $lungs_auscultation_status_id
 *
 * @property LungsAuscultationStatus $lungsAuscultationStatus
 * @property LungsInspection $lungsInspection
 * @property LungsInspection $patient
 */
class AuscultationInspection extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'auscultation_inspection';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['lungs_inspection_id', 'patient_id', 'lungs_auscultation_status_id'], 'required'],
            [['lungs_inspection_id', 'patient_id', 'lungs_auscultation_status_id'], 'integer'],
            [['lungs_auscultation_status_id'], 'exist', 'skipOnError' => true, 'targetClass' => LungsAuscultationStatus::className(), 'targetAttribute' => ['lungs_auscultation_status_id' => 'id']],
            [['lungs_inspection_id'], 'exist', 'skipOnError' => true, 'targetClass' => LungsInspection::className(), 'targetAttribute' => ['lungs_inspection_id' => 'inspection_id']],
            [['patient_id'], 'exist', 'skipOnError' => true, 'targetClass' => LungsInspection::className(), 'targetAttribute' => ['patient_id' => 'patient_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'lungs_inspection_id' => 'Lungs Inspection ID',
            'patient_id' => 'Patient ID',
            'lungs_auscultation_status_id' => 'Lungs Auscultation Status ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLungsAuscultationStatus()
    {
        return $this->hasOne(LungsAuscultationStatus::className(), ['id' => 'lungs_auscultation_status_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLungsInspection()
    {
        return $this->hasOne(LungsInspection::className(), ['inspection_id' => 'lungs_inspection_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPatient()
    {
        return $this->hasOne(LungsInspection::className(), ['patient_id' => 'patient_id']);
    }
}
