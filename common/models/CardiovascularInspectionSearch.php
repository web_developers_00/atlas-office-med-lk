<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\CardiovascularInspection;

/**
 * CardiovascularInspectionSearch represents the model behind the search form about `common\models\CardiovascularInspection`.
 */
class CardiovascularInspectionSearch extends CardiovascularInspection
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['inspection_id', 'patient_id', 'heart_status_id'], 'integer'],
            [['heart_rate', 'left_blood_pressure', 'right_blood_pressure'], 'number'],
            [['heart_tones_inspection_description', 'heart_tones_accent_inspection_description', 'heart_noises_inspection_description'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CardiovascularInspection::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'inspection_id' => $this->inspection_id,
            'patient_id' => $this->patient_id,
            'heart_status_id' => $this->heart_status_id,
            'heart_rate' => $this->heart_rate,
            'left_blood_pressure' => $this->left_blood_pressure,
            'right_blood_pressure' => $this->right_blood_pressure,
        ]);

        $query->andFilterWhere(['like', 'heart_tones_inspection_description', $this->heart_tones_inspection_description])
            ->andFilterWhere(['like', 'heart_tones_accent_inspection_description', $this->heart_tones_accent_inspection_description])
            ->andFilterWhere(['like', 'heart_noises_inspection_description', $this->heart_noises_inspection_description]);

        return $dataProvider;
    }
}
