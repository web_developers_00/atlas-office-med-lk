<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "spleen_inspection".
 *
 * @property integer $other_inspection_id
 * @property integer $patient_id
 * @property integer $spleen_status_id
 *
 * @property OtherInspection $otherInspection
 * @property OtherInspection $patient
 * @property SpleenStatus $spleenStatus
 */
class SpleenInspection extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'spleen_inspection';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['other_inspection_id', 'patient_id', 'spleen_status_id'], 'required'],
            [['other_inspection_id', 'patient_id', 'spleen_status_id'], 'integer'],
            [['other_inspection_id'], 'exist', 'skipOnError' => true, 'targetClass' => SpleenStatusInspection::className(), 'targetAttribute' => ['other_inspection_id' => 'inspection_id']],
            [['patient_id'], 'exist', 'skipOnError' => true, 'targetClass' => SpleenStatusInspection::className(), 'targetAttribute' => ['patient_id' => 'patient_id']],
            [['spleen_status_id'], 'exist', 'skipOnError' => true, 'targetClass' => SpleenStatus::className(), 'targetAttribute' => ['spleen_status_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'other_inspection_id' => 'Other Inspection ID',
            'patient_id' => 'Patient ID',
            'spleen_status_id' => 'Spleen Status ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSpleenStatusInspection()
    {
        return $this->hasOne(SpleenStatusInspection::className(), ['inspection_id' => 'other_inspection_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPatient()
    {
        return $this->hasOne(Patient::className(), ['id' => 'patient_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSpleenStatus()
    {
        return $this->hasOne(SpleenStatus::className(), ['id' => 'spleen_status_id']);
    }
}
