<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "pupils_reaction_status".
 *
 * @property integer $id
 * @property string $name
 *
 * @property NeurologicalInspection[] $neurologicalInspections
 */
class PupilsReactionStatus extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pupils_reaction_status';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            ['name', 'unique'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Реакция зрачков',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNeurologicalInspections()
    {
        return $this->hasMany(NeurologicalInspection::className(), ['pupils_reaction_status_id' => 'id']);
    }

    public function beforeDelete() {
        if (parent::beforeDelete()) {

            if (NeurologicalInspection::find()->where('pupils_reaction_status_id = :id', ['id' => $this->id])->exists()) {
                return false;
            } else {
                return true;
            }

        } else {
            return false;
        }
    }
}
