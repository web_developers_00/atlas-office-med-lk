<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;
use Arslanim\TemplateMapper\Interfaces\TemplateFillStrategy;
use Arslanim\TemplateMapper\Interfaces\CreateDocumentStrategy;

/**
 * This is the model class for table "thyroid_status_inspection".
 *
 * @property integer $inspection_id
 * @property integer $patient_id
 * @property string $thyroid_inspection_description
 *
 * @property ThyroidInspection[] $thyroidInspections
 * @property Inspection $inspection
 */
class ThyroidStatusInspection extends \yii\db\ActiveRecord implements TemplateFillStrategy, CreateDocumentStrategy
{
    public $thyroidStatusesIds = [];
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'thyroid_status_inspection';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['inspection_id', 'patient_id'], 'required'],
            [['inspection_id', 'patient_id'], 'integer'],
            [['thyroid_inspection_description'], 'string'],
            [['thyroidStatuses', 'thyroidStatusesIds'], 'safe'],
            [['inspection_id'], 'exist', 'skipOnError' => true, 'targetClass' => Inspection::className(), 'targetAttribute' => ['inspection_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'inspection_id' => 'Inspection ID',
            'patient_id' => 'Patient ID',
            'thyroidStatuses' => 'Щитовидная железа',
            'thyroid_inspection_description' => 'Дополнительное описание состояния щитовидной железы',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getThyroidInspections()
    {
        return $this->hasMany(ThyroidInspection::className(), ['general_inspection_id' => 'inspection_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInspection()
    {
        return $this->hasOne(Inspection::className(), ['id' => 'inspection_id']);
    }

    public function setThyroidStatuses($thyroidStatusesIds) {
        $this->thyroidStatusesIds = $thyroidStatusesIds;
    }

    public function getDropThyroidStatuses() {
        return ArrayHelper::map(ThyroidStatus::find()->asArray()->all(), 'id', 'name');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getThyroidStatuses() {
        return $this->hasMany(ThyroidStatus::className(), ['id' => 'thyroid_status_id'])
            ->via('thyroidInspections');
    }

    public function getThyroidStatusesIds() {
       $this->thyroidStatusesIds = ArrayHelper::getColumn($this->getThyroidInspections()->asArray()->all(), 'thyroid_status_id');
       return $this->thyroidStatusesIds;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPatient()
    {
        return $this->hasOne(Patient::className(), ['id' => 'patient_id']);
    }

    protected function updateThyroidInspection()
    {
        ThyroidInspection::deleteAll(array('general_inspection_id' => $this->inspection_id));
        if (is_array($this->thyroidStatusesIds)) {
            foreach ($this->thyroidStatusesIds as $id) {
                $thyroidInspection = new ThyroidInspection();
                $thyroidInspection->general_inspection_id = $this->inspection_id;
                $thyroidInspection->patient_id = $this->patient_id;
                $thyroidInspection->thyroid_status_id = $id;
                $thyroidInspection->save();
            }
        }
    }

    public function beforeDelete() {
        if (parent::beforeDelete()) {
            ThyroidInspection::deleteAll('general_inspection_id = :inspection_id AND patient_id = :patient_id', [
                'inspection_id' => $this->inspection_id,
                'patient_id' => $this->patient_id
            ]);

            return true;
        } else {
            return false;
        }
    }

    public function afterSave($insert, $changedAttributes)
    {
        $this->updateThyroidInspection();
        parent::afterSave($insert, $changedAttributes);
    }

    public function inspectionToString($inspections) {
        $resultArray = [];
	$inspectionString = "";        
	foreach ($inspections as $inspection) {
            array_push($resultArray, $inspection->name);
        }
	if (count($resultArray)) {
	    $inspectionString = implode(", ", $resultArray) . '; ';
	}
	
	return $inspectionString;
    }

    public function mapObject($template) {
        $template->setValue('thyroidStatuses', $this->inspectionToString($this->thyroidStatuses));
        $template->setValue('thyroid_inspection_description', $this->thyroid_inspection_description);

        return $template;
    }

    public function createSection($phpWord, $section) {
        $thyroidStatuses = $this->inspectionToString($this->thyroidStatuses);
        $thyroid_inspection_description = $this->thyroid_inspection_description;

        if ($thyroidStatuses || $thyroid_inspection_description) {
            
            $textrun = $section->createTextRun('textParagraphStyle');
            $textrun->addText($this->attributeLabels()['thyroidStatuses'] . ": ", 'inspectionPositionTextStyle');

            $textrun->addText($thyroidStatuses, 'textStyle');
            $textrun->addText($thyroid_inspection_description, 'textStyle');
        }
    }
}
