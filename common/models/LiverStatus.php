<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "liver_status".
 *
 * @property integer $id
 * @property string $name
 *
 * @property LiverInspection[] $liverInspections
 */
class LiverStatus extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'liver_status';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            ['name', 'unique'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Состояние печени',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLiverInspections()
    {
        return $this->hasMany(LiverInspection::className(), ['liver_status_id' => 'id']);
    }
}
