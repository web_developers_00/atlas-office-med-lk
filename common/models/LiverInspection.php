<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "liver_inspection".
 *
 * @property integer $other_inspection_id
 * @property integer $patient_id
 * @property integer $liver_status_id
 *
 * @property LiverStatus $liverStatus
 * @property OtherInspection $otherInspection
 * @property OtherInspection $patient
 */
class LiverInspection extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'liver_inspection';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['other_inspection_id', 'patient_id', 'liver_status_id'], 'required'],
            [['other_inspection_id', 'patient_id', 'liver_status_id'], 'integer'],
            [['liver_status_id'], 'exist', 'skipOnError' => true, 'targetClass' => LiverStatus::className(), 'targetAttribute' => ['liver_status_id' => 'id']],
            [['other_inspection_id'], 'exist', 'skipOnError' => true, 'targetClass' => LiverStatusInspection::className(), 'targetAttribute' => ['other_inspection_id' => 'inspection_id']],
            [['patient_id'], 'exist', 'skipOnError' => true, 'targetClass' => LiverStatusInspection::className(), 'targetAttribute' => ['patient_id' => 'patient_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'other_inspection_id' => 'Other Inspection ID',
            'patient_id' => 'Patient ID',
            'liver_status_id' => 'Liver Status ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLiverStatus()
    {
        return $this->hasOne(LiverStatus::className(), ['id' => 'liver_status_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLiverStatusInspection()
    {
        return $this->hasOne(LiverStatusInspection::className(), ['inspection_id' => 'other_inspection_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPatient()
    {
        return $this->hasOne(Patient::className(), ['patient_id' => 'patient_id']);
    }
}
