<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "rib_cage_status".
 *
 * @property integer $id
 * @property string $name
 *
 * @property RibCageInspection[] $ribCageInspections
 */
class RibCageStatus extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'rib_cage_status';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            ['name', 'unique'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Грудная клетка',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRibCageInspections()
    {
        return $this->hasMany(RibCageInspection::className(), ['rib_cage_status_id' => 'id']);
    }
    
    public function beforeDelete() {
        if (parent::beforeDelete()) {

            if (RibCageInspection::find()->where('rib_cage_status_id = :id', ['id' => $this->id])->exists()) {
                return false;
            } else {
                return true;
            }

        } else {
            return false;
        }
    }
}
