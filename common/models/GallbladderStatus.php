<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "gallbladder_status".
 *
 * @property integer $id
 * @property string $name
 *
 * @property GallbladderInspection[] $gallbladderInspections
 */
class GallbladderStatus extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'gallbladder_status';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            ['name', 'unique'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Желчный пузырь',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGallbladderInspections()
    {
        return $this->hasMany(GallbladderInspection::className(), ['gallbladder_status_id' => 'id']);
    }

    public function beforeDelete() {
        if (parent::beforeDelete()) {

            if (GallbladderInspection::find()->where('gallbladder_status_id = :id', ['id' => $this->id])->exists()) {
                return false;
            } else {
                return true;
            }

        } else {
            return false;
        }
    }
}
