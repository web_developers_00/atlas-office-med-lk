<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "thyroid_inspection".
 *
 * @property integer $general_inspection_id
 * @property integer $patient_id
 * @property integer $thyroid_status_id
 *
 * @property GeneralInspection $generalInspection
 * @property GeneralInspection $patient
 * @property ThyroidStatus $thyroidStatus
 */
class ThyroidInspection extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'thyroid_inspection';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['general_inspection_id', 'patient_id', 'thyroid_status_id'], 'required'],
            [['general_inspection_id', 'patient_id', 'thyroid_status_id'], 'integer'],
            [['general_inspection_id'], 'exist', 'skipOnError' => true, 'targetClass' => ThyroidStatusInspection::className(), 'targetAttribute' => ['general_inspection_id' => 'inspection_id']],
            [['patient_id'], 'exist', 'skipOnError' => true, 'targetClass' => ThyroidStatusInspection::className(), 'targetAttribute' => ['patient_id' => 'patient_id']],
            [['thyroid_status_id'], 'exist', 'skipOnError' => true, 'targetClass' => ThyroidStatus::className(), 'targetAttribute' => ['thyroid_status_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'general_inspection_id' => 'General Inspection ID',
            'patient_id' => 'Patient ID',
            'thyroid_status_id' => 'Thyroid Status ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getThyroidStatusInspection()
    {
        return $this->hasOne(ThyroidStatusInspection::className(), ['inspection_id' => 'general_inspection_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPatient()
    {
        return $this->hasOne(Patient::className(), ['id' => 'patient_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getThyroidStatus()
    {
        return $this->hasOne(ThyroidStatus::className(), ['id' => 'thyroid_status_id']);
    }
}
