<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;
use Arslanim\TemplateMapper\Interfaces\TemplateFillStrategy;
use Arslanim\TemplateMapper\Interfaces\CreateDocumentStrategy;

/**
 * This is the model class for table "kidneys_status_inspection".
 *
 * @property integer $inspection_id
 * @property integer $patient_id
 * @property string $kidneys_inspection_description
 *
 * @property KidneysInspection[] $kidneysInspections
 * @property Inspection $inspection
 */
class KidneysStatusInspection extends \yii\db\ActiveRecord implements TemplateFillStrategy, CreateDocumentStrategy
{
    public $kidneyStatusesIds = [];
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'kidneys_status_inspection';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['inspection_id', 'patient_id'], 'required'],
            [['inspection_id', 'patient_id'], 'integer'],
            [['kidneys_inspection_description'], 'string'],
            [['inspection_id'], 'exist', 'skipOnError' => true, 'targetClass' => Inspection::className(), 'targetAttribute' => ['inspection_id' => 'id']],
            [['kidneyStatusesIds', 'kidneyStatuses'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'inspection_id' => 'Inspection ID',
            'patient_id' => 'Patient ID',            
            'kidneyStatuses' => 'Почки',
            'kidneys_inspection_description' => 'Почки дополнительно',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKidneysInspections()
    {
        return $this->hasMany(KidneysInspection::className(), ['other_inspection_id' => 'inspection_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInspection()
    {
        return $this->hasOne(Inspection::className(), ['id' => 'inspection_id']);
    }

    public function setKidneyStatuses($kidneyStatusesIds) {
        $this->kidneyStatusesIds = $kidneyStatusesIds;
    }

    public function getDropKidneyStatuses() {
        return ArrayHelper::map(KidneysStatus::find()->asArray()->all(), 'id', 'name');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKidneyStatuses() {
        return $this->hasMany(KidneysStatus::className(), ['id' => 'kidneys_status_id'])
            ->via('kidneysInspections');
    }

    public function getKidneyStatusesIds() {
       $this->kidneyStatusesIds = ArrayHelper::getColumn($this->getKidneysInspections()->asArray()->all(), 'kidneys_status_id');
       return $this->kidneyStatusesIds;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPatient()
    {
        return $this->hasOne(Patient::className(), ['id' => 'patient_id']);
    }

    protected function updateKidneyInspection()
    {
        KidneysInspection::deleteAll(array('other_inspection_id' => $this->inspection_id));
        if (is_array($this->kidneyStatusesIds)) {
            foreach ($this->kidneyStatusesIds as $id) {
                $kidneysInspection = new KidneysInspection();
                $kidneysInspection->other_inspection_id = $this->inspection_id;
                $kidneysInspection->patient_id = $this->patient_id;
                $kidneysInspection->kidneys_status_id = $id;
                $kidneysInspection->save();
            }
        }
    }

    public function afterSave($insert, $changedAttributes)
    {
        $this->updateKidneyInspection();
        parent::afterSave($insert, $changedAttributes);
    }

    public function beforeDelete() {
        if (parent::beforeDelete()) {
            KidneysInspection::deleteAll('other_inspection_id = :inspection_id AND patient_id = :patient_id', [
                'inspection_id' => $this->inspection_id,
                'patient_id' => $this->patient_id
            ]);

            return true;
        } else {
            return false;
        }
    }

    public function inspectionToString($inspections) {
        $resultArray = [];
        foreach ($inspections as $inspection) {
            array_push($resultArray, $inspection->name);
        }

        return (count($resultArray)) ? (implode(", ", $resultArray)) : ("");
    }

    public function mapObject($template) {
        $template->setValue('kidneyStatuses', $this->inspectionToString($this->kidneyStatuses));

        return $template;
    }

    public function createSection($phpWord, $section) {
        $kidneyStatuses = $this->inspectionToString($this->kidneyStatuses);

        if ($kidneyStatuses) {
            $textrun = $section->createTextRun('textParagraphStyle');
            $textrun->addText($this->attributeLabels()['kidneyStatuses'] . ": ", 'inspectionPositionTextStyle');

            $textrun->addText($kidneyStatuses, 'textStyle');
        }
    }
}
