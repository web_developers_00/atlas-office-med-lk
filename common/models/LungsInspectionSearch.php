<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\LungsInspection;

/**
 * LungsInspectionSearch represents the model behind the search form about `common\models\LungsInspection`.
 */
class LungsInspectionSearch extends LungsInspection
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['inspection_id', 'patient_id'], 'integer'],
            [['breathing_rate', 'oxygen_saturation'], 'number'],
            [['rib_cage_inspection_description', 'percussion_inspection_description', 'voice_trembling_inspection_description', 'auscultation_inspection_drscription', 'crepitation_inspection_description'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = LungsInspection::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'inspection_id' => $this->inspection_id,
            'patient_id' => $this->patient_id,
            'breathing_rate' => $this->breathing_rate,
            'oxygen_saturation' => $this->oxygen_saturation,
        ]);

        $query->andFilterWhere(['like', 'rib_cage_inspection_description', $this->rib_cage_inspection_description])
            ->andFilterWhere(['like', 'percussion_inspection_description', $this->percussion_inspection_description])
            ->andFilterWhere(['like', 'voice_trembling_inspection_description', $this->voice_trembling_inspection_description])
            ->andFilterWhere(['like', 'auscultation_inspection_drscription', $this->auscultation_inspection_drscription])
            ->andFilterWhere(['like', 'crepitation_inspection_description', $this->crepitation_inspection_description]);

        return $dataProvider;
    }
}
