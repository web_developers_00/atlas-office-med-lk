<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "finger_nose_status".
 *
 * @property integer $id
 * @property string $name
 *
 * @property FingerNoseInspection[] $fingerNoseInspections
 */
class FingerNoseStatus extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'finger_nose_status';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            ['name', 'unique'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Пальценосовая проба',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFingerNoseInspections()
    {
        return $this->hasMany(FingerNoseInspection::className(), ['finger_nose_status_id' => 'id']);
    }

    public function beforeDelete() {
        if (parent::beforeDelete()) {

            if (FingerNoseInspection::find()->where('finger_nose_status_id = :id', ['id' => $this->id])->exists()) {
                return false;
            } else {
                return true;
            }

        } else {
            return false;
        }
    }
}
