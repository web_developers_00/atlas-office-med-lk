<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "lungs_auscultation_status".
 *
 * @property integer $id
 * @property string $name
 *
 * @property AuscultationInspection[] $auscultationInspections
 */
class LungsAuscultationStatus extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'lungs_auscultation_status';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            ['name', 'unique'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Аускультативно',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuscultationInspections()
    {
        return $this->hasMany(AuscultationInspection::className(), ['lungs_auscultation_status_id' => 'id']);
    }

    public function beforeDelete() {
        if (parent::beforeDelete()) {

            if (AuscultationInspection::find()->where('lungs_auscultation_status_id = :id', ['id' => $this->id])->exists()) {
                return false;
            } else {
                return true;
            }

        } else {
            return false;
        }
    }
}
