<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "heart_tones_accent_status".
 *
 * @property integer $id
 * @property string $name
 * @property HeartTonesAccentInspection[] $heartTonesAccentInspections
 * @property HeartTonesInspection[] $heartTonesInspections
 */
class HeartTonesAccentStatus extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'heart_tones_accent_status';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            ['name', 'unique'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Акцент тонов',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHeartTonesAccentInspections()
    {
        return $this->hasMany(HeartTonesAccentInspection::className(), ['heart_tones_accent_status_id' => 'id']);
    }

    public function beforeDelete() {
        if (parent::beforeDelete()) {

            if (HeartTonesAccentInspection::find()->where('heart_tones_accent_status_id = :id', ['id' => $this->id])->exists()) {
                return false;
            } else {
                return true;
            }

        } else {
            return false;
        }
    }
}
