<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "voice_trembling_status".
 *
 * @property integer $id
 * @property string $name
 *
 * @property VoiceTremblingInspection[] $voiceTremblingInspections
 */
class VoiceTremblingStatus extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'voice_trembling_status';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            ['name', 'unique'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Голосовое дрожание',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVoiceTremblingInspections()
    {
        return $this->hasMany(VoiceTremblingInspection::className(), ['voice_trembling_status_id' => 'id']);
    }

    public function beforeDelete() {
        if (parent::beforeDelete()) {

            if (VoiceTremblingInspection::find()->where('voice_trembling_status_id = :id', ['id' => $this->id])->exists()) {
                return false;
            } else {
                return true;
            }

        } else {
            return false;
        }
    }
}
