<?php

namespace common\models;

use Yii;
use Arslanim\TemplateMapper\Interfaces\TemplateFillStrategy;
use Arslanim\TemplateMapper\Interfaces\CreateDocumentStrategy;

/**
 * This is the model class for table "edema_inspection".
 *
 * @property integer $inspection_id
 * @property integer $patient_id
 * @property integer $edema
 * @property string $edema_description
 *
 * @property Inspection $inspection
 */
class EdemaInspection extends \yii\db\ActiveRecord implements TemplateFillStrategy, CreateDocumentStrategy
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'edema_inspection';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['inspection_id', 'patient_id'], 'required'],
            [['inspection_id', 'patient_id', 'edema'], 'integer'],
            [['edema_description'], 'string'],
            [['inspection_id'], 'exist', 'skipOnError' => true, 'targetClass' => Inspection::className(), 'targetAttribute' => ['inspection_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'inspection_id' => 'Inspection ID',
            'patient_id' => 'Patient ID',
            'edema' => 'Отеки',
            'edema_description' => 'Описание отёков',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInspection()
    {
        return $this->hasOne(Inspection::className(), ['id' => 'inspection_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPatient()
    {
        return $this->hasOne(Patient::className(), ['id' => 'patient_id']);
    }

    public function mapObject($template) {
        $template->setValue('edema',  isset($this->edema) ? (($this->edema) ? ('есть') : ('нет')) : (Yii::$app->params['systemValues']['hyphenValue']));
        $template->setValue('edema_description', $this->edema_description);

        return $template;
    }

    public function createSection($phpWord, $section) {
        $edema = isset($this->edema) ? (($this->edema) ? ('есть; ') : ('нет; ')) : ("");
        $edema_description = $this->edema_description;

        if ($edema || $edema_description) {

            $textrun = $section->createTextRun('textParagraphStyle');
            $textrun->addText($this->attributeLabels()['edema'] . ": ", 'inspectionPositionTextStyle');
            
            $textrun->addText($edema, 'textStyle');
            $textrun->addText($edema_description, 'textStyle');
        }
    }
}
