<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;
use Arslanim\TemplateMapper\Interfaces\TemplateFillStrategy;
use Arslanim\TemplateMapper\Interfaces\CreateDocumentStrategy;

/**
 * This is the model class for table "lungs_inspection".
 *
 * @property integer $inspection_id
 * @property integer $patient_id
 * @property double $breathing_rate
 * @property double $oxygen_saturation
 * @property string $rib_cage_inspection_description
 * @property string $percussion_inspection_description
 * @property string $voice_trembling_inspection_description
 * @property string $auscultation_inspection_drscription
 * @property string $crepitation_inspection_description
 *
 * @property AuscultationInspection[] $auscultationInspections
 * @property AuscultationInspection[] $auscultationInspections0
 * @property CrepitationInspection[] $crepitationInspections
 * @property CrepitationInspection[] $crepitationInspections0
 * @property Inspection $inspection
 * @property Inspection $patient
 * @property PercussionInspection[] $percussionInspections
 * @property PercussionInspection[] $percussionInspections0
 * @property RibCageInspection[] $ribCageInspections
 * @property RibCageInspection[] $ribCageInspections0
 * @property VoiceTremblingInspection[] $voiceTremblingInspections
 * @property VoiceTremblingInspection[] $voiceTremblingInspections0
 */
class LungsInspection extends \yii\db\ActiveRecord implements TemplateFillStrategy, CreateDocumentStrategy
{
    public $lungsAuscultationStatusesIds = [];
    public $crepitationStatusesIds = [];
    public $lungsPercussionStatusesIds = [];
    public $ribCageStatusesIds = [];
    public $voiceTremblingStatusesIds = [];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'lungs_inspection';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['inspection_id', 'patient_id'], 'required'],
            [['inspection_id', 'patient_id'], 'integer'],
            [['breathing_rate', 'oxygen_saturation'], 'number'],
            [['rib_cage_inspection_description', 'percussion_inspection_description', 'voice_trembling_inspection_description', 'auscultation_inspection_drscription', 'crepitation_inspection_description'], 'string'],
            [['inspection_id'], 'exist', 'skipOnError' => true, 'targetClass' => Inspection::className(), 'targetAttribute' => ['inspection_id' => 'id']],
            [['patient_id'], 'exist', 'skipOnError' => true, 'targetClass' => Inspection::className(), 'targetAttribute' => ['patient_id' => 'patient_id']],
            [['lungsAuscultationStatusesIds', 'lungsAuscultationStatuses', 'crepitationStatusesIds', 'crepitationStatuses', 'lungsPercussionStatusesIds', 'lungsPercussionStatuses', 'ribCageStatusesIds', 'ribCageStatuses', 'voiceTremblingStatusesIds', 'voiceTremblingStatuses'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'inspection_id' => 'Inspection ID',
            'patient_id' => 'Patient ID',
            'breathing_rate' => 'Частота дыхания',
            'oxygen_saturation' => 'Сатурация кислорода',
            'rib_cage_inspection_description' => 'Грудная клетка дополнение',
            'ribCageStatuses' => 'Грудная клетка',
            'percussion_inspection_description' => 'Перкуторно дополнение',
            'lungsPercussionStatuses' => 'Перкуторно',
            'voice_trembling_inspection_description' => 'Голосовое дрожание дополнение',
            'voiceTremblingStatuses' => 'Дрожание голоса',
            'auscultation_inspection_drscription' => 'Аускультатично дополнение',
            'lungsAuscultationStatuses' => 'Аускультативно',
            'crepitation_inspection_description' => 'Хрипы дополнение',
            'crepitationStatuses' => 'Хрипы',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuscultationInspections()
    {
        return $this->hasMany(AuscultationInspection::className(), ['lungs_inspection_id' => 'inspection_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuscultationInspections0()
    {
        return $this->hasMany(AuscultationInspection::className(), ['patient_id' => 'patient_id']);
    }

    public function setLungsAuscultationStatuses($lungsAuscultationStatusesIds) {
        $this->lungsAuscultationStatusesIds = $lungsAuscultationStatusesIds;
    }

    public function getDropLungsAuscultationStatuses() {
        return ArrayHelper::map(LungsAuscultationStatus::find()->asArray()->all(), 'id', 'name');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLungsAuscultationStatuses() {
        return $this->hasMany(LungsAuscultationStatus::className(), ['id' => 'lungs_auscultation_status_id'])
            ->via('auscultationInspections');
    }

    public function getLungsAuscultationStatusesIds() {
       $this->lungsAuscultationStatusesIds = ArrayHelper::getColumn($this->getAuscultationInspections()->asArray()->all(), 'lungs_auscultation_status_id');
       return $this->lungsAuscultationStatusesIds;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCrepitationInspections()
    {
        return $this->hasMany(CrepitationInspection::className(), ['lungs_inspection_id' => 'inspection_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCrepitationInspections0()
    {
        return $this->hasMany(CrepitationInspection::className(), ['patient_id' => 'patient_id']);
    }

    public function setCrepitationStatuses($crepitationStatusesIds) {
        $this->crepitationStatusesIds = $crepitationStatusesIds;
    }

    public function getDropCrepitationStatuses() {
        return ArrayHelper::map(CrepitationStatus::find()->asArray()->all(), 'id', 'name');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCrepitationStatuses() {
        return $this->hasMany(CrepitationStatus::className(), ['id' => 'crepitation_status_id'])
            ->via('crepitationInspections');
    }

    public function getCrepitationStatusesIds() {
       $this->crepitationStatusesIds = ArrayHelper::getColumn($this->getCrepitationInspections()->asArray()->all(), 'crepitation_status_id');
       return $this->crepitationStatusesIds;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInspection()
    {
        return $this->hasOne(Inspection::className(), ['id' => 'inspection_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPatient()
    {
        return $this->hasOne(Patient::className(), ['id' => 'patient_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPercussionInspections()
    {
        return $this->hasMany(PercussionInspection::className(), ['lungs_inspection_id' => 'inspection_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPercussionInspections0()
    {
        return $this->hasMany(PercussionInspection::className(), ['patient_id' => 'patient_id']);
    }

    public function setLungsPercussionStatuses($lungsPercussionStatusesIds) {
        $this->lungsPercussionStatusesIds = $lungsPercussionStatusesIds;
    }

    public function getDropLungsPercussionStatuses() {
        return ArrayHelper::map(PercussionStatus::find()->asArray()->all(), 'id', 'name');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLungsPercussionStatuses() {
        return $this->hasMany(PercussionStatus::className(), ['id' => 'percussion_status_id'])
            ->via('percussionInspections');
    }

    public function getLungsPercussionStatusesIds() {
       $this->lungsPercussionStatusesIds = ArrayHelper::getColumn($this->getPercussionInspections()->asArray()->all(), 'percussion_status_id');
       return $this->lungsPercussionStatusesIds;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRibCageInspections()
    {
        return $this->hasMany(RibCageInspection::className(), ['lungs_inspection_id' => 'inspection_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRibCageInspections0()
    {
        return $this->hasMany(RibCageInspection::className(), ['patient_id' => 'patient_id']);
    }

    public function setRibCageStatuses($ribCageStatusesIds) {
        $this->ribCageStatusesIds = $ribCageStatusesIds;
    }

    public function getDropRibCageStatuses() {
        return ArrayHelper::map(RibCageStatus::find()->asArray()->all(), 'id', 'name');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRibCageStatuses() {
        return $this->hasMany(RibCageStatus::className(), ['id' => 'rib_cage_status_id'])
            ->via('ribCageInspections');
    }

    public function getRibCageStatusesIds() {
       $this->ribCageStatusesIds = ArrayHelper::getColumn($this->getRibCageInspections()->asArray()->all(), 'rib_cage_status_id');
       return $this->ribCageStatusesIds;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVoiceTremblingInspections()
    {
        return $this->hasMany(VoiceTremblingInspection::className(), ['lungs_inspection_id' => 'inspection_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVoiceTremblingInspections0()
    {
        return $this->hasMany(VoiceTremblingInspection::className(), ['patient_id' => 'patient_id']);
    }

    public function setVoiceTremblingStatuses($voiceTremblingStatusesIds) {
        $this->voiceTremblingStatusesIds = $voiceTremblingStatusesIds;
    }

    public function getDropVoiceTremblingStatuses() {
        return ArrayHelper::map(VoiceTremblingStatus::find()->asArray()->all(), 'id', 'name');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVoiceTremblingStatuses() {
        return $this->hasMany(VoiceTremblingStatus::className(), ['id' => 'voice_trembling_status_id'])
            ->via('voiceTremblingInspections');
    }

    public function getVoiceTremblingStatusesIds() {
       $this->voiceTremblingStatusesIds = ArrayHelper::getColumn($this->getVoiceTremblingInspections()->asArray()->all(), 'voice_trembling_status_id');
       return $this->voiceTremblingStatusesIds;
    }

    public function inspectionToString($inspections) {
        $resultArray = [];
	$inspectionString = "";        
	foreach ($inspections as $inspection) {
            array_push($resultArray, $inspection->name);
        }
	if (count($resultArray)) {
	    $inspectionString = implode(", ", $resultArray) . '; ';
	}
	
	return $inspectionString;
    }

    protected function updateVoiceTremblingInspection()
    {
        VoiceTremblingInspection::deleteAll(array('lungs_inspection_id' => $this->inspection_id));
        if (is_array($this->voiceTremblingStatusesIds)) {
            foreach ($this->voiceTremblingStatusesIds as $id) {
                $voiceTremblingInspection = new VoiceTremblingInspection();
                $voiceTremblingInspection->lungs_inspection_id = $this->inspection_id;
                $voiceTremblingInspection->patient_id = $this->patient_id;
                $voiceTremblingInspection->voice_trembling_status_id = $id;
                $voiceTremblingInspection->save();
            }
        }
    }

    protected function updateRibCageInspection()
    {
        RibCageInspection::deleteAll(array('lungs_inspection_id' => $this->inspection_id));
        if (is_array($this->ribCageStatusesIds)) {
            foreach ($this->ribCageStatusesIds as $id) {
                $ribCageInspection = new RibCageInspection();
                $ribCageInspection->lungs_inspection_id = $this->inspection_id;
                $ribCageInspection->patient_id = $this->patient_id;
                $ribCageInspection->rib_cage_status_id = $id;
                $ribCageInspection->save();
            }
        }
    }

    protected function updateAuscultationInspection()
    {
        AuscultationInspection::deleteAll(array('lungs_inspection_id' => $this->inspection_id));
        if (is_array($this->lungsAuscultationStatusesIds)) {
            foreach ($this->lungsAuscultationStatusesIds as $id) {
                $auscultationInspection = new AuscultationInspection();
                $auscultationInspection->lungs_inspection_id = $this->inspection_id;
                $auscultationInspection->patient_id = $this->patient_id;
                $auscultationInspection->lungs_auscultation_status_id = $id;
                $auscultationInspection->save();
            }
        }
    }

    protected function updateCrepitationInspection()
    {
        CrepitationInspection::deleteAll(array('lungs_inspection_id' => $this->inspection_id));
        if (is_array($this->crepitationStatusesIds)) {
            foreach ($this->crepitationStatusesIds as $id) {
                $crepitationInspection = new CrepitationInspection();
                $crepitationInspection->lungs_inspection_id = $this->inspection_id;
                $crepitationInspection->patient_id = $this->patient_id;
                $crepitationInspection->crepitation_status_id = $id;
                $crepitationInspection->save();
            }
        }
    }

    protected function updatePercussionInspection()
    {
        PercussionInspection::deleteAll(array('lungs_inspection_id' => $this->inspection_id));
        if (is_array($this->lungsPercussionStatusesIds)) {
            foreach ($this->lungsPercussionStatusesIds as $id) {
                $percussionInspection = new PercussionInspection();
                $percussionInspection->lungs_inspection_id = $this->inspection_id;
                $percussionInspection->patient_id = $this->patient_id;
                $percussionInspection->percussion_status_id = $id;
                $percussionInspection->save();
            }
        }
    }

    public function beforeDelete() {
        if (parent::beforeDelete()) {
            AuscultationInspection::deleteAll('lungs_inspection_id = :lungs_inspection_id AND patient_id = :patient_id', [
                'lungs_inspection_id' => $this->inspection_id,
                'patient_id' => $this->patient_id
            ]);
            CrepitationInspection::deleteAll('lungs_inspection_id = :lungs_inspection_id AND patient_id = :patient_id', [
                'lungs_inspection_id' => $this->inspection_id,
                'patient_id' => $this->patient_id
            ]);
            PercussionInspection::deleteAll('lungs_inspection_id = :lungs_inspection_id AND patient_id = :patient_id', [
                'lungs_inspection_id' => $this->inspection_id,
                'patient_id' => $this->patient_id
            ]);
            RibCageInspection::deleteAll('lungs_inspection_id = :lungs_inspection_id AND patient_id = :patient_id', [
                'lungs_inspection_id' => $this->inspection_id,
                'patient_id' => $this->patient_id
            ]);
            VoiceTremblingInspection::deleteAll('lungs_inspection_id = :lungs_inspection_id AND patient_id = :patient_id', [
                'lungs_inspection_id' => $this->inspection_id,
                'patient_id' => $this->patient_id
            ]);
            return true;
        } else {
            return false;
        }
    }

    public function afterSave($insert, $changedAttributes)
    {
        $this->updateAuscultationInspection();
        $this->updateCrepitationInspection();
        $this->updatePercussionInspection();
        $this->updateRibCageInspection();
        $this->updateVoiceTremblingInspection();
        parent::afterSave($insert, $changedAttributes);
    }

    public function mapObject($template) {
        $template->setValue('breathing_rate', ($this->breathing_rate) ? ($this->breathing_rate) : (Yii::$app->params['systemValues']['hyphenValue']));
        $template->setValue('oxygen_saturation', ($this->oxygen_saturation) ? ($this->oxygen_saturation) : (Yii::$app->params['systemValues']['hyphenValue']));
        $template->setValue('lungsAuscultationStatuses', $this->inspectionToString($this->lungsAuscultationStatuses));
        $template->setValue('auscultation_inspection_drscription', $this->auscultation_inspection_drscription);
        $template->setValue('crepitationStatuses', $this->inspectionToString($this->crepitationStatuses));
        $template->setValue('crepitation_inspection_description', $this->crepitation_inspection_description);
        $template->setValue('lungsPercussionStatuses', $this->inspectionToString($this->lungsPercussionStatuses));
        $template->setValue('ribCageStatuses', $this->inspectionToString($this->ribCageStatuses));
        $template->setValue('voiceTremblingStatuses', $this->inspectionToString($this->voiceTremblingStatuses));

        return $template;
    }

    public function createSection($phpWord, $section) {
        $breathing_rate = ($this->breathing_rate) ? ($this->breathing_rate) : ("");
        $oxygen_saturation = ($this->oxygen_saturation) ? ($this->oxygen_saturation) : ("");
        $lungsAuscultationStatuses = $this->inspectionToString($this->lungsAuscultationStatuses);
        $auscultation_inspection_drscription = $this->auscultation_inspection_drscription;
        $crepitationStatuses = $this->inspectionToString($this->crepitationStatuses);
        $crepitation_inspection_description = $this->crepitation_inspection_description;
        $lungsPercussionStatuses = $this->inspectionToString($this->lungsPercussionStatuses);
        $ribCageStatuses = $this->inspectionToString($this->ribCageStatuses);
        $voiceTremblingStatuses = $this->inspectionToString($this->voiceTremblingStatuses);

        if ($breathing_rate) {
            $textrun = $section->createTextRun('textParagraphStyle');
            $textrun->addText($this->attributeLabels()['breathing_rate'] . ": ", 'inspectionPositionTextStyle');
            $textrun->addText($breathing_rate, 'textStyle');
        }

        if ($oxygen_saturation) {
            $textrun = $section->createTextRun('textParagraphStyle');
            $textrun->addText($this->attributeLabels()['oxygen_saturation'] . ": ", 'inspectionPositionTextStyle');
            $textrun->addText($oxygen_saturation, 'textStyle');
        }

        if ($lungsAuscultationStatuses || $auscultation_inspection_drscription) {
            $textrun = $section->createTextRun('textParagraphStyle');
            $textrun->addText($this->attributeLabels()['lungsAuscultationStatuses'] . ": ", 'inspectionPositionTextStyle');

            $textrun->addText($lungsAuscultationStatuses, 'textStyle');
            $textrun->addText($auscultation_inspection_drscription, 'textStyle');
        }

        if ($crepitationStatuses || $crepitation_inspection_description) {
            $textrun = $section->createTextRun('textParagraphStyle');
            $textrun->addText($this->attributeLabels()['crepitationStatuses'] . ": ", 'inspectionPositionTextStyle');

            $textrun->addText($crepitationStatuses, 'textStyle');
            $textrun->addText($crepitation_inspection_description, 'textStyle');
        }

        if ($lungsPercussionStatuses) {
            $textrun = $section->createTextRun('textParagraphStyle');
            $textrun->addText($this->attributeLabels()['lungsPercussionStatuses'] . ": ", 'inspectionPositionTextStyle');

            $textrun->addText($lungsPercussionStatuses, 'textStyle');
        }

        if ($ribCageStatuses) {
            $textrun = $section->createTextRun('textParagraphStyle');
            $textrun->addText($this->attributeLabels()['ribCageStatuses'] . ": ", 'inspectionPositionTextStyle');

            $textrun->addText($ribCageStatuses, 'textStyle');
        }

        if ($voiceTremblingStatuses) {
            $textrun = $section->createTextRun('textParagraphStyle');
            $textrun->addText($this->attributeLabels()['voiceTremblingStatuses'] . ": ", 'inspectionPositionTextStyle');

            $textrun->addText($voiceTremblingStatuses, 'textStyle');
        }
    }
}
