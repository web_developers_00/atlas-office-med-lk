<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "skin_inspection".
 *
 * @property integer $general_inspection_id
 * @property integer $patient_id
 * @property integer $skin_status_id
 *
 * @property GeneralInspection $generalInspection
 * @property GeneralInspection $patient
 * @property SkinStatus $skinStatus
 */
class SkinInspection extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'skin_inspection';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['general_inspection_id', 'patient_id', 'skin_status_id'], 'required'],
            [['general_inspection_id', 'patient_id', 'skin_status_id'], 'integer'],
            [['general_inspection_id'], 'exist', 'skipOnError' => true, 'targetClass' => SkinStatusInspection::className(), 'targetAttribute' => ['general_inspection_id' => 'inspection_id']],
            [['patient_id'], 'exist', 'skipOnError' => true, 'targetClass' => SkinStatusInspection::className(), 'targetAttribute' => ['patient_id' => 'patient_id']],
            [['skin_status_id'], 'exist', 'skipOnError' => true, 'targetClass' => SkinStatus::className(), 'targetAttribute' => ['skin_status_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'general_inspection_id' => 'General Inspection ID',
            'patient_id' => 'Patient ID',
            'skin_status_id' => 'Skin Status ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSkinStatusInspection()
    {
        return $this->hasOne(SkinStatusInspection::className(), ['inspection_id' => 'general_inspection_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSkinStatus()
    {
        return $this->hasOne(SkinStatus::className(), ['id' => 'skin_status_id']);
    }
}
