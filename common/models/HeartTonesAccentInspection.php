<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "heart_tones_accent_inspection".
 *
 * @property integer $cardiovascular_inspection_id
 * @property integer $patient_id
 * @property integer $heart_tones_accent_status_id
 *
 * @property CardiovascularInspection $cardiovascularInspection
 * @property CardiovascularInspection $patient
 * @property HeartTonesStatus $heartTonesAccentStatus
 */
class HeartTonesAccentInspection extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'heart_tones_accent_inspection';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['cardiovascular_inspection_id', 'patient_id', 'heart_tones_accent_status_id'], 'required'],
            [['cardiovascular_inspection_id', 'patient_id', 'heart_tones_accent_status_id'], 'integer'],
            [['cardiovascular_inspection_id'], 'exist', 'skipOnError' => true, 'targetClass' => CardiovascularInspection::className(), 'targetAttribute' => ['cardiovascular_inspection_id' => 'inspection_id']],
            [['patient_id'], 'exist', 'skipOnError' => true, 'targetClass' => CardiovascularInspection::className(), 'targetAttribute' => ['patient_id' => 'patient_id']],
            [['heart_tones_accent_status_id'], 'exist', 'skipOnError' => true, 'targetClass' => HeartTonesAccentStatus::className(), 'targetAttribute' => ['heart_tones_accent_status_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'cardiovascular_inspection_id' => 'Cardiovascular Inspection ID',
            'patient_id' => 'Patient ID',
            'heart_tones_accent_status_id' => 'Heart Tones Accent Status ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCardiovascularInspection()
    {
        return $this->hasOne(CardiovascularInspection::className(), ['inspection_id' => 'cardiovascular_inspection_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPatient()
    {
        return $this->hasOne(CardiovascularInspection::className(), ['patient_id' => 'patient_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHeartTonesAccentStatus()
    {
        return $this->hasOne(HeartTonesAccentStatus::className(), ['id' => 'heart_tones_accent_status_id']);
    }
}
