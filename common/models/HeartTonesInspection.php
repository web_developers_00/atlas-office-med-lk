<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "heart_tones_inspection".
 *
 * @property integer $cardiovascular_inspection_id
 * @property integer $patient_id
 * @property integer $heart_tones_status_id
 *
 * @property CardiovascularInspection $cardiovascularInspection
 * @property HeartTonesStatus $heartTonesStatus
 * @property CardiovascularInspection $patient
 */
class HeartTonesInspection extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'heart_tones_inspection';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['cardiovascular_inspection_id', 'patient_id', 'heart_tones_status_id'], 'required'],
            [['cardiovascular_inspection_id', 'patient_id', 'heart_tones_status_id'], 'integer'],
            [['cardiovascular_inspection_id'], 'exist', 'skipOnError' => true, 'targetClass' => CardiovascularInspection::className(), 'targetAttribute' => ['cardiovascular_inspection_id' => 'inspection_id']],
            [['heart_tones_status_id'], 'exist', 'skipOnError' => true, 'targetClass' => HeartTonesStatus::className(), 'targetAttribute' => ['heart_tones_status_id' => 'id']],
            [['patient_id'], 'exist', 'skipOnError' => true, 'targetClass' => CardiovascularInspection::className(), 'targetAttribute' => ['patient_id' => 'patient_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'cardiovascular_inspection_id' => 'Cardiovascular Inspection ID',
            'patient_id' => 'Patient ID',
            'heart_tones_status_id' => 'Heart Tones Status ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCardiovascularInspection()
    {
        return $this->hasOne(CardiovascularInspection::className(), ['inspection_id' => 'cardiovascular_inspection_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHeartTonesStatus()
    {
        return $this->hasOne(HeartTonesStatus::className(), ['id' => 'heart_tones_status_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPatient()
    {
        return $this->hasOne(CardiovascularInspection::className(), ['patient_id' => 'patient_id']);
    }
}
