<?php

namespace common\models;

use Yii;
use Arslanim\TemplateMapper\Interfaces\TemplateFillStrategy;
use Arslanim\TemplateMapper\Interfaces\CreateDocumentStrategy;

/**
 * This is the model class for table "patient".
 *
 * @property integer $id
 * @property string $patient_name
 * @property string $birth_date
 * @property string $address
 * @property string $phone
 * @property string $create_date
 *
 * @property Inspection[] $inspections
 */
class Patient extends \yii\db\ActiveRecord implements TemplateFillStrategy, CreateDocumentStrategy
{

    public function behaviors() { 
        return [ 
            'timestamp' => [ 
                'class' => 'yii\behaviors\TimestampBehavior', 
                'attributes' => [ 
                    \yii\db\ActiveRecord::EVENT_BEFORE_INSERT => ['create_date'],
                ], 
                'value' => function() { 
                    return date('Y-m-d');
                }
            ], 
            /*'encryption' => [
                'class' => '\nickcv\encrypter\behaviors\EncryptionBehavior',
                'attributes' => [
                    'patient_name',
                    'address',
                    'phone',
                ],
            ],*/
        ]; 
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'patient';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['patient_name', 'birth_date', 'address', 'phone'], 'required'],
            [['birth_date', 'create_date'], 'safe'],
            [['patient_name', 'address', 'phone'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'patient_name' => 'ФИО пациента',
            'birth_date' => 'Дата рождения',
            'address' => 'Адрес',
            'phone' => 'Телефон',
            'create_date' => 'Дата регистрации',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInspections()
    {
        return $this->hasMany(Inspection::className(), ['patient_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContracts()
    {
        return $this->hasMany(Contract::className(), ['patient_id' => 'id']);
    }

    /**
     * Calculates full years of patient.
     * @param string $birthDate
     * @return string
     */
    public function getFullYears($birthDate) {
        $dateTime = new \DateTime($birthDate);
        $interval = $dateTime->diff(new \DateTime(date('Y-m-d')));
        return $interval->format('%Y');
    }

    public function beforeDelete() {
        if (parent::beforeDelete()) {

            $inspection = Inspection::findOne(['patient_id' => $this->id]);
            if ($inspection) $inspection->delete();

            Contract::deleteAll('patient_id = :patient_id', [
                'patient_id' => $this->id,
            ]);

            return true;
        } else {
            return false;
        }
    }

    public function mapObject($template) {
        $template->setValue('patient_name', ($this->patient_name) ? ($this->patient_name) : (Yii::$app->params['systemValues']['defaultValue']));  
        $template->setValue('birth_date', ($this->birth_date) ? ($this->birth_date) : (Yii::$app->params['systemValues']['defaultValue']));  
        $template->setValue('address', ($this->address) ? ($this->address) : (Yii::$app->params['systemValues']['defaultValue']));  
        $template->setValue('phone', ($this->phone) ? ($this->phone) : (Yii::$app->params['systemValues']['defaultValue']));

        return $template;
    }

    public function createSection($phpWord, $section) {
        $patient_name = $this->patient_name;
        $birth_date = $this->birth_date;
        $address = $this->address;
        $phone = $this->phone;

        if ($patient_name) {
            $textrun = $section->createTextRun('textParagraphStyle');
            $textrun->addText($this->attributeLabels()['patient_name'] . ": ", 'inspectionPositionTextStyle');
            $textrun->addText($patient_name, 'textStyle');
        }
        if ($birth_date) {
            $textrun = $section->createTextRun('textParagraphStyle');
            $textrun->addText($this->attributeLabels()['birth_date'] . ": ", 'inspectionPositionTextStyle');
            $textrun->addText($birth_date, 'textStyle');
        }
        if ($address) {
            $textrun = $section->createTextRun('textParagraphStyle');
            $textrun->addText($this->attributeLabels()['address'] . ": ", 'inspectionPositionTextStyle');
            $textrun->addText($address, 'textStyle');
        }
        if ($phone) {
            $textrun = $section->createTextRun('textParagraphStyle');
            $textrun->addText($this->attributeLabels()['phone'] . ": ", 'inspectionPositionTextStyle');
            $textrun->addText($phone, 'textStyle');
        }
    }
}
