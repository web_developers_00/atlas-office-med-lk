<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;
use Arslanim\TemplateMapper\Interfaces\TemplateFillStrategy;

/**
 * This is the model class for table "general_inspection".
 *
 * @property integer $inspection_id
 * @property integer $patient_id
 * @property integer $helth_status_id
 * @property integer $feeding_status_id
 * @property integer $peripheral_lymph_nodes_status_id
 * @property double $body_mass_index
 * @property string $rash
 * @property integer $edema
 * @property integer $dysuria
 * @property string $skin_inspection_description
 * @property string $thyroid_inspection_description
 * @property string $mammary_gland_inspection_description
 * @property string $osteoarticular_inspection_description
 * @property string $stool_inspection_description
 * @property string $tongue_inspection_description
 * @property string $edema_description
 *
 * @property FeedingStatus $feedingStatus
 * @property HelthStatus $helthStatus
 * @property Inspection $inspection
 * @property PeripheralLymphNodesStatus $peripheralLymphNodesStatus
 * @property Inspection $patient
 * @property MammaryGlandInspection[] $mammaryGlandInspections
 * @property MammaryGlandInspection[] $mammaryGlandInspections0
 * @property OsteoarticularInspection[] $osteoarticularInspections
 * @property OsteoarticularInspection[] $osteoarticularInspections0
 * @property SkinInspection[] $skinInspections
 * @property SkinInspection[] $skinInspections0
 * @property StoolInspection[] $stoolInspections
 * @property StoolInspection[] $stoolInspections0
 * @property ThyroidInspection[] $thyroidInspections
 * @property ThyroidInspection[] $thyroidInspections0
 * @property TongueInspection[] $tongueInspections
 * @property TongueInspection[] $tongueInspections0
 */
class GeneralInspection extends \yii\db\ActiveRecord implements TemplateFillStrategy
{
    public $skinStatusesIds = [];
    public $thyroidStatusesIds = [];
    public $mammaryGlandStatusesIds = [];
    public $osteoarticularSystemStatusesIds = [];
    public $stoolStatusesIds = [];
    public $tongueStatusesIds = [];
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'general_inspection';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['inspection_id', 'patient_id', /*'helth_status_id', 'feeding_status_id', 'peripheral_lymph_nodes_status_id'*/], 'required'],
            [['inspection_id', 'patient_id', 'helth_status_id', 'feeding_status_id', 'peripheral_lymph_nodes_status_id', 'edema', 'dysuria'], 'integer'],
            [['body_mass_index'], 'number'],
            [['rash', 'skin_inspection_description', 'thyroid_inspection_description', 'mammary_gland_inspection_description', 'osteoarticular_inspection_description', 'stool_inspection_description', 'tongue_inspection_description', 'edema_description'], 'string'],
            [['feeding_status_id'], 'exist', 'skipOnError' => true, 'targetClass' => FeedingStatus::className(), 'targetAttribute' => ['feeding_status_id' => 'id']],
            [['helth_status_id'], 'exist', 'skipOnError' => true, 'targetClass' => HelthStatus::className(), 'targetAttribute' => ['helth_status_id' => 'id']],
            [['inspection_id'], 'exist', 'skipOnError' => true, 'targetClass' => Inspection::className(), 'targetAttribute' => ['inspection_id' => 'id']],
            [['peripheral_lymph_nodes_status_id'], 'exist', 'skipOnError' => true, 'targetClass' => PeripheralLymphNodesStatus::className(), 'targetAttribute' => ['peripheral_lymph_nodes_status_id' => 'id']],
            [['patient_id'], 'exist', 'skipOnError' => true, 'targetClass' => Inspection::className(), 'targetAttribute' => ['patient_id' => 'patient_id']],
            [['skinStatuses', 'skinStatusesIds', 'thyroidStatuses', 'thyroidStatusesIds', 'mammaryGlandStatuses', 'mammaryGlandStatusesIds', 'osteoarticularSystemStatuses', 'osteoarticularSystemStatusesIds', 'stoolStatuses', 'stoolStatusesIds', 'tongueStatuses', 'tongueStatusesIds'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'inspection_id' => 'Inspection ID',
            'patient_id' => 'Patient ID',
            'helth_status_id' => 'Общее состояние здоровья',
            'feeding_status_id' => 'Питание',
            'peripheral_lymph_nodes_status_id' => 'Периферические лимфоузлы',
            'body_mass_index' => 'Индекс массы тела',
            'rash' => 'Сыпь',
            'edema' => 'Отеки',
            'dysuria' => 'Дизурии',
            'skinStatuses' => 'Кожные покровы',
            'skin_inspection_description' => 'Дополнительное описание состояния кожных покровов',
            'thyroidStatuses' => 'Щитовидная железа',
            'thyroid_inspection_description' => 'Дополнительное описание состояния щитовидной железы',
            'mammaryGlandStatuses' => 'Молочные железы',
            'mammary_gland_inspection_description' => 'Дополнительное описание состояния молочных желез',
            'osteoarticularSystemStatuses' => 'Костно-суставная система',
            'osteoarticular_inspection_description' => 'Дополнительное описание состояния костно-суставной системы',
            'stoolStatuses' => 'Стул',
            'stool_inspection_description' => 'Дополнительное описание состояния стула',
            'tongueStatuses' => 'Язык',
            'tongue_inspection_description' => 'Дополнительное описание состояния языка',
            'edema_description' => 'Дополнительное описание отёков',
        ];
    }

    public function extraFields()
    {
        return ['feedingStatus'];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFeedingStatus()
    {
        return $this->hasOne(FeedingStatus::className(), ['id' => 'feeding_status_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHelthStatus()
    {
        return $this->hasOne(HelthStatus::className(), ['id' => 'helth_status_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInspection()
    {
        return $this->hasOne(Inspection::className(), ['id' => 'inspection_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPeripheralLymphNodesStatus()
    {
        return $this->hasOne(PeripheralLymphNodesStatus::className(), ['id' => 'peripheral_lymph_nodes_status_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPatient()
    {
        return $this->hasOne(Patient::className(), ['id' => 'patient_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMammaryGlandInspections()
    {
        return $this->hasMany(MammaryGlandInspection::className(), ['general_inspection_id' => 'inspection_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMammaryGlandInspections0()
    {
        return $this->hasMany(MammaryGlandInspection::className(), ['patient_id' => 'patient_id']);
    }

    public function setMammaryGlandStatuses($mammaryGlandStatusesIds) {
        $this->mammaryGlandStatusesIds = $mammaryGlandStatusesIds;
    }

    public function getDropMammaryGlandStatuses() {
        return ArrayHelper::map(MammaryStatus::find()->asArray()->all(), 'id', 'name');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMammaryGlandStatuses() {
        return $this->hasMany(MammaryStatus::className(), ['id' => 'mammary_status_id'])
            ->via('mammaryGlandInspections');
    }

    public function getMammaryGlandStatusesIds() {
       $this->mammaryGlandStatusesIds = ArrayHelper::getColumn($this->getMammaryGlandInspections()->asArray()->all(), 'mammary_status_id');
       return $this->mammaryGlandStatusesIds;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOsteoarticularInspections()
    {
        return $this->hasMany(OsteoarticularInspection::className(), ['general_inspection_id' => 'inspection_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOsteoarticularInspections0()
    {
        return $this->hasMany(OsteoarticularInspection::className(), ['patient_id' => 'patient_id']);
    }

    public function setOsteoarticularSystemStatuses($osteoarticularSystemStatusesIds) {
        $this->osteoarticularSystemStatusesIds = $osteoarticularSystemStatusesIds;
    }

    public function getDropOsteoarticularSystemStatuses() {
        return ArrayHelper::map(OsteoarticularSystemStatus::find()->asArray()->all(), 'id', 'name');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOsteoarticularSystemStatuses() {
        return $this->hasMany(OsteoarticularSystemStatus::className(), ['id' => 'osteoarticular_system_status_id'])
            ->via('osteoarticularInspections');
    }

    public function getOsteoarticularSystemStatusesIds() {
       $this->osteoarticularSystemStatusesIds = ArrayHelper::getColumn($this->getOsteoarticularInspections()->asArray()->all(), 'osteoarticular_system_status_id');
       return $this->osteoarticularSystemStatusesIds;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSkinInspections()
    {
        return $this->hasMany(SkinInspection::className(), ['general_inspection_id' => 'inspection_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSkinInspections0()
    {
        return $this->hasMany(SkinInspection::className(), ['patient_id' => 'patient_id']);
    }

    public function setSkinStatuses($skinStatusesIds) {
        $this->skinStatusesIds = $skinStatusesIds;
    }

    public function getDropSkinStatuses() {
        return ArrayHelper::map(SkinStatus::find()->asArray()->all(), 'id', 'name');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSkinStatuses() {
        return $this->hasMany(SkinStatus::className(), ['id' => 'skin_status_id'])
            ->via('skinInspections');
    }

    public function getSkinStatusesIds() {
       $this->skinStatusesIds = ArrayHelper::getColumn($this->getSkinInspections()->asArray()->all(), 'skin_status_id');
       return $this->skinStatusesIds;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStoolInspections()
    {
        return $this->hasMany(StoolInspection::className(), ['general_inspection_id' => 'inspection_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStoolInspections0()
    {
        return $this->hasMany(StoolInspection::className(), ['patient_id' => 'patient_id']);
    }

    public function setStoolStatuses($stoolStatusesIds) {
        $this->stoolStatusesIds = $stoolStatusesIds;
    }

    public function getDropStoolStatuses() {
        return ArrayHelper::map(StoolStatus::find()->asArray()->all(), 'id', 'name');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStoolStatuses() {
        return $this->hasMany(StoolStatus::className(), ['id' => 'stool_status_id'])
            ->via('stoolInspections');
    }

    public function getStoolStatusesIds() {
       $this->stoolStatusesIds = ArrayHelper::getColumn($this->getStoolInspections()->asArray()->all(), 'stool_status_id');
       return $this->stoolStatusesIds;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getThyroidInspections()
    {
        return $this->hasMany(ThyroidInspection::className(), ['general_inspection_id' => 'inspection_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getThyroidInspections0()
    {
        return $this->hasMany(ThyroidInspection::className(), ['patient_id' => 'patient_id']);
    }

    public function setThyroidStatuses($thyroidStatusesIds) {
        $this->thyroidStatusesIds = $thyroidStatusesIds;
    }

    public function getDropThyroidStatuses() {
        return ArrayHelper::map(ThyroidStatus::find()->asArray()->all(), 'id', 'name');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getThyroidStatuses() {
        return $this->hasMany(ThyroidStatus::className(), ['id' => 'thyroid_status_id'])
            ->via('thyroidInspections');
    }

    public function getThyroidStatusesIds() {
       $this->thyroidStatusesIds = ArrayHelper::getColumn($this->getThyroidInspections()->asArray()->all(), 'thyroid_status_id');
       return $this->thyroidStatusesIds;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTongueInspections()
    {
        return $this->hasMany(TongueInspection::className(), ['general_inspection_id' => 'inspection_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTongueInspections0()
    {
        return $this->hasMany(TongueInspection::className(), ['patient_id' => 'patient_id']);
    }

    public function setTongueStatuses($tongueStatusesIds) {
        $this->tongueStatusesIds = $tongueStatusesIds;
    }

    public function getDropTongueStatuses() {
        return ArrayHelper::map(TongueStatus::find()->asArray()->all(), 'id', 'name');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTongueStatuses() {
        return $this->hasMany(TongueStatus::className(), ['id' => 'tongue_status_id'])
            ->via('tongueInspections');
    }

    public function getTongueStatusesIds() {
       $this->tongueStatusesIds = ArrayHelper::getColumn($this->getTongueInspections()->asArray()->all(), 'tongue_status_id');
       return $this->tongueStatusesIds;
    }

    public function inspectionToString($inspections) {
        $resultArray = [];
        foreach ($inspections as $inspection) {
            array_push($resultArray, $inspection->name);
        }

        return (count($resultArray)) ? (implode(", ", $resultArray)) : (\Yii::$app->params['systemValues']['defaultValue']);
    }

    public function beforeDelete() {
        if (parent::beforeDelete()) {
            ThyroidInspection::deleteAll('general_inspection_id = :general_inspection_id AND patient_id = :patient_id', [
                'general_inspection_id' => $this->inspection_id,
                'patient_id' => $this->patient_id
            ]);
            SkinInspection::deleteAll('general_inspection_id = :general_inspection_id AND patient_id = :patient_id', [
                'general_inspection_id' => $this->inspection_id,
                'patient_id' => $this->patient_id
            ]);
            MammaryGlandInspection::deleteAll('general_inspection_id = :general_inspection_id AND patient_id = :patient_id', [
                'general_inspection_id' => $this->inspection_id,
                'patient_id' => $this->patient_id
            ]);
            OsteoarticularInspection::deleteAll('general_inspection_id = :general_inspection_id AND patient_id = :patient_id', [
                'general_inspection_id' => $this->inspection_id,
                'patient_id' => $this->patient_id
            ]);
            StoolInspection::deleteAll('general_inspection_id = :general_inspection_id AND patient_id = :patient_id', [
                'general_inspection_id' => $this->inspection_id,
                'patient_id' => $this->patient_id
            ]);
            TongueInspection::deleteAll('general_inspection_id = :general_inspection_id AND patient_id = :patient_id', [
                'general_inspection_id' => $this->inspection_id,
                'patient_id' => $this->patient_id
            ]);
            return true;
        } else {
            return false;
        }
    }

    public function afterSave($insert, $changedAttributes)
    {
        $this->updateSkinInspection();
        $this->updateThyroidInspection();
        $this->updateMammaryGlandInspection();
        $this->updateOsteoarticularSystemInspection();
        $this->updateStoolInspection();
        $this->updateTongueInspection();
        parent::afterSave($insert, $changedAttributes);
    }

    protected function updateOsteoarticularSystemInspection()
    {
        OsteoarticularInspection::deleteAll(array('general_inspection_id' => $this->inspection_id));
        if (is_array($this->osteoarticularSystemStatusesIds)) {
            foreach ($this->osteoarticularSystemStatusesIds as $id) {
                $osteoarticularInspection = new OsteoarticularInspection();
                $osteoarticularInspection->general_inspection_id = $this->inspection_id;
                $osteoarticularInspection->patient_id = $this->patient_id;
                $osteoarticularInspection->osteoarticular_system_status_id = $id;
                $osteoarticularInspection->save();
            }
        }
    }

    protected function updateSkinInspection()
    {
        SkinInspection::deleteAll(array('general_inspection_id' => $this->inspection_id));
        if (is_array($this->skinStatusesIds)) {
            foreach ($this->skinStatusesIds as $id) {
                $skinInspection = new SkinInspection();
                $skinInspection->general_inspection_id = $this->inspection_id;
                $skinInspection->patient_id = $this->patient_id;
                $skinInspection->skin_status_id = $id;
                $skinInspection->save();
            }
        }
    }

    protected function updateThyroidInspection()
    {
        ThyroidInspection::deleteAll(array('general_inspection_id' => $this->inspection_id));
        if (is_array($this->thyroidStatusesIds)) {
            foreach ($this->thyroidStatusesIds as $id) {
                $thyroidInspection = new ThyroidInspection();
                $thyroidInspection->general_inspection_id = $this->inspection_id;
                $thyroidInspection->patient_id = $this->patient_id;
                $thyroidInspection->thyroid_status_id = $id;
                $thyroidInspection->save();
            }
        }
    }

    protected function updateMammaryGlandInspection()
    {
        MammaryGlandInspection::deleteAll(array('general_inspection_id' => $this->inspection_id));
        if (is_array($this->mammaryGlandStatusesIds)) {
            foreach ($this->mammaryGlandStatusesIds as $id) {
                $mammaryGlandInspection = new MammaryGlandInspection();
                $mammaryGlandInspection->general_inspection_id = $this->inspection_id;
                $mammaryGlandInspection->patient_id = $this->patient_id;
                $mammaryGlandInspection->mammary_status_id = $id;
                $mammaryGlandInspection->save();
            }
        }
    }

    protected function updateStoolInspection()
    {
        StoolInspection::deleteAll(array('general_inspection_id' => $this->inspection_id));
        if (is_array($this->stoolStatusesIds)) {
            foreach ($this->stoolStatusesIds as $id) {
                $stoolInspection = new StoolInspection();
                $stoolInspection->general_inspection_id = $this->inspection_id;
                $stoolInspection->patient_id = $this->patient_id;
                $stoolInspection->stool_status_id = $id;
                $stoolInspection->save();
            }
        }
    }

    protected function updateTongueInspection() {
        TongueInspection::deleteAll(array('general_inspection_id' => $this->inspection_id));
        if (is_array($this->tongueStatusesIds)) {
            foreach ($this->tongueStatusesIds as $id) {
                $tongueInspection = new TongueInspection();
                $tongueInspection->general_inspection_id = $this->inspection_id;
                $tongueInspection->patient_id = $this->patient_id;
                $tongueInspection->tongue_status_id = $id;
                $tongueInspection->save();
            }
        }
    }

    public function mapObject($template) {
        $template->setValue('helth_status', isset($this->helthStatus) ? ($this->helthStatus->name) : (Yii::$app->params['systemValues']['defaultValue']));
        $template->setValue('feeding_status', isset($this->feedingStatus) ? ($this->feedingStatus->name) : (Yii::$app->params['systemValues']['defaultValue']));
        $template->setValue('peripheral_lymph_nodes_status', isset($this->peripheralLymphNodesStatus) ? ($this->peripheralLymphNodesStatus->name) : (Yii::$app->params['systemValues']['defaultValue']));
        $template->setValue('body_mass_index', isset($this->body_mass_index) ? ($this->body_mass_index) : (Yii::$app->params['systemValues']['defaultValue']));
        $template->setValue('rash', ($this->rash) ? ($this->rash) : (Yii::$app->params['systemValues']['defaultValue']));
        $template->setValue('dysuria', isset($this->dysuria) ? (($this->dysuria) ? ('есть') : ('нет')) : (Yii::$app->params['systemValues']['defaultValue']));
        $template->setValue('edema',  isset($this->edema) ? (($this->edema) ? ('есть') : ('нет')) : (Yii::$app->params['systemValues']['defaultValue']));
        $template->setValue('edema_description', $this->edema_description);
        $template->setValue('skinStatuses', $this->inspectionToString($this->skinStatuses));
        $template->setValue('thyroidStatuses', $this->inspectionToString($this->thyroidStatuses));
        $template->setValue('thyroid_inspection_description', $this->thyroid_inspection_description);
        $template->setValue('mammaryGlandStatuses', $this->inspectionToString($this->mammaryGlandStatuses));
        $template->setValue('mammary_gland_inspection_description', $this->mammary_gland_inspection_description);
        $template->setValue('osteoarticularSystemStatuses', $this->inspectionToString($this->osteoarticularSystemStatuses));
        $template->setValue('osteoarticular_inspection_description', $this->osteoarticular_inspection_description);
        $template->setValue('stoolStatuses', $this->inspectionToString($this->stoolStatuses));
        $template->setValue('stool_inspection_description', $this->stool_inspection_description);
        $template->setValue('tongueStatuses', $this->inspectionToString($this->tongueStatuses));

        return $template;
    }
}
