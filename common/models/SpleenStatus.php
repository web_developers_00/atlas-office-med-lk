<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "spleen_status".
 *
 * @property integer $id
 * @property string $name
 *
 * @property SpleenInspection[] $spleenInspections
 */
class SpleenStatus extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'spleen_status';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            ['name', 'unique'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Состояние селезёнки',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSpleenInspections()
    {
        return $this->hasMany(SpleenInspection::className(), ['spleen_status_id' => 'id']);
    }
}
