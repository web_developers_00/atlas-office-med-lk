<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;
use Arslanim\TemplateMapper\Interfaces\TemplateFillStrategy;

/**
 * This is the model class for table "other_inspection".
 *
 * @property integer $inspection_id
 * @property integer $patient_id
 *
 * @property GallbladderInspection[] $gallbladderInspections
 * @property GallbladderInspection[] $gallbladderInspections0
 * @property KidneysInspection[] $kidneysInspections
 * @property KidneysInspection[] $kidneysInspections0
 * @property LiverInspection[] $liverInspections
 * @property LiverInspection[] $liverInspections0
 * @property Inspection $inspection
 * @property Inspection $patient
 * @property SpleenInspection[] $spleenInspections
 * @property SpleenInspection[] $spleenInspections0
 * @property StomachInspection[] $stomachInspections
 * @property StomachInspection[] $stomachInspections0
 */
class OtherInspection extends \yii\db\ActiveRecord implements TemplateFillStrategy
{
    public $gallbladderStatusesIds = [];
    public $kidneyStatusesIds = [];
    public $liverStatusesIds = [];
    public $spleenStatusesIds = [];
    public $stomachStatusesIds = [];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'other_inspection';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['inspection_id', 'patient_id'], 'required'],
            [['inspection_id', 'patient_id'], 'integer'],
            [['gallbladder_inspection_description', 'kidneys_inspection_description', 'liver_inspection_description', 'spleen_inspection_description', 'stomach_inspection_description'], 'string'],
            [['inspection_id'], 'exist', 'skipOnError' => true, 'targetClass' => Inspection::className(), 'targetAttribute' => ['inspection_id' => 'id']],
            [['patient_id'], 'exist', 'skipOnError' => true, 'targetClass' => Inspection::className(), 'targetAttribute' => ['patient_id' => 'patient_id']],
            [['gallbladderStatusesIds', 'gallbladderStatuses', 'kidneyStatusesIds', 'kidneyStatuses', 'liverStatusesIds', 'liverStatuses', 'spleenStatusesIds', 'spleenStatuses', 'stomachStatusesIds', 'stomachStatuses'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'inspection_id' => 'Inspection ID',
            'patient_id' => 'Patient ID',
            'gallbladderStatuses' => 'Желчный пузырь',
            'gallbladder_inspection_description' => 'Желчный пузырь дополнительно',
            'kidneyStatuses' => 'Почки',
            'kidneys_inspection_description' => 'Почки дополнительно',
            'liverStatuses' => 'Печень',
            'liver_inspection_description' => 'Печень дополнительно',
            'spleenStatuses' => 'Селезёнка',
            'spleen_inspection_description' => 'Селезёнка дополнительно',
            'stomachStatuses' => 'Живот',
            'stomach_inspection_description' => 'Живот дополнительно',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGallbladderInspections()
    {
        return $this->hasMany(GallbladderInspection::className(), ['other_inspection_id' => 'inspection_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGallbladderInspections0()
    {
        return $this->hasMany(GallbladderInspection::className(), ['patient_id' => 'patient_id']);
    }

    public function setGallbladderStatuses($gallbladderStatusesIds) {
        $this->gallbladderStatusesIds = $gallbladderStatusesIds;
    }

    public function getDropGallbladderStatuses() {
        return ArrayHelper::map(GallbladderStatus::find()->asArray()->all(), 'id', 'name');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGallbladderStatuses() {
        return $this->hasMany(GallbladderStatus::className(), ['id' => 'gallbladder_status_id'])
            ->via('gallbladderInspections');
    }

    public function getGallbladderStatusesIds() {
       $this->gallbladderStatusesIds = ArrayHelper::getColumn($this->getGallbladderInspections()->asArray()->all(), 'gallbladder_status_id');
       return $this->gallbladderStatusesIds;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKidneysInspections()
    {
        return $this->hasMany(KidneysInspection::className(), ['other_inspection_id' => 'inspection_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKidneysInspections0()
    {
        return $this->hasMany(KidneysInspection::className(), ['patient_id' => 'patient_id']);
    }

    public function setKidneyStatuses($kidneyStatusesIds) {
        $this->kidneyStatusesIds = $kidneyStatusesIds;
    }

    public function getDropKidneyStatuses() {
        return ArrayHelper::map(KidneysStatus::find()->asArray()->all(), 'id', 'name');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKidneyStatuses() {
        return $this->hasMany(KidneysStatus::className(), ['id' => 'kidneys_status_id'])
            ->via('kidneysInspections');
    }

    public function getKidneyStatusesIds() {
       $this->kidneyStatusesIds = ArrayHelper::getColumn($this->getKidneysInspections()->asArray()->all(), 'kidneys_status_id');
       return $this->kidneyStatusesIds;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLiverInspections()
    {
        return $this->hasMany(LiverInspection::className(), ['other_inspection_id' => 'inspection_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLiverInspections0()
    {
        return $this->hasMany(LiverInspection::className(), ['patient_id' => 'patient_id']);
    }

    public function setLiverStatuses($liverStatusesIds) {
        $this->liverStatusesIds = $liverStatusesIds;
    }

    public function getDropLiverStatuses() {
        return ArrayHelper::map(LiverStatus::find()->asArray()->all(), 'id', 'name');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLiverStatuses() {
        return $this->hasMany(LiverStatus::className(), ['id' => 'liver_status_id'])
            ->via('liverInspections');
    }

    public function getLiverStatusesIds() {
       $this->liverStatusesIds = ArrayHelper::getColumn($this->getLiverInspections()->asArray()->all(), 'liver_status_id');
       return $this->liverStatusesIds;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInspection()
    {
        return $this->hasOne(Inspection::className(), ['id' => 'inspection_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPatient()
    {
        return $this->hasOne(Patient::className(), ['id' => 'patient_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSpleenInspections()
    {
        return $this->hasMany(SpleenInspection::className(), ['other_inspection_id' => 'inspection_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSpleenInspections0()
    {
        return $this->hasMany(SpleenInspection::className(), ['patient_id' => 'patient_id']);
    }

    public function setSpleenStatuses($spleenStatusesIds) {
        $this->spleenStatusesIds = $spleenStatusesIds;
    }

    public function getDropSpleenStatuses() {
        return ArrayHelper::map(SpleenStatus::find()->asArray()->all(), 'id', 'name');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSpleenStatuses() {
        return $this->hasMany(SpleenStatus::className(), ['id' => 'spleen_status_id'])
            ->via('spleenInspections');
    }

    public function getSpleenStatusesIds() {
       $this->spleenStatusesIds = ArrayHelper::getColumn($this->getSpleenInspections()->asArray()->all(), 'spleen_status_id');
       return $this->spleenStatusesIds;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStomachInspections()
    {
        return $this->hasMany(StomachInspection::className(), ['other_inspection_id' => 'inspection_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStomachInspections0()
    {
        return $this->hasMany(StomachInspection::className(), ['patient_id' => 'patient_id']);
    }

    public function setStomachStatuses($stomachStatusesIds) {
        $this->stomachStatusesIds = $stomachStatusesIds;
    }

    public function getDropStomachStatuses() {
        return ArrayHelper::map(StomachStatus::find()->asArray()->all(), 'id', 'name');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStomachStatuses() {
        return $this->hasMany(StomachStatus::className(), ['id' => 'stomach_status_id'])
            ->via('stomachInspections');
    }

    public function getStomachStatusesIds() {
       $this->stomachStatusesIds = ArrayHelper::getColumn($this->getStomachInspections()->asArray()->all(), 'stomach_status_id');
       return $this->stomachStatusesIds;
    }

    protected function updateGallbladderInspection()
    {
        GallbladderInspection::deleteAll(array('other_inspection_id' => $this->inspection_id));
        if (is_array($this->gallbladderStatusesIds)) {
            foreach ($this->gallbladderStatusesIds as $id) {
                $gallbladderInspection = new GallbladderInspection();
                $gallbladderInspection->other_inspection_id = $this->inspection_id;
                $gallbladderInspection->patient_id = $this->patient_id;
                $gallbladderInspection->gallbladder_status_id = $id;
                $gallbladderInspection->save();
            }
        }
    }

    protected function updateKidneyInspection()
    {
        KidneysInspection::deleteAll(array('other_inspection_id' => $this->inspection_id));
        if (is_array($this->kidneyStatusesIds)) {
            foreach ($this->kidneyStatusesIds as $id) {
                $kidneysInspection = new KidneysInspection();
                $kidneysInspection->other_inspection_id = $this->inspection_id;
                $kidneysInspection->patient_id = $this->patient_id;
                $kidneysInspection->kidneys_status_id = $id;
                $kidneysInspection->save();
            }
        }
    }

    protected function updateLiverInspection()
    {
        LiverInspection::deleteAll(array('other_inspection_id' => $this->inspection_id));
        if (is_array($this->liverStatusesIds)) {
            foreach ($this->liverStatusesIds as $id) {
                $liverInspection = new LiverInspection();
                $liverInspection->other_inspection_id = $this->inspection_id;
                $liverInspection->patient_id = $this->patient_id;
                $liverInspection->liver_status_id = $id;
                $liverInspection->save();
            }
        }
    }

    protected function updateSpleenInspection()
    {
        SpleenInspection::deleteAll(array('other_inspection_id' => $this->inspection_id));
        if (is_array($this->spleenStatusesIds)) {
            foreach ($this->spleenStatusesIds as $id) {
                $spleenInspection = new SpleenInspection();
                $spleenInspection->other_inspection_id = $this->inspection_id;
                $spleenInspection->patient_id = $this->patient_id;
                $spleenInspection->spleen_status_id = $id;
                $spleenInspection->save();
            }
        }
    }

    protected function updateStomachInspection()
    {
        StomachInspection::deleteAll(array('other_inspection_id' => $this->inspection_id));
        if (is_array($this->stomachStatusesIds)) {
            foreach ($this->stomachStatusesIds as $id) {
                $stomachInspection = new StomachInspection();
                $stomachInspection->other_inspection_id = $this->inspection_id;
                $stomachInspection->patient_id = $this->patient_id;
                $stomachInspection->stomach_status_id = $id;
                $stomachInspection->save();
            }
        }
    }

    public function inspectionToString($inspections) {
        $resultArray = [];
        foreach ($inspections as $inspection) {
            array_push($resultArray, $inspection->name);
        }

        return (count($resultArray)) ? (implode(", ", $resultArray)) : (\Yii::$app->params['systemValues']['defaultValue']);
    }

    public function afterSave($insert, $changedAttributes)
    {
        $this->updateLiverInspection();
        $this->updateStomachInspection();
        $this->updateSpleenInspection();
        $this->updateKidneyInspection();
        $this->updateGallbladderInspection();
        parent::afterSave($insert, $changedAttributes);
    }

    public function beforeDelete() {
        if (parent::beforeDelete()) {
            GallbladderInspection::deleteAll('other_inspection_id = :other_inspection_id AND patient_id = :patient_id', [
                'other_inspection_id' => $this->inspection_id,
                'patient_id' => $this->patient_id
            ]);
            StomachInspection::deleteAll('other_inspection_id = :other_inspection_id AND patient_id = :patient_id', [
                'other_inspection_id' => $this->inspection_id,
                'patient_id' => $this->patient_id
            ]);
            SpleenInspection::deleteAll('other_inspection_id = :other_inspection_id AND patient_id = :patient_id', [
                'other_inspection_id' => $this->inspection_id,
                'patient_id' => $this->patient_id
            ]);
            LiverInspection::deleteAll('other_inspection_id = :other_inspection_id AND patient_id = :patient_id', [
                'other_inspection_id' => $this->inspection_id,
                'patient_id' => $this->patient_id
            ]);
            KidneysInspection::deleteAll('other_inspection_id = :other_inspection_id AND patient_id = :patient_id', [
                'other_inspection_id' => $this->inspection_id,
                'patient_id' => $this->patient_id
            ]);

            return true;
        } else {
            return false;
        }
    }

    public function mapObject($template) {
        $template->setValue('gallbladderStatuses', $this->inspectionToString($this->gallbladderStatuses));
        $template->setValue('kidneyStatuses', $this->inspectionToString($this->kidneyStatuses));
        $template->setValue('liverStatuses', $this->inspectionToString($this->liverStatuses));
        $template->setValue('liver_inspection_description', ($this->liver_inspection_description) ? ($this->liver_inspection_description) : (Yii::$app->params['systemValues']['defaultValue']));
        $template->setValue('spleenStatuses', $this->inspectionToString($this->spleenStatuses));
        $template->setValue('stomachStatuses', $this->inspectionToString($this->stomachStatuses));
        $template->setValue('stomach_inspection_description', ($this->stomach_inspection_description) ? ($this->stomach_inspection_description) : (Yii::$app->params['systemValues']['defaultValue']));

        return $template;
    }
}
