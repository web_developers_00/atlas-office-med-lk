<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;
use Arslanim\TemplateMapper\Interfaces\TemplateFillStrategy;
use Arslanim\TemplateMapper\Interfaces\CreateDocumentStrategy;

/**
 * This is the model class for table "skin_status_inspection".
 *
 * @property integer $inspection_id
 * @property integer $patient_id
 * @property string $skin_inspection_description
 *
 * @property SkinInspection[] $skinInspections
 * @property Inspection $inspection
 */
class SkinStatusInspection extends \yii\db\ActiveRecord implements TemplateFillStrategy, CreateDocumentStrategy
{
    public $skinStatusesIds = [];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'skin_status_inspection';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['inspection_id', 'patient_id'], 'required'],
            [['inspection_id', 'patient_id'], 'integer'],
            [['skin_inspection_description', 'rash'], 'string'],
            [['skinStatuses', 'skinStatusesIds'], 'safe'],
            [['inspection_id'], 'exist', 'skipOnError' => true, 'targetClass' => Inspection::className(), 'targetAttribute' => ['inspection_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'inspection_id' => 'Inspection ID',
            'patient_id' => 'Patient ID',
            'skinStatuses' => 'Кожные покровы',
            'skin_inspection_description' => 'Дополнительное описание состояния кожных покровов',
            'rash' => 'Сыпь',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSkinInspections()
    {
        return $this->hasMany(SkinInspection::className(), ['general_inspection_id' => 'inspection_id']);
    }

    public function setSkinStatuses($skinStatusesIds) {
        $this->skinStatusesIds = $skinStatusesIds;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSkinStatuses() {
        return $this->hasMany(SkinStatus::className(), ['id' => 'skin_status_id'])
            ->via('skinInspections');
    }

    public function getDropSkinStatuses() {
        return ArrayHelper::map(SkinStatus::find()->asArray()->all(), 'id', 'name');
    }

    public function getSkinStatusesIds() {
       $this->skinStatusesIds = ArrayHelper::getColumn($this->getSkinInspections()->asArray()->all(), 'skin_status_id');
       return $this->skinStatusesIds;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInspection()
    {
        return $this->hasOne(Inspection::className(), ['id' => 'inspection_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPatient()
    {
        return $this->hasOne(Patient::className(), ['id' => 'patient_id']);
    }

    protected function updateSkinInspection() {
        SkinInspection::deleteAll(array('general_inspection_id' => $this->inspection_id));
        if (is_array($this->skinStatusesIds)) {
            foreach ($this->skinStatusesIds as $id) {
                $skinInspection = new SkinInspection();
                $skinInspection->general_inspection_id = $this->inspection_id;
                $skinInspection->patient_id = $this->patient_id;
                $skinInspection->skin_status_id = $id;
                $skinInspection->save();
            }
        }
    }

    public function beforeDelete() {
        if (parent::beforeDelete()) {
            SkinInspection::deleteAll('general_inspection_id = :inspection_id AND patient_id = :patient_id', [
                'inspection_id' => $this->inspection_id,
                'patient_id' => $this->patient_id
            ]);
            return true;
        } else {
            return false;
        }
    }

    public function afterSave($insert, $changedAttributes)
    {
        $this->updateSkinInspection();
        parent::afterSave($insert, $changedAttributes);
    }

    public function inspectionToString($inspections) {
        $resultArray = [];
        foreach ($inspections as $inspection) {
            array_push($resultArray, $inspection->name);
        }

        return (count($resultArray)) ? (implode(", ", $resultArray)) : ("");
    }

    public function mapObject($template) {
        $template->setValue('skinStatuses', $this->inspectionToString($this->skinStatuses));
        $template->setValue('rash', ($this->rash) ? ($this->rash) : (Yii::$app->params['systemValues']['hyphenValue']));

        return $template;
    }
    
    public function createSection($phpWord, $section) {
        $skinStatuses = $this->inspectionToString($this->skinStatuses);
        $rash = $this->rash;

        if ($skinStatuses) {
            $textrun = $section->createTextRun('textParagraphStyle');
            $textrun->addText($this->attributeLabels()['skinStatuses'] . ": ", 'inspectionPositionTextStyle');
            $textrun->addText($skinStatuses, 'textStyle');
        }

        if ($rash) {
            $textrun = $section->createTextRun('textParagraphStyle');
            $textrun->addText($this->attributeLabels()['rash'] . ": ", 'inspectionPositionTextStyle');
            $textrun->addText($rash, 'textStyle');
        }
    }
}
