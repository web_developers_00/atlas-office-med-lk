<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "heart_tones_status".
 *
 * @property integer $id
 * @property string $name
 *
 * @property HeartTonesInspection[] $heartTonesAccentInspections
 */
class HeartTonesStatus extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'heart_tones_status';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            ['name', 'unique'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Тоны сердца',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHeartTonesInspections()
    {
        return $this->hasMany(HeartTonesInspection::className(), ['heart_tones_status_id' => 'id']);
    }

    public function beforeDelete() {
        if (parent::beforeDelete()) {

            if (HeartTonesInspection::find()->where('heart_tones_status_id = :id', ['id' => $this->id])->exists()) {
                return false;
            } else {
                return true;
            }

        } else {
            return false;
        }
    }
}
