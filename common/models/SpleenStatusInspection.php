<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;
use Arslanim\TemplateMapper\Interfaces\TemplateFillStrategy;
use Arslanim\TemplateMapper\Interfaces\CreateDocumentStrategy;

/**
 * This is the model class for table "spleen_status_inspection".
 *
 * @property integer $inspection_id
 * @property integer $patient_id
 * @property string $spleen_inspection_description
 *
 * @property SpleenInspection[] $spleenInspections
 * @property Inspection $inspection
 */
class SpleenStatusInspection extends \yii\db\ActiveRecord implements TemplateFillStrategy, CreateDocumentStrategy
{
    public $spleenStatusesIds = [];
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'spleen_status_inspection';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['inspection_id', 'patient_id'], 'required'],
            [['inspection_id', 'patient_id'], 'integer'],
            [['spleen_inspection_description'], 'string'],
            [['inspection_id'], 'exist', 'skipOnError' => true, 'targetClass' => Inspection::className(), 'targetAttribute' => ['inspection_id' => 'id']],
            [['spleenStatusesIds', 'spleenStatuses'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'inspection_id' => 'Inspection ID',
            'patient_id' => 'Patient ID',
            'spleenStatuses' => 'Селезёнка',
            'spleen_inspection_description' => 'Селезёнка дополнительно',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSpleenInspections()
    {
        return $this->hasMany(SpleenInspection::className(), ['other_inspection_id' => 'inspection_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInspection()
    {
        return $this->hasOne(Inspection::className(), ['id' => 'inspection_id']);
    }

    public function setSpleenStatuses($spleenStatusesIds) {
        $this->spleenStatusesIds = $spleenStatusesIds;
    }

    public function getDropSpleenStatuses() {
        return ArrayHelper::map(SpleenStatus::find()->asArray()->all(), 'id', 'name');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSpleenStatuses() {
        return $this->hasMany(SpleenStatus::className(), ['id' => 'spleen_status_id'])
            ->via('spleenInspections');
    }

    public function getSpleenStatusesIds() {
       $this->spleenStatusesIds = ArrayHelper::getColumn($this->getSpleenInspections()->asArray()->all(), 'spleen_status_id');
       return $this->spleenStatusesIds;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPatient()
    {
        return $this->hasOne(Patient::className(), ['id' => 'patient_id']);
    }

    protected function updateSpleenInspection()
    {
        SpleenInspection::deleteAll(array('other_inspection_id' => $this->inspection_id));
        if (is_array($this->spleenStatusesIds)) {
            foreach ($this->spleenStatusesIds as $id) {
                $spleenInspection = new SpleenInspection();
                $spleenInspection->other_inspection_id = $this->inspection_id;
                $spleenInspection->patient_id = $this->patient_id;
                $spleenInspection->spleen_status_id = $id;
                $spleenInspection->save();
            }
        }
    }

    public function beforeDelete() {
        if (parent::beforeDelete()) {
            SpleenInspection::deleteAll('other_inspection_id = :inspection_id AND patient_id = :patient_id', [
                'inspection_id' => $this->inspection_id,
                'patient_id' => $this->patient_id
            ]);

            return true;
        } else {
            return false;
        }
    }

    public function afterSave($insert, $changedAttributes)
    {
        $this->updateSpleenInspection();
        parent::afterSave($insert, $changedAttributes);
    }

    public function inspectionToString($inspections) {
        $resultArray = [];
        foreach ($inspections as $inspection) {
            array_push($resultArray, $inspection->name);
        }

        return (count($resultArray)) ? (implode(", ", $resultArray)) : ("");
    }

    public function mapObject($template) {
        $template->setValue('spleenStatuses', $this->inspectionToString($this->spleenStatuses));

        return $template;
    }

    public function createSection($phpWord, $section) {
        $spleenStatuses = $this->inspectionToString($this->spleenStatuses);

        if ($spleenStatuses) {
            $textrun = $section->createTextRun('textParagraphStyle');
            $textrun->addText($this->attributeLabels()['spleenStatuses'] . ": ", 'inspectionPositionTextStyle');

            $textrun->addText($spleenStatuses, 'textStyle');
        }
    }
}
