<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;
use Arslanim\TemplateMapper\Interfaces\TemplateFillStrategy;
use Arslanim\TemplateMapper\Interfaces\CreateDocumentStrategy;

/**
 * This is the model class for table "stool_status_inspection".
 *
 * @property integer $inspection_id
 * @property integer $patient_id
 * @property string $stool_inspection_description
 *
 * @property StoolInspection[] $stoolInspections
 * @property Inspection $inspection
 */
class StoolStatusInspection extends \yii\db\ActiveRecord implements TemplateFillStrategy, CreateDocumentStrategy
{
    public $stoolStatusesIds = [];
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'stool_status_inspection';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['inspection_id', 'patient_id'], 'required'],
            [['inspection_id', 'patient_id'], 'integer'],
            [['stool_inspection_description'], 'string'],
            [['inspection_id'], 'exist', 'skipOnError' => true, 'targetClass' => Inspection::className(), 'targetAttribute' => ['inspection_id' => 'id']],
            [['stoolStatuses', 'stoolStatusesIds'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'inspection_id' => 'Inspection ID',
            'patient_id' => 'Patient ID',
            'stool_inspection_description' => 'Stool Inspection Description',
            'stoolStatuses' => 'Стул',
            'stool_inspection_description' => 'Дополнительное описание состояния стула',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStoolInspections()
    {
        return $this->hasMany(StoolInspection::className(), ['general_inspection_id' => 'inspection_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInspection()
    {
        return $this->hasOne(Inspection::className(), ['id' => 'inspection_id']);
    }

    public function setStoolStatuses($stoolStatusesIds) {
        $this->stoolStatusesIds = $stoolStatusesIds;
    }

    public function getDropStoolStatuses() {
        return ArrayHelper::map(StoolStatus::find()->asArray()->all(), 'id', 'name');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStoolStatuses() {
        return $this->hasMany(StoolStatus::className(), ['id' => 'stool_status_id'])
            ->via('stoolInspections');
    }

    public function getStoolStatusesIds() {
       $this->stoolStatusesIds = ArrayHelper::getColumn($this->getStoolInspections()->asArray()->all(), 'stool_status_id');
       return $this->stoolStatusesIds;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPatient()
    {
        return $this->hasOne(Patient::className(), ['id' => 'patient_id']);
    }

    protected function updateStoolInspection()
    {
        StoolInspection::deleteAll(array('general_inspection_id' => $this->inspection_id));
        if (is_array($this->stoolStatusesIds)) {
            foreach ($this->stoolStatusesIds as $id) {
                $stoolInspection = new StoolInspection();
                $stoolInspection->general_inspection_id = $this->inspection_id;
                $stoolInspection->patient_id = $this->patient_id;
                $stoolInspection->stool_status_id = $id;
                $stoolInspection->save();
            }
        }
    }

    public function beforeDelete() {
        if (parent::beforeDelete()) {
            StoolInspection::deleteAll('general_inspection_id = :inspection_id AND patient_id = :patient_id', [
                'inspection_id' => $this->inspection_id,
                'patient_id' => $this->patient_id
            ]);

            return true;
        } else {
            return false;
        }
    }

    public function afterSave($insert, $changedAttributes)
    {
        $this->updateStoolInspection();

        parent::afterSave($insert, $changedAttributes);
    }

    public function inspectionToString($inspections) {
        $resultArray = [];
	$inspectionString = "";        
	foreach ($inspections as $inspection) {
            array_push($resultArray, $inspection->name);
        }
	if (count($resultArray)) {
	    $inspectionString = implode(", ", $resultArray) . '; ';
	}
	
	return $inspectionString;
    }

    public function mapObject($template) {
        $template->setValue('stoolStatuses', $this->inspectionToString($this->stoolStatuses));
        $template->setValue('stool_inspection_description', $this->stool_inspection_description);

        return $template;
    }

    public function createSection($phpWord, $section) {
        $stoolStatuses = $this->inspectionToString($this->stoolStatuses);
        $stool_inspection_description = $this->stool_inspection_description;

        if ($stoolStatuses || $stool_inspection_description) {
            $textrun = $section->createTextRun('textParagraphStyle');
            $textrun->addText($this->attributeLabels()['stoolStatuses'] . ": ", 'inspectionPositionTextStyle');

            $textrun->addText($stoolStatuses, 'textStyle');
            $textrun->addText($stool_inspection_description, 'textStyle');
        }
    }
}
