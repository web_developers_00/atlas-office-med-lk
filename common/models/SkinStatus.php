<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "skin_status".
 *
 * @property integer $id
 * @property string $name
 *
 * @property SkinInspection[] $skinInspections
 */
class SkinStatus extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'skin_status';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            ['name', 'unique'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Кожные покровы',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSkinInspections()
    {
        return $this->hasMany(SkinInspection::className(), ['skin_status_id' => 'id']);
    }

    public function beforeDelete() {
        if (parent::beforeDelete()) {

            if (SkinInspection::find()->where('skin_status_id = :id', ['id' => $this->id])->exists()) {
                return false;
            } else {
                return true;
            }

        } else {
            return false;
        }
    }
}
