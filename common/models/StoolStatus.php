<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "stool_status".
 *
 * @property integer $id
 * @property string $name
 *
 * @property StoolInspection[] $stoolInspections
 */
class StoolStatus extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'stool_status';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            ['name', 'unique'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Стул',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStoolInspections()
    {
        return $this->hasMany(StoolInspection::className(), ['stool_status_id' => 'id']);
    }

    public function beforeDelete() {
        if (parent::beforeDelete()) {

            if (StoolInspection::find()->where('stool_status_id = :id', ['id' => $this->id])->exists()) {
                return false;
            } else {
                return true;
            }

        } else {
            return false;
        }
    }
}
