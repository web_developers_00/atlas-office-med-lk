<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "extremities_movement_status".
 *
 * @property integer $id
 * @property string $name
 *
 * @property ExtremitiesMovementInspection[] $extremitiesMovementInspections
 */
class ExtremitiesMovementStatus extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'extremities_movement_status';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            ['name', 'unique'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Движения в конечностях',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExtremitiesMovementInspections()
    {
        return $this->hasMany(ExtremitiesMovementInspection::className(), ['extremities_movement_status_id' => 'id']);
    }

    public function beforeDelete() {
        if (parent::beforeDelete()) {

            if (ExtremitiesMovementInspection::find()->where('extremities_movement_status_id = :id', ['id' => $this->id])->exists()) {
                return false;
            } else {
                return true;
            }

        } else {
            return false;
        }
    }
}
