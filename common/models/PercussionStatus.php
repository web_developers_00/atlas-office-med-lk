<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "percussion_status".
 *
 * @property integer $id
 * @property string $name
 *
 * @property PercussionInspection[] $percussionInspections
 */
class PercussionStatus extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'percussion_status';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            ['name', 'unique'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Перкуторно',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPercussionInspections()
    {
        return $this->hasMany(PercussionInspection::className(), ['percussion_status_id' => 'id']);
    }

    public function beforeDelete() {
        if (parent::beforeDelete()) {

            if (PercussionInspection::find()->where('percussion_status_id = :id', ['id' => $this->id])->exists()) {
                return false;
            } else {
                return true;
            }

        } else {
            return false;
        }
    }
}
