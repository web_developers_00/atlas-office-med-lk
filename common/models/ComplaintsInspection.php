<?php

namespace common\models;

use Yii;
use Arslanim\TemplateMapper\Interfaces\TemplateFillStrategy;
use Arslanim\TemplateMapper\Interfaces\CreateDocumentStrategy;

/**
 * This is the model class for table "complaints_inspection".
 *
 * @property integer $inspection_id
 * @property integer $patient_id
 * @property string $complaints
 * @property string $anamnesis
 *
 * @property Inspection $inspection
 */
class ComplaintsInspection extends \yii\db\ActiveRecord implements TemplateFillStrategy, CreateDocumentStrategy
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'complaints_inspection';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['inspection_id', 'patient_id'], 'required'],
            [['inspection_id', 'patient_id'], 'integer'],
            [['complaints', 'anamnesis'], 'string'],
            [['inspection_id'], 'exist', 'skipOnError' => true, 'targetClass' => Inspection::className(), 'targetAttribute' => ['inspection_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'inspection_id' => 'Inspection ID',
            'patient_id' => 'Patient ID',
            'complaints' => 'Жалобы',
            'anamnesis' => 'Анамнез',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInspection()
    {
        return $this->hasOne(Inspection::className(), ['id' => 'inspection_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPatient()
    {
        return $this->hasOne(Patient::className(), ['id' => 'patient_id']);
    }

    public function mapObject($template) {
        $template->setValue('complaints', ($this->complaints) ? ($this->complaints) : (Yii::$app->params['systemValues']['hyphenValue']));
        $template->setValue('anamnesis', ($this->anamnesis) ? ($this->anamnesis) : (Yii::$app->params['systemValues']['hyphenValue']));

        return $template;
    }

    public function createSection($phpWord, $section) {
        $complaints = $this->complaints;
        $anamnesis = $this->anamnesis;

        if ($anamnesis) {
            $textrun = $section->createTextRun('textParagraphStyle');
            $textrun->addText($this->attributeLabels()['anamnesis'] . ": ", 'inspectionPositionTextStyle');
            $textrun->addText($anamnesis, 'textStyle');
        }
        if ($complaints) {
            $textrun = $section->createTextRun('textParagraphStyle');
            $textrun->addText($this->attributeLabels()['complaints'] . ": ", 'inspectionPositionTextStyle');
            $textrun->addText($complaints, 'textStyle');
        }
    }
}
