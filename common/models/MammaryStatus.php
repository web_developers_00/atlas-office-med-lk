<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "mammary_status".
 *
 * @property integer $id
 * @property string $name
 *
 * @property MammaryGlandInspection[] $mammaryGlandInspections
 */
class MammaryStatus extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mammary_status';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            ['name', 'unique'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Молочные железы',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMammaryGlandInspections()
    {
        return $this->hasMany(MammaryGlandInspection::className(), ['mammary_status_id' => 'id']);
    }

    public function beforeDelete() {
        if (parent::beforeDelete()) {

            if (MammaryGlandInspection::find()->where('mammary_status_id = :id', ['id' => $this->id])->exists()) {
                return false;
            } else {
                return true;
            }

        } else {
            return false;
        }
    }
}
