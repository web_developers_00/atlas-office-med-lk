<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;
use Arslanim\TemplateMapper\Interfaces\TemplateFillStrategy;
use Arslanim\TemplateMapper\Interfaces\CreateDocumentStrategy;

/**
 * This is the model class for table "cardiovascular_inspection".
 *
 * @property integer $inspection_id
 * @property integer $patient_id
 * @property integer $heart_status_id
 * @property double $heart_rate
 * @property double $left_blood_pressure
 * @property double $right_blood_pressure
 * @property string $heart_tones_inspection_description
 * @property string $heart_tones_accent_inspection_description
 * @property string $heart_noises_inspection_description
 *
 * @property HeartStatus $heartStatus
 * @property Inspection $inspection
 * @property Inspection $patient
 * @property HeartNoisesInspection[] $heartNoisesInspections
 * @property HeartNoisesInspection[] $heartNoisesInspections0
 * @property HeartTonesAccentInspection[] $heartTonesAccentInspections
 * @property HeartTonesAccentInspection[] $heartTonesAccentInspections0
 * @property HeartTonesInspection[] $heartTonesInspections
 * @property HeartTonesInspection[] $heartTonesInspections0
 */
class CardiovascularInspection extends \yii\db\ActiveRecord implements TemplateFillStrategy, CreateDocumentStrategy
{
    public $heartTonesStatusesIds = [];
    public $heartTonesAccentStatusesIds = [];
    public $heartNoisesStatusesIds = [];
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cardiovascular_inspection';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['inspection_id', 'patient_id', 'heart_status_id'], 'required'],
            [['inspection_id', 'patient_id', 'heart_status_id'], 'integer'],
            [['heart_rate'], 'number'],
            [['left_blood_pressure', 'right_blood_pressure'], 'string', 'max' => 255],
            [['heart_tones_inspection_description', 'heart_tones_accent_inspection_description', 'heart_noises_inspection_description'], 'string'],
            [['heart_status_id'], 'exist', 'skipOnError' => true, 'targetClass' => HeartStatus::className(), 'targetAttribute' => ['heart_status_id' => 'id']],
            [['inspection_id'], 'exist', 'skipOnError' => true, 'targetClass' => Inspection::className(), 'targetAttribute' => ['inspection_id' => 'id']],
            [['patient_id'], 'exist', 'skipOnError' => true, 'targetClass' => Inspection::className(), 'targetAttribute' => ['patient_id' => 'patient_id']],
            [['heartTonesStatusesIds', 'heartTonesStatuses', 'heartTonesAccentStatusesIds', 'heartTonesAccentStatuses', 'heartNoisesStatusesIds', 'heartNoisesStatuses'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'inspection_id' => 'Inspection ID',
            'patient_id' => 'Patient ID',
            'heart_status_id' => 'Сердце',
            'heart_rate' => 'Частота сердечных сокращений',
            'left_blood_pressure' => 'Артериальное давление слева',
            'right_blood_pressure' => 'Артериальное давление справа',
            'heartTonesStatuses' => 'Тоны',
            'heart_tones_inspection_description' => 'Heart Tones Inspection Description',
            'heartTonesAccentStatuses' => 'Акцент',
            'heart_tones_accent_inspection_description' => 'Heart Tones Accent Inspection Description',
            'heartNoisesStatuses' => 'Шумы',
            'heart_noises_inspection_description' => 'Heart Noises Inspection Description',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHeartStatus()
    {
        return $this->hasOne(HeartStatus::className(), ['id' => 'heart_status_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInspection()
    {
        return $this->hasOne(Inspection::className(), ['id' => 'inspection_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPatient()
    {
        return $this->hasOne(Patient::className(), ['id' => 'patient_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHeartNoisesInspections()
    {
        return $this->hasMany(HeartNoisesInspection::className(), ['cardiovascular_inspection_id' => 'inspection_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHeartNoisesInspections0()
    {
        return $this->hasMany(HeartNoisesInspection::className(), ['patient_id' => 'patient_id']);
    }

    public function setHeartNoisesStatuses($heartNoisesStatusesIds) {
        $this->heartNoisesStatusesIds = $heartNoisesStatusesIds;
    }

    public function getDropHeartNoisesStatuses() {
        return ArrayHelper::map(HeartNoisesStatus::find()->asArray()->all(), 'id', 'name');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHeartNoisesStatuses() {
        return $this->hasMany(HeartNoisesStatus::className(), ['id' => 'heart_noises_status_id'])
            ->via('heartNoisesInspections');
    }

    public function getHeartNoisesStatusesIds() {
       $this->heartNoisesStatusesIds = ArrayHelper::getColumn($this->getHeartNoisesInspections()->asArray()->all(), 'heart_noises_status_id');
       return $this->heartNoisesStatusesIds;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHeartTonesAccentInspections()
    {
        return $this->hasMany(HeartTonesAccentInspection::className(), ['cardiovascular_inspection_id' => 'inspection_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHeartTonesAccentInspections0()
    {
        return $this->hasMany(HeartTonesAccentInspection::className(), ['patient_id' => 'patient_id']);
    }

    public function setHeartTonesAccentStatuses($heartTonesAccentStatusesIds) {
        $this->heartTonesAccentStatusesIds = $heartTonesAccentStatusesIds;
    }

    public function getDropHeartTonesAccentStatuses() {
        return ArrayHelper::map(HeartTonesAccentStatus::find()->asArray()->all(), 'id', 'name');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHeartTonesAccentStatuses() {
        return $this->hasMany(HeartTonesAccentStatus::className(), ['id' => 'heart_tones_accent_status_id'])
            ->via('heartTonesAccentInspections');
    }

    public function getHeartTonesAccentStatusesIds() {
       $this->heartTonesAccentStatusesIds = ArrayHelper::getColumn($this->getHeartTonesAccentInspections()->asArray()->all(), 'heart_tones_accent_status_id');
       return $this->heartTonesAccentStatusesIds;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHeartTonesInspections()
    {
        return $this->hasMany(HeartTonesInspection::className(), ['cardiovascular_inspection_id' => 'inspection_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHeartTonesInspections0()
    {
        return $this->hasMany(HeartTonesInspection::className(), ['patient_id' => 'patient_id']);
    }

     public function setHeartTonesStatuses($heartTonesStatusesIds) {
        $this->heartTonesStatusesIds = $heartTonesStatusesIds;
    }

    public function getDropHeartTonesStatuses() {
        return ArrayHelper::map(HeartTonesStatus::find()->asArray()->all(), 'id', 'name');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHeartTonesStatuses() {
        return $this->hasMany(HeartTonesStatus::className(), ['id' => 'heart_tones_status_id'])
            ->via('heartTonesInspections');
    }

    public function getHeartTonesStatusesIds() {
       $this->heartTonesStatusesIds = ArrayHelper::getColumn($this->getHeartTonesInspections()->asArray()->all(), 'heart_tones_status_id');
       return $this->heartTonesStatusesIds;
    }

    public function inspectionToString($inspections) {
        $resultArray = [];
	$inspectionString = "";        
	foreach ($inspections as $inspection) {
            array_push($resultArray, $inspection->name);
        }
	if (count($resultArray)) {
	    $inspectionString = implode(", ", $resultArray) . '; ';
	}
	
	return $inspectionString;
    }

    protected function updateHeartTonesInspection()
    {
        HeartTonesInspection::deleteAll(array('cardiovascular_inspection_id' => $this->inspection_id));
        if (is_array($this->heartTonesStatusesIds)) {
            foreach ($this->heartTonesStatusesIds as $id) {
                $heartTonesInspection = new HeartTonesInspection();
                $heartTonesInspection->cardiovascular_inspection_id = $this->inspection_id;
                $heartTonesInspection->patient_id = $this->patient_id;
                $heartTonesInspection->heart_tones_status_id = $id;
                $heartTonesInspection->save();
            }
        }
    }

    protected function updateHeartTonesAccentInspection()
    {
        HeartTonesAccentInspection::deleteAll(array('cardiovascular_inspection_id' => $this->inspection_id));
        if (is_array($this->heartTonesAccentStatusesIds)) {
            foreach ($this->heartTonesAccentStatusesIds as $id) {
                $heartTonesAccentInspection = new HeartTonesAccentInspection();
                $heartTonesAccentInspection->cardiovascular_inspection_id = $this->inspection_id;
                $heartTonesAccentInspection->patient_id = $this->patient_id;
                $heartTonesAccentInspection->heart_tones_accent_status_id = $id;
                $heartTonesAccentInspection->save();
            }
        }
    }

    protected function updateHeartNoisesAccentInspection()
    {
        HeartNoisesInspection::deleteAll(array('cardiovascular_inspection_id' => $this->inspection_id));
        if (is_array($this->heartNoisesStatusesIds)) {
            foreach ($this->heartNoisesStatusesIds as $id) {
                $heartNoisesInspection = new HeartNoisesInspection();
                $heartNoisesInspection->cardiovascular_inspection_id = $this->inspection_id;
                $heartNoisesInspection->patient_id = $this->patient_id;
                $heartNoisesInspection->heart_noises_status_id = $id;
                $heartNoisesInspection->save();
            }
        }
    }

    public function beforeDelete() {
        if (parent::beforeDelete()) {
            HeartTonesInspection::deleteAll('cardiovascular_inspection_id = :cardiovascular_inspection_id AND patient_id = :patient_id', [
                'cardiovascular_inspection_id' => $this->inspection_id,
                'patient_id' => $this->patient_id
            ]);
            HeartTonesAccentInspection::deleteAll('cardiovascular_inspection_id = :cardiovascular_inspection_id AND patient_id = :patient_id', [
                'cardiovascular_inspection_id' => $this->inspection_id,
                'patient_id' => $this->patient_id
            ]);
            HeartNoisesInspection::deleteAll('cardiovascular_inspection_id = :cardiovascular_inspection_id AND patient_id = :patient_id', [
                'cardiovascular_inspection_id' => $this->inspection_id,
                'patient_id' => $this->patient_id
            ]);
            return true;
        } else {
            return false;
        }
    }

    public function afterSave($insert, $changedAttributes)
    {
        $this->updateHeartTonesInspection();
        $this->updateHeartTonesAccentInspection();
        $this->updateHeartNoisesAccentInspection();
        parent::afterSave($insert, $changedAttributes);
    }

    public function mapObject($template) {
        $template->setValue('heart_status', isset($this->heartStatus) ? ($this->heartStatus->name) : (Yii::$app->params['systemValues']['hyphenValue']));
        $template->setValue('heart_rate', ($this->heart_rate) ? ($this->heart_rate) : (Yii::$app->params['systemValues']['hyphenValue']));
        $template->setValue('left_blood_pressure', ($this->left_blood_pressure) ? ($this->left_blood_pressure) : (Yii::$app->params['systemValues']['hyphenValue']));
        $template->setValue('right_blood_pressure', ($this->right_blood_pressure) ? ($this->right_blood_pressure) : (Yii::$app->params['systemValues']['hyphenValue']));
        $template->setValue('heartTonesStatuses', $this->inspectionToString($this->heartTonesStatuses));
        $template->setValue('heart_tones_inspection_description', $this->heart_tones_inspection_description);
        $template->setValue('heartTonesAccentStatuses', $this->inspectionToString($this->heartTonesAccentStatuses));
        $template->setValue('heart_tones_accent_inspection_description', $this->heart_tones_accent_inspection_description);
        $template->setValue('heartNoisesStatuses', $this->inspectionToString($this->heartNoisesStatuses));
        $template->setValue('heart_noises_inspection_description', $this->heart_noises_inspection_description);

        return $template;
    }

    public function createSection($phpWord, $section) {
        $heart_status = isset($this->heartStatus) ? ($this->heartStatus->name) : ("");
        $heart_rate = ($this->heart_rate) ? ($this->heart_rate) : ("");
        $left_blood_pressure = ($this->left_blood_pressure) ? ($this->left_blood_pressure) : ("");
        $right_blood_pressure = ($this->right_blood_pressure) ? ($this->right_blood_pressure) : ("");
        $heartTonesStatuses = $this->inspectionToString($this->heartTonesStatuses);
        $heart_tones_inspection_description = $this->heart_tones_inspection_description;
        $heartTonesAccentStatuses = $this->inspectionToString($this->heartTonesAccentStatuses);
        $heart_tones_accent_inspection_description = $this->heart_tones_accent_inspection_description;
        $heartNoisesStatuses = $this->inspectionToString($this->heartNoisesStatuses);
        $heart_noises_inspection_description = $this->heart_noises_inspection_description;

        if ($heart_status) {
            $textrun = $section->createTextRun('textParagraphStyle');
            $textrun->addText($this->attributeLabels()['heart_status_id'] . ": ", 'inspectionPositionTextStyle');
            $textrun->addText($heart_status, 'textStyle');
        }

        if ($heart_rate) {
            $textrun = $section->createTextRun('textParagraphStyle');
            $textrun->addText($this->attributeLabels()['heart_rate'] . ": ", 'inspectionPositionTextStyle');
            $textrun->addText($heart_rate, 'textStyle');
        }

        if ($left_blood_pressure) {
            $textrun = $section->createTextRun('textParagraphStyle');
            $textrun->addText($this->attributeLabels()['left_blood_pressure'] . ": ", 'inspectionPositionTextStyle');
            $textrun->addText($left_blood_pressure, 'textStyle');
        }

        if ($right_blood_pressure) {
            $textrun = $section->createTextRun('textParagraphStyle');
            $textrun->addText($this->attributeLabels()['right_blood_pressure'] . ": ", 'inspectionPositionTextStyle');
            $textrun->addText($right_blood_pressure, 'textStyle');
        }

        if ($heartTonesStatuses || $heart_tones_inspection_description) {
            $textrun = $section->createTextRun('textParagraphStyle');
            $textrun->addText($this->attributeLabels()['heartTonesStatuses'] . ": ", 'inspectionPositionTextStyle');

            $textrun->addText($heartTonesStatuses, 'textStyle');
            $textrun->addText($heart_tones_inspection_description, 'textStyle');
        }

        if ($heartTonesAccentStatuses || $heart_tones_accent_inspection_description) {
            $textrun = $section->createTextRun('textParagraphStyle');
            $textrun->addText($this->attributeLabels()['heartTonesAccentStatuses'] . ": ", 'inspectionPositionTextStyle');

            $textrun->addText($heartTonesAccentStatuses, 'textStyle');
            $textrun->addText($heart_tones_accent_inspection_description, 'textStyle');
        }

        if ($heartNoisesStatuses || $heart_noises_inspection_description) {
            $textrun = $section->createTextRun('textParagraphStyle');
            $textrun->addText($this->attributeLabels()['heartNoisesStatuses'] . ": ", 'inspectionPositionTextStyle');

            $textrun->addText($heartNoisesStatuses, 'textStyle');
            $textrun->addText($heart_noises_inspection_description, 'textStyle');
        }
    }
}
