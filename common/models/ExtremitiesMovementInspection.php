<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "extremities_movement_inspection".
 *
 * @property integer $neurological_inspection_id
 * @property integer $patient_id
 * @property integer $extremities_movement_status_id
 *
 * @property ExtremitiesMovementStatus $extremitiesMovementStatus
 * @property NeurologicalInspection $neurologicalInspection
 * @property NeurologicalInspection $patient
 */
class ExtremitiesMovementInspection extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'extremities_movement_inspection';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['neurological_inspection_id', 'patient_id', 'extremities_movement_status_id'], 'required'],
            [['neurological_inspection_id', 'patient_id', 'extremities_movement_status_id'], 'integer'],
            [['extremities_movement_status_id'], 'exist', 'skipOnError' => true, 'targetClass' => ExtremitiesMovementStatus::className(), 'targetAttribute' => ['extremities_movement_status_id' => 'id']],
            [['neurological_inspection_id'], 'exist', 'skipOnError' => true, 'targetClass' => NeurologicalInspection::className(), 'targetAttribute' => ['neurological_inspection_id' => 'inspection_id']],
            [['patient_id'], 'exist', 'skipOnError' => true, 'targetClass' => NeurologicalInspection::className(), 'targetAttribute' => ['patient_id' => 'patient_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'neurological_inspection_id' => 'Neurological Inspection ID',
            'patient_id' => 'Patient ID',
            'extremities_movement_status_id' => 'Extremities Movement Status ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExtremitiesMovementStatus()
    {
        return $this->hasOne(ExtremitiesMovementStatus::className(), ['id' => 'extremities_movement_status_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNeurologicalInspection()
    {
        return $this->hasOne(NeurologicalInspection::className(), ['inspection_id' => 'neurological_inspection_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPatient()
    {
        return $this->hasOne(NeurologicalInspection::className(), ['patient_id' => 'patient_id']);
    }
}
