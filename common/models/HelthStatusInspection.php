<?php

namespace common\models;

use Yii;
use Arslanim\TemplateMapper\Interfaces\TemplateFillStrategy;
use Arslanim\TemplateMapper\Interfaces\CreateDocumentStrategy;

/**
 * This is the model class for table "helth_status_inspection".
 *
 * @property integer $inspection_id
 * @property integer $patient_id
 * @property integer $helth_status_id
 *
 * @property HelthStatus $helthStatus
 * @property Inspection $inspection
 */
class HelthStatusInspection extends \yii\db\ActiveRecord implements TemplateFillStrategy, CreateDocumentStrategy
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'helth_status_inspection';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['inspection_id', 'patient_id'], 'required'],
            [['inspection_id', 'patient_id', 'helth_status_id'], 'integer'],
            [['helth_status_id'], 'exist', 'skipOnError' => true, 'targetClass' => HelthStatus::className(), 'targetAttribute' => ['helth_status_id' => 'id']],
            [['inspection_id'], 'exist', 'skipOnError' => true, 'targetClass' => Inspection::className(), 'targetAttribute' => ['inspection_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'inspection_id' => 'Inspection ID',
            'patient_id' => 'Patient ID',
            'helth_status_id' => 'Общее состояние здоровья',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHelthStatus()
    {
        return $this->hasOne(HelthStatus::className(), ['id' => 'helth_status_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInspection()
    {
        return $this->hasOne(Inspection::className(), ['id' => 'inspection_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPatient()
    {
        return $this->hasOne(Patient::className(), ['id' => 'patient_id']);
    }

    public function mapObject($template) {
        $template->setValue('helth_status', isset($this->helthStatus) ? ($this->helthStatus->name) : (Yii::$app->params['systemValues']['hyphenValue']));

        return $template;
    }

    public function createSection($phpWord, $section) {
        $helthStatus = $this->helthStatus;

        if ($helthStatus) {
            $textrun = $section->createTextRun('textParagraphStyle');
            $textrun->addText($helthStatus->attributeLabels()['name'] . ": ", 'inspectionPositionTextStyle');
            $textrun->addText($helthStatus->name, 'textStyle');
        }
    }
}
