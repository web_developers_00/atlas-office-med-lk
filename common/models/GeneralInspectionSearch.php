<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\GeneralInspection;

/**
 * GeneralInspectionSearch represents the model behind the search form about `common\models\GeneralInspection`.
 */
class GeneralInspectionSearch extends GeneralInspection
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['inspection_id', 'patient_id', 'helth_status_id', 'feeding_status_id', 'peripheral_lymph_nodes_status_id', 'edema', 'dysuria'], 'integer'],
            [['body_mass_index'], 'number'],
            [['rash', 'skin_inspection_description', 'thyroid_inspection_description', 'mammary_gland_inspection_description', 'osteoarticular_inspection_description', 'stool_inspection_description', 'tongue_inspection_description'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = GeneralInspection::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'inspection_id' => $this->inspection_id,
            'patient_id' => $this->patient_id,
            'helth_status_id' => $this->helth_status_id,
            'feeding_status_id' => $this->feeding_status_id,
            'peripheral_lymph_nodes_status_id' => $this->peripheral_lymph_nodes_status_id,
            'body_mass_index' => $this->body_mass_index,
            'edema' => $this->edema,
            'dysuria' => $this->dysuria,
        ]);

        $query->andFilterWhere(['like', 'rash', $this->rash])
            ->andFilterWhere(['like', 'skin_inspection_description', $this->skin_inspection_description])
            ->andFilterWhere(['like', 'thyroid_inspection_description', $this->thyroid_inspection_description])
            ->andFilterWhere(['like', 'mammary_gland_inspection_description', $this->mammary_gland_inspection_description])
            ->andFilterWhere(['like', 'osteoarticular_inspection_description', $this->osteoarticular_inspection_description])
            ->andFilterWhere(['like', 'stool_inspection_description', $this->stool_inspection_description])
            ->andFilterWhere(['like', 'tongue_inspection_description', $this->tongue_inspection_description]);

        return $dataProvider;
    }
}
