<?php

namespace common\models;

interface TemplateFillStrategy {
    public function fillTemplatePart($templateProcessor);
}
