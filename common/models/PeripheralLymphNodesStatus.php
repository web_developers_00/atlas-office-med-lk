<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "peripheral_lymph_nodes_status".
 *
 * @property integer $id
 * @property string $name
 *
 * @property GeneralInspection[] $generalInspections
 */
class PeripheralLymphNodesStatus extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'peripheral_lymph_nodes_status';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            ['name', 'unique'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Периферических лимфоузлы',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGeneralInspections()
    {
        return $this->hasMany(GeneralInspection::className(), ['peripheral_lymph_nodes_status_id' => 'id']);
    }

    public function beforeDelete() {
        if (parent::beforeDelete()) {

            if (GeneralInspection::find()->where('peripheral_lymph_nodes_status_id = :id', ['id' => $this->id])->exists()) {
                return false;
            } else {
                return true;
            }

        } else {
            return false;
        }
    }
}
