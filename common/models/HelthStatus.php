<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "helth_status".
 *
 * @property integer $id
 * @property string $name
 *
 * @property GeneralInspection[] $generalInspections
 */
class HelthStatus extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'helth_status';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            ['name', 'unique'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Общее состояние здоровья',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGeneralInspections()
    {
        return $this->hasMany(GeneralInspection::className(), ['helth_status_id' => 'id']);
    }

    public function beforeDelete() {
        if (parent::beforeDelete()) {

            if (GeneralInspection::find()->where('helth_status_id = :id', ['id' => $this->id])->exists()) {
                return false;
            } else {
                return true;
            }

        } else {
            return false;
        }
    }
}
