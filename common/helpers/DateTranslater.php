<?php

namespace common\helpers;

class DateTranslater {

    private static $monthNumberToWord = array(
        1 => 'Января',
        2 => 'Февраля',
        3 => 'Марта',
        4 => 'Апреля',
        5 => 'Мая',
        6 => 'Июня',
        7 => 'Июля',
        8 => 'Августа',
        9 => 'Сентября',
        10 => 'Октября',
        11 => 'Ноября',
        12 => 'Декабря',
    );

    /**
    *
    * Takes date like 'yyyy-mm-dd'
    *
    / Returns 'dd Month yyyy'
    */
    public static function translateDate($date) {
        return '«' . date('d', strtotime($date)) . '» ' . self::$monthNumberToWord[(date('n', strtotime($date)))] . date(' Y г.', strtotime($date));
    }
}
