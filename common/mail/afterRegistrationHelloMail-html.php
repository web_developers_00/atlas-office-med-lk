<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user common\models\User */

?>
<div class="hello-mail">
    <p>Добрый день, уважаемый <?= Html::encode($user->username) ?>!</p>

    <p>Мы рады приветствовать Вас на ресурсе IT&Транспорт!</p><br>
	<p>Ваши регистрационные данные:</p>
	<p>Фамилия: <?= Html::encode($user->profile->surname) ?></p>
	<p>Имя: <?= Html::encode($user->profile->name) ?></p>
	<p>Отчество: <?= Html::encode($user->profile->lastname) ?></p>
	<p>Логин: <?= Html::encode($user->username) ?></p>
	<p>Пароль: <?= Html::encode($password) ?></p>

    <p>С уважением,<br> администрация ресурса IT&Транспорт</p>


</div>
