<?php

use yii\db\Schema;
use yii\db\Migration;

class m160722_125421_med_office extends Migration {
    public function safeUp() {

        //+user profile
        $this->createTable('administrator', [
            'user_id' => Schema::TYPE_PK,
            'surname' => Schema::TYPE_STRING,
            'name' => Schema::TYPE_STRING,
            'lastname' => Schema::TYPE_STRING,
            'phone' => Schema::TYPE_STRING,
        ]);
        $this->createIndex('FK_administrator_user_id', 'administrator', 'user_id');
        $this->addForeignKey('FK_administrator_user_id', 'administrator', 'user_id', 'user', 'id', 'RESTRICT', 'RESTRICT');

        //+patient profile
        $this->createTable('patient', [
            'id' => Schema::TYPE_PK,
            'patient_name' => Schema::TYPE_STRING . ' NOT NULL',
            'birth_date' => Schema::TYPE_DATE . ' NOT NULL',
            'address' => Schema::TYPE_STRING . ' NOT NULL',
            'phone' => Schema::TYPE_STRING . ' NOT NULL',
            'create_date' => Schema::TYPE_DATE . ' NOT NULL',
        ]);

        //directories  
        //++обследование
        $this->createTable('survey', [
            'id' => Schema::TYPE_PK,
            'name' => Schema::TYPE_STRING . ' NOT NULL',
        ]);
        $this->insert('survey', [
            'name' => 'ЛДГ',
        ]);
        $this->insert('survey', [
            'name' => 'ГГТП',
        ]);
        $this->insert('survey', [
            'name' => 'биллирубин',
        ]);
        $this->insert('survey', [
            'name' => 'триглицериды',
        ]);
        $this->insert('survey', [
            'name' => 'ВЛПНП',
        ]);
        $this->insert('survey', [
            'name' => 'ВЛПВП',
        ]);
        $this->insert('survey', [
            'name' => 'креатенин',
        ]);
        $this->insert('survey', [
            'name' => 'мочевина',
        ]);
        $this->insert('survey', [
            'name' => 'полный анализ крови',
        ]);

        //++общее состояние
        $this->createTable('helth_status', [
            'id' => Schema::TYPE_PK,
            'name' => Schema::TYPE_STRING . ' NOT NULL',
        ]);
        $this->insert('helth_status', [
            'name' => 'удовлетворительное',
        ]);
        $this->insert('helth_status', [
            'name' => 'среднетяжёлое',
        ]);
        $this->insert('helth_status', [
            'name' => 'тяжёлое',
        ]);

        //++питание
        $this->createTable('feeding_status', [
            'id' => Schema::TYPE_PK,
            'name' => Schema::TYPE_STRING . ' NOT NULL',
        ]);
        $this->insert('feeding_status', [
            'name' => 'нормальное',
        ]);
        $this->insert('feeding_status', [
            'name' => 'избыточное',
        ]);
        $this->insert('feeding_status', [
            'name' => 'пониженное',
        ]);

        //++кожные покровы
        $this->createTable('skin_status', [
            'id' => Schema::TYPE_PK,
            'name' => Schema::TYPE_STRING . ' NOT NULL',
        ]);
        $this->insert('skin_status', [
            'name' => 'бледные',
        ]);
        $this->insert('skin_status', [
            'name' => 'желтушные',
        ]);
        $this->insert('skin_status', [
            'name' => 'физиологическая окраска',
        ]);

        //++щитовидная железа
        $this->createTable('thyroid_status', [
            'id' => Schema::TYPE_PK,
            'name' => Schema::TYPE_STRING . ' NOT NULL',
        ]);
        $this->insert('thyroid_status', [
            'name' => 'не увеличена',
        ]);
        $this->insert('thyroid_status', [
            'name' => 'гиперплазия I степени',
        ]);
        $this->insert('thyroid_status', [
            'name' => 'гиперплазия II степени',
        ]);
        $this->insert('thyroid_status', [
            'name' => 'гиперплазия III степени',
        ]);

        //++молочные железы
        $this->createTable('mammary_status', [
            'id' => Schema::TYPE_PK,
            'name' => Schema::TYPE_STRING . ' NOT NULL',
        ]);
        $this->insert('mammary_status', [
            'name' => 'очаговые образования не пальпируются',
        ]);
        $this->insert('mammary_status', [
            'name' => 'очаговые образования пальпируются',
        ]);

        //++костно-суставная система
        $this->createTable('osteoarticular_system_status', [
            'id' => Schema::TYPE_PK,
            'name' => Schema::TYPE_STRING . ' NOT NULL',
        ]);
        $this->insert('osteoarticular_system_status', [
            'name' => 'возрастные изменения',
        ]);
        $this->insert('osteoarticular_system_status', [
            'name' => 'без особенностей',
        ]);

        //++грудная клетка
        $this->createTable('rib_cage_status', [
            'id' => Schema::TYPE_PK,
            'name' => Schema::TYPE_STRING . ' NOT NULL',
        ]);
        $this->insert('rib_cage_status', [
            'name' => 'нормостеничная',
        ]);
        $this->insert('rib_cage_status', [
            'name' => 'эмфизематозная',
        ]);
        $this->insert('rib_cage_status', [
            'name' => 'астеничная',
        ]);
        $this->insert('rib_cage_status', [
            'name' => 'ассиметричная',
        ]);

        //++перкуторно
        $this->createTable('percussion_status', [
            'id' => Schema::TYPE_PK,
            'name' => Schema::TYPE_STRING . ' NOT NULL',
        ]);
        $this->insert('percussion_status', [
            'name' => 'ясный лёгочный звук',
        ]);
        $this->insert('percussion_status', [
            'name' => 'ясный лёгочный звук с коробочным оттенком',
        ]);
        $this->insert('percussion_status', [
            'name' => 'притупление справа',
        ]);
        $this->insert('percussion_status', [
            'name' => 'притупление слева',
        ]);
        $this->insert('percussion_status', [
            'name' => 'справа',
        ]);
        $this->insert('percussion_status', [
            'name' => 'слева',
        ]);

        //++голосовое дрожание
        $this->createTable('voice_trembling_status', [
            'id' => Schema::TYPE_PK,
            'name' => Schema::TYPE_STRING . ' NOT NULL',
        ]);
        $this->insert('voice_trembling_status', [
            'name' => 'проводится',
        ]);
        $this->insert('voice_trembling_status', [
            'name' => 'не проводится',
        ]);
        $this->insert('voice_trembling_status', [
            'name' => 'не проводится справа',
        ]);
        $this->insert('voice_trembling_status', [
            'name' => 'не проводится слева',
        ]);
        $this->insert('voice_trembling_status', [
            'name' => 'справа',
        ]);
        $this->insert('voice_trembling_status', [
            'name' => 'слева',
        ]);

        //++аускультатично (легкие)
        $this->createTable('lungs_auscultation_status', [
            'id' => Schema::TYPE_PK,
            'name' => Schema::TYPE_STRING . ' NOT NULL',
        ]);
        $this->insert('lungs_auscultation_status', [
            'name' => 'нормальное везикулярное дыхание',
        ]);
        $this->insert('lungs_auscultation_status', [
            'name' => 'ослабленное везикулярное дыхание',
        ]);
        $this->insert('lungs_auscultation_status', [
            'name' => 'жесткое везикулярное дыхание',
        ]);

        //++хрипы
        $this->createTable('crepitation_status', [
            'id' => Schema::TYPE_PK,
            'name' => Schema::TYPE_STRING . ' NOT NULL',
        ]);
        $this->insert('crepitation_status', [
            'name' => 'влажный',
        ]);
        $this->insert('crepitation_status', [
            'name' => 'сухой',
        ]);
        $this->insert('crepitation_status', [
            'name' => 'крепитирующие',
        ]);

        //++состояние сердца
        $this->createTable('heart_status', [
            'id' => Schema::TYPE_PK,
            'name' => Schema::TYPE_STRING . ' NOT NULL',
        ]);
        $this->insert('heart_status', [
            'name' => 'увеличено',
        ]);
        $this->insert('heart_status', [
            'name' => 'не увеличино',
        ]);

        //++тоны сердца
        $this->createTable('heart_tones_status', [
            'id' => Schema::TYPE_PK,
            'name' => Schema::TYPE_STRING . ' NOT NULL',
        ]);
        $this->insert('heart_tones_status', [
            'name' => 'ритмичные',
        ]);
        $this->insert('heart_tones_status', [
            'name' => 'аритмичные',
        ]);

        //++акцент тонов
        $this->createTable('heart_tones_accent_status', [
            'id' => Schema::TYPE_PK,
            'name' => Schema::TYPE_STRING . ' NOT NULL',
        ]);
        $this->insert('heart_tones_accent_status', [
            'name' => 'II тона на аорте',
        ]);
        $this->insert('heart_tones_accent_status', [
            'name' => 'II тона на лёгочной артерии',
        ]);

        //++шумы сердца
        $this->createTable('heart_noises_status', [
            'id' => Schema::TYPE_PK,
            'name' => Schema::TYPE_STRING . ' NOT NULL',
        ]);
        $this->insert('heart_noises_status', [
            'name' => 'выслушиваются',
        ]);
        $this->insert('heart_noises_status', [
            'name' => 'не выслушиваются',
        ]);

        //++состояние языка
        $this->createTable('tongue_status', [
            'id' => Schema::TYPE_PK,
            'name' => Schema::TYPE_STRING . ' NOT NULL',
        ]);
        $this->insert('tongue_status', [
            'name' => 'чистый',
        ]);
        $this->insert('tongue_status', [
            'name' => 'обложен',
        ]);
        $this->insert('tongue_status', [
            'name' => 'влажный',
        ]);
        $this->insert('tongue_status', [
            'name' => 'сухой',
        ]);
        $this->insert('tongue_status', [
            'name' => 'по срединной линии',
        ]);
        $this->insert('tongue_status', [
            'name' => 'девиация',
        ]);

        //++состояние печени
        $this->createTable('liver_status', [
            'id' => Schema::TYPE_PK,
            'name' => Schema::TYPE_STRING . ' NOT NULL',
        ]);
        $this->insert('liver_status', [
            'name' => 'не пальпируется',
        ]);
        $this->insert('liver_status', [
            'name' => 'увеличена',
        ]);
        $this->insert('liver_status', [
            'name' => 'болезненная',
        ]);
        $this->insert('liver_status', [
            'name' => 'безболезненная',
        ]);
        $this->insert('liver_status', [
            'name' => 'плотная',
        ]);

        //++состояие живота
        $this->createTable('stomach_status', [
            'id' => Schema::TYPE_PK,
            'name' => Schema::TYPE_STRING . ' NOT NULL',
        ]);
        $this->insert('stomach_status', [
            'name' => 'симметричный',
        ]);
        $this->insert('stomach_status', [
            'name' => 'ассиметричный',
        ]);
        $this->insert('stomach_status', [
            'name' => 'мягкий',
        ]);
        $this->insert('stomach_status', [
            'name' => 'болезненный',
        ]);
        $this->insert('stomach_status', [
            'name' => 'безболезненный',
        ]);

        //++состояние желчного пузыря
        $this->createTable('gallbladder_status', [
            'id' => Schema::TYPE_PK,
            'name' => Schema::TYPE_STRING . ' NOT NULL',
        ]);
        $this->insert('gallbladder_status', [
            'name' => 'пальпируется',
        ]);
        $this->insert('gallbladder_status', [
            'name' => 'не пальпируется',
        ]);

        //++состояние селезёнки
        $this->createTable('spleen_status', [
            'id' => Schema::TYPE_PK,
            'name' => Schema::TYPE_STRING . ' NOT NULL',
        ]);
        $this->insert('spleen_status', [
            'name' => 'не пальпируется',
        ]);
        $this->insert('spleen_status', [
            'name' => 'увеличена',
        ]);

        //++состояние почек
        $this->createTable('kidneys_status', [
            'id' => Schema::TYPE_PK,
            'name' => Schema::TYPE_STRING . ' NOT NULL',
        ]);
        $this->insert('kidneys_status', [
            'name' => 'не пальпируются',
        ]);
        $this->insert('kidneys_status', [
            'name' => 'пальпируются',
        ]);
        $this->insert('kidneys_status', [
            'name' => 'синдром поколачивания отрицательный',
        ]);
        $this->insert('kidneys_status', [
            'name' => 'синдром поколачивания положительный',
        ]);
        $this->insert('kidneys_status', [
            'name' => 'справа',
        ]);
        $this->insert('kidneys_status', [
            'name' => 'слева',
        ]);

        //++стул
        $this->createTable('stool_status', [
            'id' => Schema::TYPE_PK,
            'name' => Schema::TYPE_STRING . ' NOT NULL',
        ]);
        $this->insert('stool_status', [
            'name' => 'регулярный',
        ]);
        $this->insert('stool_status', [
            'name' => 'запоры',
        ]);

        //++разница зрачков
        $this->createTable('pupils_diff_status', [
            'id' => Schema::TYPE_PK,
            'name' => Schema::TYPE_STRING . ' NOT NULL',
        ]);
        $this->insert('pupils_diff_status', [
            'name' => 'S > d',
        ]);
        $this->insert('pupils_diff_status', [
            'name' => 'S < d',
        ]);
        $this->insert('pupils_diff_status', [
            'name' => 'S = d',
        ]);

        //++реакция зрачков
        $this->createTable('pupils_reaction_status', [
            'id' => Schema::TYPE_PK,
            'name' => Schema::TYPE_STRING . ' NOT NULL',
        ]);
        $this->insert('pupils_reaction_status', [
            'name' => 'живая',
        ]);
        $this->insert('pupils_reaction_status', [
            'name' => 'вялая',
        ]);

        //++ПНП
        $this->createTable('finger_nose_status', [
            'id' => Schema::TYPE_PK,
            'name' => Schema::TYPE_STRING . ' NOT NULL',
        ]);
        $this->insert('finger_nose_status', [
            'name' => 'выполняется точно',
        ]);
        $this->insert('finger_nose_status', [
            'name' => 'выполняется не точно',
        ]);
        $this->insert('finger_nose_status', [
            'name' => 'справа',
        ]);
        $this->insert('finger_nose_status', [
            'name' => 'слева',
        ]);

        //++движения в конечностях
        $this->createTable('extremities_movement_status', [
            'id' => Schema::TYPE_PK,
            'name' => Schema::TYPE_STRING . ' NOT NULL',
        ]);
        $this->insert('extremities_movement_status', [
            'name' => 'сохранены',
        ]);
        $this->insert('extremities_movement_status', [
            'name' => 'ограничены',
        ]);
        $this->insert('extremities_movement_status', [
            'name' => 'справа',
        ]);
        $this->insert('extremities_movement_status', [
            'name' => 'слева',
        ]);

        //++перифирические лимфоузлы
        $this->createTable('peripheral_lymph_nodes_status', [
            'id' => Schema::TYPE_PK,
            'name' => Schema::TYPE_STRING . ' NOT NULL',
        ]);
        $this->insert('peripheral_lymph_nodes_status', [
            'name' => 'увеличены',
        ]);
        $this->insert('peripheral_lymph_nodes_status', [
            'name' => 'не увеличены',
        ]);

        //+patient inspections
        $this->createTable('inspection', [
            'id' => Schema::TYPE_INTEGER.' NOT NULL',
            'patient_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'inspection_date' => Schema::TYPE_DATE . ' NOT NULL',
            'anamnesis' => Schema::TYPE_TEXT,
            'complaints' => Schema::TYPE_TEXT,
            'diagnosis' => Schema::TYPE_TEXT,//allow null
            'treatment' => Schema::TYPE_TEXT,//allow null
        ]);
        $this->createIndex('fk_inspection_patient_id', 'inspection', 'patient_id');
        $this->addForeignKey('fk_inspection_patient_id', 'inspection', 'patient_id', 'patient', 'id', 'RESTRICT', 'RESTRICT');
        $this->addPrimaryKey('inspection_pk', 'inspection', ['id', 'patient_id']);
        //+patient survey
        $this->createTable('survey_inspection', [
            'survey_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'patient_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'inspection_id' => Schema::TYPE_INTEGER . ' NOT NULL',
        ]);
        $this->createIndex('fk_survey_inspection_survey_id', 'survey_inspection', 'survey_id');
        $this->createIndex('fk_survey_inspection_patient_id', 'survey_inspection', 'patient_id');
        $this->createIndex('fk_survey_inspection_inspection_id', 'survey_inspection', 'inspection_id');
        $this->addForeignKey('fk_survey_inspection_survey_id', 'survey_inspection', 'survey_id', 'survey', 'id', 'RESTRICT', 'RESTRICT');
        $this->addForeignKey('fk_survey_inspection_inspection_id', 'survey_inspection', 'inspection_id', 'inspection', 'id', 'RESTRICT', 'RESTRICT');
        $this->addPrimaryKey('survey_inspection_pk', 'survey_inspection', ['survey_id', 'patient_id', 'inspection_id']);

        //+neurological inspection
        $this->createTable('neurological_inspection', [
            'inspection_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'patient_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'pupils_diff_status_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'pupils_reaction_status_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'finger_nose_inspection_description' => Schema::TYPE_TEXT,
            'extremities_movement_inspection_description' => Schema::TYPE_TEXT,
        ]);
        $this->createIndex('fk_neurological_inspection_inspection_id', 'neurological_inspection', 'inspection_id');
        $this->createIndex('fk_neurological_inspection_patient_id', 'neurological_inspection', 'patient_id');
        $this->createIndex('fk_neurological_inspection_pupils_diff_status_id', 'neurological_inspection', 'pupils_diff_status_id');
        $this->createIndex('fk_neurological_inspection_pupils_reaction_status_id', 'neurological_inspection', 'pupils_reaction_status_id');
        $this->addForeignKey('fk_neurological_inspection_inspection_id', 'neurological_inspection', 'inspection_id', 'inspection', 'id', 'RESTRICT', 'RESTRICT');
        $this->addForeignKey('fk_neurological_inspection_pupils_diff_status_id', 'neurological_inspection', 'pupils_diff_status_id', 'pupils_diff_status', 'id', 'RESTRICT', 'RESTRICT');
        $this->addForeignKey('fk_neurological_inspection_pupils_reaction_status_id', 'neurological_inspection', 'pupils_reaction_status_id', 'pupils_reaction_status', 'id', 'RESTRICT', 'RESTRICT');
        $this->addPrimaryKey('neurological_inspection_pk', 'neurological_inspection', ['inspection_id', 'patient_id']);
        //+finger nose inspection
        $this->createTable('finger_nose_inspection', [
            'neurological_inspection_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'patient_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'finger_nose_status_id' => Schema::TYPE_INTEGER . ' NOT NULL',
        ]);
        $this->createIndex('fk_finger_nose_inspection_neurological_inspection_id', 'finger_nose_inspection', 'neurological_inspection_id');
        $this->createIndex('fk_finger_nose_inspection_patient_id', 'finger_nose_inspection', 'patient_id');
        $this->createIndex('fk_finger_nose_inspection_finger_nose_status_id', 'finger_nose_inspection', 'finger_nose_status_id');
        $this->addForeignKey('fk_finger_nose_inspection_neurological_inspection_id', 'finger_nose_inspection', 'neurological_inspection_id', 'neurological_inspection', 'inspection_id', 'RESTRICT', 'RESTRICT');
        $this->addForeignKey('fk_finger_nose_inspection_finger_nose_status_id', 'finger_nose_inspection', 'finger_nose_status_id', 'finger_nose_status', 'id', 'RESTRICT', 'RESTRICT');
        $this->addPrimaryKey('finger_nose_inspection_pk', 'finger_nose_inspection', ['neurological_inspection_id', 'patient_id', 'finger_nose_status_id']);
        //+extremities movement inspection
        $this->createTable('extremities_movement_inspection', [
            'neurological_inspection_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'patient_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'extremities_movement_status_id' => Schema::TYPE_INTEGER . ' NOT NULL',
        ]);
        $this->createIndex('fk_extrem_mov_inspection_neurological_inspection_id', 'extremities_movement_inspection', 'neurological_inspection_id');
        $this->createIndex('fk_extrem_mov_inspection_patient_id', 'extremities_movement_inspection', 'patient_id');
        $this->createIndex('fk_extrem_mov_inspection_extremities_movement_status_id', 'extremities_movement_inspection', 'extremities_movement_status_id');
        $this->addForeignKey('fk_extrem_mov_inspection_neurological_inspection_id', 'extremities_movement_inspection', 'neurological_inspection_id', 'neurological_inspection', 'inspection_id', 'RESTRICT', 'RESTRICT');
        $this->addForeignKey('fk_extrem_mov_inspection_extremities_movement_status_id', 'extremities_movement_inspection', 'extremities_movement_status_id', 'extremities_movement_status', 'id', 'RESTRICT', 'RESTRICT');
        $this->addPrimaryKey('extremities_movement_inspection_pk', 'extremities_movement_inspection', ['neurological_inspection_id', 'patient_id', 'extremities_movement_status_id']);

        //+lungs inspection
        $this->createTable('lungs_inspection', [
            'inspection_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'patient_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'breathing_rate' => Schema::TYPE_DOUBLE,
            'oxygen_saturation' => Schema::TYPE_DOUBLE,
            'rib_cage_inspection_description' => Schema::TYPE_TEXT,
            'percussion_inspection_description' => Schema::TYPE_TEXT,
            'voice_trembling_inspection_description' => Schema::TYPE_TEXT,
            'auscultation_inspection_drscription' => Schema::TYPE_TEXT,
            'crepitation_inspection_description' => Schema::TYPE_TEXT,
        ]);
        $this->createIndex('fk_lungs_inspection_inspection_id', 'lungs_inspection', 'inspection_id');
        $this->createIndex('fk_lungs_inspection_patient_id', 'lungs_inspection', 'patient_id');
        $this->addForeignKey('fk_lungs_inspection_inspection_id', 'lungs_inspection', 'inspection_id', 'inspection', 'id', 'RESTRICT', 'RESTRICT');
        $this->addPrimaryKey('lungs_inspection_pk', 'lungs_inspection', ['inspection_id', 'patient_id']);
        //+rib cage inspection
        $this->createTable('rib_cage_inspection', [
            'lungs_inspection_id' => Schema::TYPE_INTEGER . ' NOT NUll',
            'patient_id' => Schema::TYPE_INTEGER . ' NOT NUll',
            'rib_cage_status_id' => Schema::TYPE_INTEGER . ' NOT NULL',
        ]);
        $this->createIndex('fk_rib_cage_inspection_lungs_inspection_id', 'rib_cage_inspection', 'lungs_inspection_id');
        $this->createIndex('fk_rib_cage_inspection_patient_id', 'rib_cage_inspection', 'patient_id');
        $this->createIndex('fk_rib_cage_inspection_rib_cage_status_id', 'rib_cage_inspection', 'rib_cage_status_id');
        $this->addForeignKey('fk_rib_cage_inspection_lungs_inspection_id', 'rib_cage_inspection', 'lungs_inspection_id', 'lungs_inspection', 'inspection_id', 'RESTRICT', 'RESTRICT');
        $this->addForeignKey('fk_rib_cage_inspection_rib_cage_status_id', 'rib_cage_inspection', 'rib_cage_status_id', 'rib_cage_status', 'id', 'RESTRICT', 'RESTRICT');
        $this->addPrimaryKey('rib_cage_inspection_pk', 'rib_cage_inspection', ['lungs_inspection_id', 'patient_id', 'rib_cage_status_id']);
        //+percussion inspection
        $this->createTable('percussion_inspection', [
            'lungs_inspection_id' => Schema::TYPE_INTEGER . ' NOT NUll',
            'patient_id' => Schema::TYPE_INTEGER . ' NOT NUll',
            'percussion_status_id' => Schema::TYPE_INTEGER . ' NOT NULL',
        ]);
        $this->createIndex('fk_percussion_inspection_lungs_inspection_id', 'percussion_inspection', 'lungs_inspection_id');
        $this->createIndex('fk_percussion_inspection_patient_id', 'percussion_inspection', 'patient_id');
        $this->createIndex('fk_percussion_inspection_percussion_status_id', 'percussion_inspection', 'percussion_status_id');
        $this->addForeignKey('fk_percussion_inspection_lungs_inspection_id', 'percussion_inspection', 'lungs_inspection_id', 'lungs_inspection', 'inspection_id', 'RESTRICT', 'RESTRICT');
        $this->addForeignKey('fk_percussion_inspection_percussion_status_id', 'percussion_inspection', 'percussion_status_id', 'percussion_status', 'id', 'RESTRICT', 'RESTRICT');
        $this->addPrimaryKey('percussion_inspection_pk', 'percussion_inspection', ['lungs_inspection_id', 'patient_id', 'percussion_status_id']);
        //+voice trembling inspection
        $this->createTable('voice_trembling_inspection', [
            'lungs_inspection_id' => Schema::TYPE_INTEGER . ' NOT NUll',
            'patient_id' => Schema::TYPE_INTEGER . ' NOT NUll',
            'voice_trembling_status_id' => Schema::TYPE_INTEGER . ' NOT NULL',
        ]);
        $this->createIndex('fk_v_t_inspection_lungs_inspection_id', 'voice_trembling_inspection', 'lungs_inspection_id');
        $this->createIndex('fk_v_t_inspection_patient_id', 'voice_trembling_inspection', 'patient_id');
        $this->createIndex('fk_v_t_inspection_v_t_status_id', 'voice_trembling_inspection', 'voice_trembling_status_id');
        $this->addForeignKey('fk_v_t_inspection_lungs_inspection_id', 'voice_trembling_inspection', 'lungs_inspection_id', 'lungs_inspection', 'inspection_id', 'RESTRICT', 'RESTRICT');
        $this->addForeignKey('fk_v_t_inspection_v_t_status_id', 'voice_trembling_inspection', 'voice_trembling_status_id', 'voice_trembling_status', 'id', 'RESTRICT', 'RESTRICT');
        $this->addPrimaryKey('voice_trembling_inspection_pk', 'voice_trembling_inspection', ['lungs_inspection_id', 'patient_id', 'voice_trembling_status_id']);
        //+auscultation inspection
        $this->createTable('auscultation_inspection', [
            'lungs_inspection_id' => Schema::TYPE_INTEGER . ' NOT NUll',
            'patient_id' => Schema::TYPE_INTEGER . ' NOT NUll',
            'lungs_auscultation_status_id' => Schema::TYPE_INTEGER . ' NOT NULL',
        ]);
        $this->createIndex('fk_aus_inspection_lungs_inspection_id', 'auscultation_inspection', 'lungs_inspection_id');
        $this->createIndex('fk_aus_inspection_patient_id', 'auscultation_inspection', 'patient_id');
        $this->createIndex('fk_aus_inspection_lungs_aus_status_id', 'auscultation_inspection', 'lungs_auscultation_status_id');
        $this->addForeignKey('fk_aus_inspection_lungs_inspection_id', 'auscultation_inspection', 'lungs_inspection_id', 'lungs_inspection', 'inspection_id', 'RESTRICT', 'RESTRICT');
        $this->addForeignKey('fk_aus_inspection_lungs_aus_status_id', 'auscultation_inspection', 'lungs_auscultation_status_id', 'lungs_auscultation_status', 'id', 'RESTRICT', 'RESTRICT');
        $this->addPrimaryKey('lung_ausc_inspection_pk', 'auscultation_inspection', ['lungs_inspection_id', 'patient_id', 'lungs_auscultation_status_id']);
        //+crepitation inspection
        $this->createTable('crepitation_inspection', [
            'lungs_inspection_id' => Schema::TYPE_INTEGER . ' NOT NUll',
            'patient_id' => Schema::TYPE_INTEGER . ' NOT NUll',
            'crepitation_status_id' => Schema::TYPE_INTEGER . ' NOT NULL',
        ]);
        $this->createIndex('fk_crepitation_inspection_lungs_inspection_id', 'crepitation_inspection', 'lungs_inspection_id');
        $this->createIndex('fk_crepitation_inspection_patient_id', 'crepitation_inspection', 'patient_id');
        $this->createIndex('fk_crepitation_inspection_lungs_crepitation_status_id', 'crepitation_inspection', 'crepitation_status_id');
        $this->addForeignKey('fk_crepitation_inspection_lungs_inspection_id', 'crepitation_inspection', 'lungs_inspection_id', 'lungs_inspection', 'inspection_id', 'RESTRICT', 'RESTRICT');
        $this->addForeignKey('fk_crepitation_inspection_lungs_crepitation_status_id', 'crepitation_inspection', 'crepitation_status_id', 'crepitation_status', 'id', 'RESTRICT', 'RESTRICT');
        $this->addPrimaryKey('crepitation_inspection_pk', 'crepitation_inspection', ['lungs_inspection_id', 'patient_id', 'crepitation_status_id']);

        //+cardiovascular inspection
        $this->createTable('cardiovascular_inspection', [
            'inspection_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'patient_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'heart_status_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'heart_rate' => Schema::TYPE_DOUBLE,
            'left_blood_pressure' => Schema::TYPE_DOUBLE,
            'right_blood_pressure' => Schema::TYPE_DOUBLE,
            'heart_tones_inspection_description' => Schema::TYPE_TEXT,
            'heart_tones_accent_inspection_description' => Schema::TYPE_TEXT,
            'heart_noises_inspection_description' => Schema::TYPE_TEXT,
        ]);
        $this->createIndex('fk_cardio_inspection_inspection_id', 'cardiovascular_inspection', 'inspection_id');
        $this->createIndex('fk_cardio_inspection_patient_id', 'cardiovascular_inspection', 'patient_id');
        $this->createIndex('fk_cardio_inspection_heart_status_id', 'cardiovascular_inspection', 'heart_status_id');
        $this->addForeignKey('fk_cardio_inspection_inspection_id', 'cardiovascular_inspection', 'inspection_id', 'inspection', 'id', 'RESTRICT', 'RESTRICT');
        $this->addForeignKey('fk_cardio_inspection_heart_status_id', 'cardiovascular_inspection', 'heart_status_id', 'heart_status', 'id', 'RESTRICT', 'RESTRICT');
        $this->addPrimaryKey('cardiovascular_inspection_pk', 'cardiovascular_inspection', ['inspection_id', 'patient_id']);
        //+heart tones inspection
        $this->createTable('heart_tones_inspection', [
            'cardiovascular_inspection_id' => Schema::TYPE_INTEGER . ' NOT NUll',
            'patient_id' => Schema::TYPE_INTEGER . ' NOT NUll',
            'heart_tones_status_id' => Schema::TYPE_INTEGER . ' NOT NULL',
        ]);
        $this->createIndex('fk_h_t_inspection_cardio_inspection_id', 'heart_tones_inspection', 'cardiovascular_inspection_id');
        $this->createIndex('fk_h_t_inspection_patient_id', 'heart_tones_inspection', 'patient_id');
        $this->createIndex('fk_h_t_inspection_h_t_status_id', 'heart_tones_inspection', 'heart_tones_status_id');
        $this->addForeignKey('fk_h_t_inspection_cardio_inspection_id', 'heart_tones_inspection', 'cardiovascular_inspection_id', 'cardiovascular_inspection', 'inspection_id', 'RESTRICT', 'RESTRICT');
        $this->addForeignKey('fk_h_t_inspection_h_t_status_id', 'heart_tones_inspection', 'heart_tones_status_id', 'heart_tones_status', 'id', 'RESTRICT', 'RESTRICT');
        $this->addPrimaryKey('heart_tones_inspection_pk', 'heart_tones_inspection', ['cardiovascular_inspection_id', 'patient_id', 'heart_tones_status_id']);
        //+tones accent inspection
        $this->createTable('heart_tones_accent_inspection', [
            'cardiovascular_inspection_id' => Schema::TYPE_INTEGER . ' NOT NUll',
            'patient_id' => Schema::TYPE_INTEGER . ' NOT NUll',
            'heart_tones_accent_status_id' => Schema::TYPE_INTEGER . ' NOT NULL',
        ]);
        $this->createIndex('fk_t_a_inspection_cardio_inspection_id', 'heart_tones_accent_inspection', 'cardiovascular_inspection_id');
        $this->createIndex('fk_t_a_inspection_patient_id', 'heart_tones_accent_inspection', 'patient_id');
        $this->createIndex('fk_t_a_inspection_t_a_status_id', 'heart_tones_accent_inspection', 'heart_tones_accent_status_id');
        $this->addForeignKey('fk_t_a_inspection_cardio_inspection_id', 'heart_tones_accent_inspection', 'cardiovascular_inspection_id', 'cardiovascular_inspection', 'inspection_id', 'RESTRICT', 'RESTRICT');
        $this->addForeignKey('fk_t_a_inspection_t_a_status_id', 'heart_tones_accent_inspection', 'heart_tones_accent_status_id', 'heart_tones_accent_status', 'id', 'RESTRICT', 'RESTRICT');
        $this->addPrimaryKey('tones_accent_inspection_pk', 'heart_tones_accent_inspection', ['cardiovascular_inspection_id', 'patient_id', 'heart_tones_accent_status_id']);
        //+heart noises inspection
        $this->createTable('heart_noises_inspection', [
            'cardiovascular_inspection_id' => Schema::TYPE_INTEGER . ' NOT NUll',
            'patient_id' => Schema::TYPE_INTEGER . ' NOT NUll',
            'heart_noises_status_id' => Schema::TYPE_INTEGER . ' NOT NULL',
        ]);
        $this->createIndex('fk_h_n_inspection_cardio_inspection_id', 'heart_noises_inspection', 'cardiovascular_inspection_id');
        $this->createIndex('fk_h_n_inspection_patient_id', 'heart_noises_inspection', 'patient_id');
        $this->createIndex('fk_h_n_inspection_h_n_status_id', 'heart_noises_inspection', 'heart_noises_status_id');
        $this->addForeignKey('fk_h_n_inspection_cardio_inspection_id', 'heart_noises_inspection', 'cardiovascular_inspection_id', 'cardiovascular_inspection', 'inspection_id', 'RESTRICT', 'RESTRICT');
        $this->addForeignKey('fk_h_n_inspection_h_n_status_id', 'heart_noises_inspection', 'heart_noises_status_id', 'heart_noises_status', 'id', 'RESTRICT', 'RESTRICT');
        $this->addPrimaryKey('heart_noises_inspection_pk', 'heart_noises_inspection', ['cardiovascular_inspection_id', 'patient_id', 'heart_noises_status_id']);

        //+general inspection
        $this->createTable('general_inspection', [
            'inspection_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'patient_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'helth_status_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'feeding_status_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'peripheral_lymph_nodes_status_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'body_mass_index' => Schema::TYPE_DOUBLE,
            'rash' => Schema::TYPE_TEXT,
            'edema' => Schema::TYPE_BOOLEAN,
            'edema_description' => Schema::TYPE_TEXT,
            'dysuria' => Schema::TYPE_BOOLEAN,
            'skin_inspection_description' => Schema::TYPE_TEXT,
            'thyroid_inspection_description' => Schema::TYPE_TEXT,
            'mammary_gland_inspection_description' => Schema::TYPE_TEXT,
            'osteoarticular_inspection_description' => Schema::TYPE_TEXT,
            'stool_inspection_description' => Schema::TYPE_TEXT,
            'tongue_inspection_description' => Schema::TYPE_TEXT,
        ]);
        $this->createIndex('fk_general_inspection_inspection_id', 'general_inspection', 'inspection_id');
        $this->createIndex('fk_general_inspection_patient_id', 'general_inspection', 'patient_id');
        $this->createIndex('fk_general_inspection_helth_status_id', 'general_inspection', 'helth_status_id');
        $this->createIndex('fk_general_inspection_feeding_status_id', 'general_inspection', 'feeding_status_id');
        $this->createIndex('fk_general_inspection_lymph_nodes_status_id', 'general_inspection', 'peripheral_lymph_nodes_status_id');
        $this->addForeignKey('fk_general_inspection_inspection_id', 'general_inspection', 'inspection_id', 'inspection', 'id', 'RESTRICT', 'RESTRICT');
        $this->addForeignKey('fk_general_inspection_helth_status_id', 'general_inspection', 'helth_status_id', 'helth_status', 'id', 'RESTRICT', 'RESTRICT');
        $this->addForeignKey('fk_general_inspection_feeding_status_id', 'general_inspection', 'feeding_status_id', 'feeding_status', 'id', 'RESTRICT', 'RESTRICT');
        $this->addForeignKey('fk_general_inspection_lymph_nodes_status_id', 'general_inspection', 'peripheral_lymph_nodes_status_id', 'peripheral_lymph_nodes_status', 'id', 'RESTRICT', 'RESTRICT');
        $this->addPrimaryKey('general_inspection_pk', 'general_inspection', ['inspection_id', 'patient_id']);
        //+skin inspection
        $this->createTable('skin_inspection', [
            'general_inspection_id' => Schema::TYPE_INTEGER . ' NOT NUll',
            'patient_id' => Schema::TYPE_INTEGER . ' NOT NUll',
            'skin_status_id' => Schema::TYPE_INTEGER . ' NOT NULL',
        ]);
        $this->createIndex('fk_skin_inspection_general_inspection_id', 'skin_inspection', 'general_inspection_id');
        $this->createIndex('fk_skin_inspection_patient_id', 'skin_inspection', 'patient_id');
        $this->createIndex('fk_skin_inspection_skin_status_id', 'skin_inspection', 'skin_status_id');
        $this->addForeignKey('fk_skin_inspection_general_inspection_id', 'skin_inspection', 'general_inspection_id', 'general_inspection', 'inspection_id', 'RESTRICT', 'RESTRICT');
        $this->addForeignKey('fk_skin_inspection_skin_status_id', 'skin_inspection', 'skin_status_id', 'skin_status', 'id', 'RESTRICT', 'RESTRICT');
        $this->addPrimaryKey('skin_inspection_pk', 'skin_inspection', ['general_inspection_id', 'patient_id', 'skin_status_id']);
        //+thyroid inspection
        $this->createTable('thyroid_inspection', [
            'general_inspection_id' => Schema::TYPE_INTEGER . ' NOT NUll',
            'patient_id' => Schema::TYPE_INTEGER . ' NOT NUll',
            'thyroid_status_id' => Schema::TYPE_INTEGER . ' NOT NULL',
        ]);
        $this->createIndex('fk_thyroid_inspection_general_inspection_id', 'thyroid_inspection', 'general_inspection_id');
        $this->createIndex('fk_thyroid_inspection_patient_id', 'thyroid_inspection', 'patient_id');
        $this->createIndex('fk_thyroid_inspection_skin_status_id', 'thyroid_inspection', 'thyroid_status_id');
        $this->addForeignKey('fk_thyroid_inspection_general_inspection_id', 'thyroid_inspection', 'general_inspection_id', 'general_inspection', 'inspection_id', 'RESTRICT', 'RESTRICT');
        $this->addForeignKey('fk_thyroid_inspection_skin_status_id', 'thyroid_inspection', 'thyroid_status_id', 'thyroid_status', 'id', 'RESTRICT', 'RESTRICT');
        $this->addPrimaryKey('thyroid_inspection_pk', 'thyroid_inspection', ['general_inspection_id', 'patient_id', 'thyroid_status_id']);
        //+mammary gland inspection
        $this->createTable('mammary_gland_inspection', [
            'general_inspection_id' => Schema::TYPE_INTEGER . ' NOT NUll',
            'patient_id' => Schema::TYPE_INTEGER . ' NOT NUll',
            'mammary_status_id' => Schema::TYPE_INTEGER . ' NOT NULL',
        ]);
        $this->createIndex('fk_m_g_inspection_general_inspection_id', 'mammary_gland_inspection', 'general_inspection_id');
        $this->createIndex('fk_m_g_inspection_patient_id', 'mammary_gland_inspection', 'patient_id');
        $this->createIndex('fk_m_g_inspection_mammary_status_id', 'mammary_gland_inspection', 'mammary_status_id');
        $this->addForeignKey('fk_m_g_inspection_general_inspection_id', 'mammary_gland_inspection', 'general_inspection_id', 'general_inspection', 'inspection_id', 'RESTRICT', 'RESTRICT');
        $this->addForeignKey('fk_m_g_inspection_mammary_status_id', 'mammary_gland_inspection', 'mammary_status_id', 'mammary_status', 'id', 'RESTRICT', 'RESTRICT');
        $this->addPrimaryKey('mammary_gland_inspection_pk', 'mammary_gland_inspection', ['general_inspection_id', 'patient_id', 'mammary_status_id']);
        //+osteoarticular inspection
        $this->createTable('osteoarticular_inspection', [
            'general_inspection_id' => Schema::TYPE_INTEGER . ' NOT NUll',
            'patient_id' => Schema::TYPE_INTEGER . ' NOT NUll',
            'osteoarticular_system_status_id' => Schema::TYPE_INTEGER . ' NOT NULL',
        ]);
        $this->createIndex('fk_osteo_inspection_general_inspection_id', 'osteoarticular_inspection', 'general_inspection_id');
        $this->createIndex('fk_osteo_inspection_patient_id', 'osteoarticular_inspection', 'patient_id');
        $this->createIndex('fk_osteo_inspection_osteo_status_id', 'osteoarticular_inspection', 'osteoarticular_system_status_id');
        $this->addForeignKey('fk_osteo_inspection_general_inspection_id', 'osteoarticular_inspection', 'general_inspection_id', 'general_inspection', 'inspection_id', 'RESTRICT', 'RESTRICT');
        $this->addForeignKey('fk_osteo_inspection_osteo_status_id', 'osteoarticular_inspection', 'osteoarticular_system_status_id', 'osteoarticular_system_status', 'id', 'RESTRICT', 'RESTRICT');
        $this->addPrimaryKey('osteoarticular_inspection_pk', 'osteoarticular_inspection', ['general_inspection_id', 'patient_id', 'osteoarticular_system_status_id']);
        //+stool inspection
        $this->createTable('stool_inspection', [
            'general_inspection_id' => Schema::TYPE_INTEGER . ' NOT NUll',
            'patient_id' => Schema::TYPE_INTEGER . ' NOT NUll',
            'stool_status_id' => Schema::TYPE_INTEGER . ' NOT NULL',
        ]);
        $this->createIndex('fk_stool_inspection_general_inspection_id', 'stool_inspection', 'general_inspection_id');
        $this->createIndex('fk_stool_inspection_patient_id', 'stool_inspection', 'patient_id');
        $this->createIndex('fk_stool_inspection_stool_status_id', 'stool_inspection', 'stool_status_id');
        $this->addForeignKey('fk_stool_inspection_general_inspection_id', 'stool_inspection', 'general_inspection_id', 'general_inspection', 'inspection_id', 'RESTRICT', 'RESTRICT');
        $this->addForeignKey('fk_stool_inspection_stool_status_id', 'stool_inspection', 'stool_status_id', 'stool_status', 'id', 'RESTRICT', 'RESTRICT');
        $this->addPrimaryKey('stool_inspection_pk', 'stool_inspection', ['general_inspection_id', 'patient_id', 'stool_status_id']);
        //+tongue inspection
        $this->createTable('tongue_inspection', [
            'general_inspection_id' => Schema::TYPE_INTEGER . ' NOT NUll',
            'patient_id' => Schema::TYPE_INTEGER . ' NOT NUll',
            'tongue_status_id' => Schema::TYPE_INTEGER . ' NOT NULL',
        ]);
        $this->createIndex('fk_tongue_inspection_general_inspection_id', 'tongue_inspection', 'general_inspection_id');
        $this->createIndex('fk_tongue_inspection_patient_id', 'tongue_inspection', 'patient_id');
        $this->createIndex('fk_tongue_inspection_stool_status_id', 'tongue_inspection', 'tongue_status_id');
        $this->addForeignKey('fk_tongue_inspection_general_inspection_id', 'tongue_inspection', 'general_inspection_id', 'general_inspection', 'inspection_id', 'RESTRICT', 'RESTRICT');
        $this->addForeignKey('fk_tongue_inspection_stool_status_id', 'tongue_inspection', 'tongue_status_id', 'tongue_status', 'id', 'RESTRICT', 'RESTRICT');
        $this->addPrimaryKey('tongue_inspection_pk', 'tongue_inspection', ['general_inspection_id', 'patient_id', 'tongue_status_id']);

        //+other_inspection
        $this->createTable('other_inspection', [
            'inspection_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'patient_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'gallbladder_inspection_description' => Schema::TYPE_TEXT,
            'kidneys_inspection_description' => Schema::TYPE_TEXT,
            'liver_inspection_description' => Schema::TYPE_TEXT,
            'spleen_inspection_description' => Schema::TYPE_TEXT,
            'stomach_inspection_description' => Schema::TYPE_TEXT,
        ]);
        $this->createIndex('fk_other_inspection_inspection_id', 'other_inspection', 'inspection_id');
        $this->createIndex('fk_other_inspection_patient_id', 'other_inspection', 'patient_id');
        $this->addForeignKey('fk_other_inspection_inspection_id', 'other_inspection', 'inspection_id', 'inspection', 'id', 'RESTRICT', 'RESTRICT');
        $this->addPrimaryKey('other_inspection_pk', 'other_inspection', ['inspection_id', 'patient_id']);
        //+stomach_inspection
        $this->createTable('stomach_inspection', [
            'other_inspection_id' => Schema::TYPE_INTEGER . ' NOT NUll',
            'patient_id' => Schema::TYPE_INTEGER . ' NOT NUll',
            'stomach_status_id' => Schema::TYPE_INTEGER . ' NOT NULL',
        ]);
        $this->createIndex('fk_stomach_inspection_other_inspection_id', 'stomach_inspection', 'other_inspection_id');
        $this->createIndex('fk_stomach_inspection_patient_id', 'stomach_inspection', 'patient_id');
        $this->createIndex('fk_stomach_inspection_stomach_status_id', 'stomach_inspection', 'stomach_status_id');
        $this->addForeignKey('fk_stomach_inspection_other_inspection_id', 'stomach_inspection', 'other_inspection_id', 'other_inspection', 'inspection_id', 'RESTRICT', 'RESTRICT');
        $this->addForeignKey('fk_stomach_inspection_stomach_status_id', 'stomach_inspection', 'stomach_status_id', 'stomach_status', 'id', 'RESTRICT', 'RESTRICT');
        $this->addPrimaryKey('stomach_inspection_pk', 'stomach_inspection', ['other_inspection_id', 'patient_id', 'stomach_status_id']);
        //+liver_inspection
        $this->createTable('liver_inspection', [
            'other_inspection_id' => Schema::TYPE_INTEGER . ' NOT NUll',
            'patient_id' => Schema::TYPE_INTEGER . ' NOT NUll',
            'liver_status_id' => Schema::TYPE_INTEGER . ' NOT NULL',
        ]);
        $this->createIndex('fk_liver_inspection_other_inspection_id', 'liver_inspection', 'other_inspection_id');
        $this->createIndex('fk_liver_inspection_patient_id', 'liver_inspection', 'patient_id');
        $this->createIndex('fk_liver_inspection_liver_status_id', 'liver_inspection', 'liver_status_id');
        $this->addForeignKey('fk_liver_inspection_other_inspection_id', 'liver_inspection', 'other_inspection_id', 'other_inspection', 'inspection_id', 'RESTRICT', 'RESTRICT');
        $this->addForeignKey('fk_liver_inspection_liver_status_id', 'liver_inspection', 'liver_status_id', 'liver_status', 'id', 'RESTRICT', 'RESTRICT');
        $this->addPrimaryKey('liver_inspection_pk', 'liver_inspection', ['other_inspection_id', 'patient_id', 'liver_status_id']);
        //+gallbladder_inspection
        $this->createTable('gallbladder_inspection', [
            'other_inspection_id' => Schema::TYPE_INTEGER . ' NOT NUll',
            'patient_id' => Schema::TYPE_INTEGER . ' NOT NUll',
            'gallbladder_status_id' => Schema::TYPE_INTEGER . ' NOT NULL',
        ]);
        $this->createIndex('fk_gallbladder_inspection_other_inspection_id', 'gallbladder_inspection', 'other_inspection_id');
        $this->createIndex('fk_gallbladder_inspection_patient_id', 'gallbladder_inspection', 'patient_id');
        $this->createIndex('fk_gallbladder_inspection_gallbladder_status_id', 'gallbladder_inspection', 'gallbladder_status_id');
        $this->addForeignKey('fk_gallbladder_inspection_other_inspection_id', 'gallbladder_inspection', 'other_inspection_id', 'other_inspection', 'inspection_id', 'RESTRICT', 'RESTRICT');
        $this->addForeignKey('fk_gallbladder_inspection_gallbladder_status_id', 'gallbladder_inspection', 'gallbladder_status_id', 'gallbladder_status', 'id', 'RESTRICT', 'RESTRICT');
        $this->addPrimaryKey('gallbladder_inspection_pk', 'gallbladder_inspection', ['other_inspection_id', 'patient_id', 'gallbladder_status_id']);
        //+spleen_inspection
        $this->createTable('spleen_inspection', [
            'other_inspection_id' => Schema::TYPE_INTEGER . ' NOT NUll',
            'patient_id' => Schema::TYPE_INTEGER . ' NOT NUll',
            'spleen_status_id' => Schema::TYPE_INTEGER . ' NOT NULL',
        ]);
        $this->createIndex('fk_spleen_inspection_other_inspection_id', 'spleen_inspection', 'other_inspection_id');
        $this->createIndex('fk_spleen_inspection_patient_id', 'spleen_inspection', 'patient_id');
        $this->createIndex('fk_spleen_inspection_spleen_status_id', 'spleen_inspection', 'spleen_status_id');
        $this->addForeignKey('fk_spleen_inspection_other_inspection_id', 'spleen_inspection', 'other_inspection_id', 'other_inspection', 'inspection_id', 'RESTRICT', 'RESTRICT');
        $this->addForeignKey('fk_spleen_inspection_spleen_status_id', 'spleen_inspection', 'spleen_status_id', 'spleen_status', 'id', 'RESTRICT', 'RESTRICT');
        $this->addPrimaryKey('spleen_inspection_pk', 'spleen_inspection', ['other_inspection_id', 'patient_id', 'spleen_status_id']);
        //+kidneys_inspection
        $this->createTable('kidneys_inspection', [
            'other_inspection_id' => Schema::TYPE_INTEGER . ' NOT NUll',
            'patient_id' => Schema::TYPE_INTEGER . ' NOT NUll',
            'kidneys_status_id' => Schema::TYPE_INTEGER . ' NOT NULL',
        ]);
        $this->createIndex('fk_kidneys_inspection_other_inspection_id', 'kidneys_inspection', 'other_inspection_id');
        $this->createIndex('fk_kidneys_inspection_patient_id', 'kidneys_inspection', 'patient_id');
        $this->createIndex('fk_kidneys_inspection_kidneys_status_id', 'kidneys_inspection', 'kidneys_status_id');
        $this->addForeignKey('fk_kidneys_inspection_other_inspection_id', 'kidneys_inspection', 'other_inspection_id', 'other_inspection', 'inspection_id', 'RESTRICT', 'RESTRICT');
        $this->addForeignKey('fk_kidneys_inspection_kidneys_status_id', 'kidneys_inspection', 'kidneys_status_id', 'kidneys_status', 'id', 'RESTRICT', 'RESTRICT');
        $this->addPrimaryKey('kidneys_inspection_pk', 'kidneys_inspection', ['other_inspection_id', 'patient_id', 'kidneys_status_id']);


    }

    public function safeDown() {
        //+other inspection
        $this->dropTable('kidneys_inspection');
        $this->dropTable('gallbladder_inspection');
        $this->dropTable('spleen_inspection');
        $this->dropTable('liver_inspection');
        $this->dropTable('stomach_inspection');
        $this->dropTable('other_inspection');
        //+general inspection
        $this->dropTable('tongue_inspection');
        $this->dropTable('stool_inspection');
        $this->dropTable('osteoarticular_inspection');
        $this->dropTable('mammary_gland_inspection');
        $this->dropTable('thyroid_inspection');
        $this->dropTable('skin_inspection');
        $this->dropTable('general_inspection');
        //cardiovascular inspection
        $this->dropTable('heart_noises_inspection');
        $this->dropTable('heart_tones_accent_inspection');
        $this->dropTable('heart_tones_inspection');
        $this->dropTable('cardiovascular_inspection');
        //lungs inspection
        $this->dropTable('crepitation_inspection');
        $this->dropTable('auscultation_inspection');
        $this->dropTable('voice_trembling_inspection');
        $this->dropTable('percussion_inspection');
        $this->dropTable('rib_cage_inspection');
        $this->dropTable('lungs_inspection');
        //neurological inspection
        $this->dropTable('extremities_movement_inspection');
        $this->dropTable('finger_nose_inspection');
        $this->dropTable('neurological_inspection');
        //drop inspections
        $this->dropTable('survey_inspection');
        $this->dropTable('inspection');
        //drop directories
        $this->dropTable('survey');
        $this->dropTable('helth_status');
        $this->dropTable('feeding_status');
        $this->dropTable('skin_status');
        $this->dropTable('thyroid_status');
        $this->dropTable('mammary_status');
        $this->dropTable('osteoarticular_system_status');
        $this->dropTable('rib_cage_status');
        $this->dropTable('percussion_status');
        $this->dropTable('voice_trembling_status');
        $this->dropTable('lungs_auscultation_status');
        $this->dropTable('crepitation_status');
        $this->dropTable('heart_status');
        $this->dropTable('heart_tones_status');
        $this->dropTable('heart_tones_accent_status');
        $this->dropTable('heart_noises_status');
        $this->dropTable('tongue_status');
        $this->dropTable('liver_status');
        $this->dropTable('stomach_status');
        $this->dropTable('gallbladder_status');
        $this->dropTable('spleen_status');
        $this->dropTable('kidneys_status');
        $this->dropTable('stool_status');
        $this->dropTable('pupils_diff_status');
        $this->dropTable('pupils_reaction_status');
        $this->dropTable('finger_nose_status');
        $this->dropTable('extremities_movement_status');
        $this->dropTable('peripheral_lymph_nodes_status');
        //patient
        $this->dropTable('patient');
        //user profile
        $this->dropTable('administrator');
    }
}
