$(function () {
	var $region = $('[name="region"]'),
		$district = $('[name="district"]'),
		$city = $('[name="Order[city]"]'),
		$street = $('[name="Order[street]"]'),
		$building = $('[name="Order[house]"]'),
		$apartment = $('[name="Order[apartment_number]"]');

	var $tooltip = $('.tooltip');

	$.kladr.setDefault({
		parentInput: '.address-form',
		verify: true,
		select: function (obj) {
			setLabel($(this), obj.type);
			$tooltip.hide();
		},
		check: function (obj) {
			var $input = $(this);

			if (obj) {
				setLabel($input, obj.type);
				$tooltip.hide();
			}
			else {
				showError($input, 'Введено неверно');
			}
		},
		checkBefore: function () {
			var $input = $(this);

			if (!$.trim($input.val())) {
				$tooltip.hide();
				return false;
			}
		}
	});

	$region.kladr('type', $.kladr.type.region);
	$district.kladr('type', $.kladr.type.district);
	$city.kladr('type', $.kladr.type.city);
	$street.kladr('type', $.kladr.type.street);
	$building.kladr('type', $.kladr.type.building);

	// Отключаем проверку введённых данных для строений
	$building.kladr('verify', false);

	function setLabel($input, text) {
		text = text.charAt(0).toUpperCase() + text.substr(1).toLowerCase();
		$input.parent().find('label').text(text);
	}

	function showError($input, message) {
		$tooltip.find('span').text(message);

		var inputOffset = $input.offset(),
			inputWidth = $input.outerWidth(),
			inputHeight = $input.outerHeight();

		var tooltipHeight = $tooltip.outerHeight();

		$tooltip.css({
			left: (inputOffset.left + inputWidth + 10) + 'px',
			top: (inputOffset.top + (inputHeight - tooltipHeight) / 2 - 1) + 'px'
		});

		$tooltip.show();
	}

	$region.trigger("change");
	$district.trigger("change");

	$city.on('keyup', function(event) {
		if ($(this).val() === '') {
			//clear and disable rest fields
			$street.val('').prop('disabled', true).trigger("keyup");
		} else {
			$street.prop('disabled', false);
		}
	});
	$street.on('keyup', function(event) {
		if ($(this).val() === '') {
			//clear and disable rest fields
			$building.val('').prop('disabled', true).trigger("keyup");
		} else {
			$building.prop('disabled', false);
		}
	});
	$building.on('keyup', function(event) {
		if ($(this).val() === '') {
			//clear and disable rest fields
			$apartment.val('').prop('disabled', true).trigger("keyup");
		} else {
			$apartment.prop('disabled', false);
		}
	});
});